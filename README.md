# Weather pilot

A simple, basic weather station, standalone type.

## Features
### Can log
	- Temperature : TPM75C from TI.
	- Humidity  : SHT21 sensirion sensor.
	- Air quality like CO2, etc etc...
	- Photodiode Sensor : OPT101

### Can send logged data
	- Using gsm to internet - provision
	- Write data on SD card locally: interface with MCU will be there - provision
	- bluetooth enabled : LMX9838 -  provision
	- WiFi module - TI CC3000,provision

### Device power source
	- Primary power source is Solar panel.
	- Secondary power source is Li-ion Battery.
	- battery management : BQ24210(Linear), BQ25504(switch Mode)provision.

### On board
	- Solar charge controller.
	- Battery charge controller and manager.
	- All sensors.
	- MCU.

### MCU to be used
	- ATMEGA2560 16 AU 100 TQFN.