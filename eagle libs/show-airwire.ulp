/*
The MIT License (MIT)

Copyright (c) 2013 Eric Stevens <estevens05+ulp@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#usage "<b>Show Single Airwire</b>"
"<p>Context menu item that turns off all airwires except the airwire selected.</p>"
"<p>Example eagle.scr entry to add this menu item:</p>"
"<pre>BRD:<br />"
"# Uncomment the following line if you want to remove all other custom WIRE<br />"
"# CONTEXT menu commands. See HELP SET for more details.<br />"
"#SET CONTEXT WIRE ;<br />"
"SET CONTEXT WIRE   'Show Airwire'   'run show-airwire';<br />"
"</pre>"
"<p><author>Copyright &copy; 2013, Eric Stevens &lt;estevens05+ulp@gmail.com&gt;</author></p>"

#require 6.0

string Version = "Version 1.0.0";
string cmd = "";

string valid_object;

if (board)
{
  board(B)
  {
    B.signals(W)
    {
      if (ingroup(W))
      {
        valid_object = W.name;
      }
    }
  }
}

if (strlen(valid_object))
{
  cmd = "RATSNEST ! *; RATSNEST " + valid_object + ";";
}

exit(cmd);
