<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="dots" multiple="1" display="yes" altdistance="0.025" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="120" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DINA3_L">
<frame x1="0" y1="0" x2="388.62" y2="264.16" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94" font="vector">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
</symbol>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
<symbol name="A3L-LOC">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
<symbol name="FRAME_A_L">
<frame x1="0" y1="0" x2="279.4" y2="215.9" columns="6" rows="5" layer="94" border-bottom="no"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA3_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA3_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="287.02" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="A3L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FRAME_A_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt; A Size , 8 1/2 x 11 INCH, Landscape&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="FRAME_A_L" x="0" y="0" addlevel="always"/>
<gate name="G$2" symbol="DOCFIELD" x="172.72" y="0" addlevel="always"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="DCJACK_2MM_PTH">
<description>DJ Jack 2.0mm PTH Right-Angle</description>
<wire x1="4.5" y1="14.2" x2="2.4" y2="14.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.2032" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.2032" layer="51"/>
<wire x1="4.5" y1="0" x2="-4.5" y2="0" width="0.2032" layer="51"/>
<wire x1="4.5" y1="3.3" x2="4.5" y2="8.4" width="0.2032" layer="21"/>
<wire x1="4.5" y1="14.2" x2="4.5" y2="13.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="14.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="14.2" x2="-2.6" y2="14.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="4.5" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="4.5" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="12.45" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="12.45" x2="-4.5" y2="14.2" width="0.2032" layer="51"/>
<wire x1="4.5" y1="3.3" x2="4.5" y2="8.35" width="0.2032" layer="51"/>
<wire x1="4.5" y1="8.35" x2="4.5" y2="8.4" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="14.2" x2="2.65" y2="14.2" width="0.2032" layer="51"/>
<wire x1="4.5" y1="14.2" x2="2.65" y2="14.2" width="0.2032" layer="51"/>
<wire x1="2.65" y1="14.2" x2="2.4" y2="14.2" width="0.2032" layer="51"/>
<wire x1="4.5" y1="14.2" x2="4.5" y2="8.35" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="12.45" x2="4.4" y2="12.45" width="0.2032" layer="51"/>
<pad name="PWR" x="0" y="13.6" drill="3.2"/>
<pad name="GND" x="0" y="7.35" drill="2.8"/>
<pad name="GNDBREAK" x="4.8" y="10.75" drill="2.8" rot="R90"/>
<text x="-5.08" y="0" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.35" y="0" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-3.302" y="1.27" size="0.8128" layer="51">DC 2.0/2.1</text>
</package>
<package name="DCJACK_2MM_SMT">
<description>2.0/2.1mm DC Jack - SMT
&lt;p&gt;4UConnector: 03267&lt;/p&gt;
&lt;p&gt;Note: Small tRestrict polygon's were added to the ground pads to improve solderability when this part is used in combination with a ground pour.  By default, Eagle will product four large bridges to the ground pour significantly increasing the heat distribution on the pads and preventing lead-free solder from reflowing in certain situations.  For more details, see: http://www.microbuilder.eu/Blog/09-12-14/Reducing_Thermals_for_Large_Pads_in_Eagle.aspx&lt;/p&gt;</description>
<wire x1="-4" y1="4.5" x2="-5" y2="3.5" width="0.127" layer="51" curve="90"/>
<wire x1="-5" y1="3.5" x2="-5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-5" y1="-3.5" x2="-4" y2="-4.5" width="0.127" layer="51" curve="90"/>
<wire x1="-4" y1="-4.5" x2="10.254" y2="-4.5" width="0.127" layer="51"/>
<wire x1="10.254" y1="-4.5" x2="10.254" y2="-1.492" width="0.127" layer="51"/>
<wire x1="10.254" y1="-1.492" x2="9" y2="-1.492" width="0.127" layer="51"/>
<wire x1="9" y1="-1.492" x2="9" y2="4.5" width="0.127" layer="51"/>
<wire x1="9" y1="4.5" x2="-4" y2="4.5" width="0.127" layer="51"/>
<wire x1="-4" y1="4.5" x2="-5" y2="3.5" width="0.127" layer="21" curve="90"/>
<wire x1="-5" y1="3.5" x2="-5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-5" y1="-3.5" x2="-4" y2="-4.5" width="0.127" layer="21" curve="90"/>
<wire x1="10.254" y1="-4.5" x2="10.254" y2="-1.492" width="0.127" layer="21"/>
<wire x1="10.254" y1="-1.492" x2="9" y2="-1.492" width="0.127" layer="21"/>
<wire x1="9" y1="-1.492" x2="9" y2="4.5" width="0.127" layer="21"/>
<wire x1="-1.668" y1="4.5" x2="-4" y2="4.5" width="0.127" layer="21"/>
<wire x1="4.682" y1="4.5" x2="1.588" y2="4.5" width="0.127" layer="21"/>
<wire x1="9" y1="4.5" x2="7.938" y2="4.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-1.684" y2="-4.5" width="0.127" layer="21"/>
<wire x1="1.588" y1="-4.5" x2="4.666" y2="-4.5" width="0.127" layer="21"/>
<wire x1="7.938" y1="-4.5" x2="10.254" y2="-4.5" width="0.127" layer="21"/>
<smd name="PWR1" x="0" y="5.5" dx="2.4" dy="2" layer="1"/>
<smd name="PWR2" x="6.2" y="5.5" dx="2.4" dy="2" layer="1"/>
<smd name="GNDBREAK" x="6.2" y="-5.5" dx="2.4" dy="2" layer="1"/>
<smd name="GND" x="0" y="-5.5" dx="2.4" dy="2" layer="1"/>
<text x="0.762" y="2.794" size="1.4224" layer="21" ratio="12" rot="R90">+</text>
<text x="-1.016" y="-3.81" size="0.8128" layer="21">GND</text>
<hole x="0" y="0" drill="1.6"/>
<hole x="4.5" y="0" drill="1.8"/>
<polygon width="0.0254" layer="41" spacing="0.254">
<vertex x="1.27" y="-5.7404"/>
<vertex x="1.27" y="-5.2578"/>
<vertex x="1.2954" y="-5.2578"/>
<vertex x="1.2954" y="-5.7404"/>
</polygon>
<polygon width="0.0254" layer="41" spacing="0.254">
<vertex x="-0.254" y="-4.4196"/>
<vertex x="0.2286" y="-4.4196"/>
<vertex x="0.2286" y="-4.445"/>
<vertex x="-0.254" y="-4.445"/>
</polygon>
<polygon width="0.0254" layer="41" spacing="0.254">
<vertex x="4.9022" y="-5.7404"/>
<vertex x="4.9022" y="-5.2578"/>
<vertex x="4.9276" y="-5.2578"/>
<vertex x="4.9276" y="-5.7404"/>
</polygon>
<polygon width="0.0254" layer="41" spacing="0.254">
<vertex x="6.4262" y="-4.445"/>
<vertex x="5.9436" y="-4.445"/>
<vertex x="5.9436" y="-4.4196"/>
<vertex x="6.4262" y="-4.4196"/>
</polygon>
</package>
<package name="1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622" cap="flat"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378" cap="flat"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622" cap="flat"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378" cap="flat"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923" cap="flat"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923" cap="flat"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923" cap="flat"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923" cap="flat"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419" cap="flat"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332" cap="flat"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331" cap="flat"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331" cap="flat"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361" cap="flat"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208" cap="flat"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165" cap="flat"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361" cap="flat"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337" cap="flat"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102" cap="flat"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="-2.5908" y1="1.7272" x2="-1.8542" y2="1.7272" width="0.127" layer="21"/>
<wire x1="-2.2352" y1="1.3208" x2="-2.2352" y2="2.1082" width="0.127" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139" cap="flat"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139" cap="flat"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139" cap="flat"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139" cap="flat"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135" cap="flat"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135" cap="flat"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135" cap="flat"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135" cap="flat"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642" cap="flat"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716" cap="flat"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985" cap="flat"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172" cap="flat"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177" cap="flat"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073" cap="flat"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064" cap="flat"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376" cap="flat"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488" cap="flat"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638" cap="flat"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781" cap="flat"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781" cap="flat"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626" cap="flat"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992" cap="flat"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586" cap="flat"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757" cap="flat"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="0.4826" x2="-2.1082" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-0.4826" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="0.4826" x2="2.9718" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-0.4826" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487" cap="flat"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487" cap="flat"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="31"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="31"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.27" y="-2.54" size="1.016" layer="21" font="vector">+</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.45" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="-0.75" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-0.75" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="1" x2="0.35" y2="1" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.35" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="51" curve="-180" cap="flat"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.4" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.625" x2="0.4" y2="1.625" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED_0603">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.825" x2="0.3" y2="0.825" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.825" x2="0.3" y2="0.825" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="SMLK34">
<wire x1="-2" y1="1" x2="1.7" y2="1" width="0.127" layer="21"/>
<wire x1="1.7" y1="1" x2="2" y2="0.7" width="0.127" layer="21"/>
<wire x1="2" y1="0.7" x2="2" y2="-1" width="0.127" layer="21"/>
<wire x1="2" y1="-1" x2="-2" y2="-1" width="0.127" layer="21"/>
<wire x1="-2" y1="-1" x2="-2" y2="1" width="0.127" layer="21"/>
<wire x1="-1.8" y1="0.8" x2="-1.8" y2="-0.8" width="0.127" layer="25"/>
<wire x1="-1.8" y1="-0.8" x2="1.8" y2="-0.8" width="0.127" layer="25"/>
<wire x1="1.8" y1="-0.8" x2="1.8" y2="0.6" width="0.127" layer="25"/>
<wire x1="1.8" y1="0.6" x2="1.6" y2="0.8" width="0.127" layer="25"/>
<wire x1="1.6" y1="0.8" x2="-1.8" y2="0.8" width="0.127" layer="25"/>
<smd name="A" x="0.9" y="0" dx="3.65" dy="1.74" layer="1" rot="R180"/>
<smd name="K" x="-2.1" y="0" dx="1.35" dy="1.74" layer="1" rot="R180"/>
<text x="-2.8" y="-3.97" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.8" y="-5.37" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="1.73" y="-2.178" size="1.016" layer="21" font="vector">A</text>
<text x="-2.938" y="-2.178" size="1.016" layer="21" font="vector">K</text>
</package>
</packages>
<symbols>
<symbol name="DCBARREL">
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="3.175" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="3.175" x2="-4.445" y2="1.905" width="0.254" layer="94" curve="180"/>
<wire x1="0" y1="1.905" x2="-4.445" y2="1.905" width="0.254" layer="94"/>
<wire x1="0" y1="3.175" x2="-4.445" y2="3.175" width="0.254" layer="94"/>
<text x="-5.08" y="5.08" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
<pin name="PWR" x="2.54" y="2.54" visible="pad" length="short" direction="pwr" rot="R180"/>
<pin name="GNDBREAK" x="2.54" y="0" visible="pad" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="2.54" y="-2.54" visible="pad" length="short" direction="pwr" rot="R180"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="DCBARREL" prefix="CN" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;2.0mm DC Barrel Jack&lt;/b&gt;&lt;/p&gt;
&lt;b&gt;DCJACK_2MM_PTH&lt;/b&gt; - Through Hole Jack (4UConnector: 05537)</description>
<gates>
<gate name="G$1" symbol="DCBARREL" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="DCJACK_2MM_PTH">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GNDBREAK" pad="GNDBREAK"/>
<connect gate="G$1" pin="PWR" pad="PWR"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMT" package="DCJACK_2MM_SMT">
<connects>
<connect gate="G$1" pin="GND" pad="GNDBREAK"/>
<connect gate="G$1" pin="GNDBREAK" pad="GND"/>
<connect gate="G$1" pin="PWR" pad="PWR1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;


- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K&lt;br&gt;

&lt;p&gt;
Source: http://www.osram.convergy.de/</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMLK34" package="SMLK34">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="USB-MINIB">
<description>&lt;b&gt;USB Series Mini-B Surface Mounted&lt;/b&gt;</description>
<wire x1="-1.3" y1="3.8" x2="0.8" y2="3.8" width="0.2032" layer="21"/>
<wire x1="3.3" y1="3.1" x2="3.3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2.2" x2="3.3" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="-3.8" x2="-1.3" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-5.9" y1="3.8" x2="-5.9" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-5.9" y1="-3.8" x2="-4.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-5.9" y1="3.8" x2="-4.5" y2="3.8" width="0.2032" layer="51"/>
<smd name="D+" x="2.5" y="0" dx="2.5" dy="0.5" layer="1"/>
<smd name="D-" x="2.5" y="0.8" dx="2.5" dy="0.5" layer="1"/>
<smd name="GND" x="2.5" y="-1.6" dx="2.5" dy="0.5" layer="1"/>
<smd name="ID" x="2.5" y="-0.8" dx="2.5" dy="0.5" layer="1"/>
<smd name="MTN3" x="-3" y="-4.5" dx="2.5" dy="2" layer="1"/>
<smd name="MTN1" x="-3" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="MTN4" x="2.5" y="-4.5" dx="2.5" dy="2" layer="1"/>
<smd name="MTN2" x="2.5" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="VBUS" x="2.5" y="1.6" dx="2.5" dy="0.5" layer="1"/>
<text x="-3.81" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="0" size="0.4064" layer="27">&gt;VALUE</text>
<hole x="0" y="2.2" drill="0.9"/>
<hole x="0" y="-2.2" drill="0.9"/>
</package>
<package name="2X3">
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.605" x2="-0.635" y2="-1.605" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796" shape="octagon"/>
<text x="-1.27" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
</package>
<package name="2X3-NS">
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.2032" layer="51"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.2032" layer="51"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.2032" layer="51"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.2032" layer="51"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="-2.875" x2="-3.175" y2="-2.875" width="0.2032" layer="51"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
</package>
<package name="2X3_OFFSET">
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-4.78" x2="-3.175" y2="-4.78" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.1176" diameter="1.8796" shape="offset" rot="R270"/>
<pad name="2" x="-2.54" y="1.27" drill="1.1176" diameter="1.8796" shape="offset" rot="R90"/>
<pad name="3" x="0" y="-1.27" drill="1.1176" diameter="1.8796" shape="offset" rot="R270"/>
<pad name="4" x="0" y="1.27" drill="1.1176" diameter="1.8796" shape="offset" rot="R90"/>
<pad name="5" x="2.54" y="-1.27" drill="1.1176" diameter="1.8796" shape="offset" rot="R270"/>
<pad name="6" x="2.54" y="1.27" drill="1.1176" diameter="1.8796" shape="offset" rot="R90"/>
<text x="-4.445" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="-3.175" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
</package>
<package name="2X3_LOCK">
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.605" x2="-0.635" y2="-1.605" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="3" x="2.54" y="-0.254" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="4" x="2.54" y="2.286" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796" shape="octagon"/>
<text x="-1.27" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
</package>
<package name="2X3_TEST_POINTS">
<pad name="1" x="-2.54" y="-1.27" drill="0.508" stop="no"/>
<pad name="2" x="-2.54" y="1.27" drill="0.508" stop="no"/>
<pad name="3" x="0" y="-1.27" drill="0.508" stop="no"/>
<pad name="4" x="0" y="1.27" drill="0.508" stop="no"/>
<pad name="5" x="2.54" y="-1.27" drill="0.508" stop="no"/>
<pad name="6" x="2.54" y="1.27" drill="0.508" stop="no"/>
<wire x1="-2.8956" y1="-2.0574" x2="-2.2098" y2="-2.0574" width="0.2032" layer="21"/>
<circle x="-2.54" y="1.27" radius="0.61065625" width="0" layer="29"/>
<circle x="0" y="1.27" radius="0.61065625" width="0" layer="29"/>
<circle x="2.54" y="1.27" radius="0.61065625" width="0" layer="29"/>
<circle x="-2.54" y="-1.27" radius="0.61065625" width="0" layer="29"/>
<circle x="0" y="-1.27" radius="0.61065625" width="0" layer="29"/>
<circle x="2.54" y="-1.27" radius="0.61065625" width="0" layer="29"/>
</package>
<package name="1X02">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X2">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-SMD">
<description>2mm SMD side-entry connector. tDocu layer indicates the actual physical plastic housing. +/- indicate SparkFun standard batteries and wiring.</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="0" size="0.4064" layer="27">&gt;Value</text>
<text x="2.159" y="-4.445" size="1.27" layer="51">+</text>
<text x="-2.921" y="-4.445" size="1.27" layer="51">-</text>
</package>
<package name="1X02_BIG">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
</package>
<package name="JST-2-SMD-VERT">
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="2.54" y="-5.08" size="1.27" layer="25">&gt;Name</text>
<text x="2.24" y="3.48" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="R_SW_TH">
<wire x1="-1.651" y1="19.2532" x2="-1.651" y2="-1.3716" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-1.3716" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="19.2532" x2="13.589" y2="19.2532" width="0.2032" layer="21"/>
<wire x1="13.589" y1="19.2532" x2="13.589" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="13.589" y1="-2.2352" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.6002"/>
<pad name="P$2" x="0" y="16.9926" drill="1.6002"/>
<pad name="P$3" x="12.0904" y="15.494" drill="1.6002"/>
<pad name="P$4" x="12.0904" y="8.4582" drill="1.6002"/>
</package>
<package name="SCREWTERMINAL-5MM-2">
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LOCK">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="MOLEX-1X2_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="1X02_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS">
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
</package>
<package name="1X02_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-2-PTH">
<wire x1="-2" y1="0" x2="-2" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.6" x2="-2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-1.6" x2="-2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="6" x2="2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="6" x2="2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-1.6" x2="2" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2" y1="-1.6" x2="2" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="1X02_XTRA_BIG">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
</package>
<package name="1X02_PP_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS">
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT">
<description>&lt;H3&gt;JST-2-PTH-KIT&lt;/h3&gt;
2-Pin JST, through-hole connector&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2">
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM">
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.127" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.127" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="JST-2-PTH-VERT">
<wire x1="-2.95" y1="-2.25" x2="-2.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="2.25" x2="2.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="2.95" y1="2.25" x2="2.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-2.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-1" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="2" x="1" y="-0.55" drill="0.7" diameter="1.6256"/>
<text x="-1.984" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="2.016" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="0.616" y="0.75" size="1.27" layer="51">+</text>
<text x="-1.384" y="0.75" size="1.27" layer="51">-</text>
</package>
<package name="1X04">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X4">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1.27MM">
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="4.699" y2="0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.381" x2="4.699" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.381" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04_LOCK">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.1176" x2="8.6106" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.8636" x2="8.6106" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
</package>
<package name="MOLEX-1X4_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
</package>
<package name="1X04-SMD">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="1X04_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-4-PTH">
<wire x1="-4.5" y1="-5" x2="-5.2" y2="-5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5" x2="-5.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-6.3" x2="-6" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-6" y1="-6.3" x2="-6" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-6" y1="1.1" x2="6" y2="1.1" width="0.2032" layer="21"/>
<wire x1="6" y1="1.1" x2="6" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="6" y1="-6.3" x2="5.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-6.3" x2="5.2" y2="-5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-5" x2="4.5" y2="-5" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-5" drill="0.7"/>
<pad name="2" x="-1" y="-5" drill="0.7"/>
<pad name="3" x="1" y="-5" drill="0.7"/>
<pad name="4" x="3" y="-5" drill="0.7"/>
<text x="-2.27" y="0.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.27" y="-1" size="0.4064" layer="27">&gt;Value</text>
<text x="-3.4" y="-4.3" size="1.27" layer="51">+</text>
<text x="-1.4" y="-4.3" size="1.27" layer="51">-</text>
<text x="0.7" y="-4.1" size="0.8" layer="51">S</text>
<text x="2.7" y="-4.1" size="0.8" layer="51">S</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4_LOCK">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="6.8222" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1MM-RA">
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.73" y="1.73" size="0.4064" layer="25" rot="R180">&gt;NAME</text>
<text x="3.46" y="1.73" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="1X04_SMD_STRAIGHT_COMBO">
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="1.25" x2="8.99" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="-1.25" x2="8.32" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.32" y1="1.25" x2="8.99" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-SMD_LONG">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="JST-4-PTH-VERT">
<wire x1="-4.95" y1="-2.25" x2="-4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-4.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="2" x="-1" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="3" x="1" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="4" x="3" y="-0.55" drill="0.7" diameter="1.6256"/>
<text x="-3" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="1" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="-1.4" y="0.75" size="1.27" layer="51">+</text>
<text x="0.6" y="0.75" size="1.27" layer="51">-</text>
<text x="2.7" y="0.95" size="0.8" layer="51">Y</text>
<text x="-3.3" y="0.95" size="0.8" layer="51">B</text>
</package>
<package name="1X08">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="1X08_LOCK">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="1X08_LOCK_LONGPADS">
<wire x1="1.524" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="4.064" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="6.604" y1="0" x2="6.096" y2="0" width="0.2032" layer="21"/>
<wire x1="9.144" y1="0" x2="8.636" y2="0" width="0.2032" layer="21"/>
<wire x1="11.684" y1="0" x2="11.176" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="14.224" y1="0" x2="13.716" y2="0" width="0.2032" layer="21"/>
<wire x1="16.764" y1="0" x2="16.256" y2="0" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0" x2="19.05" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.9906" x2="18.7706" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0" x2="19.05" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.9906" x2="18.7706" y2="1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0" x2="18.796" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.905" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
<rectangle x1="7.3279" y1="-0.2921" x2="7.9121" y2="0.2921" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.2921" x2="10.4521" y2="0.2921" layer="51"/>
<rectangle x1="12.4079" y1="-0.2921" x2="12.9921" y2="0.2921" layer="51"/>
<rectangle x1="14.9479" y1="-0.2921" x2="15.5321" y2="0.2921" layer="51"/>
<rectangle x1="17.4879" y1="-0.2921" x2="18.0721" y2="0.2921" layer="51"/>
</package>
<package name="1X08_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-8">
<wire x1="-2.3" y1="3.4" x2="26.76" y2="3.4" width="0.2032" layer="21"/>
<wire x1="26.76" y1="3.4" x2="26.76" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="26.76" y1="-2.8" x2="26.76" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="26.76" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="26.76" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="26.76" y1="3.15" x2="27.16" y2="3.15" width="0.2032" layer="51"/>
<wire x1="27.16" y1="3.15" x2="27.16" y2="2.15" width="0.2032" layer="51"/>
<wire x1="27.16" y1="2.15" x2="26.76" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="5" x="14" y="0" drill="1.2" diameter="2.032"/>
<pad name="6" x="17.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="7" x="21" y="0" drill="1.2" diameter="2.032"/>
<pad name="8" x="24.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_SMD">
<wire x1="1.37" y1="1.25" x2="-19.15" y2="1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="1.25" x2="-19.15" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-19.15" y1="-1.25" x2="-19.15" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-19.15" y1="1.25" x2="-15.963" y2="1.25" width="0.127" layer="21"/>
<wire x1="-18.63" y1="-1.25" x2="-19.15" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="-1.817" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.377" y1="1.25" x2="-0.703" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.457" y1="1.25" x2="-5.783" y2="1.25" width="0.127" layer="21"/>
<wire x1="-14.537" y1="1.25" x2="-10.863" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.329" y1="-1.25" x2="-6.831" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.409" y1="-1.25" x2="-11.911" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-13.489" y1="-1.25" x2="-16.991" y2="-1.25" width="0.127" layer="21"/>
<smd name="7" x="-15.24" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="5" x="-10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="8" x="-17.78" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6" x="-12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-19.05" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-19.05" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_SMD_ALT">
<wire x1="1.37" y1="1.25" x2="-19.15" y2="1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="1.25" x2="-19.15" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="1.25" x2="-19.15" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-18.63" y1="1.25" x2="-19.15" y2="1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="1.25" x2="-1.817" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-19.15" y1="-1.25" x2="-15.963" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-13.403" y1="1.25" x2="-17.077" y2="1.25" width="0.127" layer="21"/>
<wire x1="-8.323" y1="1.25" x2="-11.997" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.243" y1="1.25" x2="-6.917" y2="1.25" width="0.127" layer="21"/>
<wire x1="-14.451" y1="-1.25" x2="-10.949" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-9.371" y1="-1.25" x2="-5.869" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.291" y1="-1.25" x2="-0.789" y2="-1.25" width="0.127" layer="21"/>
<smd name="7" x="-15.24" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="5" x="-10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="-5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="8" x="-17.78" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="-12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="-7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="-2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<text x="-19.05" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-19.05" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_SMD_COMBINED">
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="19.15" y1="1.25" x2="19.15" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-1.25" x2="-1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="19.15" y1="-1.25" x2="18.503" y2="-1.25" width="0.127" layer="21"/>
<wire x1="18.63" y1="1.25" x2="19.15" y2="1.25" width="0.127" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.723" y2="1.25" width="0.127" layer="21"/>
<wire x1="14.537" y1="-1.25" x2="13.403" y2="-1.25" width="0.127" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.127" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.127" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.127" layer="21"/>
<wire x1="8.409" y1="1.25" x2="9.371" y2="1.25" width="0.127" layer="21"/>
<wire x1="10.949" y1="1.25" x2="11.911" y2="1.25" width="0.127" layer="21"/>
<wire x1="13.489" y1="1.25" x2="14.451" y2="1.25" width="0.127" layer="21"/>
<wire x1="16.029" y1="1.25" x2="16.991" y2="1.25" width="0.127" layer="21"/>
<wire x1="17.077" y1="-1.25" x2="15.943" y2="-1.25" width="0.127" layer="21"/>
<wire x1="11.997" y1="-1.25" x2="10.863" y2="-1.25" width="0.127" layer="21"/>
<wire x1="9.457" y1="-1.25" x2="8.323" y2="-1.25" width="0.127" layer="21"/>
<wire x1="6.917" y1="-1.25" x2="5.783" y2="-1.25" width="0.127" layer="21"/>
<wire x1="4.377" y1="-1.25" x2="3.243" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.837" y1="-1.25" x2="0.703" y2="-1.25" width="0.127" layer="21"/>
<wire x1="17.78" y1="1.27" x2="17.78" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="15.24" y1="1.27" x2="15.24" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="12.7" y1="1.27" x2="12.7" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="10.16" y1="1.27" x2="10.16" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<smd name="7@2" x="15.24" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="5@2" x="10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3@2" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="8@2" x="17.78" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6@2" x="12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4@2" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2@2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="5" x="10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6" x="12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="7" x="15.24" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="8" x="17.78" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1@2" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="BM08B-SRSS-TB">
<description>JST crimp connector: 1mm pitch, top entry</description>
<wire x1="-5" y1="3.3" x2="5" y2="3.3" width="0.127" layer="51"/>
<wire x1="-5" y1="0.4" x2="-5" y2="3.3" width="0.127" layer="51"/>
<wire x1="5" y1="0.4" x2="5" y2="3.3" width="0.127" layer="51"/>
<wire x1="-5" y1="0.4" x2="5" y2="0.4" width="0.127" layer="51"/>
<wire x1="-4.1" y1="0.35" x2="-5.05" y2="0.35" width="0.2032" layer="21"/>
<wire x1="-5.05" y1="0.35" x2="-5.05" y2="1.35" width="0.2032" layer="21"/>
<wire x1="5.05" y1="0.35" x2="4.15" y2="0.35" width="0.2032" layer="21"/>
<wire x1="5.05" y1="0.35" x2="5.05" y2="1.35" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="3.4" x2="3.9" y2="3.4" width="0.2032" layer="21"/>
<circle x="-4.4" y="-0.35" radius="0.1118" width="0.4064" layer="21"/>
<smd name="1" x="-3.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="2" x="-2.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="3" x="-1.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="4" x="-0.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="5" x="0.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="6" x="1.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="7" x="2.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="8" x="3.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="P$9" x="4.8" y="2.525" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<smd name="P$10" x="-4.8" y="2.525" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<text x="-3.8" y="2.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.8" y="1.3" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_SMD_MALE">
<text x="-1" y="3.321" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1" y="-4.591" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="19.05" y2="-1.25" width="0.127" layer="51"/>
<wire x1="19.05" y1="-1.25" x2="19.05" y2="1.25" width="0.127" layer="51"/>
<wire x1="19.05" y1="1.25" x2="-1.27" y2="1.25" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.64" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="0" x2="0.32" y2="2.75" layer="51"/>
<rectangle x1="4.76" y1="0" x2="5.4" y2="2.75" layer="51"/>
<rectangle x1="9.84" y1="0" x2="10.48" y2="2.75" layer="51"/>
<rectangle x1="2.22" y1="-2.75" x2="2.86" y2="0" layer="51" rot="R180"/>
<rectangle x1="7.3" y1="-2.75" x2="7.94" y2="0" layer="51" rot="R180"/>
<rectangle x1="12.38" y1="-2.75" x2="13.02" y2="0" layer="51" rot="R180"/>
<smd name="1" x="0" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="3" x="5.08" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="4" x="7.62" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="5" x="10.16" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="6" x="12.7" y="0" dx="1.02" dy="6" layer="1"/>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="-1.25" x2="-0.635" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="1.25" x2="-0.635" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.762" y1="1.25" x2="1.778" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.302" y1="1.25" x2="4.318" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.842" y1="1.25" x2="6.858" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.382" y1="1.25" x2="9.398" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.922" y1="1.25" x2="11.938" y2="1.25" width="0.1778" layer="21"/>
<wire x1="1.778" y1="-1.25" x2="0.762" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="4.318" y1="-1.25" x2="3.302" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="6.858" y1="-1.25" x2="5.842" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="9.398" y1="-1.25" x2="8.382" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="11.938" y1="-1.25" x2="10.922" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="19.05" y1="-1.25" x2="19.05" y2="1.25" width="0.1778" layer="21"/>
<circle x="15.24" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="17.78" y="0" radius="0.64" width="0.127" layer="51"/>
<rectangle x1="14.92" y1="0" x2="15.56" y2="2.75" layer="51"/>
<rectangle x1="17.46" y1="-2.75" x2="18.1" y2="0" layer="51" rot="R180"/>
<smd name="7" x="15.24" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="8" x="17.78" y="0" dx="1.02" dy="6" layer="1"/>
<wire x1="16.002" y1="1.25" x2="17.018" y2="1.25" width="0.1778" layer="21"/>
<wire x1="17.018" y1="-1.25" x2="16.002" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="19.05" y1="-1.25" x2="18.415" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="19.05" y1="1.25" x2="18.415" y2="1.25" width="0.1778" layer="21"/>
<wire x1="13.462" y1="1.25" x2="14.478" y2="1.25" width="0.1778" layer="21"/>
<wire x1="14.478" y1="-1.25" x2="13.462" y2="-1.25" width="0.1778" layer="21"/>
</package>
<package name="1X08_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="1X08_PIN1_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="USB-5PIN">
<wire x1="7.62" y1="12.7" x2="0" y2="12.7" width="0.254" layer="94"/>
<wire x1="0" y1="12.7" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<text x="5.334" y="1.778" size="2.54" layer="94" rot="R90">USB</text>
<pin name="D+" x="-2.54" y="5.08" visible="pad" length="short"/>
<pin name="D-" x="-2.54" y="7.62" visible="pad" length="short"/>
<pin name="VBUS" x="-2.54" y="10.16" visible="pad" length="short"/>
<pin name="GND" x="-2.54" y="0" visible="pad" length="short"/>
<pin name="ID" x="-2.54" y="2.54" visible="pad" length="short"/>
<pin name="MTN1" x="2.54" y="15.24" visible="pad" length="short" rot="R270"/>
<pin name="MTN2" x="5.08" y="15.24" visible="pad" length="short" rot="R270"/>
<pin name="MTN3" x="2.54" y="-5.08" visible="pad" length="short" rot="R90"/>
<pin name="MTN4" x="5.08" y="-5.08" visible="pad" length="short" rot="R90"/>
</symbol>
<symbol name="AVR_SPI_PROGRAMMER_6">
<wire x1="-5.08" y1="-5.08" x2="7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<text x="-4.318" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.064" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="8.89" y="0.635" size="1.27" layer="94">MOSI</text>
<text x="-11.938" y="-2.032" size="1.27" layer="94">RESET</text>
<text x="-11.938" y="0.508" size="1.27" layer="94">SCK</text>
<text x="-11.938" y="3.302" size="1.27" layer="94">MISO</text>
<text x="8.89" y="3.048" size="1.27" layer="94">+5</text>
<text x="8.89" y="-2.032" size="1.27" layer="94">GND</text>
<pin name="1" x="-7.62" y="2.54" visible="pad" direction="pas" function="dot"/>
<pin name="2" x="10.16" y="2.54" visible="pad" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-7.62" y="0" visible="pad" direction="pas" function="dot"/>
<pin name="4" x="10.16" y="0" visible="pad" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-7.62" y="-2.54" visible="pad" direction="pas" function="dot"/>
<pin name="6" x="10.16" y="-2.54" visible="pad" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="M02">
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M04">
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M08">
<wire x1="1.27" y1="-10.16" x2="-5.08" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-7.62" x2="0" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="-5.08" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="10.16" x2="0" y2="10.16" width="0.6096" layer="94"/>
<text x="-5.08" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="5.08" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB-MINIB">
<description>&lt;b&gt;Mini-USB "B" connector with 5th pin broken out.&lt;/b&gt;&lt;p&gt;
Created new symbol breaking out 5th "ID" pin in mini/micro USB connector spec.  See: http://en.wikipedia.org/wiki/Mini_usb#Cables.  Uses same footprint as 4-pin symbol.&lt;p&gt;
 

Also added pins to connect to mounting / shield pads if required (probably not generally needed as signals aren't shielded once they leave the connector).</description>
<gates>
<gate name="G$1" symbol="USB-5PIN" x="0" y="0"/>
</gates>
<devices>
<device name="-5PIN" package="USB-MINIB">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="ID" pad="ID"/>
<connect gate="G$1" pin="MTN1" pad="MTN3"/>
<connect gate="G$1" pin="MTN2" pad="MTN1"/>
<connect gate="G$1" pin="MTN3" pad="MTN2"/>
<connect gate="G$1" pin="MTN4" pad="MTN4"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AVR_SPI_PRG_6" prefix="J">
<description>&lt;b&gt;AVR ISP 6 Pin&lt;/b&gt;
This is the reduced ISP connector for AVR programming. Common on Arduino. This footprint will take up less PCB space and can be used with a 10-pin to 6-pin adapter such as SKU: BOB-08508

&lt;b&gt;**Special note about "TEST_POINT" package.&lt;/b&gt; The stop mask is on the top side, so if you want your programming test points to be on the bottom of your board, make sure to place this package on the bottom side of the board. This also ensure that the orientation to program from the bottom side will be correct.</description>
<gates>
<gate name="G$1" symbol="AVR_SPI_PROGRAMMER_6" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="2X3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NS" package="2X3-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="OFFSET_PADS" package="2X3_OFFSET">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2X3_LOCK" package="2X3_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TESTPOINTS" package="2X3_TEST_POINTS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M02" prefix="JP">
<description>Standard 2-pin 0.1" header. Use with &lt;br&gt;
- straight break away headers ( PRT-00116)&lt;br&gt;
- right angle break away headers (PRT-00553)&lt;br&gt;
- swiss pins (PRT-00743)&lt;br&gt;
- machine pins (PRT-00117)&lt;br&gt;
- female headers (PRT-00115)&lt;br&gt;&lt;br&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08433</description>
<gates>
<gate name="G$1" symbol="M02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08352"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROCKER" package="R_SW_TH">
<connects>
<connect gate="G$1" pin="1" pad="P$3"/>
<connect gate="G$1" pin="2" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09914" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-2-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M04" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 4&lt;/b&gt;
Standard 4-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="G$1" symbol="M04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.27MM" package="1X04-1.27MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X04_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X04_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X04-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X04_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X04_NO_SILK" package="1X04_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-4-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09916" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="1X04-1MM-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_STRAIGHT_COMBO" package="1X04_SMD_STRAIGHT_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_LONG" package="1X04-SMD_LONG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-4-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M08" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 8&lt;/b&gt;
Standard 8-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115).</description>
<gates>
<gate name="G$1" symbol="M08" x="-2.54" y="0"/>
</gates>
<devices>
<device name="1X08" package="1X08">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X08_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X08_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X08_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-8" package="SCREWTERMINAL-3.5MM-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT" package="1X08_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT-ALT" package="1X08_SMD_ALT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-COMBO" package="1X08_SMD_COMBINED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BM08B-SRSS-TB" package="BM08B-SRSS-TB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-MALE" package="1X08_SMD_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11292"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="1X08_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1X08_PIN1_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="weatherpilot_lbr">
<packages>
<package name="DQC10_P84X2P4">
<smd name="1" x="-1.0287" y="0.999996875" dx="0.6604" dy="0.3048" layer="1"/>
<smd name="2" x="-1.0287" y="0.5" dx="0.6604" dy="0.3048" layer="1"/>
<smd name="3" x="-1.0287" y="0" dx="0.6604" dy="0.3048" layer="1"/>
<smd name="4" x="-1.0287" y="-0.5" dx="0.6604" dy="0.3048" layer="1"/>
<smd name="5" x="-1.0287" y="-0.999996875" dx="0.6604" dy="0.3048" layer="1"/>
<smd name="6" x="1.0287" y="-0.999996875" dx="0.6604" dy="0.3048" layer="1"/>
<smd name="7" x="1.0287" y="-0.5" dx="0.6604" dy="0.3048" layer="1"/>
<smd name="8" x="1.0287" y="0" dx="0.6604" dy="0.3048" layer="1"/>
<smd name="9" x="1.0287" y="0.5" dx="0.6604" dy="0.3048" layer="1"/>
<smd name="10" x="1.0287" y="0.999996875" dx="0.6604" dy="0.3048" layer="1"/>
<smd name="11" x="0" y="0" dx="0.9398" dy="2.5146" layer="1"/>
<pad name="V" x="0" y="-0.7874" drill="0.254"/>
<wire x1="-0.4064" y1="1.1938" x2="-0.4064" y2="0.889" width="0.1524" layer="31"/>
<wire x1="-0.4064" y1="0.889" x2="-0.254" y2="0.889" width="0.1524" layer="31"/>
<wire x1="-0.1016" y1="1.1938" x2="-0.4064" y2="1.1938" width="0.1524" layer="31"/>
<wire x1="-0.4064" y1="0.6858" x2="-0.4064" y2="0.1016" width="0.1524" layer="31"/>
<wire x1="-0.4064" y1="0.1016" x2="-0.254" y2="0.1016" width="0.1524" layer="31"/>
<wire x1="-0.4064" y1="-0.1016" x2="-0.4064" y2="-0.6858" width="0.1524" layer="31"/>
<wire x1="-0.4064" y1="-0.6858" x2="-0.254" y2="-0.6858" width="0.1524" layer="31"/>
<wire x1="-0.4064" y1="-0.889" x2="-0.4064" y2="-1.1938" width="0.1524" layer="31"/>
<wire x1="-0.4064" y1="-1.1938" x2="-0.1016" y2="-1.1938" width="0.1524" layer="31"/>
<wire x1="-0.1016" y1="-1.1938" x2="-0.1016" y2="-1.0414" width="0.1524" layer="31"/>
<wire x1="0.1016" y1="1.1938" x2="0.1016" y2="1.0414" width="0.1524" layer="31"/>
<wire x1="0.4064" y1="0.889" x2="0.4064" y2="1.1938" width="0.1524" layer="31"/>
<wire x1="0.4064" y1="1.1938" x2="0.1016" y2="1.1938" width="0.1524" layer="31"/>
<wire x1="0.4064" y1="0.1016" x2="0.4064" y2="0.6858" width="0.1524" layer="31"/>
<wire x1="0.4064" y1="0.6858" x2="0.254" y2="0.6858" width="0.1524" layer="31"/>
<wire x1="0.4064" y1="-0.6858" x2="0.4064" y2="-0.1016" width="0.1524" layer="31"/>
<wire x1="0.4064" y1="-0.1016" x2="0.254" y2="-0.1016" width="0.1524" layer="31"/>
<wire x1="0.1016" y1="-1.1938" x2="0.4064" y2="-1.1938" width="0.1524" layer="31"/>
<wire x1="0.4064" y1="-1.1938" x2="0.4064" y2="-0.889" width="0.1524" layer="31"/>
<wire x1="0.4064" y1="-0.889" x2="0.254" y2="-0.889" width="0.1524" layer="31"/>
<wire x1="-0.2032" y1="1.3208" x2="0.2032" y2="1.3208" width="0.1524" layer="29"/>
<wire x1="0.2032" y1="1.3208" x2="0.5334" y2="1.3208" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="1.3208" x2="0.5334" y2="0.9906" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="0.9906" x2="-0.5334" y2="0.9906" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="0.9906" x2="-0.5334" y2="1.3208" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="0.5842" x2="0.5334" y2="0.5842" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="0.5842" x2="0.5334" y2="0.2032" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="0.2032" x2="-0.5334" y2="0.2032" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="0.2032" x2="-0.5334" y2="0.5842" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="-0.2032" x2="0.5334" y2="-0.2032" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="-0.2032" x2="0.5334" y2="-0.5842" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="-0.5842" x2="-0.5334" y2="-0.5842" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="-0.5842" x2="-0.5334" y2="-0.2032" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="-0.9906" x2="0.5334" y2="-0.9906" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="-0.9906" x2="0.5334" y2="-1.3208" width="0.1524" layer="29"/>
<wire x1="0.2032" y1="-1.3208" x2="-0.2032" y2="-1.3208" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="-1.3208" x2="-0.5334" y2="-0.9906" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="1.3208" x2="-0.2032" y2="1.3208" width="0.1524" layer="29"/>
<wire x1="-0.2032" y1="1.3208" x2="-0.2032" y2="-1.3208" width="0.1524" layer="29"/>
<wire x1="-0.2032" y1="-1.3208" x2="-0.5334" y2="-1.3208" width="0.1524" layer="29"/>
<wire x1="-0.5334" y1="-0.9906" x2="-0.5334" y2="0.9906" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="0.9906" x2="0.5334" y2="-0.9906" width="0.1524" layer="29"/>
<wire x1="0.5334" y1="-1.3208" x2="0.2032" y2="-1.3208" width="0.1524" layer="29"/>
<wire x1="0.2032" y1="-1.3208" x2="0.2032" y2="1.3208" width="0.1524" layer="29"/>
<wire x1="0.635" y1="-1.5494" x2="1.0668" y2="-1.5494" width="0.1524" layer="51"/>
<wire x1="-1.0668" y1="-1.5494" x2="-0.635" y2="-1.5494" width="0.1524" layer="51"/>
<wire x1="1.0668" y1="1.5494" x2="0.635" y2="1.5494" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.5494" x2="-1.0668" y2="1.5494" width="0.1524" layer="51"/>
<wire x1="-1.7018" y1="0.9144" x2="-1.7018" y2="1.0668" width="0" layer="51" curve="-208"/>
<wire x1="-1.0668" y1="-1.5494" x2="1.0668" y2="-1.5494" width="0.1524" layer="25"/>
<wire x1="1.0668" y1="-1.5494" x2="1.0668" y2="1.5494" width="0.1524" layer="25"/>
<wire x1="1.0668" y1="1.5494" x2="0.3048" y2="1.5494" width="0.1524" layer="25"/>
<wire x1="0.3048" y1="1.5494" x2="-0.3048" y2="1.5494" width="0.1524" layer="25"/>
<wire x1="-0.3048" y1="1.5494" x2="-1.0668" y2="1.5494" width="0.1524" layer="25"/>
<wire x1="-1.0668" y1="1.5494" x2="-1.0668" y2="-1.5494" width="0.1524" layer="25"/>
<wire x1="-1.6256" y1="0.9906" x2="-1.778" y2="0.9906" width="0.1524" layer="25" curve="-180"/>
<wire x1="-1.778" y1="0.9906" x2="-1.6256" y2="0.9906" width="0.1524" layer="25" curve="-180"/>
<wire x1="0.3048" y1="1.5494" x2="-0.3048" y2="1.5494" width="0" layer="25" curve="-180"/>
<text x="-2.8702" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.159" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.159" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762"/>
<pad name="2" x="2.5" y="0" drill="0.762"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="1.473" x2="1.498" y2="1.473" width="0.1524" layer="21"/>
<wire x1="-1.473" y1="-1.473" x2="1.498" y2="-1.473" width="0.1524" layer="21"/>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.667" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.667" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="AXIAL-0.3EZ">
<description>This is the "EZ" version of the standard .3" spaced resistor package.&lt;br&gt;
It has a reduced top mask to make it harder to install upside-down.</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="-3.81" y="0" radius="0.508" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.523634375" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="3.81" y="0" radius="1.04726875" width="0" layer="30"/>
</package>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="0.508" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="0.254" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL2">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-LARGE">
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-4.826" y="0" drill="0.9" diameter="1.905"/>
<pad name="2" x="4.572" y="0" drill="0.9" diameter="1.905"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="GRM43D">
<wire x1="2.25" y1="1.6" x2="1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.3" y1="1.8" x2="2.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.8" x2="2.3" y2="-1.8" width="0.127" layer="21"/>
<smd name="A" x="1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<smd name="C" x="-1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<text x="-2" y="2" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-2" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.2" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="2.2" y2="1.6" layer="51"/>
</package>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.5588" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-5MM">
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.5" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="AXIAL-5MM">
<wire x1="-1.14" y1="0.762" x2="1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0.762" x2="1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="-0.762" x2="-1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="-0.762" x2="-1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.394" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.394" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.5" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.5" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.17" size="0.4" layer="25">&gt;Name</text>
<text x="-1.032" y="-0.208" size="0.4" layer="21" ratio="15">&gt;Value</text>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-0.8" y="0.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.7" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CTZ3">
<description>CTZ3 Series land pattern for variable capacitor - CTZ3E-50C-W1-PF</description>
<wire x1="-1.6" y1="1.4" x2="-1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.25" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.4" x2="-1" y2="2.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.2" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.05" y1="2.25" x2="-1.7" y2="1.45" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.45" x2="-1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.35" x2="-1.05" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.05" y1="2.25" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.7" y1="-2.35" x2="1.05" y2="-2.35" width="0.127" layer="21"/>
<smd name="+" x="0" y="2.05" dx="1.5" dy="1.2" layer="1"/>
<smd name="-" x="0" y="-2.05" dx="1.5" dy="1.2" layer="1"/>
<text x="-2" y="3" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2" y="-3.4" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="CAP-PTH-SMALLEZ">
<description>This is the "EZ" version of the .1" spaced ceramic thru-hole cap.&lt;br&gt;
It has reduced top mask to make it harder to put the component on the wrong side of the board.</description>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651" stop="no"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651" stop="no"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
<circle x="0" y="0" radius="0.898025" width="0" layer="30"/>
<circle x="2.54" y="0" radius="0.915809375" width="0" layer="30"/>
<circle x="0" y="0" radius="0.40160625" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.40160625" width="0" layer="29"/>
</package>
<package name="EIA3216">
<wire x1="-1" y1="-1.2" x2="-2.5" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.2" x2="-2.5" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.2" x2="-1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.2" x2="2.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-1.2" x2="2.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-0.8" x2="2.5" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.8" x2="2.1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="1.2" x2="1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="0.381" y1="1.016" x2="0.381" y2="-1.016" width="0.127" layer="21"/>
<smd name="C" x="-1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="A" x="1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<text x="-2.54" y="1.381" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.408" y="1.332" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="EIA3528">
<wire x1="-0.9" y1="-1.6" x2="-2.6" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-1.6" x2="-2.6" y2="1.55" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="1.55" x2="-0.9" y2="1.55" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.55" x2="2.2" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-1.55" x2="2.6" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-1.2" x2="2.6" y2="1.25" width="0.2032" layer="21"/>
<wire x1="2.6" y1="1.25" x2="2.2" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="0.609" y1="1.311" x2="0.609" y2="-1.286" width="0.2032" layer="21" style="longdash"/>
<smd name="C" x="-1.65" y="0" dx="2.5" dy="1.2" layer="1" rot="R90"/>
<smd name="A" x="1.65" y="0" dx="2.5" dy="1.2" layer="1" rot="R90"/>
<text x="-2.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.24" y="-1.37" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="CPOL-RADIAL-100UF-25V">
<wire x1="-0.635" y1="1.27" x2="-1.905" y2="1.27" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.25" width="0.2032" layer="21"/>
<pad name="2" x="-1.27" y="0" drill="0.7" diameter="1.651"/>
<pad name="1" x="1.27" y="0" drill="0.7" diameter="1.651" shape="square"/>
<text x="-1.905" y="-4.318" size="0.8128" layer="27">&gt;Value</text>
<text x="-0.762" y="1.651" size="0.4064" layer="25">&gt;Name</text>
</package>
<package name="CPOL-RADIAL-10UF-25V">
<wire x1="-0.762" y1="1.397" x2="-1.778" y2="1.397" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2.5" width="0.2032" layer="21"/>
<pad name="1" x="1.27" y="0" drill="0.7" diameter="1.651" shape="square"/>
<pad name="2" x="-1.27" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.889" y="1.524" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.905" y="-3.683" size="0.8128" layer="27">&gt;Value</text>
</package>
<package name="EIA7343">
<wire x1="-5" y1="2.5" x2="-2" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="2.5" x2="-5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.5" x2="-2" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="2" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="-1.5" x2="4" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-2.5" x2="2" y2="-2.5" width="0.2032" layer="21"/>
<smd name="C" x="-3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<smd name="A" x="3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.27" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="PANASONIC_G">
<description>&lt;b&gt;Panasonic Aluminium Electrolytic Capacitor VS-Serie Package G&lt;/b&gt;</description>
<wire x1="-5.1" y1="5.1" x2="2.8" y2="5.1" width="0.1016" layer="51"/>
<wire x1="2.8" y1="5.1" x2="5.1" y2="2.8" width="0.1016" layer="51"/>
<wire x1="5.1" y1="2.8" x2="5.1" y2="-2.8" width="0.1016" layer="51"/>
<wire x1="5.1" y1="-2.8" x2="2.8" y2="-5.1" width="0.1016" layer="51"/>
<wire x1="2.8" y1="-5.1" x2="-5.1" y2="-5.1" width="0.1016" layer="51"/>
<wire x1="-5.1" y1="-5.1" x2="-5.1" y2="5.1" width="0.1016" layer="51"/>
<wire x1="-5.1" y1="1" x2="-5.1" y2="5.1" width="0.2032" layer="21"/>
<wire x1="-5.1" y1="5.1" x2="2.8" y2="5.1" width="0.2032" layer="21"/>
<wire x1="2.8" y1="5.1" x2="5.1" y2="2.8" width="0.2032" layer="21"/>
<wire x1="5.1" y1="2.8" x2="5.1" y2="1" width="0.2032" layer="21"/>
<wire x1="5.1" y1="-1" x2="5.1" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.1" y1="-2.8" x2="2.8" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-5.1" x2="-5.1" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-5.1" y1="-5.1" x2="-5.1" y2="-1" width="0.2032" layer="21"/>
<wire x1="-4.85" y1="-1" x2="4.85" y2="-1" width="0.2032" layer="21" curve="156.699401" cap="flat"/>
<wire x1="-4.85" y1="1" x2="4.85" y2="1" width="0.2032" layer="21" curve="-156.699401" cap="flat"/>
<wire x1="-3.25" y1="3.7" x2="-3.25" y2="-3.65" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="4.95" width="0.1016" layer="51"/>
<smd name="-" x="-4.25" y="0" dx="3.9" dy="1.6" layer="1"/>
<smd name="+" x="4.25" y="0" dx="3.9" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-5.85" y1="-0.45" x2="-4.9" y2="0.45" layer="51"/>
<rectangle x1="4.9" y1="-0.45" x2="5.85" y2="0.45" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-3.3" y="3.6"/>
<vertex x="-4.05" y="2.75"/>
<vertex x="-4.65" y="1.55"/>
<vertex x="-4.85" y="0.45"/>
<vertex x="-4.85" y="-0.45"/>
<vertex x="-4.65" y="-1.55"/>
<vertex x="-4.05" y="-2.75"/>
<vertex x="-3.3" y="-3.6"/>
<vertex x="-3.3" y="3.55"/>
</polygon>
</package>
<package name="PANASONIC_E">
<description>&lt;b&gt;Panasonic Aluminium Electrolytic Capacitor VS-Serie Package E&lt;/b&gt;</description>
<wire x1="-4.1" y1="4.1" x2="1.8" y2="4.1" width="0.1016" layer="51"/>
<wire x1="1.8" y1="4.1" x2="4.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="1.8" x2="4.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="-1.8" x2="1.8" y2="-4.1" width="0.1016" layer="51"/>
<wire x1="1.8" y1="-4.1" x2="-4.1" y2="-4.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="4.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="0.9" x2="-4.1" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="4.1" x2="1.8" y2="4.1" width="0.2032" layer="21"/>
<wire x1="1.8" y1="4.1" x2="4.1" y2="1.8" width="0.2032" layer="21"/>
<wire x1="4.1" y1="1.8" x2="4.1" y2="0.9" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-0.9" x2="4.1" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-1.8" x2="1.8" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="1.8" y1="-4.1" x2="-4.1" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="3.25" x2="-2.2" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.85" y1="0.9" x2="3.85" y2="0.9" width="0.2032" layer="21" curve="-153.684915" cap="flat"/>
<wire x1="-3.85" y1="-0.9" x2="3.85" y2="-0.9" width="0.2032" layer="21" curve="153.684915" cap="flat"/>
<circle x="0" y="0" radius="3.95" width="0.1016" layer="51"/>
<smd name="-" x="-3" y="0" dx="3.8" dy="1.4" layer="1"/>
<smd name="+" x="3" y="0" dx="3.8" dy="1.4" layer="1"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-4.5" y1="-0.35" x2="-3.8" y2="0.35" layer="51"/>
<rectangle x1="3.8" y1="-0.35" x2="4.5" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.25" y="3.2"/>
<vertex x="-3" y="2.5"/>
<vertex x="-3.6" y="1.5"/>
<vertex x="-3.85" y="0.65"/>
<vertex x="-3.85" y="-0.65"/>
<vertex x="-3.55" y="-1.6"/>
<vertex x="-2.95" y="-2.55"/>
<vertex x="-2.25" y="-3.2"/>
<vertex x="-2.25" y="3.15"/>
</polygon>
</package>
<package name="PANASONIC_C">
<description>&lt;b&gt;Panasonic Aluminium Electrolytic Capacitor VS-Serie Package E&lt;/b&gt;</description>
<wire x1="-2.6" y1="2.45" x2="1.6" y2="2.45" width="0.2032" layer="21"/>
<wire x1="1.6" y1="2.45" x2="2.7" y2="1.35" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.75" x2="1.6" y2="-2.85" width="0.2032" layer="21"/>
<wire x1="1.6" y1="-2.85" x2="-2.6" y2="-2.85" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="2.45" x2="1.6" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.6" y1="2.45" x2="2.7" y2="1.35" width="0.1016" layer="51"/>
<wire x1="2.7" y1="-1.75" x2="1.6" y2="-2.85" width="0.1016" layer="51"/>
<wire x1="1.6" y1="-2.85" x2="-2.6" y2="-2.85" width="0.1016" layer="51"/>
<wire x1="-2.6" y1="2.45" x2="-2.6" y2="0.35" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-2.85" x2="-2.6" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="2.7" y1="1.35" x2="2.7" y2="0.35" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.75" x2="2.7" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="2.45" x2="-2.6" y2="-2.85" width="0.1016" layer="51"/>
<wire x1="2.7" y1="1.35" x2="2.7" y2="-1.75" width="0.1016" layer="51"/>
<wire x1="-2.4" y1="0.35" x2="2.45" y2="0.3" width="0.2032" layer="21" curve="-156.699401"/>
<wire x1="2.5" y1="-0.7" x2="-2.4" y2="-0.75" width="0.2032" layer="21" curve="-154.694887"/>
<circle x="0.05" y="-0.2" radius="2.5004" width="0.1016" layer="51"/>
<smd name="-" x="-1.8" y="-0.2" dx="2.2" dy="0.65" layer="1"/>
<smd name="+" x="1.9" y="-0.2" dx="2.2" dy="0.65" layer="1"/>
<text x="-2.6" y="2.7" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.6" y="-3.45" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="NIPPON_F80">
<wire x1="-3.3" y1="3.3" x2="1.7" y2="3.3" width="0.2032" layer="21"/>
<wire x1="1.7" y1="3.3" x2="3.3" y2="2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2" x2="1.7" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="1.7" y1="-3.3" x2="-3.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="1.7" y2="3.3" width="0.1016" layer="51"/>
<wire x1="1.7" y1="3.3" x2="3.3" y2="2" width="0.1016" layer="51"/>
<wire x1="3.3" y1="-2" x2="1.7" y2="-3.3" width="0.1016" layer="51"/>
<wire x1="1.7" y1="-3.3" x2="-3.3" y2="-3.3" width="0.1016" layer="51"/>
<wire x1="-3.3" y1="3.3" x2="-3.3" y2="0.685" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-0.685" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2" x2="3.3" y2="0.685" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2" x2="3.3" y2="-0.685" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="-3.3" y2="-3.3" width="0.1016" layer="51"/>
<wire x1="3.3" y1="2" x2="3.3" y2="-2" width="0.1016" layer="51"/>
<wire x1="-3.1" y1="0.685" x2="3.1" y2="0.685" width="0.2032" layer="21" curve="-156.500033"/>
<wire x1="3.1" y1="-0.685" x2="-3.1" y2="-0.685" width="0.2032" layer="21" curve="-154.748326"/>
<circle x="0" y="0" radius="3.15" width="0.1016" layer="51"/>
<smd name="-" x="-2.4" y="0" dx="2.95" dy="1" layer="1"/>
<smd name="+" x="2.4" y="0" dx="2.95" dy="1" layer="1"/>
<text x="-3.2" y="3.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.85" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="PANASONIC_D">
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="0.95" x2="-3.25" y2="3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="0.95" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-0.95" x2="3.25" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="2.95" y1="0.95" x2="-2.95" y2="0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="+" x="2.4" y="0" dx="3" dy="1.4" layer="1"/>
<smd name="-" x="-2.4" y="0" dx="3" dy="1.4" layer="1"/>
<text x="-1.75" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-1.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="CPOL-RADIAL-1000UF-63V">
<wire x1="-3.175" y1="1.905" x2="-4.445" y2="1.905" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="8.001" width="0.2032" layer="21"/>
<pad name="2" x="-3.81" y="0" drill="1.016" diameter="1.6764"/>
<pad name="1" x="3.81" y="0" drill="1.016" diameter="1.651" shape="square"/>
<text x="-2.54" y="8.89" size="0.8128" layer="27">&gt;Value</text>
<text x="-2.54" y="10.16" size="0.8128" layer="25">&gt;Name</text>
</package>
<package name="CPOL-RADIAL-1000UF-25V">
<wire x1="-1.905" y1="1.27" x2="-3.175" y2="1.27" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="5.461" width="0.2032" layer="21"/>
<pad name="2" x="-2.54" y="0" drill="0.7" diameter="1.651"/>
<pad name="1" x="2.54" y="0" drill="0.7" diameter="1.651" shape="square"/>
<text x="-1.905" y="-4.318" size="0.8128" layer="27">&gt;Value</text>
<text x="-0.762" y="2.921" size="0.4064" layer="25">&gt;Name</text>
</package>
<package name="VISHAY_C">
<wire x1="0" y1="1.27" x2="0" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.0574" y1="4.2926" x2="-2.0574" y2="-4.2926" width="0.127" layer="21"/>
<wire x1="-2.0574" y1="-4.2926" x2="2.0574" y2="-4.2926" width="0.127" layer="21"/>
<wire x1="2.0574" y1="-4.2926" x2="2.0574" y2="4.2926" width="0.127" layer="21"/>
<wire x1="2.0574" y1="4.2926" x2="-2.0574" y2="4.2926" width="0.127" layer="21"/>
<smd name="+" x="0" y="3.048" dx="3.556" dy="1.778" layer="1"/>
<smd name="-" x="0" y="-3.048" dx="3.556" dy="1.778" layer="1"/>
<text x="-1.905" y="4.445" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="PANASONIC_H13">
<wire x1="-6.75" y1="6.75" x2="4" y2="6.75" width="0.1016" layer="51"/>
<wire x1="4" y1="6.75" x2="6.75" y2="4" width="0.1016" layer="51"/>
<wire x1="6.75" y1="4" x2="6.75" y2="-4" width="0.1016" layer="51"/>
<wire x1="6.75" y1="-4" x2="4" y2="-6.75" width="0.1016" layer="51"/>
<wire x1="4" y1="-6.75" x2="-6.75" y2="-6.75" width="0.1016" layer="51"/>
<wire x1="-6.75" y1="-6.75" x2="-6.75" y2="6.75" width="0.1016" layer="51"/>
<wire x1="-6.75" y1="1" x2="-6.75" y2="6.75" width="0.2032" layer="21"/>
<wire x1="-6.75" y1="6.75" x2="4" y2="6.75" width="0.2032" layer="21"/>
<wire x1="4" y1="6.75" x2="6.75" y2="4" width="0.2032" layer="21"/>
<wire x1="6.75" y1="4" x2="6.75" y2="1" width="0.2032" layer="21"/>
<wire x1="6.75" y1="-1" x2="6.75" y2="-4" width="0.2032" layer="21"/>
<wire x1="6.75" y1="-4" x2="4" y2="-6.75" width="0.2032" layer="21"/>
<wire x1="4" y1="-6.75" x2="-6.75" y2="-6.75" width="0.2032" layer="21"/>
<wire x1="-6.75" y1="-6.75" x2="-6.75" y2="-1" width="0.2032" layer="21"/>
<wire x1="-6.55" y1="-1.2" x2="6.45" y2="-1.2" width="0.2032" layer="21" curve="156.692742" cap="flat"/>
<wire x1="-6.55" y1="1.2" x2="6.55" y2="1.2" width="0.2032" layer="21" curve="-156.697982" cap="flat"/>
<wire x1="-5" y1="4.25" x2="-4.95" y2="-4.35" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="6.6" width="0.1016" layer="51"/>
<smd name="-" x="-4.7" y="0" dx="5" dy="1.6" layer="1"/>
<smd name="+" x="4.7" y="0" dx="5" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-7.55" y1="-0.45" x2="-6.6" y2="0.45" layer="51"/>
<rectangle x1="6.6" y1="-0.45" x2="7.55" y2="0.45" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-5" y="4.2"/>
<vertex x="-5.75" y="3.15"/>
<vertex x="-6.25" y="2.05"/>
<vertex x="-6.55" y="0.45"/>
<vertex x="-6.55" y="-0.45"/>
<vertex x="-6.35" y="-1.65"/>
<vertex x="-5.75" y="-3.25"/>
<vertex x="-5" y="-4.2"/>
</polygon>
</package>
<package name="EIA6032">
<wire x1="3.2" y1="-1.6" x2="3.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.8" y1="-1.6" x2="3.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="3.2" y1="1.6" x2="-2.8" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.8" y1="1.6" x2="-3.4" y2="1" width="0.127" layer="21"/>
<wire x1="-3.4" y1="1" x2="-3.4" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.8" y1="-1.6" x2="-3.4" y2="-1" width="0.127" layer="21"/>
<smd name="P$1" x="-2.3" y="0" dx="1.5" dy="2.4" layer="1"/>
<smd name="P$2" x="2.3" y="0" dx="1.5" dy="2.4" layer="1"/>
</package>
<package name="EN_J2">
<description>Type J2 package for SMD supercap PRT-10317 (p# EEC-EN0F204J2)</description>
<wire x1="-2.5" y1="-3.5" x2="2.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="3.5" x2="2.1" y2="3.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="3.5" x2="-2.5" y2="3.1" width="0.127" layer="51"/>
<wire x1="-2.5" y1="3.1" x2="-2.5" y2="2.3" width="0.127" layer="51"/>
<wire x1="2.1" y1="3.5" x2="2.5" y2="3.1" width="0.127" layer="51"/>
<wire x1="2.5" y1="3.1" x2="2.5" y2="2.3" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-3.5" x2="-2.5" y2="-2.3" width="0.127" layer="51"/>
<wire x1="2.5" y1="-3.5" x2="2.5" y2="-2.3" width="0.127" layer="51"/>
<wire x1="-2.5908" y1="-2.413" x2="-2.5654" y2="2.4384" width="0.127" layer="21" curve="-91.212564"/>
<wire x1="2.5908" y1="-2.413" x2="2.5654" y2="2.4384" width="0.127" layer="21" curve="86.79344"/>
<wire x1="1.7272" y1="-1.27" x2="1.7272" y2="-2.0828" width="0.127" layer="21"/>
<wire x1="1.3462" y1="-1.6764" x2="2.159" y2="-1.6764" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.4" width="0.127" layer="51"/>
<smd name="-" x="0" y="2.8" dx="5" dy="2.4" layer="1"/>
<smd name="+" x="0" y="-3.2" dx="5" dy="1.6" layer="1"/>
<text x="-2.28" y="0.66" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.31" y="-1.21" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="EIA3528-KIT">
<description>&lt;h3&gt;EIA3528-KIT&lt;/h3&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has longer pads to make hand soldering easier.&lt;br&gt;</description>
<wire x1="-0.9" y1="-1.6" x2="-3.1" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.6" x2="-3.1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="1.55" x2="-0.9" y2="1.55" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.55" x2="2.7" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.55" x2="3.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.1" y1="-1.2" x2="3.1" y2="1.25" width="0.2032" layer="21"/>
<wire x1="3.1" y1="1.25" x2="2.7" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.7" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="0.609" y1="1.311" x2="0.609" y2="-1.286" width="0.4" layer="21" style="longdash"/>
<smd name="C" x="-1.9" y="0" dx="1.7" dy="2.5" layer="1"/>
<smd name="A" x="1.9" y="0" dx="1.7" dy="2.5" layer="1"/>
<text x="-2.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.24" y="-1.37" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="EIA3216-KIT">
<description>&lt;h3&gt;EIA3216-KIT&lt;/h3&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has longer pads to make hand soldering easier.&lt;br&gt;</description>
<wire x1="-1" y1="-1.2" x2="-3" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.2" x2="-3" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.2" x2="-1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.2" x2="2.6" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-1.2" x2="3" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="3" y1="-0.8" x2="3" y2="0.8" width="0.2032" layer="21"/>
<wire x1="3" y1="0.8" x2="2.6" y2="1.2" width="0.2032" layer="21"/>
<wire x1="2.6" y1="1.2" x2="1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="0.381" y1="1.016" x2="0.381" y2="-1.016" width="0.127" layer="21"/>
<smd name="C" x="-1.65" y="0" dx="1.9" dy="1.6" layer="1"/>
<smd name="A" x="1.65" y="0" dx="1.9" dy="1.6" layer="1"/>
<text x="-2.54" y="1.381" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.408" y="1.332" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="DFN6">
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<circle x="-1.1" y="1.05" radius="0.1581125" width="0.127" layer="21"/>
<text x="-1.5" y="1.75" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.35" size="0.6096" layer="27">&gt;VALUE</text>
<smd name="GND" x="-1.5" y="0" dx="1" dy="0.45" layer="1" roundness="20"/>
<smd name="VDD" x="1.5" y="0" dx="1" dy="0.45" layer="1" roundness="20"/>
<smd name="SCL" x="1.5" y="1" dx="1" dy="0.45" layer="1" roundness="20"/>
<smd name="DNC2" x="1.5" y="-1" dx="1" dy="0.45" layer="1" roundness="20"/>
<smd name="DNC" x="-1.5" y="-1" dx="1" dy="0.45" layer="1" roundness="20"/>
<smd name="SDA" x="-1.5" y="1" dx="1" dy="0.45" layer="1" roundness="20"/>
<smd name="T_GND" x="0" y="0" dx="1" dy="2" layer="1" roundness="20"/>
</package>
<package name="BLE112-A">
<description>Bluetooth Low Energy single-mode module - Bluetooth 4.0, chip antenna</description>
<wire x1="-6.05" y1="-9.05" x2="-6.05" y2="9.05" width="0.127" layer="21"/>
<wire x1="-6.05" y1="9.05" x2="6" y2="9.05" width="0.127" layer="21"/>
<wire x1="6" y1="9.05" x2="6" y2="-9.05" width="0.127" layer="21"/>
<wire x1="6" y1="-9.05" x2="-6.05" y2="-9.05" width="0.127" layer="21"/>
<smd name="AVDD1" x="-6.05" y="6.9" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="AVDD2" x="-6.05" y="5.65" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="DVDD" x="3.1" y="-9.05" dx="0.85" dy="3" layer="1" roundness="100" rot="R180"/>
<smd name="GND1" x="-6.05" y="8.15" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="GND2" x="-6.05" y="-3.1" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="GND3" x="4.35" y="-9.05" dx="0.85" dy="3" layer="1" roundness="100" rot="R180"/>
<smd name="GND4" x="6" y="3.15" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P0_0" x="6" y="0.65" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P0_1" x="6" y="-0.6" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P0_2" x="6" y="-1.85" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P0_3" x="6" y="-3.1" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P0_4" x="6" y="-4.35" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P0_5" x="6" y="-5.6" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P0_6" x="6" y="-6.85" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P0_7" x="1.85" y="-9.05" dx="0.85" dy="3" layer="1" roundness="100" rot="R180"/>
<smd name="P1_0" x="0.6" y="-9.05" dx="0.85" dy="3" layer="1" roundness="100" rot="R180"/>
<smd name="P1_1" x="-0.65" y="-9.05" dx="0.85" dy="3" layer="1" roundness="100" rot="R180"/>
<smd name="P1_2" x="-1.9" y="-9.05" dx="0.85" dy="3" layer="1" roundness="100" rot="R180"/>
<smd name="P1_3" x="-3.15" y="-9.05" dx="0.85" dy="3" layer="1" roundness="100" rot="R180"/>
<smd name="P1_4" x="-4.4" y="-9.05" dx="0.85" dy="3" layer="1" roundness="100" rot="R180"/>
<smd name="P1_5" x="-6.05" y="-6.85" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P1_6" x="-6.05" y="-0.6" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P1_7" x="-6.05" y="0.65" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P2_0" x="-6.05" y="1.9" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P2_1" x="-6.05" y="3.15" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="P2_2" x="-6.05" y="4.4" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="RESET" x="6" y="1.9" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="USB+" x="-6.05" y="-4.35" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="USB-" x="-6.05" y="-5.6" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<smd name="VDD_USB" x="-6.05" y="-1.85" dx="0.85" dy="3" layer="1" roundness="100" rot="R90"/>
<text x="-4.35" y="7.8" size="0.8128" layer="21" rot="SR0">1</text>
<text x="-4.35" y="-7.25" size="0.8128" layer="21" rot="SR0">13</text>
<text x="3" y="-7.25" size="0.8128" layer="21" rot="SR0">22</text>
<text x="3" y="2.75" size="0.8128" layer="21" rot="SR0">30</text>
<text x="-6" y="9.4" size="1.016" layer="25" rot="SR0">&gt;NAME</text>
<text x="0" y="9.4" size="1.016" layer="27" rot="SR0">&gt;VALUE</text>
</package>
<package name="RAK410">
<wire x1="-11.557" y1="-12.573" x2="-11.557" y2="-14.478" width="0.127" layer="51"/>
<wire x1="-11.557" y1="-14.478" x2="-11.303" y2="-14.478" width="0.127" layer="51"/>
<wire x1="11.557" y1="-12.573" x2="11.557" y2="-14.478" width="0.127" layer="51"/>
<wire x1="11.557" y1="-14.478" x2="11.303" y2="-14.478" width="0.127" layer="51"/>
<wire x1="-11.557" y1="9.906" x2="-11.557" y2="15.494" width="0.127" layer="51"/>
<wire x1="-11.557" y1="15.494" x2="11.557" y2="15.494" width="0.127" layer="51"/>
<wire x1="11.557" y1="15.494" x2="11.557" y2="9.906" width="0.127" layer="51"/>
<smd name="P$1" x="-11.557" y="9.144" dx="2.032" dy="1.524" layer="1"/>
<smd name="P$2" x="-11.557" y="7.239" dx="2.032" dy="1.524" layer="1"/>
<smd name="P$3" x="-11.557" y="5.334" dx="2.032" dy="1.524" layer="1"/>
<smd name="P$4" x="-11.557" y="3.429" dx="2.032" dy="1.524" layer="1"/>
<smd name="P$5" x="-11.557" y="1.524" dx="2.032" dy="1.524" layer="1"/>
<smd name="P$6" x="-11.557" y="-0.381" dx="2.032" dy="1.524" layer="1"/>
<smd name="P$7" x="-11.557" y="-2.286" dx="2.032" dy="1.524" layer="1"/>
<smd name="P$8" x="-11.557" y="-4.191" dx="2.032" dy="1.524" layer="1"/>
<smd name="P$9" x="-11.557" y="-6.096" dx="2.032" dy="1.524" layer="1"/>
<smd name="P$10" x="-11.557" y="-8.001" dx="2.032" dy="1.524" layer="1"/>
<smd name="P$11" x="-11.557" y="-9.906" dx="2.032" dy="1.524" layer="1"/>
<smd name="P$12" x="-11.557" y="-11.811" dx="2.032" dy="1.524" layer="1"/>
<smd name="P$13" x="-10.4648" y="-14.478" dx="2.032" dy="1.524" layer="1" rot="R90"/>
<smd name="P$14" x="-8.5598" y="-14.478" dx="2.032" dy="1.524" layer="1" rot="R90"/>
<smd name="P$15" x="-6.6548" y="-14.478" dx="2.032" dy="1.524" layer="1" rot="R90"/>
<smd name="P$16" x="-4.7498" y="-14.478" dx="2.032" dy="1.524" layer="1" rot="R90"/>
<smd name="P$17" x="-2.8448" y="-14.478" dx="2.032" dy="1.524" layer="1" rot="R90"/>
<smd name="P$18" x="-0.9398" y="-14.478" dx="2.032" dy="1.524" layer="1" rot="R90"/>
<smd name="P$19" x="0.9652" y="-14.478" dx="2.032" dy="1.524" layer="1" rot="R90"/>
<smd name="P$20" x="2.8702" y="-14.478" dx="2.032" dy="1.524" layer="1" rot="R90"/>
<smd name="P$21" x="4.7752" y="-14.478" dx="2.032" dy="1.524" layer="1" rot="R90"/>
<smd name="P$22" x="6.6802" y="-14.478" dx="2.032" dy="1.524" layer="1" rot="R90"/>
<smd name="P$23" x="8.5852" y="-14.478" dx="2.032" dy="1.524" layer="1" rot="R90"/>
<smd name="P$24" x="10.4902" y="-14.478" dx="2.032" dy="1.524" layer="1" rot="R90"/>
<smd name="P$25" x="11.557" y="-11.811" dx="2.032" dy="1.524" layer="1" rot="R180"/>
<smd name="P$26" x="11.557" y="-9.906" dx="2.032" dy="1.524" layer="1" rot="R180"/>
<smd name="P$27" x="11.557" y="-8.001" dx="2.032" dy="1.524" layer="1" rot="R180"/>
<smd name="P$28" x="11.557" y="-6.096" dx="2.032" dy="1.524" layer="1" rot="R180"/>
<smd name="P$29" x="11.557" y="-4.191" dx="2.032" dy="1.524" layer="1" rot="R180"/>
<smd name="P$30" x="11.557" y="-2.286" dx="2.032" dy="1.524" layer="1" rot="R180"/>
<smd name="P$31" x="11.557" y="-0.381" dx="2.032" dy="1.524" layer="1" rot="R180"/>
<smd name="P$32" x="11.557" y="1.524" dx="2.032" dy="1.524" layer="1" rot="R180"/>
<smd name="P$33" x="11.557" y="3.429" dx="2.032" dy="1.524" layer="1" rot="R180"/>
<smd name="P$34" x="11.557" y="5.334" dx="2.032" dy="1.524" layer="1" rot="R180"/>
<smd name="P$35" x="11.557" y="7.239" dx="2.032" dy="1.524" layer="1" rot="R180"/>
<smd name="P$36" x="11.557" y="9.144" dx="2.032" dy="1.524" layer="1" rot="R180"/>
<text x="-2.54" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SIM900">
<wire x1="-12" y1="-12" x2="12" y2="-12" width="0.127" layer="21"/>
<wire x1="12" y1="-12" x2="12" y2="12" width="0.127" layer="21"/>
<wire x1="12" y1="12" x2="-12" y2="12" width="0.127" layer="21"/>
<wire x1="-12" y1="12" x2="-12" y2="-12" width="0.127" layer="21"/>
<wire x1="-9" y1="8" x2="-8" y2="8.5" width="0.15" layer="21"/>
<wire x1="-8" y1="8.5" x2="-8" y2="7.5" width="0.15" layer="21"/>
<wire x1="-8" y1="7.5" x2="-9" y2="8" width="0.15" layer="21"/>
<wire x1="-13" y1="13" x2="13" y2="13" width="0.15" layer="39"/>
<wire x1="13" y1="13" x2="13" y2="-13" width="0.15" layer="39"/>
<wire x1="13" y1="-13" x2="-13" y2="-13" width="0.15" layer="39"/>
<wire x1="-13" y1="-13" x2="-13" y2="13" width="0.15" layer="39"/>
<smd name="9" x="-11.25" y="0" dx="2.5" dy="0.8" layer="1"/>
<smd name="8" x="-11.25" y="1" dx="2.5" dy="0.8" layer="1"/>
<smd name="7" x="-11.25" y="2" dx="2.5" dy="0.8" layer="1"/>
<smd name="6" x="-11.25" y="3" dx="2.5" dy="0.8" layer="1"/>
<smd name="5" x="-11.25" y="4" dx="2.5" dy="0.8" layer="1"/>
<smd name="4" x="-11.25" y="5" dx="2.5" dy="0.8" layer="1"/>
<smd name="3" x="-11.25" y="6" dx="2.5" dy="0.8" layer="1"/>
<smd name="2" x="-11.25" y="7" dx="2.5" dy="0.8" layer="1"/>
<smd name="1" x="-11.25" y="8" dx="2.5" dy="0.8" layer="1"/>
<smd name="10" x="-11.25" y="-1" dx="2.5" dy="0.8" layer="1"/>
<smd name="11" x="-11.25" y="-2" dx="2.5" dy="0.8" layer="1"/>
<smd name="12" x="-11.25" y="-3" dx="2.5" dy="0.8" layer="1"/>
<smd name="13" x="-11.25" y="-4" dx="2.5" dy="0.8" layer="1"/>
<smd name="14" x="-11.25" y="-5" dx="2.5" dy="0.8" layer="1"/>
<smd name="15" x="-11.25" y="-6" dx="2.5" dy="0.8" layer="1"/>
<smd name="16" x="-11.25" y="-7" dx="2.5" dy="0.8" layer="1"/>
<smd name="17" x="-11.25" y="-8" dx="2.5" dy="0.8" layer="1"/>
<smd name="26" x="0" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="25" x="-1" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="24" x="-2" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="23" x="-3" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="22" x="-4" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="21" x="-5" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="20" x="-6" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="19" x="-7" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="18" x="-8" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="27" x="1" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="28" x="2" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="29" x="3" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="30" x="4" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="31" x="5" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="32" x="6" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="33" x="7" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="34" x="8" y="-11.25" dx="2.5" dy="0.8" layer="1" rot="R90"/>
<smd name="43" x="11.25" y="0" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="42" x="11.25" y="-1" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="41" x="11.25" y="-2" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="40" x="11.25" y="-3" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="39" x="11.25" y="-4" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="38" x="11.25" y="-5" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="37" x="11.25" y="-6" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="36" x="11.25" y="-7" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="35" x="11.25" y="-8" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="44" x="11.25" y="1" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="45" x="11.25" y="2" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="46" x="11.25" y="3" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="47" x="11.25" y="4" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="48" x="11.25" y="5" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="49" x="11.25" y="6" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="50" x="11.25" y="7" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="51" x="11.25" y="8" dx="2.5" dy="0.8" layer="1" rot="R180"/>
<smd name="60" x="0" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="59" x="1" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="58" x="2" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="57" x="3" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="56" x="4" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="55" x="5" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="54" x="6" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="53" x="7" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="52" x="8" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="61" x="-1" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="62" x="-2" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="63" x="-3" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="64" x="-4" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="65" x="-5" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="66" x="-6" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="67" x="-7" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="68" x="-8" y="11.25" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<text x="-8" y="13.5" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="1.5" y="13.5" size="1.27" layer="25" font="vector">&gt;VALUE</text>
</package>
<package name="QFN24_4X4">
<smd name="1" x="-1.8923" y="1.250009375" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="2" x="-1.8923" y="0.7500125" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="3" x="-1.8923" y="0.2499875" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="4" x="-1.8923" y="-0.2499875" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="5" x="-1.8923" y="-0.7500125" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="6" x="-1.8923" y="-1.250009375" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="7" x="-1.250009375" y="-1.8923" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="8" x="-0.7500125" y="-1.8923" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="9" x="-0.2499875" y="-1.8923" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="10" x="0.2499875" y="-1.8923" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="11" x="0.7500125" y="-1.8923" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="12" x="1.250009375" y="-1.8923" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="13" x="1.8923" y="-1.250009375" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="14" x="1.8923" y="-0.7500125" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="15" x="1.8923" y="-0.2499875" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="16" x="1.8923" y="0.2499875" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="17" x="1.8923" y="0.7500125" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="18" x="1.8923" y="1.250009375" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="19" x="1.250009375" y="1.8923" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="20" x="0.7500125" y="1.8923" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="21" x="0.2499875" y="1.8923" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="22" x="-0.2499875" y="1.8923" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="23" x="-0.7500125" y="1.8923" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="24" x="-1.250009375" y="1.8923" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="25" x="0" y="0" dx="2.794" dy="2.794" layer="1"/>
<wire x1="1.9812" y1="1.7272" x2="1.9812" y2="1.9812" width="0.1524" layer="51"/>
<wire x1="1.7272" y1="-1.9812" x2="1.9812" y2="-1.9812" width="0.1524" layer="51"/>
<wire x1="-1.9812" y1="-1.7272" x2="-1.9812" y2="-1.9812" width="0.1524" layer="51"/>
<wire x1="-1.7272" y1="1.9812" x2="-1.9812" y2="1.9812" width="0.1524" layer="51"/>
<wire x1="-1.9812" y1="-1.9812" x2="-1.7272" y2="-1.9812" width="0.1524" layer="51"/>
<wire x1="1.9812" y1="-1.9812" x2="1.9812" y2="-1.7272" width="0.1524" layer="51"/>
<wire x1="1.9812" y1="1.9812" x2="1.7272" y2="1.9812" width="0.1524" layer="51"/>
<wire x1="-1.9812" y1="1.9812" x2="-1.9812" y2="1.7272" width="0.1524" layer="51"/>
<wire x1="0.0508" y1="-2.54" x2="0.0508" y2="-2.794" width="0.1524" layer="51"/>
<wire x1="0.0508" y1="-2.794" x2="0.4318" y2="-2.794" width="0.1524" layer="51"/>
<wire x1="0.4318" y1="-2.794" x2="0.4318" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="0.4318" y1="-2.54" x2="0.0508" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="2.54" x2="0.5588" y2="2.794" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="2.794" x2="0.9398" y2="2.794" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="2.794" x2="0.9398" y2="2.54" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="2.54" x2="0.5588" y2="2.54" width="0.1524" layer="51"/>
<text x="-3.5052" y="1.2446" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<wire x1="-1.9812" y1="0.7112" x2="-0.7112" y2="1.9812" width="0.1524" layer="25"/>
<wire x1="1.397" y1="1.9812" x2="1.0922" y2="1.9812" width="0.1524" layer="25"/>
<wire x1="0.9144" y1="1.9812" x2="0.6096" y2="1.9812" width="0.1524" layer="25"/>
<wire x1="0.4064" y1="1.9812" x2="0.1016" y2="1.9812" width="0.1524" layer="25"/>
<wire x1="-0.1016" y1="1.9812" x2="-0.4064" y2="1.9812" width="0.1524" layer="25"/>
<wire x1="-0.6096" y1="1.9812" x2="-0.9144" y2="1.9812" width="0.1524" layer="25"/>
<wire x1="-1.0922" y1="1.9812" x2="-1.397" y2="1.9812" width="0.1524" layer="25"/>
<wire x1="-1.9812" y1="1.397" x2="-1.9812" y2="1.0922" width="0.1524" layer="25"/>
<wire x1="-1.9812" y1="0.9144" x2="-1.9812" y2="0.6096" width="0.1524" layer="25"/>
<wire x1="-1.9812" y1="0.4064" x2="-1.9812" y2="0.1016" width="0.1524" layer="25"/>
<wire x1="-1.9812" y1="-0.1016" x2="-1.9812" y2="-0.4064" width="0.1524" layer="25"/>
<wire x1="-1.9812" y1="-0.6096" x2="-1.9812" y2="-0.9144" width="0.1524" layer="25"/>
<wire x1="-1.9812" y1="-1.0922" x2="-1.9812" y2="-1.397" width="0.1524" layer="25"/>
<wire x1="-1.397" y1="-1.9812" x2="-1.0922" y2="-1.9812" width="0.1524" layer="25"/>
<wire x1="-0.9144" y1="-1.9812" x2="-0.6096" y2="-1.9812" width="0.1524" layer="25"/>
<wire x1="-0.4064" y1="-1.9812" x2="-0.1016" y2="-1.9812" width="0.1524" layer="25"/>
<wire x1="0.1016" y1="-1.9812" x2="0.4064" y2="-1.9812" width="0.1524" layer="25"/>
<wire x1="0.6096" y1="-1.9812" x2="0.9144" y2="-1.9812" width="0.1524" layer="25"/>
<wire x1="1.0922" y1="-1.9812" x2="1.397" y2="-1.9812" width="0.1524" layer="25"/>
<wire x1="1.9812" y1="-1.397" x2="1.9812" y2="-1.0922" width="0.1524" layer="25"/>
<wire x1="1.9812" y1="-0.9144" x2="1.9812" y2="-0.6096" width="0.1524" layer="25"/>
<wire x1="1.9812" y1="-0.4064" x2="1.9812" y2="-0.1016" width="0.1524" layer="25"/>
<wire x1="1.9812" y1="0.1016" x2="1.9812" y2="0.4064" width="0.1524" layer="25"/>
<wire x1="1.9812" y1="0.6096" x2="1.9812" y2="0.9144" width="0.1524" layer="25"/>
<wire x1="1.9812" y1="1.0922" x2="1.9812" y2="1.397" width="0.1524" layer="25"/>
<wire x1="-1.9812" y1="-1.9812" x2="1.9812" y2="-1.9812" width="0.1524" layer="25"/>
<wire x1="1.9812" y1="-1.9812" x2="1.9812" y2="1.9812" width="0.1524" layer="25"/>
<wire x1="1.9812" y1="1.9812" x2="-1.9812" y2="1.9812" width="0.1524" layer="25"/>
<wire x1="-1.9812" y1="1.9812" x2="-1.9812" y2="-1.9812" width="0.1524" layer="25"/>
<text x="-3.5052" y="1.2446" size="1.27" layer="25" ratio="6" rot="SR0">*</text>
<text x="-2.8702" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
</package>
<package name="TQFP100">
<description>&lt;b&gt;100-lead Thin Quad Flat Pack Package Outline&lt;/b&gt;</description>
<wire x1="-7" y1="6.25" x2="-6.25" y2="7" width="0.254" layer="21"/>
<wire x1="-6.25" y1="7" x2="6.75" y2="7" width="0.254" layer="21"/>
<wire x1="6.75" y1="7" x2="7" y2="6.75" width="0.254" layer="21"/>
<wire x1="7" y1="6.75" x2="7" y2="-6.75" width="0.254" layer="21"/>
<wire x1="7" y1="-6.75" x2="6.75" y2="-7" width="0.254" layer="21"/>
<wire x1="6.75" y1="-7" x2="-6.75" y2="-7" width="0.254" layer="21"/>
<wire x1="-6.75" y1="-7" x2="-7" y2="-6.75" width="0.254" layer="21"/>
<wire x1="-7" y1="-6.75" x2="-7" y2="6.25" width="0.254" layer="21"/>
<circle x="-6" y="6" radius="0.2499" width="0.254" layer="21"/>
<smd name="1" x="-8" y="6" dx="1.5" dy="0.3" layer="1"/>
<smd name="2" x="-8" y="5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="3" x="-8" y="5" dx="1.5" dy="0.3" layer="1"/>
<smd name="4" x="-8" y="4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="5" x="-8" y="4" dx="1.5" dy="0.3" layer="1"/>
<smd name="6" x="-8" y="3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="7" x="-8" y="3" dx="1.5" dy="0.3" layer="1"/>
<smd name="8" x="-8" y="2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="9" x="-8" y="2" dx="1.5" dy="0.3" layer="1"/>
<smd name="10" x="-8" y="1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="11" x="-8" y="1" dx="1.5" dy="0.3" layer="1"/>
<smd name="12" x="-8" y="0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="13" x="-8" y="0" dx="1.5" dy="0.3" layer="1"/>
<smd name="14" x="-8" y="-0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="15" x="-8" y="-1" dx="1.5" dy="0.3" layer="1"/>
<smd name="16" x="-8" y="-1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="17" x="-8" y="-2" dx="1.5" dy="0.3" layer="1"/>
<smd name="18" x="-8" y="-2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="19" x="-8" y="-3" dx="1.5" dy="0.3" layer="1"/>
<smd name="20" x="-8" y="-3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="21" x="-8" y="-4" dx="1.5" dy="0.3" layer="1"/>
<smd name="22" x="-8" y="-4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="23" x="-8" y="-5" dx="1.5" dy="0.3" layer="1"/>
<smd name="24" x="-8" y="-5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="25" x="-8" y="-6" dx="1.5" dy="0.3" layer="1"/>
<smd name="26" x="-6" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="27" x="-5.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="28" x="-5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="29" x="-4.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="30" x="-4" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="31" x="-3.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="32" x="-3" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="33" x="-2.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="34" x="-2" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="35" x="-1.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="36" x="-1" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="37" x="-0.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="38" x="0" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="39" x="0.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="40" x="1" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="41" x="1.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="42" x="2" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="43" x="2.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="44" x="3" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="45" x="3.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="46" x="4" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="47" x="4.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="48" x="5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="49" x="5.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="50" x="6" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="51" x="8" y="-6" dx="1.5" dy="0.3" layer="1"/>
<smd name="52" x="8" y="-5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="53" x="8" y="-5" dx="1.5" dy="0.3" layer="1"/>
<smd name="54" x="8" y="-4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="55" x="8" y="-4" dx="1.5" dy="0.3" layer="1"/>
<smd name="56" x="8" y="-3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="57" x="8" y="-3" dx="1.5" dy="0.3" layer="1"/>
<smd name="58" x="8" y="-2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="59" x="8" y="-2" dx="1.5" dy="0.3" layer="1"/>
<smd name="60" x="8" y="-1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="61" x="8" y="-1" dx="1.5" dy="0.3" layer="1"/>
<smd name="62" x="8" y="-0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="63" x="8" y="0" dx="1.5" dy="0.3" layer="1"/>
<smd name="64" x="8" y="0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="65" x="8" y="1" dx="1.5" dy="0.3" layer="1"/>
<smd name="66" x="8" y="1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="67" x="8" y="2" dx="1.5" dy="0.3" layer="1"/>
<smd name="68" x="8" y="2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="69" x="8" y="3" dx="1.5" dy="0.3" layer="1"/>
<smd name="70" x="8" y="3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="71" x="8" y="4" dx="1.5" dy="0.3" layer="1"/>
<smd name="72" x="8" y="4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="73" x="8" y="5" dx="1.5" dy="0.3" layer="1"/>
<smd name="74" x="8" y="5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="75" x="8" y="6" dx="1.5" dy="0.3" layer="1"/>
<smd name="76" x="6" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="77" x="5.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="78" x="5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="79" x="4.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="80" x="4" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="81" x="3.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="82" x="3" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="83" x="2.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="84" x="2" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="85" x="1.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="86" x="1" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="87" x="0.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="88" x="0" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="89" x="-0.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="90" x="-1" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="91" x="-1.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="92" x="-2" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="93" x="-2.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="94" x="-3" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="95" x="-3.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="96" x="-4" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="97" x="-4.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="98" x="-5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="99" x="-5.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="100" x="-6" y="8" dx="0.3" dy="1.5" layer="1"/>
<text x="-5.08" y="3.81" size="0.4064" layer="25">&gt;NAME</text>
<text x="-5.08" y="-3.81" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-8.1999" y1="5.8499" x2="-7.15" y2="6.1502" layer="51"/>
<rectangle x1="-8.1999" y1="5.35" x2="-7.15" y2="5.6501" layer="51"/>
<rectangle x1="-8.1999" y1="4.8499" x2="-7.15" y2="5.1502" layer="51"/>
<rectangle x1="-8.1999" y1="4.35" x2="-7.15" y2="4.6501" layer="51"/>
<rectangle x1="-8.1999" y1="3.8499" x2="-7.15" y2="4.1502" layer="51"/>
<rectangle x1="-8.1999" y1="3.35" x2="-7.15" y2="3.6501" layer="51"/>
<rectangle x1="-8.1999" y1="2.8499" x2="-7.15" y2="3.1502" layer="51"/>
<rectangle x1="-8.1999" y1="2.35" x2="-7.15" y2="2.6501" layer="51"/>
<rectangle x1="-8.1999" y1="1.8499" x2="-7.15" y2="2.1502" layer="51"/>
<rectangle x1="-8.1999" y1="1.35" x2="-7.15" y2="1.6501" layer="51"/>
<rectangle x1="-8.1999" y1="0.8499" x2="-7.15" y2="1.1502" layer="51"/>
<rectangle x1="-8.1999" y1="0.35" x2="-7.15" y2="0.6501" layer="51"/>
<rectangle x1="-8.1999" y1="-0.1501" x2="-7.15" y2="0.1502" layer="51"/>
<rectangle x1="-8.1999" y1="-0.65" x2="-7.15" y2="-0.3499" layer="51"/>
<rectangle x1="-8.1999" y1="-1.1501" x2="-7.15" y2="-0.8498" layer="51"/>
<rectangle x1="-8.1999" y1="-1.65" x2="-7.15" y2="-1.3499" layer="51"/>
<rectangle x1="-8.1999" y1="-2.1501" x2="-7.15" y2="-1.8498" layer="51"/>
<rectangle x1="-8.1999" y1="-2.65" x2="-7.15" y2="-2.3499" layer="51"/>
<rectangle x1="-8.1999" y1="-3.1501" x2="-7.15" y2="-2.8498" layer="51"/>
<rectangle x1="-8.1999" y1="-3.65" x2="-7.15" y2="-3.3499" layer="51"/>
<rectangle x1="-8.1999" y1="-4.1501" x2="-7.15" y2="-3.8498" layer="51"/>
<rectangle x1="-8.1999" y1="-4.65" x2="-7.15" y2="-4.3499" layer="51"/>
<rectangle x1="-8.1999" y1="-5.1501" x2="-7.15" y2="-4.8498" layer="51"/>
<rectangle x1="-8.1999" y1="-5.65" x2="-7.15" y2="-5.3499" layer="51"/>
<rectangle x1="-8.1999" y1="-6.1501" x2="-7.15" y2="-5.8498" layer="51"/>
<rectangle x1="-6.1501" y1="-8.1999" x2="-5.8498" y2="-7.15" layer="51"/>
<rectangle x1="-5.65" y1="-8.1999" x2="-5.3499" y2="-7.15" layer="51"/>
<rectangle x1="-5.1501" y1="-8.1999" x2="-4.8498" y2="-7.15" layer="51"/>
<rectangle x1="-4.65" y1="-8.1999" x2="-4.3499" y2="-7.15" layer="51"/>
<rectangle x1="-4.1501" y1="-8.1999" x2="-3.8498" y2="-7.15" layer="51"/>
<rectangle x1="-3.65" y1="-8.1999" x2="-3.3499" y2="-7.15" layer="51"/>
<rectangle x1="-3.1501" y1="-8.1999" x2="-2.8498" y2="-7.15" layer="51"/>
<rectangle x1="-2.65" y1="-8.1999" x2="-2.3499" y2="-7.15" layer="51"/>
<rectangle x1="-2.1501" y1="-8.1999" x2="-1.8498" y2="-7.15" layer="51"/>
<rectangle x1="-1.65" y1="-8.1999" x2="-1.3499" y2="-7.15" layer="51"/>
<rectangle x1="-1.1501" y1="-8.1999" x2="-0.8498" y2="-7.15" layer="51"/>
<rectangle x1="-0.65" y1="-8.1999" x2="-0.3499" y2="-7.15" layer="51"/>
<rectangle x1="-0.1501" y1="-8.1999" x2="0.1502" y2="-7.15" layer="51"/>
<rectangle x1="0.35" y1="-8.1999" x2="0.6501" y2="-7.15" layer="51"/>
<rectangle x1="0.8499" y1="-8.1999" x2="1.1502" y2="-7.15" layer="51"/>
<rectangle x1="1.35" y1="-8.1999" x2="1.6501" y2="-7.15" layer="51"/>
<rectangle x1="1.8499" y1="-8.1999" x2="2.1502" y2="-7.15" layer="51"/>
<rectangle x1="2.35" y1="-8.1999" x2="2.6501" y2="-7.15" layer="51"/>
<rectangle x1="2.8499" y1="-8.1999" x2="3.1502" y2="-7.15" layer="51"/>
<rectangle x1="3.35" y1="-8.1999" x2="3.6501" y2="-7.15" layer="51"/>
<rectangle x1="3.8499" y1="-8.1999" x2="4.1502" y2="-7.15" layer="51"/>
<rectangle x1="4.35" y1="-8.1999" x2="4.6501" y2="-7.15" layer="51"/>
<rectangle x1="4.8499" y1="-8.1999" x2="5.1502" y2="-7.15" layer="51"/>
<rectangle x1="5.35" y1="-8.1999" x2="5.6501" y2="-7.15" layer="51"/>
<rectangle x1="5.8499" y1="-8.1999" x2="6.1502" y2="-7.15" layer="51"/>
<rectangle x1="7.1501" y1="-6.1501" x2="8.2" y2="-5.8498" layer="51"/>
<rectangle x1="7.1501" y1="-5.65" x2="8.2" y2="-5.3499" layer="51"/>
<rectangle x1="7.1501" y1="-5.1501" x2="8.2" y2="-4.8498" layer="51"/>
<rectangle x1="7.1501" y1="-4.65" x2="8.2" y2="-4.3499" layer="51"/>
<rectangle x1="7.1501" y1="-4.1501" x2="8.2" y2="-3.8498" layer="51"/>
<rectangle x1="7.1501" y1="-3.65" x2="8.2" y2="-3.3499" layer="51"/>
<rectangle x1="7.1501" y1="-3.1501" x2="8.2" y2="-2.8498" layer="51"/>
<rectangle x1="7.1501" y1="-2.65" x2="8.2" y2="-2.3499" layer="51"/>
<rectangle x1="7.1501" y1="-2.1501" x2="8.2" y2="-1.8498" layer="51"/>
<rectangle x1="7.1501" y1="-1.65" x2="8.2" y2="-1.3499" layer="51"/>
<rectangle x1="7.1501" y1="-1.1501" x2="8.2" y2="-0.8498" layer="51"/>
<rectangle x1="7.1501" y1="-0.65" x2="8.2" y2="-0.3499" layer="51"/>
<rectangle x1="7.1501" y1="-0.1501" x2="8.2" y2="0.1502" layer="51"/>
<rectangle x1="7.1501" y1="0.35" x2="8.2" y2="0.6501" layer="51"/>
<rectangle x1="7.1501" y1="0.8499" x2="8.2" y2="1.1502" layer="51"/>
<rectangle x1="7.1501" y1="1.35" x2="8.2" y2="1.6501" layer="51"/>
<rectangle x1="7.1501" y1="1.8499" x2="8.2" y2="2.1502" layer="51"/>
<rectangle x1="7.1501" y1="2.35" x2="8.2" y2="2.6501" layer="51"/>
<rectangle x1="7.1501" y1="2.8499" x2="8.2" y2="3.1502" layer="51"/>
<rectangle x1="7.1501" y1="3.35" x2="8.2" y2="3.6501" layer="51"/>
<rectangle x1="7.1501" y1="3.8499" x2="8.2" y2="4.1502" layer="51"/>
<rectangle x1="7.1501" y1="4.35" x2="8.2" y2="4.6501" layer="51"/>
<rectangle x1="7.1501" y1="4.8499" x2="8.2" y2="5.1502" layer="51"/>
<rectangle x1="7.1501" y1="5.35" x2="8.2" y2="5.6501" layer="51"/>
<rectangle x1="7.1501" y1="5.8499" x2="8.2" y2="6.1502" layer="51"/>
<rectangle x1="5.8499" y1="7.1501" x2="6.1502" y2="8.2" layer="51"/>
<rectangle x1="5.35" y1="7.1501" x2="5.6501" y2="8.2" layer="51"/>
<rectangle x1="4.8499" y1="7.1501" x2="5.1502" y2="8.2" layer="51"/>
<rectangle x1="4.35" y1="7.1501" x2="4.6501" y2="8.2" layer="51"/>
<rectangle x1="3.8499" y1="7.1501" x2="4.1502" y2="8.2" layer="51"/>
<rectangle x1="3.35" y1="7.1501" x2="3.6501" y2="8.2" layer="51"/>
<rectangle x1="2.8499" y1="7.1501" x2="3.1502" y2="8.2" layer="51"/>
<rectangle x1="2.35" y1="7.1501" x2="2.6501" y2="8.2" layer="51"/>
<rectangle x1="1.8499" y1="7.1501" x2="2.1502" y2="8.2" layer="51"/>
<rectangle x1="1.35" y1="7.1501" x2="1.6501" y2="8.2" layer="51"/>
<rectangle x1="0.8499" y1="7.1501" x2="1.1502" y2="8.2" layer="51"/>
<rectangle x1="0.35" y1="7.1501" x2="0.6501" y2="8.2" layer="51"/>
<rectangle x1="-0.1501" y1="7.1501" x2="0.1502" y2="8.2" layer="51"/>
<rectangle x1="-0.65" y1="7.1501" x2="-0.3499" y2="8.2" layer="51"/>
<rectangle x1="-1.1501" y1="7.1501" x2="-0.8498" y2="8.2" layer="51"/>
<rectangle x1="-1.65" y1="7.1501" x2="-1.3499" y2="8.2" layer="51"/>
<rectangle x1="-2.1501" y1="7.1501" x2="-1.8498" y2="8.2" layer="51"/>
<rectangle x1="-2.65" y1="7.1501" x2="-2.3499" y2="8.2" layer="51"/>
<rectangle x1="-3.1501" y1="7.1501" x2="-2.8498" y2="8.2" layer="51"/>
<rectangle x1="-3.65" y1="7.1501" x2="-3.3499" y2="8.2" layer="51"/>
<rectangle x1="-4.1501" y1="7.1501" x2="-3.8498" y2="8.2" layer="51"/>
<rectangle x1="-4.65" y1="7.1501" x2="-4.3499" y2="8.2" layer="51"/>
<rectangle x1="-5.1501" y1="7.1501" x2="-4.8498" y2="8.2" layer="51"/>
<rectangle x1="-5.65" y1="7.1501" x2="-5.3499" y2="8.2" layer="51"/>
<rectangle x1="-6.1501" y1="7.1501" x2="-5.8498" y2="8.2" layer="51"/>
</package>
<package name="PHB_1MM27_2X05_SNT">
<description>&lt;p&gt;&lt;b&gt;Pin Header Box-Type, 1.27mm Pitch,  2x05 circuits, Straight, No Locking, THT&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;&lt;u&gt;Important Note&lt;/u&gt; regarding to THT-versions: see description of library&lt;p&gt;</description>
<wire x1="1.2" y1="-2.625" x2="6.575" y2="-2.625" width="0.1016" layer="21"/>
<wire x1="6.575" y1="-2.625" x2="6.575" y2="2.625" width="0.1016" layer="21"/>
<wire x1="6.575" y1="2.625" x2="-6.575" y2="2.625" width="0.1016" layer="21"/>
<wire x1="-6.575" y1="2.625" x2="-6.575" y2="-2.625" width="0.1016" layer="21"/>
<wire x1="-5.55" y1="-1.75" x2="-1.2" y2="-1.75" width="0.1016" layer="21"/>
<wire x1="1.2" y1="-1.75" x2="5.55" y2="-1.75" width="0.1016" layer="21"/>
<wire x1="5.55" y1="-1.75" x2="5.55" y2="1.75" width="0.1016" layer="21"/>
<wire x1="5.55" y1="1.75" x2="-5.55" y2="1.75" width="0.1016" layer="21"/>
<wire x1="-5.55" y1="1.75" x2="-5.55" y2="-1.75" width="0.1016" layer="21"/>
<wire x1="-6.575" y1="-2.625" x2="-1.2" y2="-2.625" width="0.1016" layer="21"/>
<wire x1="-1.2" y1="-2.625" x2="1.2" y2="-2.625" width="0.1016" layer="21"/>
<wire x1="1.2" y1="-2.625" x2="1.2" y2="-1.75" width="0.1016" layer="21"/>
<wire x1="-1.2" y1="-1.75" x2="-1.2" y2="-2.625" width="0.1016" layer="21"/>
<wire x1="-6.7469" y1="2.7781" x2="6.7469" y2="2.7781" width="0.1016" layer="39"/>
<wire x1="6.7469" y1="2.7781" x2="6.7469" y2="-2.7781" width="0.1016" layer="39"/>
<wire x1="6.7469" y1="-2.7781" x2="-6.7469" y2="-2.7781" width="0.1016" layer="39"/>
<wire x1="-6.7469" y1="-2.7781" x2="-6.7469" y2="2.7781" width="0.1016" layer="39"/>
<pad name="6" x="0" y="0.635" drill="0.65"/>
<pad name="5" x="0" y="-0.635" drill="0.65"/>
<pad name="8" x="1.27" y="0.635" drill="0.65"/>
<pad name="7" x="1.27" y="-0.635" drill="0.65"/>
<pad name="10" x="2.54" y="0.635" drill="0.65"/>
<pad name="9" x="2.54" y="-0.635" drill="0.65"/>
<pad name="4" x="-1.27" y="0.635" drill="0.65"/>
<pad name="3" x="-1.27" y="-0.635" drill="0.65"/>
<pad name="2" x="-2.54" y="0.635" drill="0.65"/>
<pad name="1" x="-2.54" y="-0.635" drill="0.65" first="yes"/>
<text x="-4.0481" y="-1.0319" size="0.8128" layer="21" ratio="9">1</text>
<text x="-4.1275" y="0.2381" size="0.8128" layer="21" ratio="9">2</text>
<text x="-6.5881" y="2.9369" size="1.27" layer="25" ratio="9">&gt;NAME</text>
<text x="7.0644" y="4.2068" size="1.27" layer="27" ratio="9" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.74" y1="0.435" x2="-2.34" y2="0.835" layer="51"/>
<rectangle x1="-2.74" y1="-0.835" x2="-2.34" y2="-0.435" layer="51"/>
<rectangle x1="-1.47" y1="0.435" x2="-1.07" y2="0.835" layer="51"/>
<rectangle x1="-1.47" y1="-0.835" x2="-1.07" y2="-0.435" layer="51"/>
<rectangle x1="-0.2" y1="0.435" x2="0.2" y2="0.835" layer="51"/>
<rectangle x1="-0.2" y1="-0.835" x2="0.2" y2="-0.435" layer="51"/>
<rectangle x1="1.07" y1="0.435" x2="1.47" y2="0.835" layer="51"/>
<rectangle x1="1.07" y1="-0.835" x2="1.47" y2="-0.435" layer="51"/>
<rectangle x1="2.34" y1="0.435" x2="2.74" y2="0.835" layer="51"/>
<rectangle x1="2.34" y1="-0.835" x2="2.74" y2="-0.435" layer="51"/>
<polygon width="0.1016" layer="21">
<vertex x="-2.54" y="-2.6194"/>
<vertex x="-2.8575" y="-3.2544"/>
<vertex x="-2.2225" y="-3.2544"/>
</polygon>
</package>
<package name="PHB_1MM27_2X05_SNS">
<description>&lt;p&gt;&lt;b&gt;Pin Header Box-Type, 1.27mm Pitch,  2x05 circuits, Straight, No Locking, SMT&lt;/b&gt;&lt;/p&gt;</description>
<wire x1="-4.1275" y1="-2.8575" x2="-6.8263" y2="-2.8575" width="0.1016" layer="39"/>
<wire x1="-6.8263" y1="-2.8575" x2="-6.8263" y2="2.8575" width="0.1016" layer="39"/>
<wire x1="-6.8263" y1="2.8575" x2="-3.4925" y2="2.8575" width="0.1016" layer="39"/>
<wire x1="-3.4925" y1="2.8575" x2="-3.4925" y2="3.81" width="0.1016" layer="39"/>
<wire x1="-3.4925" y1="3.81" x2="3.4925" y2="3.81" width="0.1016" layer="39"/>
<wire x1="3.4925" y1="3.81" x2="3.4925" y2="2.8575" width="0.1016" layer="39"/>
<wire x1="3.4925" y1="2.8575" x2="6.8263" y2="2.8575" width="0.1016" layer="39"/>
<wire x1="6.8263" y1="2.8575" x2="6.8263" y2="-2.8575" width="0.1016" layer="39"/>
<wire x1="6.8263" y1="-2.8575" x2="3.4926" y2="-2.8575" width="0.1016" layer="39"/>
<wire x1="3.4926" y1="-2.8575" x2="3.4926" y2="-3.81" width="0.1016" layer="39"/>
<wire x1="3.4926" y1="-3.81" x2="-4.1275" y2="-3.81" width="0.1016" layer="39"/>
<wire x1="-4.1275" y1="-3.81" x2="-4.1275" y2="-2.8575" width="0.1016" layer="39"/>
<wire x1="6.575" y1="-2.625" x2="6.575" y2="2.625" width="0.1016" layer="21"/>
<wire x1="-6.575" y1="2.625" x2="-6.575" y2="-2.625" width="0.1016" layer="21"/>
<wire x1="5.55" y1="-1.75" x2="5.55" y2="1.75" width="0.1016" layer="21"/>
<wire x1="-5.55" y1="1.75" x2="-5.55" y2="-1.75" width="0.1016" layer="21"/>
<wire x1="-1.2" y1="-2.625" x2="1.2" y2="-2.625" width="0.1016" layer="51"/>
<wire x1="1.2" y1="-2.625" x2="1.2" y2="-1.75" width="0.1016" layer="51"/>
<wire x1="-1.2" y1="-1.75" x2="-1.2" y2="-2.625" width="0.1016" layer="51"/>
<wire x1="-3.385" y1="2.625" x2="-6.575" y2="2.625" width="0.1016" layer="21"/>
<wire x1="3.4" y1="2.625" x2="-3.4" y2="2.625" width="0.1016" layer="51"/>
<wire x1="6.575" y1="2.625" x2="3.385" y2="2.625" width="0.1016" layer="21"/>
<wire x1="-3.41" y1="-1.75" x2="-1.2" y2="-1.75" width="0.1016" layer="51"/>
<wire x1="1.2" y1="-1.75" x2="3.41" y2="-1.75" width="0.1016" layer="51"/>
<wire x1="-5.55" y1="-1.75" x2="-3.39" y2="-1.75" width="0.1016" layer="21"/>
<wire x1="3.39" y1="-1.75" x2="5.55" y2="-1.75" width="0.1016" layer="21"/>
<wire x1="3.41" y1="1.75" x2="-3.41" y2="1.75" width="0.1016" layer="51"/>
<wire x1="-3.41" y1="1.75" x2="-5.55" y2="1.75" width="0.1016" layer="21"/>
<wire x1="5.55" y1="1.75" x2="3.39" y2="1.75" width="0.1016" layer="21"/>
<wire x1="-3.425" y1="-2.625" x2="-1.2" y2="-2.625" width="0.1016" layer="51"/>
<wire x1="1.2" y1="-2.625" x2="3.425" y2="-2.625" width="0.1016" layer="51"/>
<wire x1="-6.575" y1="-2.625" x2="-3.41" y2="-2.625" width="0.1016" layer="21"/>
<wire x1="3.39" y1="-2.625" x2="6.575" y2="-2.625" width="0.1016" layer="21"/>
<smd name="6" x="0" y="1.85" dx="0.76" dy="2.8" layer="1"/>
<smd name="5" x="0" y="-1.85" dx="0.76" dy="2.8" layer="1"/>
<smd name="8" x="1.27" y="1.85" dx="0.76" dy="2.8" layer="1"/>
<smd name="7" x="1.27" y="-1.85" dx="0.76" dy="2.8" layer="1"/>
<smd name="9" x="2.54" y="-1.85" dx="0.76" dy="2.8" layer="1"/>
<smd name="10" x="2.54" y="1.85" dx="0.76" dy="2.8" layer="1"/>
<smd name="2" x="-2.54" y="1.85" dx="0.76" dy="2.8" layer="1"/>
<smd name="1" x="-2.54" y="-1.85" dx="0.76" dy="2.8" layer="1"/>
<smd name="3" x="-1.27" y="-1.85" dx="0.76" dy="2.8" layer="1"/>
<smd name="4" x="-1.27" y="1.85" dx="0.76" dy="2.8" layer="1"/>
<text x="-6.5881" y="3.4949" size="1.27" layer="25" ratio="9">&gt;NAME</text>
<text x="7.0644" y="4.7648" size="1.27" layer="27" ratio="9" rot="R180">&gt;VALUE</text>
<text x="-4.0481" y="-1.0319" size="0.8128" layer="21" ratio="9">1</text>
<text x="-4.1275" y="0.2381" size="0.8128" layer="21" ratio="9">2</text>
<rectangle x1="-2.74" y1="0.435" x2="-2.34" y2="0.835" layer="51"/>
<rectangle x1="-2.74" y1="-0.835" x2="-2.34" y2="-0.435" layer="51"/>
<rectangle x1="-1.47" y1="0.435" x2="-1.07" y2="0.835" layer="51"/>
<rectangle x1="-1.47" y1="-0.835" x2="-1.07" y2="-0.435" layer="51"/>
<rectangle x1="-0.2" y1="0.435" x2="0.2" y2="0.835" layer="51"/>
<rectangle x1="-0.2" y1="-0.835" x2="0.2" y2="-0.435" layer="51"/>
<rectangle x1="1.07" y1="0.435" x2="1.47" y2="0.835" layer="51"/>
<rectangle x1="1.07" y1="-0.835" x2="1.47" y2="-0.435" layer="51"/>
<rectangle x1="2.34" y1="0.435" x2="2.74" y2="0.835" layer="51"/>
<rectangle x1="2.34" y1="-0.835" x2="2.74" y2="-0.435" layer="51"/>
<polygon width="0.1016" layer="21">
<vertex x="-3.1782" y="-2.9464"/>
<vertex x="-3.8132" y="-2.6289"/>
<vertex x="-3.8132" y="-3.2639"/>
</polygon>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED-1206">
<wire x1="-1" y1="1" x2="-2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1" x2="1" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="0" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="0.3" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="0.6" x2="-0.3" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="0" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="-0.889" y="1.397" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.778" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LED-0603">
<wire x1="0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="-0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="0" y1="0.17" x2="0.2338" y2="-0.14" width="0.2032" layer="21"/>
<wire x1="-0.0254" y1="0.1546" x2="-0.2184" y2="-0.14" width="0.2032" layer="21"/>
<smd name="C" x="0" y="0.877" dx="1" dy="1" layer="1" roundness="30"/>
<smd name="A" x="0" y="-0.877" dx="1" dy="1" layer="1" roundness="30"/>
<text x="-0.6985" y="-0.889" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="1.0795" y="-1.016" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="LED10MM">
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7" shape="square"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
</package>
<package name="FKIT-LED-1206">
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="0.5" x2="-0.55" y2="0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LED3MM-NS">
<description>&lt;h3&gt;LED 3MM - No Silk&lt;/h3&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM-KIT">
<description>&lt;h3&gt;LED5MM-KIT&lt;/h3&gt;
5MM Through-hole LED&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="LED-1206-BOTTOM">
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
</package>
<package name="SOT23-6">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt; 6 lead</description>
<wire x1="1.422" y1="-0.781" x2="-1.423" y2="-0.781" width="0.1524" layer="51"/>
<wire x1="-1.423" y1="-0.781" x2="-1.423" y2="0.781" width="0.1524" layer="21"/>
<wire x1="-1.423" y1="0.781" x2="1.422" y2="0.781" width="0.1524" layer="51"/>
<wire x1="1.422" y1="0.781" x2="1.422" y2="-0.781" width="0.1524" layer="21"/>
<circle x="-1.15" y="-0.5" radius="0.1" width="0" layer="21"/>
<smd name="1" x="-0.95" y="-1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="2" x="0" y="-1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="3" x="0.95" y="-1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="4" x="0.95" y="1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="5" x="0" y="1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="6" x="-0.95" y="1.15" dx="0.6" dy="0.9" layer="1"/>
<text x="-1.397" y="-2.672" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.397" y="1.702" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="-1.2" y1="-1.4" x2="-0.7" y2="-0.8" layer="51"/>
<rectangle x1="-0.25" y1="-1.4" x2="0.25" y2="-0.8" layer="51"/>
<rectangle x1="0.7" y1="-1.4" x2="1.2" y2="-0.8" layer="51"/>
<rectangle x1="0.7" y1="0.8" x2="1.2" y2="1.4" layer="51"/>
<rectangle x1="-0.25" y1="0.8" x2="0.25" y2="1.4" layer="51"/>
<rectangle x1="-1.2" y1="0.8" x2="-0.7" y2="1.4" layer="51"/>
</package>
<package name="SIM-HOLDER-HINGED">
<smd name="C1" x="-12.85" y="-2.54" dx="2.2" dy="1.3" layer="1"/>
<smd name="C2" x="-12.85" y="0" dx="2.2" dy="1.3" layer="1"/>
<smd name="C3" x="-12.85" y="2.54" dx="2.2" dy="1.3" layer="1"/>
<smd name="C5" x="12.85" y="-2.54" dx="2.2" dy="1.3" layer="1"/>
<smd name="C6" x="12.85" y="0" dx="2.2" dy="1.3" layer="1"/>
<smd name="C7" x="12.85" y="2.54" dx="2.2" dy="1.3" layer="1"/>
<wire x1="-15" y1="8.6" x2="13.97" y2="8.6" width="0.127" layer="21"/>
<wire x1="13.97" y1="8.6" x2="15" y2="8.6" width="0.127" layer="21"/>
<wire x1="-15" y1="-8.6" x2="13.97" y2="-8.6" width="0.127" layer="21"/>
<wire x1="13.97" y1="-8.6" x2="15" y2="-8.6" width="0.127" layer="21"/>
<wire x1="-15" y1="8.6" x2="-15" y2="-8.6" width="0.127" layer="21"/>
<wire x1="15" y1="8.6" x2="15" y2="-8.6" width="0.127" layer="21"/>
<wire x1="13.97" y1="8.6" x2="13.97" y2="6.35" width="0.127" layer="21"/>
<wire x1="13.97" y1="6.35" x2="10.16" y2="6.35" width="0.127" layer="21"/>
<wire x1="10.16" y1="6.35" x2="10.16" y2="-6.35" width="0.127" layer="21"/>
<wire x1="10.16" y1="-6.35" x2="13.97" y2="-6.35" width="0.127" layer="21"/>
<wire x1="13.97" y1="-6.35" x2="13.97" y2="-8.6" width="0.127" layer="21"/>
</package>
<package name="BU-SMA-H">
<wire x1="-1.1" y1="3.2" x2="1.1" y2="3.2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="1.1" x2="3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="1.1" y1="-3.2" x2="-1.1" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.1" x2="-3.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-3.1999" y1="3.1999" x2="3.2" y2="3.2" width="0.2032" layer="51"/>
<wire x1="3.2" y1="3.2" x2="3.2" y2="2.9" width="0.2032" layer="51"/>
<wire x1="3.2" y1="2.9" x2="3.2" y2="-2.9" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-2.9" x2="3.1999" y2="-3.1999" width="0.2032" layer="51"/>
<wire x1="3.1999" y1="-3.1999" x2="-3.2" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-3.2" x2="-3.1999" y2="3.1999" width="0.2032" layer="51"/>
<wire x1="3.2" y1="2.9" x2="4.3" y2="2.9" width="0.2032" layer="51"/>
<wire x1="4.3" y1="2.9" x2="8.3" y2="2.9" width="0.2032" layer="21"/>
<wire x1="8.3" y1="2.9" x2="8.6" y2="3.4" width="0.2032" layer="21"/>
<wire x1="10.7" y1="3.4" x2="11" y2="2.9" width="0.2032" layer="21"/>
<wire x1="11" y1="2.9" x2="11.6" y2="2.9" width="0.2032" layer="21"/>
<wire x1="11.6" y1="-2.9" x2="11.6" y2="2.9" width="0.2032" layer="21"/>
<wire x1="8.6" y1="3.4" x2="10.7" y2="3.4" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2.9" x2="4.3" y2="-2.9" width="0.2032" layer="51"/>
<wire x1="4.3" y1="-2.9" x2="8.3" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="8.3" y1="-2.9" x2="8.6" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="10.7" y1="-3.4" x2="11" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="11" y1="-2.9" x2="11.6" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="8.6" y1="-3.4" x2="10.7" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="11" y1="-2.9" x2="11" y2="2.9" width="0.2032" layer="21"/>
<wire x1="8.3" y1="-2.9" x2="8.3" y2="2.9" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.1999" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.7" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.5"/>
<pad name="2" x="-2.54" y="2.54" drill="1.6"/>
<pad name="3" x="2.54" y="2.54" drill="1.6"/>
<pad name="4" x="2.54" y="-2.54" drill="1.6"/>
<pad name="5" x="-2.54" y="-2.54" drill="1.6"/>
<text x="-2.54" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.715" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="EVQ-Q2">
<wire x1="-3.3" y1="3" x2="3.3" y2="3" width="0.127" layer="21"/>
<wire x1="3.3" y1="3" x2="3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="3.3" y1="-3" x2="-3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-3" x2="-3.3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.5033" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1" width="0.127" layer="21"/>
<smd name="B" x="-3.4" y="2" dx="3.2" dy="1.2" layer="1"/>
<smd name="B'" x="3.4" y="2" dx="3.2" dy="1.2" layer="1"/>
<smd name="A'" x="3.4" y="-2" dx="3.2" dy="1.2" layer="1"/>
<smd name="A" x="-3.4" y="-2" dx="3.2" dy="1.2" layer="1"/>
<text x="-3" y="3.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-4.8" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KMR2">
<wire x1="-2.1" y1="1.4" x2="2.1" y2="1.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.4" x2="2.1" y2="-1.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.4" x2="-2.1" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.4" x2="-2.1" y2="1.4" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-0.8" x2="-1.1" y2="-0.2" width="0.127" layer="21" curve="-90"/>
<wire x1="-1.1" y1="-0.2" x2="-1.1" y2="0.2" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0.2" x2="-0.5" y2="0.8" width="0.127" layer="21" curve="-90"/>
<wire x1="-0.5" y1="0.8" x2="0.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="0.5" y1="0.8" x2="1.1" y2="0.2" width="0.127" layer="21" curve="-90"/>
<wire x1="1.1" y1="0.2" x2="1.1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.2" x2="0.5" y2="-0.8" width="0.127" layer="21" curve="-90"/>
<wire x1="0.5" y1="-0.8" x2="-0.5" y2="-0.8" width="0.127" layer="21"/>
<smd name="1" x="2" y="0.8" dx="1" dy="1" layer="1"/>
<smd name="2" x="2" y="-0.8" dx="1" dy="1" layer="1"/>
<smd name="4" x="-2" y="-0.8" dx="1" dy="1" layer="1"/>
<smd name="3" x="-2" y="0.8" dx="1" dy="1" layer="1"/>
</package>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.4294" x2="1.4224" y2="-0.4294" width="0.2032" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.4294" x2="-1.4224" y2="0.4294" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.2684" y1="0.8104" x2="0.2684" y2="0.8104" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<text x="-0.889" y="2.159" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9525" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
</package>
<package name="SCDA">
<wire x1="-14" y1="-26.3" x2="-14" y2="2.8" width="0.127" layer="21"/>
<wire x1="-14" y1="2.8" x2="14.1" y2="2.8" width="0.127" layer="21"/>
<wire x1="14.1" y1="2.8" x2="14.1" y2="-26.3" width="0.127" layer="21"/>
<wire x1="14.1" y1="-26.3" x2="12" y2="-26.3" width="0.127" layer="21"/>
<wire x1="12" y1="-26.3" x2="-12.4" y2="-26.3" width="0.127" layer="21"/>
<wire x1="-12.4" y1="-26.3" x2="-14" y2="-26.3" width="0.127" layer="21"/>
<wire x1="-12.4" y1="-25.5" x2="12" y2="-25.7" width="0.127" layer="21" curve="-27.095878"/>
<wire x1="-12.4" y1="-25.5" x2="-12.4" y2="-26.3" width="0.127" layer="21"/>
<wire x1="12" y1="-25.7" x2="12" y2="-26.3" width="0.127" layer="21"/>
<smd name="12" x="-14.2" y="-23.5" dx="1.7" dy="3.1" layer="1"/>
<smd name="13" x="14.2" y="-24.1" dx="1.9" dy="1.9" layer="1"/>
<smd name="11" x="-14.1" y="-1.7" dx="1.7" dy="1.8" layer="1"/>
<smd name="10" x="-12.8" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="8" x="-10.4" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="7" x="-8.4" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="6" x="-6.1" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="5" x="-3.6" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="3.9" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="4" x="-1.1" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="1" x="6.4" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="1.4" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="9" x="8.9" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<rectangle x1="-14" y1="-26.3" x2="-11.7" y2="-20.2" layer="39"/>
<rectangle x1="11.3" y1="-26.3" x2="14.1" y2="-20.8" layer="39"/>
<hole x="-13" y="0" drill="1"/>
<hole x="13" y="0" drill="1"/>
<polygon width="0.127" layer="39">
<vertex x="14.1" y="-2.8"/>
<vertex x="11.3" y="-2.8"/>
<vertex x="11.3" y="-0.5"/>
<vertex x="-11.8" y="-0.4"/>
<vertex x="-11.8" y="-3.2"/>
<vertex x="-14" y="-3.2"/>
<vertex x="-14" y="4.7"/>
<vertex x="14.1" y="4.7"/>
</polygon>
</package>
<package name="SDCARD">
<wire x1="-14.2" y1="-28.81" x2="-14.2" y2="-0.01" width="0.127" layer="21"/>
<wire x1="-14.2" y1="-0.01" x2="14.3" y2="-0.01" width="0.127" layer="21"/>
<wire x1="14.3" y1="-0.01" x2="14.3" y2="-28.81" width="0.127" layer="21"/>
<wire x1="14.3" y1="-28.81" x2="11.8" y2="-28.81" width="0.127" layer="21"/>
<wire x1="11.8" y1="-28.81" x2="-12.6" y2="-28.81" width="0.127" layer="21"/>
<wire x1="-12.6" y1="-28.81" x2="-14.2" y2="-28.81" width="0.127" layer="21"/>
<wire x1="-12.6" y1="-28.01" x2="11.8" y2="-28.21" width="0.127" layer="21" curve="-27.095878"/>
<wire x1="-12.6" y1="-28.01" x2="-12.6" y2="-28.81" width="0.127" layer="21"/>
<wire x1="11.8" y1="-28.21" x2="11.8" y2="-28.81" width="0.127" layer="21"/>
<smd name="MT3" x="14.6" y="-19.01" dx="1.9" dy="1.9" layer="1"/>
<smd name="MT1" x="-14.5" y="-3.91" dx="1.7" dy="1.8" layer="1"/>
<smd name="CD" x="-12.5" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="8" x="-9.8" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="7" x="-8.1" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="6" x="-5.7" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="5" x="-3.1" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="4.3" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="4" x="-0.7" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="1" x="6.8" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="1.8" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="9" x="9.3" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="MT2" x="14.7" y="-3.91" dx="1.9" dy="1.9" layer="1"/>
<smd name="WP" x="-14.2" y="-19" dx="2.032" dy="2.032" layer="1"/>
<smd name="COM" x="-14.2" y="-9.3" dx="2.032" dy="2.032" layer="1"/>
<rectangle x1="-14" y1="-17.41" x2="-11.7" y2="-11.31" layer="39"/>
<rectangle x1="11.3" y1="-17.41" x2="14.1" y2="-11.91" layer="39"/>
<hole x="-12" y="-23.31" drill="1.5"/>
<hole x="12" y="-23.31" drill="1"/>
<polygon width="0.127" layer="39">
<vertex x="14.1" y="6.09"/>
<vertex x="11.3" y="6.09"/>
<vertex x="11.3" y="8.39"/>
<vertex x="-11.8" y="8.49"/>
<vertex x="-11.8" y="5.69"/>
<vertex x="-14" y="5.69"/>
<vertex x="-14" y="13.59"/>
<vertex x="14.1" y="13.59"/>
</polygon>
</package>
<package name="06132">
<wire x1="-13.2" y1="-25.1" x2="-13.2" y2="0" width="0.127" layer="21"/>
<wire x1="-13.2" y1="0" x2="10.6" y2="0" width="0.127" layer="21"/>
<wire x1="10.6" y1="0" x2="10.6" y2="-2.5" width="0.127" layer="21"/>
<wire x1="10.6" y1="-2.5" x2="13.2" y2="-2.5" width="0.127" layer="21"/>
<wire x1="13.2" y1="-2.5" x2="13.3" y2="-25.1" width="0.127" layer="21"/>
<wire x1="13.3" y1="-25.1" x2="12.2" y2="-25.1" width="0.127" layer="21"/>
<wire x1="12.2" y1="-25.1" x2="-12.3" y2="-25.1" width="0.127" layer="21"/>
<wire x1="-12.3" y1="-25.1" x2="-13.2" y2="-25.1" width="0.127" layer="21"/>
<wire x1="-12.4" y1="-24.8" x2="12" y2="-25" width="0.127" layer="21" curve="-27.095878"/>
<wire x1="-12.4" y1="-24.8" x2="-12.3" y2="-25.1" width="0.127" layer="21"/>
<wire x1="12" y1="-25" x2="12.2" y2="-25.1" width="0.127" layer="21"/>
<smd name="GP" x="-13.8" y="-21.8" dx="2.8" dy="2" layer="1" rot="R90"/>
<smd name="GP2" x="14.1" y="-21.8" dx="2.8" dy="2" layer="1" rot="R90"/>
<smd name="GP4" x="-13.9" y="-1" dx="2.8" dy="2" layer="1" rot="R90"/>
<smd name="WP" x="-12.2" y="0.9" dx="4" dy="0.7" layer="1" rot="R90"/>
<smd name="8" x="-9.7" y="0.9" dx="4" dy="0.7" layer="1" rot="R90"/>
<smd name="7" x="-8.1" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="6" x="-5.6" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="5" x="-3.1" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="4.4" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="4" x="-0.6" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="1" x="6.9" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="1.9" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="9" x="9.4" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="CD" x="-11" y="0.9" dx="4" dy="0.7" layer="1" rot="R90"/>
<smd name="GP3" x="11.7" y="-0.9" dx="2.8" dy="2" layer="1" rot="R90"/>
<hole x="-11.6" y="-2.1" drill="1.1"/>
<hole x="9.5" y="-2.1" drill="1.6"/>
</package>
<package name="SO8">
<wire x1="2.159" y1="1.9558" x2="-2.159" y2="1.9558" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.9558" x2="2.54" y2="-1.5748" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="1.5748" x2="-2.159" y2="1.9558" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="1.9558" x2="2.54" y2="1.5748" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-1.5748" x2="-2.159" y2="-1.9558" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.159" y1="-1.9558" x2="2.159" y2="-1.9558" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.5748" x2="2.54" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.5748" x2="-2.54" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.508" x2="-2.54" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.508" x2="-2.54" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.54" y1="-1.6002" x2="2.54" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="-1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="-0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="3.937" y="-2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-2.921" y="-1.905" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="-2.159" y1="-3.0988" x2="-1.651" y2="-1.9558" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="-3.0734" x2="0.889" y2="-1.9304" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
</package>
<package name="BATTERY_HOLDER">
<smd name="+" x="-29.21" y="0" dx="4.2" dy="3.2" layer="1" rot="R90"/>
<smd name="ADH" x="-14.56" y="0" dx="11" dy="7" layer="1" rot="R180"/>
<smd name="-" x="0.09" y="0" dx="4.2" dy="3.2" layer="1" rot="R90"/>
<wire x1="-25.31" y1="10" x2="-3.99" y2="10" width="0.127" layer="21"/>
<wire x1="-3.99" y1="10" x2="-3.99" y2="2.54" width="0.127" layer="21"/>
<wire x1="-25.31" y1="2.54" x2="-25.31" y2="10" width="0.127" layer="21"/>
<wire x1="-3.9" y1="-10" x2="-25.22" y2="-10" width="0.127" layer="21"/>
<wire x1="-25.22" y1="-10" x2="-25.22" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-3.9" y1="-3.81" x2="-3.9" y2="-10" width="0.127" layer="21"/>
<text x="-17.78" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-17.78" y="-7.62" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="AA_BATTERY_HOLDER">
<wire x1="-29.755" y1="16.45" x2="29.755" y2="16.45" width="0.127" layer="21"/>
<wire x1="-29.755" y1="-16.45" x2="29.755" y2="-16.45" width="0.127" layer="21"/>
<wire x1="-29.755" y1="16.45" x2="-29.755" y2="-16.45" width="0.127" layer="21"/>
<wire x1="29.755" y1="16.45" x2="29.755" y2="-16.45" width="0.127" layer="21"/>
<hole x="0" y="-7.5" drill="3.43"/>
<hole x="0" y="7.49" drill="3.43"/>
<pad name="+" x="27.18" y="-7.5" drill="1.02"/>
<pad name="-" x="27.18" y="7.49" drill="1.02"/>
<text x="8.965" y="10.43" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="8.965" y="-7.5" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="24" y="-7.5" size="2.54" layer="21">+</text>
<text x="-20" y="8.5" size="2.54" layer="21">+</text>
<text x="24" y="8.5" size="2.54" layer="21">-</text>
<text x="-20" y="-7.5" size="2.54" layer="21">-</text>
</package>
<package name="AA-BATTERY-HOLDER-DIMENSION">
<wire x1="-29.6" y1="16.5" x2="29.6" y2="16.5" width="0.127" layer="20"/>
<wire x1="-29.6" y1="-16.5" x2="29.6" y2="-16.5" width="0.127" layer="20"/>
<wire x1="-30" y1="16.1" x2="-30" y2="-16.1" width="0.127" layer="20"/>
<pad name="+" x="27.18" y="-7.5" drill="1.02"/>
<pad name="-" x="27.18" y="7.49" drill="1.02"/>
<text x="8.965" y="10.43" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="8.965" y="-7.5" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="24" y="-7.5" size="2.54" layer="21">+</text>
<text x="-20" y="8.5" size="2.54" layer="21">+</text>
<text x="24" y="8.5" size="2.54" layer="21">-</text>
<text x="-20" y="-7.5" size="2.54" layer="21">-</text>
<wire x1="30" y1="16.1" x2="30" y2="-16.1" width="0.127" layer="20"/>
<wire x1="-29.6" y1="16.5" x2="-30" y2="16.1" width="0.127" layer="20"/>
<wire x1="29.6" y1="16.5" x2="30" y2="16.1" width="0.127" layer="20"/>
<wire x1="-29.6" y1="-16.5" x2="-30" y2="-16.1" width="0.127" layer="20"/>
<wire x1="30" y1="-16.1" x2="29.6" y2="-16.5" width="0.127" layer="20"/>
</package>
<package name="LI-BATTERY-HOLDER">
<wire x1="10.45" y1="-38.85" x2="-10.45" y2="-38.85" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="-38.85" x2="-10.45" y2="38.85" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="38.85" x2="10.45" y2="38.85" width="0.4064" layer="21"/>
<wire x1="10.45" y1="38.85" x2="10.45" y2="-38.85" width="0.4064" layer="21"/>
<hole x="0" y="27.805" drill="3.2"/>
<hole x="0" y="-27.805" drill="3.2"/>
<text x="-1.45" y="20.17" size="5.08" layer="21" font="vector" ratio="15">+</text>
<text x="-1.45" y="-25.83" size="5.08" layer="21" font="vector" ratio="15">-</text>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<pad name="+" x="0" y="36.45" drill="2.794" diameter="3.81" shape="long"/>
<pad name="-" x="0" y="-36.45" drill="2.794" diameter="3.81" shape="long"/>
</package>
</packages>
<symbols>
<symbol name="BQ24210_DQC_10">
<pin name="VBUS" x="0" y="0"/>
<pin name="ISET" x="0" y="-2.54" direction="in"/>
<pin name="VSS" x="0" y="-5.08" direction="pwr"/>
<pin name="VTSB" x="0" y="-7.62" direction="out"/>
<pin name="TS" x="0" y="-10.16" direction="in"/>
<pin name="*PG" x="60.96" y="-12.7" direction="out" rot="R180"/>
<pin name="*EN" x="60.96" y="-10.16" direction="in" rot="R180"/>
<pin name="*CHG" x="60.96" y="-7.62" direction="out" rot="R180"/>
<pin name="VDPM" x="60.96" y="-5.08" direction="in" rot="R180"/>
<pin name="BAT" x="60.96" y="-2.54" rot="R180"/>
<pin name="EPAD" x="60.96" y="0" direction="pwr" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-17.78" x2="53.34" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-17.78" x2="53.34" y2="5.08" width="0.1524" layer="94"/>
<wire x1="53.34" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="25.7556" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="24.8158" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="CAP_POL">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="SI702X">
<wire x1="-12.7" y1="12.7" x2="-12.7" y2="-15.24" width="0.8128" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="12.7" y2="-15.24" width="0.8128" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="12.7" y2="12.7" width="0.8128" layer="94"/>
<wire x1="12.7" y1="12.7" x2="-12.7" y2="12.7" width="0.8128" layer="94"/>
<pin name="VDD" x="-17.78" y="7.62" length="middle"/>
<pin name="GND" x="-17.78" y="2.54" length="middle"/>
<pin name="SDA" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="SCL" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="DNC_2" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="DNC" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="T_GND" x="-17.78" y="-7.62" length="middle"/>
<text x="-7.62" y="15.24" size="3.81" layer="94">Si702x</text>
<text x="-10.16" y="-20.32" size="3.81" layer="95">&gt;NAME</text>
</symbol>
<symbol name="BLE112-A-VISUAL">
<wire x1="-15.24" y1="22.86" x2="15.24" y2="22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="22.86" x2="15.24" y2="-22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="-22.86" x2="-15.24" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-22.86" x2="-15.24" y2="22.86" width="0.254" layer="94"/>
<pin name="AVDD" x="-20.32" y="17.78" visible="pin" length="middle" direction="pwr"/>
<pin name="AVDD@2" x="-20.32" y="15.24" visible="pin" length="middle" direction="pwr"/>
<pin name="DVDD" x="7.62" y="-27.94" visible="pin" length="middle" direction="pwr" rot="R90"/>
<pin name="GND" x="-20.32" y="20.32" visible="pin" length="middle" direction="pwr"/>
<pin name="GND@2" x="-20.32" y="-2.54" visible="pin" length="middle" direction="pwr"/>
<pin name="GND@3" x="10.16" y="-27.94" visible="pin" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@4" x="20.32" y="10.16" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="P0_0" x="20.32" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="P0_1" x="20.32" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="P0_2" x="20.32" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="P0_3" x="20.32" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="P0_4" x="20.32" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="P0_5" x="20.32" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="P0_6" x="20.32" y="-10.16" visible="pin" length="middle" rot="R180"/>
<pin name="P0_7" x="5.08" y="-27.94" visible="pin" length="middle" rot="R90"/>
<pin name="P1_0" x="2.54" y="-27.94" visible="pin" length="middle" rot="R90"/>
<pin name="P1_1" x="0" y="-27.94" visible="pin" length="middle" rot="R90"/>
<pin name="P1_2" x="-2.54" y="-27.94" visible="pin" length="middle" rot="R90"/>
<pin name="P1_3" x="-5.08" y="-27.94" visible="pin" length="middle" rot="R90"/>
<pin name="P1_4" x="-7.62" y="-27.94" visible="pin" length="middle" rot="R90"/>
<pin name="P1_5" x="-20.32" y="-10.16" visible="pin" length="middle"/>
<pin name="P1_6" x="-20.32" y="2.54" visible="pin" length="middle"/>
<pin name="P1_7" x="-20.32" y="5.08" visible="pin" length="middle"/>
<pin name="P2_0" x="-20.32" y="7.62" visible="pin" length="middle"/>
<pin name="P2_1" x="-20.32" y="10.16" visible="pin" length="middle"/>
<pin name="P2_2" x="-20.32" y="12.7" visible="pin" length="middle"/>
<pin name="RESET" x="20.32" y="7.62" visible="pin" length="middle" direction="in" rot="R180"/>
<pin name="USB+" x="-20.32" y="-5.08" visible="pin" length="middle"/>
<pin name="USB-" x="-20.32" y="-7.62" visible="pin" length="middle"/>
<pin name="VDD_USB" x="-20.32" y="0" visible="pin" length="middle" direction="pwr"/>
<text x="1.778" y="24.13" size="2.54" layer="96">&gt;VALUE</text>
<text x="-15.24" y="24.13" size="2.54" layer="95">&gt;NAME</text>
</symbol>
<symbol name="RAK_SCH">
<wire x1="-40.64" y1="25.4" x2="-40.64" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-40.64" y1="-10.16" x2="40.64" y2="-10.16" width="0.254" layer="94"/>
<wire x1="40.64" y1="-10.16" x2="40.64" y2="25.4" width="0.254" layer="94"/>
<wire x1="40.64" y1="25.4" x2="-40.64" y2="25.4" width="0.254" layer="94"/>
<pin name="GND_1" x="27.94" y="30.48" length="middle" rot="R270"/>
<pin name="GND_2" x="25.4" y="30.48" length="middle" rot="R270"/>
<pin name="NC_1" x="-30.48" y="30.48" length="middle" rot="R270"/>
<pin name="BIAS" x="-45.72" y="15.24" length="middle"/>
<pin name="GND_3" x="22.86" y="30.48" length="middle" rot="R270"/>
<pin name="NC_2" x="-27.94" y="30.48" length="middle" rot="R270"/>
<pin name="NC_3" x="-25.4" y="30.48" length="middle" rot="R270"/>
<pin name="MCU_MODE" x="-45.72" y="10.16" length="middle"/>
<pin name="NC_4" x="-22.86" y="30.48" length="middle" rot="R270"/>
<pin name="NC_5" x="-20.32" y="30.48" length="middle" rot="R270"/>
<pin name="NC_6" x="-17.78" y="30.48" length="middle" rot="R270"/>
<pin name="GND_4" x="20.32" y="30.48" length="middle" rot="R270"/>
<pin name="GND_5" x="17.78" y="30.48" length="middle" rot="R270"/>
<pin name="NC_7" x="-15.24" y="30.48" length="middle" rot="R270"/>
<pin name="NC_8" x="-12.7" y="30.48" length="middle" rot="R270"/>
<pin name="NC_9" x="-10.16" y="30.48" length="middle" rot="R270"/>
<pin name="NC_10" x="-7.62" y="30.48" length="middle" rot="R270"/>
<pin name="MCU_ERROR" x="-45.72" y="5.08" length="middle"/>
<pin name="MCU_RESET" x="-45.72" y="0" length="middle"/>
<pin name="NC_11" x="-5.08" y="30.48" length="middle" rot="R270"/>
<pin name="NC_12" x="-2.54" y="30.48" length="middle" rot="R270"/>
<pin name="MCU_UART1_TX" x="-20.32" y="-15.24" length="middle" rot="R90"/>
<pin name="MCU_UART1_RX" x="-17.78" y="-15.24" length="middle" rot="R90"/>
<pin name="MCU_WPS" x="-10.16" y="-15.24" length="middle" rot="R90"/>
<pin name="GND_6" x="15.24" y="30.48" length="middle" rot="R270"/>
<pin name="NC_13" x="0" y="30.48" length="middle" rot="R270"/>
<pin name="MCU_SPI_INT" x="-7.62" y="-15.24" length="middle" rot="R90"/>
<pin name="MCU_BOOT" x="-5.08" y="-15.24" length="middle" rot="R90"/>
<pin name="NC_14" x="2.54" y="30.48" length="middle" rot="R270"/>
<pin name="NC_15" x="5.08" y="30.48" length="middle" rot="R270"/>
<pin name="MCU_USO_TX" x="45.72" y="-5.08" length="middle" rot="R180"/>
<pin name="MCU_USO_RX" x="45.72" y="-2.54" length="middle" rot="R180"/>
<pin name="MCU_USO_CLK" x="45.72" y="0" length="middle" rot="R180"/>
<pin name="MCU_USO_CS" x="45.72" y="2.54" length="middle" rot="R180"/>
<pin name="VDD" x="45.72" y="15.24" length="middle" rot="R180"/>
<pin name="GND" x="45.72" y="17.78" length="middle" rot="R180"/>
<text x="7.62" y="12.7" size="1.27" layer="125">&gt;NAME</text>
<text x="7.62" y="7.62" size="1.27" layer="127">&gt;VALUE</text>
</symbol>
<symbol name="SIM900">
<wire x1="-35.56" y1="30.48" x2="-35.56" y2="-35.56" width="0.4064" layer="94"/>
<wire x1="-35.56" y1="-35.56" x2="35.56" y2="-35.56" width="0.4064" layer="94"/>
<wire x1="35.56" y1="-35.56" x2="35.56" y2="35.56" width="0.4064" layer="94"/>
<wire x1="35.56" y1="35.56" x2="-30.48" y2="35.56" width="0.4064" layer="94"/>
<wire x1="-30.48" y1="35.56" x2="-35.56" y2="30.48" width="0.4064" layer="94"/>
<text x="-10.16" y="7.62" size="3.81" layer="94" font="vector">&gt;NAME</text>
<text x="-10.16" y="-12.7" size="3.81" layer="94" font="vector">&gt;VALUE</text>
<pin name="PWRKEY" x="-40.64" y="20.32" length="middle"/>
<pin name="PWRKEY_OUT" x="-40.64" y="17.78" length="middle"/>
<pin name="DTR" x="-40.64" y="15.24" length="middle"/>
<pin name="RI" x="-40.64" y="12.7" length="middle"/>
<pin name="DCD" x="-40.64" y="10.16" length="middle"/>
<pin name="DSR" x="-40.64" y="7.62" length="middle"/>
<pin name="CTS" x="-40.64" y="5.08" length="middle"/>
<pin name="RTS" x="-40.64" y="2.54" length="middle"/>
<pin name="TXD" x="-40.64" y="0" length="middle"/>
<pin name="RXD" x="-40.64" y="-2.54" length="middle"/>
<pin name="DISP_CLK" x="-40.64" y="-5.08" length="middle"/>
<pin name="DISP_DATA" x="-40.64" y="-7.62" length="middle"/>
<pin name="DISP_D/C" x="-40.64" y="-10.16" length="middle"/>
<pin name="DISP_CS" x="-40.64" y="-12.7" length="middle"/>
<pin name="VDD_EXT" x="-40.64" y="-15.24" length="middle"/>
<pin name="NRESET" x="-40.64" y="-17.78" length="middle"/>
<pin name="MIC_P" x="-17.78" y="-40.64" length="middle" rot="R90"/>
<pin name="MIC_N" x="-15.24" y="-40.64" length="middle" rot="R90"/>
<pin name="SPK_P" x="-12.7" y="-40.64" length="middle" rot="R90"/>
<pin name="SPK_N" x="-10.16" y="-40.64" length="middle" rot="R90"/>
<pin name="LINEIN_R" x="-7.62" y="-40.64" length="middle" rot="R90"/>
<pin name="LINEIN_L" x="-5.08" y="-40.64" length="middle" rot="R90"/>
<pin name="ADC" x="-2.54" y="-40.64" length="middle" rot="R90"/>
<pin name="VRTC" x="0" y="-40.64" length="middle" rot="R90"/>
<pin name="DBG_TXD" x="2.54" y="-40.64" length="middle" rot="R90"/>
<pin name="DBG_RXD" x="5.08" y="-40.64" length="middle" rot="R90"/>
<pin name="SIM_VDD" x="10.16" y="-40.64" length="middle" rot="R90"/>
<pin name="SIM_DATA" x="12.7" y="-40.64" length="middle" rot="R90"/>
<pin name="SIM_CLK" x="15.24" y="-40.64" length="middle" rot="R90"/>
<pin name="SIM_RST" x="17.78" y="-40.64" length="middle" rot="R90"/>
<pin name="SIM_PRESENCE" x="20.32" y="-40.64" length="middle" rot="R90"/>
<pin name="PWM1" x="40.64" y="-20.32" length="middle" rot="R180"/>
<pin name="PWM2" x="40.64" y="-17.78" length="middle" rot="R180"/>
<pin name="SDA" x="40.64" y="-15.24" length="middle" rot="R180"/>
<pin name="SCL" x="40.64" y="-12.7" length="middle" rot="R180"/>
<pin name="GPIO1/KBR4" x="40.64" y="-7.62" length="middle" rot="R180"/>
<pin name="GPIO2/KBR3" x="40.64" y="-5.08" length="middle" rot="R180"/>
<pin name="GPIO3/KBR2" x="40.64" y="-2.54" length="middle" rot="R180"/>
<pin name="GPIO4/KBR1" x="40.64" y="0" length="middle" rot="R180"/>
<pin name="GPIO5/KBR0" x="40.64" y="2.54" length="middle" rot="R180"/>
<pin name="GPIO6/KBC4" x="40.64" y="10.16" length="middle" rot="R180"/>
<pin name="GPIO7/KBC3" x="40.64" y="12.7" length="middle" rot="R180"/>
<pin name="GPIO8/KBC2" x="40.64" y="15.24" length="middle" rot="R180"/>
<pin name="GPIO9/KBC1" x="40.64" y="17.78" length="middle" rot="R180"/>
<pin name="GPIO10/KBC0" x="40.64" y="20.32" length="middle" rot="R180"/>
<pin name="NETLIGHT\" x="20.32" y="40.64" length="middle" rot="R270"/>
<pin name="RF_ANT" x="0" y="40.64" length="middle" rot="R270"/>
<pin name="STATUS" x="-15.24" y="40.64" length="middle" rot="R270"/>
<pin name="GPIO11" x="-17.78" y="40.64" length="middle" rot="R270"/>
<pin name="GPIO12" x="-20.32" y="40.64" length="middle" rot="R270"/>
</symbol>
<symbol name="GND">
<text x="2.54" y="0" size="1.27" layer="95" font="vector" rot="R90">GND</text>
<pin name="GND" x="0" y="0" visible="pad" length="middle" direction="pwr" rot="R90"/>
</symbol>
<symbol name="VBAT">
<text x="0" y="2.54" size="1.778" layer="95">VBAT</text>
<pin name="VBAT" x="0" y="0" visible="pad" length="middle" direction="pwr"/>
</symbol>
<symbol name="CP2104-GM">
<pin name="RI" x="0" y="0" direction="in"/>
<pin name="GND" x="0" y="-2.54" direction="pwr"/>
<pin name="D_PLUS" x="0" y="-5.08" direction="pas"/>
<pin name="D_MINUS" x="0" y="-7.62" direction="pas"/>
<pin name="VIO" x="0" y="-10.16" direction="pwr"/>
<pin name="VDD" x="0" y="-12.7" direction="pwr"/>
<pin name="REGIN" x="0" y="-15.24" direction="in"/>
<pin name="VBUS" x="0" y="-17.78" direction="in"/>
<pin name="*RST" x="0" y="-20.32" direction="pas"/>
<pin name="NC" x="0" y="-22.86" direction="nc"/>
<pin name="GPIO.3" x="0" y="-25.4" direction="pas"/>
<pin name="GPIO.2" x="0" y="-27.94" direction="pas"/>
<pin name="HEAT_TAB" x="76.2" y="0" direction="pas" rot="R180"/>
<pin name="DCD" x="76.2" y="-2.54" direction="in" rot="R180"/>
<pin name="DTR" x="76.2" y="-5.08" direction="out" rot="R180"/>
<pin name="DSR" x="76.2" y="-7.62" direction="in" rot="R180"/>
<pin name="TXD" x="76.2" y="-10.16" direction="out" rot="R180"/>
<pin name="RXD" x="76.2" y="-12.7" direction="in" rot="R180"/>
<pin name="RTS" x="76.2" y="-15.24" direction="out" rot="R180"/>
<pin name="CTS" x="76.2" y="-17.78" direction="in" rot="R180"/>
<pin name="SUSPEND" x="76.2" y="-20.32" direction="out" rot="R180"/>
<pin name="VPP" x="76.2" y="-22.86" direction="pas" rot="R180"/>
<pin name="*SUSPEND" x="76.2" y="-25.4" direction="out" rot="R180"/>
<pin name="GPIO.0" x="76.2" y="-27.94" direction="pas" rot="R180"/>
<pin name="GPIO.1" x="76.2" y="-30.48" direction="pas" rot="R180"/>
<wire x1="7.112" y1="0" x2="6.0452" y2="0.508" width="0.1524" layer="94"/>
<wire x1="7.112" y1="0" x2="6.0452" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="7.112" y1="-15.24" x2="6.0452" y2="-14.732" width="0.1524" layer="94"/>
<wire x1="7.112" y1="-15.24" x2="6.0452" y2="-15.748" width="0.1524" layer="94"/>
<wire x1="7.112" y1="-17.78" x2="6.0452" y2="-17.272" width="0.1524" layer="94"/>
<wire x1="7.112" y1="-17.78" x2="6.0452" y2="-18.288" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-2.54" x2="70.1548" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-2.54" x2="70.1548" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-4.572" x2="70.1548" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-5.588" x2="70.1548" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-7.62" x2="70.1548" y2="-7.112" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-7.62" x2="70.1548" y2="-8.128" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-9.652" x2="70.1548" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-10.668" x2="70.1548" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-12.7" x2="70.1548" y2="-12.192" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-12.7" x2="70.1548" y2="-13.208" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-14.732" x2="70.1548" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-15.748" x2="70.1548" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-17.78" x2="70.1548" y2="-17.272" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-17.78" x2="70.1548" y2="-18.288" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-19.812" x2="70.1548" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-20.828" x2="70.1548" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-24.892" x2="70.1548" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="69.088" y1="-25.908" x2="70.1548" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-35.56" x2="68.58" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="68.58" y1="-35.56" x2="68.58" y2="5.08" width="0.1524" layer="94"/>
<wire x1="68.58" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="33.3756" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="32.4358" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="ATMEGA2560">
<wire x1="-30.48" y1="78.74" x2="30.48" y2="78.74" width="0.254" layer="94"/>
<wire x1="30.48" y1="78.74" x2="30.48" y2="-76.2" width="0.254" layer="94"/>
<wire x1="30.48" y1="-76.2" x2="-30.48" y2="-76.2" width="0.254" layer="94"/>
<wire x1="-30.48" y1="-76.2" x2="-30.48" y2="78.74" width="0.254" layer="94"/>
<wire x1="10.16" y1="36.6713" x2="13.0175" y2="36.6713" width="0.127" layer="95"/>
<wire x1="19.685" y1="-72.5488" x2="22.7013" y2="-72.5488" width="0.127" layer="95"/>
<wire x1="19.8437" y1="-70.0088" x2="22.86" y2="-70.0088" width="0.127" layer="95"/>
<wire x1="-28.2575" y1="77.3113" x2="-21.2725" y2="77.3113" width="0.127" layer="95"/>
<text x="-30.48" y="-78.74" size="1.778" layer="96">&gt;VALUE</text>
<text x="-30.48" y="80.01" size="1.778" layer="95">&gt;NAME</text>
<text x="-28.0987" y="35.0838" size="1.524" layer="95">GND</text>
<text x="-28.0987" y="32.5438" size="1.524" layer="95">GND</text>
<text x="-28.0987" y="30.0038" size="1.524" layer="95">GND</text>
<text x="-28.0987" y="42.3863" size="1.524" layer="95">VCC</text>
<text x="-28.0987" y="44.9263" size="1.524" layer="95">VCC</text>
<text x="-28.0987" y="39.8463" size="1.524" layer="95">VCC</text>
<pin name="(A8)PC0" x="35.56" y="12.7" length="middle" rot="R180"/>
<pin name="(A9)PC1" x="35.56" y="15.24" length="middle" rot="R180"/>
<pin name="(A10)PC2" x="35.56" y="17.78" length="middle" rot="R180"/>
<pin name="(A11)PC3" x="35.56" y="20.32" length="middle" rot="R180"/>
<pin name="(A12)PC4" x="35.56" y="22.86" length="middle" rot="R180"/>
<pin name="(A13)PC5" x="35.56" y="25.4" length="middle" rot="R180"/>
<pin name="(A14)PC6" x="35.56" y="27.94" length="middle" rot="R180"/>
<pin name="(A15)PC7" x="35.56" y="30.48" length="middle" rot="R180"/>
<pin name="(AD0)PA0" x="35.56" y="58.42" length="middle" rot="R180"/>
<pin name="(AD1)PA1" x="35.56" y="60.96" length="middle" rot="R180"/>
<pin name="(AD2)PA2" x="35.56" y="63.5" length="middle" rot="R180"/>
<pin name="(AD3)PA3" x="35.56" y="66.04" length="middle" rot="R180"/>
<pin name="(AD4)PA4" x="35.56" y="68.58" length="middle" rot="R180"/>
<pin name="(AD5)PA5" x="35.56" y="71.12" length="middle" rot="R180"/>
<pin name="(AD6)PA6" x="35.56" y="73.66" length="middle" rot="R180"/>
<pin name="(AD7)PA7" x="35.56" y="76.2" length="middle" rot="R180"/>
<pin name="(ADC0)PF0" x="35.56" y="-55.88" length="middle" rot="R180"/>
<pin name="(ADC1)PF1" x="35.56" y="-53.34" length="middle" rot="R180"/>
<pin name="(ADC2)PF2" x="35.56" y="-50.8" length="middle" rot="R180"/>
<pin name="(ADC3)PF3" x="35.56" y="-48.26" length="middle" rot="R180"/>
<pin name="(ADC4/TCK)PF4" x="35.56" y="-45.72" length="middle" rot="R180"/>
<pin name="(ADC5/TMS)PF5" x="35.56" y="-43.18" length="middle" rot="R180"/>
<pin name="(ADC6/TDO)PF6" x="35.56" y="-40.64" length="middle" rot="R180"/>
<pin name="(ADC7/TDI)PF7" x="35.56" y="-38.1" length="middle" rot="R180"/>
<pin name="(ALE)PG2" x="35.56" y="-68.58" length="middle" rot="R180"/>
<pin name="(CLKO/ICP3/INT7)PE7" x="35.56" y="-15.24" length="middle" rot="R180"/>
<pin name="(ICP1)PD4" x="35.56" y="0" length="middle" rot="R180"/>
<pin name="(MISO/PCINT3)PB3" x="35.56" y="43.18" length="middle" rot="R180"/>
<pin name="(MOSI/PCINT2)PB2" x="35.56" y="40.64" length="middle" rot="R180"/>
<pin name="(OC0A/OC1C/PCINT7)PB7" x="35.56" y="53.34" length="middle" rot="R180"/>
<pin name="(OC0B)PG5" x="35.56" y="-60.96" length="middle" rot="R180"/>
<pin name="(OC1A/PCINT5)PB5" x="35.56" y="48.26" length="middle" rot="R180"/>
<pin name="(OC1B/PCINT6)PB6" x="35.56" y="50.8" length="middle" rot="R180"/>
<pin name="(OC2A/PCINT4)PB4" x="35.56" y="45.72" length="middle" rot="R180"/>
<pin name="(OC3A/AIN1)PE3" x="35.56" y="-25.4" length="middle" rot="R180"/>
<pin name="(OC3B/INT4)PE4" x="35.56" y="-22.86" length="middle" rot="R180"/>
<pin name="(OC3C/INT5)PE5" x="35.56" y="-20.32" length="middle" rot="R180"/>
<pin name="(RD)PG1" x="35.56" y="-71.12" length="middle" rot="R180"/>
<pin name="(RXD0/PCIN8)PE0" x="35.56" y="-33.02" length="middle" rot="R180"/>
<pin name="(RXD1/INT2)PD2" x="35.56" y="-5.08" length="middle" rot="R180"/>
<pin name="(SCK/PCINT1)PB1" x="35.56" y="38.1" length="middle" rot="R180"/>
<pin name="(SCL/INT0)PD0" x="35.56" y="-10.16" length="middle" rot="R180"/>
<pin name="(SDA/INT1)PD1" x="35.56" y="-7.62" length="middle" rot="R180"/>
<pin name="(SS/PCINT0)PB0" x="35.56" y="35.56" length="middle" rot="R180"/>
<pin name="(T0)PD7" x="35.56" y="7.62" length="middle" rot="R180"/>
<pin name="(T1)PD6" x="35.56" y="5.08" length="middle" rot="R180"/>
<pin name="(T3/INT6)PE6" x="35.56" y="-17.78" length="middle" rot="R180"/>
<pin name="(TOSC1)PG4" x="35.56" y="-63.5" length="middle" rot="R180"/>
<pin name="(TOSC2)PG3" x="35.56" y="-66.04" length="middle" rot="R180"/>
<pin name="(TXD0)PE1" x="35.56" y="-30.48" length="middle" rot="R180"/>
<pin name="(TXD1/INT3)PD3" x="35.56" y="-2.54" length="middle" rot="R180"/>
<pin name="(WR)PG0" x="35.56" y="-73.66" length="middle" rot="R180"/>
<pin name="(XCK0/AIN0)PE2" x="35.56" y="-27.94" length="middle" rot="R180"/>
<pin name="(XCK1)PD5" x="35.56" y="2.54" length="middle" rot="R180"/>
<pin name="AGND" x="-35.56" y="55.88" length="middle" direction="pwr"/>
<pin name="AREF" x="-35.56" y="60.96" length="middle"/>
<pin name="AVCC" x="-35.56" y="58.42" length="middle" direction="pwr"/>
<pin name="GND" x="-35.56" y="38.1" length="middle" direction="pwr"/>
<pin name="GND1" x="-35.56" y="35.56" visible="pad" length="middle" direction="pwr"/>
<pin name="GND2" x="-35.56" y="33.02" visible="pad" length="middle" direction="pwr"/>
<pin name="GND3" x="-35.56" y="30.48" visible="pad" length="middle" direction="pwr"/>
<pin name="PH0(RXD2)" x="-35.56" y="-73.66" length="middle"/>
<pin name="PH1(TXD2)" x="-35.56" y="-71.12" length="middle"/>
<pin name="PH2(XCK2)" x="-35.56" y="-68.58" length="middle"/>
<pin name="PH3(OC4A)" x="-35.56" y="-66.04" length="middle"/>
<pin name="PH4(OC4B)" x="-35.56" y="-63.5" length="middle"/>
<pin name="PH5(OC4C)" x="-35.56" y="-60.96" length="middle"/>
<pin name="PH6(OC2B)" x="-35.56" y="-58.42" length="middle"/>
<pin name="PH7(T4)" x="-35.56" y="-55.88" length="middle"/>
<pin name="PJ0(RXD3/PCINT9)" x="-35.56" y="-50.8" length="middle"/>
<pin name="PJ1(TXD3/PCINT10)" x="-35.56" y="-48.26" length="middle"/>
<pin name="PJ2(XCK3/PCINT11)" x="-35.56" y="-45.72" length="middle"/>
<pin name="PJ3(PCINT12)" x="-35.56" y="-43.18" length="middle"/>
<pin name="PJ4(PCINT13)" x="-35.56" y="-40.64" length="middle"/>
<pin name="PJ5(PCINT14)" x="-35.56" y="-38.1" length="middle"/>
<pin name="PJ6(PCINT15)" x="-35.56" y="-35.56" length="middle"/>
<pin name="PJ7" x="-35.56" y="-33.02" length="middle"/>
<pin name="PK0(ADC8/PCINT16)" x="-35.56" y="-27.94" length="middle"/>
<pin name="PK1(ADC9/PCINT17)" x="-35.56" y="-25.4" length="middle"/>
<pin name="PK2(ADC10/PCINT18)" x="-35.56" y="-22.86" length="middle"/>
<pin name="PK3(ADC11/PCINT19)" x="-35.56" y="-20.32" length="middle"/>
<pin name="PK4(ADC12/PCINT20)" x="-35.56" y="-17.78" length="middle"/>
<pin name="PK5(ADC13/PCINT21)" x="-35.56" y="-15.24" length="middle"/>
<pin name="PK6(ADC14/PCINT22)" x="-35.56" y="-12.7" length="middle"/>
<pin name="PK7(ADC15/PCINT23)" x="-35.56" y="-10.16" length="middle"/>
<pin name="PL0(ICP4)" x="-35.56" y="-5.08" length="middle"/>
<pin name="PL1(ICP5)" x="-35.56" y="-2.54" length="middle"/>
<pin name="PL2(T5)" x="-35.56" y="0" length="middle"/>
<pin name="PL3(OC5A)" x="-35.56" y="2.54" length="middle"/>
<pin name="PL4(OC5B)" x="-35.56" y="5.08" length="middle"/>
<pin name="PL5(OC5C)" x="-35.56" y="7.62" length="middle"/>
<pin name="PL6" x="-35.56" y="10.16" length="middle"/>
<pin name="PL7" x="-35.56" y="12.7" length="middle"/>
<pin name="RESET" x="-35.56" y="76.2" length="middle" direction="in" function="dot"/>
<pin name="VCC" x="-35.56" y="48.26" length="middle" direction="pwr"/>
<pin name="VCC1" x="-35.56" y="45.72" visible="pad" length="middle" direction="pwr"/>
<pin name="VCC2" x="-35.56" y="43.18" visible="pad" length="middle" direction="pwr"/>
<pin name="VCC3" x="-35.56" y="40.64" visible="pad" length="middle" direction="pwr"/>
<pin name="XTAL1" x="-35.56" y="66.04" length="middle" direction="in"/>
<pin name="XTAL2" x="-35.56" y="71.12" length="middle" direction="out"/>
</symbol>
<symbol name="2X05M">
<wire x1="3.81" y1="-6.35" x2="-3.81" y2="-6.35" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="6.35" x2="-3.81" y2="-6.35" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-6.35" x2="3.81" y2="6.35" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="6.35" x2="3.81" y2="6.35" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<text x="-3.81" y="-8.636" size="1.6764" layer="96">&gt;VALUE</text>
<text x="-3.81" y="6.858" size="1.6764" layer="95" ratio="12">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="9" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="SIM-HOLDER-HINGED">
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<text x="-10.16" y="11.43" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-10.16" y="-15.24" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="6.35" size="1.27" layer="94">SIM-CARD</text>
<pin name="C1" x="-12.7" y="7.62" visible="pin" length="short" direction="pas"/>
<pin name="C2" x="-12.7" y="2.54" visible="pin" length="short" direction="pas"/>
<pin name="C3" x="-12.7" y="-2.54" visible="pin" length="short" direction="pas"/>
<pin name="C5" x="-12.7" y="5.08" visible="pin" length="short" direction="pas"/>
<pin name="C6" x="-12.7" y="0" visible="pin" length="short" direction="pas"/>
<pin name="C7" x="-12.7" y="-5.08" visible="pin" length="short" direction="pas"/>
</symbol>
<symbol name="BU-SMA-H">
<wire x1="0" y1="-2.54" x2="-0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.508" x2="-0.762" y2="0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0.508" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.762" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-0.508" x2="-2.54" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="0.508" width="0.3048" layer="94" curve="-79.611142" cap="flat"/>
<wire x1="-2.54" y1="-2.54" x2="0" y2="-0.508" width="0.3048" layer="94" curve="79.611142" cap="flat"/>
<text x="2.54" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="0.762" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="2.54" y="0" length="short" direction="pas" rot="R180"/>
<pin name="GND" x="2.54" y="-2.54" length="short" direction="pwr" rot="R180"/>
<pin name="GND@1" x="2.54" y="-5.08" length="short" rot="R180"/>
<pin name="GND@2" x="2.54" y="-7.62" length="short" rot="R180"/>
<pin name="GND@3" x="2.54" y="-10.16" length="short" rot="R180"/>
</symbol>
<symbol name="SDM03MT40">
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<rectangle x1="-5.08" y1="2.54" x2="-2.54" y2="5.08" layer="94"/>
<pin name="1" x="-10.16" y="2.54" length="middle"/>
<pin name="2" x="-10.16" y="0" length="middle"/>
<pin name="3" x="-10.16" y="-2.54" length="middle"/>
<pin name="4" x="10.16" y="-2.54" length="middle" rot="R180"/>
<pin name="5" x="10.16" y="0" length="middle" rot="R180"/>
<pin name="6" x="10.16" y="2.54" length="middle" rot="R180"/>
<text x="-5.08" y="5.08" size="1.778" layer="125">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.778" layer="127">&gt;VALUE</text>
</symbol>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="XC6221A332MR-G">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="VIN" x="-5.08" y="-10.16" length="short" rot="R90"/>
<pin name="VSS" x="0" y="-10.16" length="short" rot="R90"/>
<pin name="CE" x="5.08" y="-10.16" length="short" rot="R90"/>
<pin name="NC" x="5.08" y="10.16" length="short" rot="R270"/>
<pin name="VOUT" x="-5.08" y="10.16" length="short" rot="R270"/>
</symbol>
<symbol name="SD-MMC">
<wire x1="-10.16" y1="22.86" x2="-10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-20.32" x2="-7.62" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-22.86" x2="15.24" y2="-22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="-22.86" x2="15.24" y2="22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="22.86" x2="-10.16" y2="22.86" width="0.254" layer="94"/>
<text x="-2.54" y="-7.62" size="2.1844" layer="94">SD &amp; MMC</text>
<pin name="CS" x="-12.7" y="2.54" length="short" direction="in"/>
<pin name="DATA_IN" x="-12.7" y="7.62" length="short" direction="in"/>
<pin name="VSS1" x="-12.7" y="-10.16" length="short" direction="sup"/>
<pin name="VDD" x="-12.7" y="-7.62" length="short" direction="sup"/>
<pin name="SCLK" x="-12.7" y="5.08" length="short" direction="in"/>
<pin name="VSS2" x="-12.7" y="-12.7" length="short" direction="sup"/>
<pin name="DATA_OUT" x="-12.7" y="10.16" length="short" direction="out"/>
<pin name="DAT1" x="-12.7" y="0" length="short"/>
<pin name="DAT2" x="-12.7" y="-2.54" length="short"/>
<pin name="WRITE_PROTECT" x="-12.7" y="15.24" length="short" direction="pas"/>
<pin name="CARD_DETECT" x="-12.7" y="20.32" length="short" direction="pas"/>
<pin name="COMMON_SW" x="-12.7" y="17.78" length="short" direction="pas"/>
<pin name="GND" x="-12.7" y="-17.78" length="short" direction="sup"/>
</symbol>
<symbol name="DS1307SOIC">
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<pin name="X1" x="-7.62" y="12.7" length="short" rot="R270"/>
<pin name="X2" x="-5.08" y="12.7" length="short" rot="R270"/>
<pin name="VBAT" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="GND" x="0" y="-10.16" length="short" rot="R90"/>
<pin name="SDA" x="-12.7" y="-5.08" length="short"/>
<pin name="SCL" x="-12.7" y="-2.54" length="short"/>
<pin name="SQW" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="VDD" x="2.54" y="12.7" length="short" rot="R270"/>
<text x="-10.16" y="-10.16" size="1.778" layer="126">&gt;NAME</text>
<text x="7.62" y="-10.16" size="1.778" layer="127">&gt;VALUE</text>
</symbol>
<symbol name="BATTERY">
<pin name="-" x="-10.16" y="0" visible="off" length="middle"/>
<pin name="+" x="7.62" y="0" visible="off" length="middle" rot="R180"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="5.08" y="5.08" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BQ24210_DQC_10" prefix="U">
<gates>
<gate name="A" symbol="BQ24210_DQC_10" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DQC10_P84X2P4">
<connects>
<connect gate="A" pin="*CHG" pad="8"/>
<connect gate="A" pin="*EN" pad="7"/>
<connect gate="A" pin="*PG" pad="6"/>
<connect gate="A" pin="BAT" pad="10"/>
<connect gate="A" pin="EPAD" pad="11"/>
<connect gate="A" pin="ISET" pad="2"/>
<connect gate="A" pin="TS" pad="5"/>
<connect gate="A" pin="VBUS" pad="1"/>
<connect gate="A" pin="VDPM" pad="9"/>
<connect gate="A" pin="VSS" pad="3"/>
<connect gate="A" pin="VTSB" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEETURL" value="http://www.ti.com/lit/gpn/BQ24210" constant="no"/>
<attribute name="DESCRIPTION" value="800mA, Single-Input, Single Cell Li-Ion Solar Battery Charger" constant="no"/>
<attribute name="FAMILY_NAME" value="BATTERY CHARGE MANAGEMENT" constant="no"/>
<attribute name="GENERIC_PART_NUMBER" value="BQ24210" constant="no"/>
<attribute name="INDUSTRY_STD_PKG_TYPE" value="WSON" constant="no"/>
<attribute name="MANUFACTURER" value="Texas Instruments" constant="no"/>
<attribute name="PACKAGE_DESIGNATOR" value="DQC" constant="no"/>
<attribute name="PIN_COUNT" value="10" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="AXIAL-0.3EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CAP-PTH-SMALL2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CAP-PTH-LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="GRM43D">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CAP-PTH-5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="AXIAL-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ASMD" package="CTZ3">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="CAP-PTH-SMALLEZ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_POL" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor Polarized&lt;/b&gt;
These are standard SMD and PTH capacitors. Normally 10uF, 47uF, and 100uF in electrolytic and tantalum varieties. Always verify the external diameter of the through hole cap, it varies with capacity, voltage, and manufacturer. The EIA devices should be standard.</description>
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="EIA3216">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3528" package="EIA3528">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CPOL-RADIAL-100UF-25V">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CPOL-RADIAL-10UF-25V">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7343" package="EIA7343">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="G" package="PANASONIC_G">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E" package="PANASONIC_E">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C" package="PANASONIC_C">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="F80" package="NIPPON_F80">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="PANASONIC_D">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CPOL-RADIAL-1000UF-63V">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH4" package="CPOL-RADIAL-1000UF-25V">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="VISHAY_C">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="H13" package="PANASONIC_H13">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6032" package="EIA6032">
<connects>
<connect gate="G$1" pin="+" pad="P$1"/>
<connect gate="G$1" pin="-" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EN_J2" package="EN_J2">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3528-KIT" package="EIA3528-KIT">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-KIT" package="EIA3216-KIT">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SI702X">
<gates>
<gate name="G$1" symbol="SI702X" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DFN6">
<connects>
<connect gate="G$1" pin="DNC" pad="DNC"/>
<connect gate="G$1" pin="DNC_2" pad="DNC2"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="T_GND" pad="T_GND"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BLE112-A-VISUAL" prefix="U">
<description>Bluetooth Low Energy single-mode module - Bluetooth 4.0, chip antenna</description>
<gates>
<gate name="G$1" symbol="BLE112-A-VISUAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BLE112-A">
<connects>
<connect gate="G$1" pin="AVDD" pad="AVDD1"/>
<connect gate="G$1" pin="AVDD@2" pad="AVDD2"/>
<connect gate="G$1" pin="DVDD" pad="DVDD"/>
<connect gate="G$1" pin="GND" pad="GND1"/>
<connect gate="G$1" pin="GND@2" pad="GND2"/>
<connect gate="G$1" pin="GND@3" pad="GND3"/>
<connect gate="G$1" pin="GND@4" pad="GND4"/>
<connect gate="G$1" pin="P0_0" pad="P0_0"/>
<connect gate="G$1" pin="P0_1" pad="P0_1"/>
<connect gate="G$1" pin="P0_2" pad="P0_2"/>
<connect gate="G$1" pin="P0_3" pad="P0_3"/>
<connect gate="G$1" pin="P0_4" pad="P0_4"/>
<connect gate="G$1" pin="P0_5" pad="P0_5"/>
<connect gate="G$1" pin="P0_6" pad="P0_6"/>
<connect gate="G$1" pin="P0_7" pad="P0_7"/>
<connect gate="G$1" pin="P1_0" pad="P1_0"/>
<connect gate="G$1" pin="P1_1" pad="P1_1"/>
<connect gate="G$1" pin="P1_2" pad="P1_2"/>
<connect gate="G$1" pin="P1_3" pad="P1_3"/>
<connect gate="G$1" pin="P1_4" pad="P1_4"/>
<connect gate="G$1" pin="P1_5" pad="P1_5"/>
<connect gate="G$1" pin="P1_6" pad="P1_6"/>
<connect gate="G$1" pin="P1_7" pad="P1_7"/>
<connect gate="G$1" pin="P2_0" pad="P2_0"/>
<connect gate="G$1" pin="P2_1" pad="P2_1"/>
<connect gate="G$1" pin="P2_2" pad="P2_2"/>
<connect gate="G$1" pin="RESET" pad="RESET"/>
<connect gate="G$1" pin="USB+" pad="USB+"/>
<connect gate="G$1" pin="USB-" pad="USB-"/>
<connect gate="G$1" pin="VDD_USB" pad="VDD_USB"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RAK410">
<description>RAK410 wifi module.</description>
<gates>
<gate name="G$1" symbol="RAK_SCH" x="0" y="-7.62"/>
</gates>
<devices>
<device name="" package="RAK410">
<connects>
<connect gate="G$1" pin="BIAS" pad="P$4"/>
<connect gate="G$1" pin="GND" pad="P$36"/>
<connect gate="G$1" pin="GND_1" pad="P$1"/>
<connect gate="G$1" pin="GND_2" pad="P$2"/>
<connect gate="G$1" pin="GND_3" pad="P$5"/>
<connect gate="G$1" pin="GND_4" pad="P$12"/>
<connect gate="G$1" pin="GND_5" pad="P$13"/>
<connect gate="G$1" pin="GND_6" pad="P$25"/>
<connect gate="G$1" pin="MCU_BOOT" pad="P$28"/>
<connect gate="G$1" pin="MCU_ERROR" pad="P$18"/>
<connect gate="G$1" pin="MCU_MODE" pad="P$8"/>
<connect gate="G$1" pin="MCU_RESET" pad="P$19"/>
<connect gate="G$1" pin="MCU_SPI_INT" pad="P$27"/>
<connect gate="G$1" pin="MCU_UART1_RX" pad="P$23"/>
<connect gate="G$1" pin="MCU_UART1_TX" pad="P$22"/>
<connect gate="G$1" pin="MCU_USO_CLK" pad="P$33"/>
<connect gate="G$1" pin="MCU_USO_CS" pad="P$34"/>
<connect gate="G$1" pin="MCU_USO_RX" pad="P$32"/>
<connect gate="G$1" pin="MCU_USO_TX" pad="P$31"/>
<connect gate="G$1" pin="MCU_WPS" pad="P$24"/>
<connect gate="G$1" pin="NC_1" pad="P$3"/>
<connect gate="G$1" pin="NC_10" pad="P$17"/>
<connect gate="G$1" pin="NC_11" pad="P$20"/>
<connect gate="G$1" pin="NC_12" pad="P$21"/>
<connect gate="G$1" pin="NC_13" pad="P$26"/>
<connect gate="G$1" pin="NC_14" pad="P$29"/>
<connect gate="G$1" pin="NC_15" pad="P$30"/>
<connect gate="G$1" pin="NC_2" pad="P$6"/>
<connect gate="G$1" pin="NC_3" pad="P$7"/>
<connect gate="G$1" pin="NC_4" pad="P$9"/>
<connect gate="G$1" pin="NC_5" pad="P$10"/>
<connect gate="G$1" pin="NC_6" pad="P$11"/>
<connect gate="G$1" pin="NC_7" pad="P$14"/>
<connect gate="G$1" pin="NC_8" pad="P$15"/>
<connect gate="G$1" pin="NC_9" pad="P$16"/>
<connect gate="G$1" pin="VDD" pad="P$35"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SIM900" prefix="IC">
<gates>
<gate name="G$1" symbol="SIM900" x="-22.86" y="2.54" addlevel="must"/>
<gate name="GND@1" symbol="GND" x="25.4" y="27.94" addlevel="request"/>
<gate name="GND@2" symbol="GND" x="30.48" y="27.94" addlevel="request"/>
<gate name="GND@3" symbol="GND" x="35.56" y="27.94" addlevel="request"/>
<gate name="GND@4" symbol="GND" x="40.64" y="27.94" addlevel="request"/>
<gate name="GND@5" symbol="GND" x="45.72" y="27.94" addlevel="request"/>
<gate name="GND@6" symbol="GND" x="50.8" y="27.94" addlevel="request"/>
<gate name="GND@7" symbol="GND" x="25.4" y="20.32" addlevel="request"/>
<gate name="GND@8" symbol="GND" x="30.48" y="20.32" addlevel="request"/>
<gate name="GND@9" symbol="GND" x="35.56" y="20.32" addlevel="request"/>
<gate name="GND@10" symbol="GND" x="40.64" y="20.32" addlevel="request"/>
<gate name="GND@11" symbol="GND" x="45.72" y="20.32" addlevel="request"/>
<gate name="GND@12" symbol="GND" x="50.8" y="20.32" addlevel="request"/>
<gate name="GND@13" symbol="GND" x="25.4" y="12.7" addlevel="request"/>
<gate name="GND@14" symbol="GND" x="30.48" y="12.7" addlevel="request"/>
<gate name="GND@15" symbol="GND" x="35.56" y="12.7" addlevel="request"/>
<gate name="VBAT@1" symbol="VBAT" x="27.94" y="2.54" addlevel="request"/>
<gate name="VBAT@2" symbol="VBAT" x="27.94" y="0" addlevel="request"/>
<gate name="VBAT@3" symbol="VBAT" x="27.94" y="-2.54" addlevel="request"/>
</gates>
<devices>
<device name="" package="SIM900">
<connects>
<connect gate="G$1" pin="ADC" pad="25"/>
<connect gate="G$1" pin="CTS" pad="7"/>
<connect gate="G$1" pin="DBG_RXD" pad="28"/>
<connect gate="G$1" pin="DBG_TXD" pad="27"/>
<connect gate="G$1" pin="DCD" pad="5"/>
<connect gate="G$1" pin="DISP_CLK" pad="11"/>
<connect gate="G$1" pin="DISP_CS" pad="14"/>
<connect gate="G$1" pin="DISP_D/C" pad="13"/>
<connect gate="G$1" pin="DISP_DATA" pad="12"/>
<connect gate="G$1" pin="DSR" pad="6"/>
<connect gate="G$1" pin="DTR" pad="3"/>
<connect gate="G$1" pin="GPIO1/KBR4" pad="40"/>
<connect gate="G$1" pin="GPIO10/KBC0" pad="51"/>
<connect gate="G$1" pin="GPIO11" pad="67"/>
<connect gate="G$1" pin="GPIO12" pad="68"/>
<connect gate="G$1" pin="GPIO2/KBR3" pad="41"/>
<connect gate="G$1" pin="GPIO3/KBR2" pad="42"/>
<connect gate="G$1" pin="GPIO4/KBR1" pad="43"/>
<connect gate="G$1" pin="GPIO5/KBR0" pad="44"/>
<connect gate="G$1" pin="GPIO6/KBC4" pad="47"/>
<connect gate="G$1" pin="GPIO7/KBC3" pad="48"/>
<connect gate="G$1" pin="GPIO8/KBC2" pad="49"/>
<connect gate="G$1" pin="GPIO9/KBC1" pad="50"/>
<connect gate="G$1" pin="LINEIN_L" pad="24"/>
<connect gate="G$1" pin="LINEIN_R" pad="23"/>
<connect gate="G$1" pin="MIC_N" pad="20"/>
<connect gate="G$1" pin="MIC_P" pad="19"/>
<connect gate="G$1" pin="NETLIGHT\" pad="52"/>
<connect gate="G$1" pin="NRESET" pad="16"/>
<connect gate="G$1" pin="PWM1" pad="35"/>
<connect gate="G$1" pin="PWM2" pad="36"/>
<connect gate="G$1" pin="PWRKEY" pad="1"/>
<connect gate="G$1" pin="PWRKEY_OUT" pad="2"/>
<connect gate="G$1" pin="RF_ANT" pad="60"/>
<connect gate="G$1" pin="RI" pad="4"/>
<connect gate="G$1" pin="RTS" pad="8"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCL" pad="38"/>
<connect gate="G$1" pin="SDA" pad="37"/>
<connect gate="G$1" pin="SIM_CLK" pad="32"/>
<connect gate="G$1" pin="SIM_DATA" pad="31"/>
<connect gate="G$1" pin="SIM_PRESENCE" pad="34"/>
<connect gate="G$1" pin="SIM_RST" pad="33"/>
<connect gate="G$1" pin="SIM_VDD" pad="30"/>
<connect gate="G$1" pin="SPK_N" pad="22"/>
<connect gate="G$1" pin="SPK_P" pad="21"/>
<connect gate="G$1" pin="STATUS" pad="66"/>
<connect gate="G$1" pin="TXD" pad="9"/>
<connect gate="G$1" pin="VDD_EXT" pad="15"/>
<connect gate="G$1" pin="VRTC" pad="26"/>
<connect gate="GND@1" pin="GND" pad="17"/>
<connect gate="GND@10" pin="GND" pad="59"/>
<connect gate="GND@11" pin="GND" pad="61"/>
<connect gate="GND@12" pin="GND" pad="62"/>
<connect gate="GND@13" pin="GND" pad="63"/>
<connect gate="GND@14" pin="GND" pad="64"/>
<connect gate="GND@15" pin="GND" pad="65"/>
<connect gate="GND@2" pin="GND" pad="18"/>
<connect gate="GND@3" pin="GND" pad="29"/>
<connect gate="GND@4" pin="GND" pad="39"/>
<connect gate="GND@5" pin="GND" pad="45"/>
<connect gate="GND@6" pin="GND" pad="46"/>
<connect gate="GND@7" pin="GND" pad="53"/>
<connect gate="GND@8" pin="GND" pad="54"/>
<connect gate="GND@9" pin="GND" pad="58"/>
<connect gate="VBAT@1" pin="VBAT" pad="55"/>
<connect gate="VBAT@2" pin="VBAT" pad="56"/>
<connect gate="VBAT@3" pin="VBAT" pad="57"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CP2104-GM">
<gates>
<gate name="A" symbol="CP2104-GM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN24_4X4">
<connects>
<connect gate="A" pin="*RST" pad="9"/>
<connect gate="A" pin="*SUSPEND" pad="15"/>
<connect gate="A" pin="CTS" pad="18"/>
<connect gate="A" pin="DCD" pad="24"/>
<connect gate="A" pin="DSR" pad="22"/>
<connect gate="A" pin="DTR" pad="23"/>
<connect gate="A" pin="D_MINUS" pad="4"/>
<connect gate="A" pin="D_PLUS" pad="3"/>
<connect gate="A" pin="GND" pad="2"/>
<connect gate="A" pin="GPIO.0" pad="14"/>
<connect gate="A" pin="GPIO.1" pad="13"/>
<connect gate="A" pin="GPIO.2" pad="12"/>
<connect gate="A" pin="GPIO.3" pad="11"/>
<connect gate="A" pin="HEAT_TAB" pad="25"/>
<connect gate="A" pin="NC" pad="10"/>
<connect gate="A" pin="REGIN" pad="7"/>
<connect gate="A" pin="RI" pad="1"/>
<connect gate="A" pin="RTS" pad="19"/>
<connect gate="A" pin="RXD" pad="20"/>
<connect gate="A" pin="SUSPEND" pad="17"/>
<connect gate="A" pin="TXD" pad="21"/>
<connect gate="A" pin="VBUS" pad="8"/>
<connect gate="A" pin="VDD" pad="6"/>
<connect gate="A" pin="VIO" pad="5"/>
<connect gate="A" pin="VPP" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATMEGA2560" prefix="U">
<description>Atmel 100-pin 8-bit uC&lt;BR&gt;
64/128/256 Kbytes FLASH&lt;BR&gt;
4 Kbytes EEPROM&lt;BR&gt;
8 Kbytes SRAM&lt;BR&gt;
86 General Purpose I/O pins&lt;BR&gt;
12-channels16-bit PWM&lt;BR&gt;
4 x Serial USARTs&lt;BR&gt;
16-channels 10-bit A/D&lt;BR&gt;</description>
<gates>
<gate name="1" symbol="ATMEGA2560" x="0" y="0"/>
</gates>
<devices>
<device name="AU" package="TQFP100">
<connects>
<connect gate="1" pin="(A10)PC2" pad="55"/>
<connect gate="1" pin="(A11)PC3" pad="56"/>
<connect gate="1" pin="(A12)PC4" pad="57"/>
<connect gate="1" pin="(A13)PC5" pad="58"/>
<connect gate="1" pin="(A14)PC6" pad="59"/>
<connect gate="1" pin="(A15)PC7" pad="60"/>
<connect gate="1" pin="(A8)PC0" pad="53"/>
<connect gate="1" pin="(A9)PC1" pad="54"/>
<connect gate="1" pin="(AD0)PA0" pad="78"/>
<connect gate="1" pin="(AD1)PA1" pad="77"/>
<connect gate="1" pin="(AD2)PA2" pad="76"/>
<connect gate="1" pin="(AD3)PA3" pad="75"/>
<connect gate="1" pin="(AD4)PA4" pad="74"/>
<connect gate="1" pin="(AD5)PA5" pad="73"/>
<connect gate="1" pin="(AD6)PA6" pad="72"/>
<connect gate="1" pin="(AD7)PA7" pad="71"/>
<connect gate="1" pin="(ADC0)PF0" pad="97"/>
<connect gate="1" pin="(ADC1)PF1" pad="96"/>
<connect gate="1" pin="(ADC2)PF2" pad="95"/>
<connect gate="1" pin="(ADC3)PF3" pad="94"/>
<connect gate="1" pin="(ADC4/TCK)PF4" pad="93"/>
<connect gate="1" pin="(ADC5/TMS)PF5" pad="92"/>
<connect gate="1" pin="(ADC6/TDO)PF6" pad="91"/>
<connect gate="1" pin="(ADC7/TDI)PF7" pad="90"/>
<connect gate="1" pin="(ALE)PG2" pad="70"/>
<connect gate="1" pin="(CLKO/ICP3/INT7)PE7" pad="9"/>
<connect gate="1" pin="(ICP1)PD4" pad="47"/>
<connect gate="1" pin="(MISO/PCINT3)PB3" pad="22"/>
<connect gate="1" pin="(MOSI/PCINT2)PB2" pad="21"/>
<connect gate="1" pin="(OC0A/OC1C/PCINT7)PB7" pad="26"/>
<connect gate="1" pin="(OC0B)PG5" pad="1"/>
<connect gate="1" pin="(OC1A/PCINT5)PB5" pad="24"/>
<connect gate="1" pin="(OC1B/PCINT6)PB6" pad="25"/>
<connect gate="1" pin="(OC2A/PCINT4)PB4" pad="23"/>
<connect gate="1" pin="(OC3A/AIN1)PE3" pad="5"/>
<connect gate="1" pin="(OC3B/INT4)PE4" pad="6"/>
<connect gate="1" pin="(OC3C/INT5)PE5" pad="7"/>
<connect gate="1" pin="(RD)PG1" pad="52"/>
<connect gate="1" pin="(RXD0/PCIN8)PE0" pad="2"/>
<connect gate="1" pin="(RXD1/INT2)PD2" pad="45"/>
<connect gate="1" pin="(SCK/PCINT1)PB1" pad="20"/>
<connect gate="1" pin="(SCL/INT0)PD0" pad="43"/>
<connect gate="1" pin="(SDA/INT1)PD1" pad="44"/>
<connect gate="1" pin="(SS/PCINT0)PB0" pad="19"/>
<connect gate="1" pin="(T0)PD7" pad="50"/>
<connect gate="1" pin="(T1)PD6" pad="49"/>
<connect gate="1" pin="(T3/INT6)PE6" pad="8"/>
<connect gate="1" pin="(TOSC1)PG4" pad="29"/>
<connect gate="1" pin="(TOSC2)PG3" pad="28"/>
<connect gate="1" pin="(TXD0)PE1" pad="3"/>
<connect gate="1" pin="(TXD1/INT3)PD3" pad="46"/>
<connect gate="1" pin="(WR)PG0" pad="51"/>
<connect gate="1" pin="(XCK0/AIN0)PE2" pad="4"/>
<connect gate="1" pin="(XCK1)PD5" pad="48"/>
<connect gate="1" pin="AGND" pad="99"/>
<connect gate="1" pin="AREF" pad="98"/>
<connect gate="1" pin="AVCC" pad="100"/>
<connect gate="1" pin="GND" pad="11"/>
<connect gate="1" pin="GND1" pad="32"/>
<connect gate="1" pin="GND2" pad="62"/>
<connect gate="1" pin="GND3" pad="81"/>
<connect gate="1" pin="PH0(RXD2)" pad="12"/>
<connect gate="1" pin="PH1(TXD2)" pad="13"/>
<connect gate="1" pin="PH2(XCK2)" pad="14"/>
<connect gate="1" pin="PH3(OC4A)" pad="15"/>
<connect gate="1" pin="PH4(OC4B)" pad="16"/>
<connect gate="1" pin="PH5(OC4C)" pad="17"/>
<connect gate="1" pin="PH6(OC2B)" pad="18"/>
<connect gate="1" pin="PH7(T4)" pad="27"/>
<connect gate="1" pin="PJ0(RXD3/PCINT9)" pad="63"/>
<connect gate="1" pin="PJ1(TXD3/PCINT10)" pad="64"/>
<connect gate="1" pin="PJ2(XCK3/PCINT11)" pad="65"/>
<connect gate="1" pin="PJ3(PCINT12)" pad="66"/>
<connect gate="1" pin="PJ4(PCINT13)" pad="67"/>
<connect gate="1" pin="PJ5(PCINT14)" pad="68"/>
<connect gate="1" pin="PJ6(PCINT15)" pad="69"/>
<connect gate="1" pin="PJ7" pad="79"/>
<connect gate="1" pin="PK0(ADC8/PCINT16)" pad="89"/>
<connect gate="1" pin="PK1(ADC9/PCINT17)" pad="88"/>
<connect gate="1" pin="PK2(ADC10/PCINT18)" pad="87"/>
<connect gate="1" pin="PK3(ADC11/PCINT19)" pad="86"/>
<connect gate="1" pin="PK4(ADC12/PCINT20)" pad="85"/>
<connect gate="1" pin="PK5(ADC13/PCINT21)" pad="84"/>
<connect gate="1" pin="PK6(ADC14/PCINT22)" pad="83"/>
<connect gate="1" pin="PK7(ADC15/PCINT23)" pad="82"/>
<connect gate="1" pin="PL0(ICP4)" pad="35"/>
<connect gate="1" pin="PL1(ICP5)" pad="36"/>
<connect gate="1" pin="PL2(T5)" pad="37"/>
<connect gate="1" pin="PL3(OC5A)" pad="38"/>
<connect gate="1" pin="PL4(OC5B)" pad="39"/>
<connect gate="1" pin="PL5(OC5C)" pad="40"/>
<connect gate="1" pin="PL6" pad="41"/>
<connect gate="1" pin="PL7" pad="42"/>
<connect gate="1" pin="RESET" pad="30"/>
<connect gate="1" pin="VCC" pad="10"/>
<connect gate="1" pin="VCC1" pad="31"/>
<connect gate="1" pin="VCC2" pad="61"/>
<connect gate="1" pin="VCC3" pad="80"/>
<connect gate="1" pin="XTAL1" pad="34"/>
<connect gate="1" pin="XTAL2" pad="33"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PHB_1MM27_2X05_*" prefix="X">
<description>&lt;p&gt;&lt;b&gt;Pin Header Box-Type, 1.27mm/0.5"/50mil Pitch, 2x05 Circuits&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;&lt;u&gt;Note:&lt;/u&gt; see library description for an important note about THT-version&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="2X05M" x="0" y="0"/>
</gates>
<devices>
<device name="SNT" package="PHB_1MM27_2X05_SNT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SNS" package="PHB_1MM27_2X05_SNS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LEDs&lt;/b&gt;
Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. 5mm - Spark Fun Electronics SKU : COM-00529 (and others)</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED3MM-NS">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED5MM-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SIM-HOLDER-HINGED">
<gates>
<gate name="G$1" symbol="SIM-HOLDER-HINGED" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SIM-HOLDER-HINGED">
<connects>
<connect gate="G$1" pin="C1" pad="C1"/>
<connect gate="G$1" pin="C2" pad="C2"/>
<connect gate="G$1" pin="C3" pad="C3"/>
<connect gate="G$1" pin="C5" pad="C5"/>
<connect gate="G$1" pin="C6" pad="C6"/>
<connect gate="G$1" pin="C7" pad="C7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RF-SMA">
<gates>
<gate name="G$1" symbol="BU-SMA-H" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BU-SMA-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="GND@1" pad="3"/>
<connect gate="G$1" pin="GND@2" pad="4"/>
<connect gate="G$1" pin="GND@3" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SDM03MT40-7-F">
<description>30mA Surface Mount Schottky Barrier Diode in SOT-26 package,
offers low capacitance and low forward voltage drop, designed with
Guard Ring for Transient Protection. Ideal for low logic level
applications.
&lt;br&gt;

&lt;br&gt;
Features and Benefits
 Low Forward Voltage Drop
 Guard Ring Die Construction for Transient Protection
 Ideal for low logic level applications
 Low Capacitance
 Totally Lead-Free &amp; Fully RoHS Compliant (Note 1 &amp; 2)
Halogen and Antimony Free. “Green” Device(Note 3)
Mechanical Data
 Case: SOT26
 Case Material: Molded Plastic, "Green" Molding Compound,
Note 5. UL Flammability Classification Rating 94V-0
 Moisture Sensitivity: Level 1 per J-STD-020D
 Polarity: See Diagram
 Leads: Matte Tin (Lead Free), Solderable per MIL-STD-202,
Method 208
 Lead Free Plating (Matte Tin Finish annealed over Copper
leadframe).
 Weight: 0.016 grams (approximate)</description>
<gates>
<gate name="G$1" symbol="SDM03MT40" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SPST_TACT" prefix="SW">
<description>SMT 6mm switch, EVQQ2 series
&lt;p&gt;http://www.ladyada.net/library/eagle&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="-EVQQ2" package="EVQ-Q2">
<connects>
<connect gate="G$1" pin="P" pad="A"/>
<connect gate="G$1" pin="P1" pad="A'"/>
<connect gate="G$1" pin="S" pad="B"/>
<connect gate="G$1" pin="S1" pad="B'"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-KMR2" package="KMR2">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="3"/>
<connect gate="G$1" pin="S" pad="2"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XC6221A332MR-G">
<gates>
<gate name="G$1" symbol="XC6221A332MR-G" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="CE" pad="3"/>
<connect gate="G$1" pin="NC" pad="4"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
<connect gate="G$1" pin="VSS" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SDMMC">
<description>&lt;b&gt;SD MMC Card holder&lt;/b&gt;
&lt;p&gt;
4UCON part #06132 - easy to solder</description>
<gates>
<gate name="G$1" symbol="SD-MMC" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SCDA" package="SCDA">
<connects>
<connect gate="G$1" pin="CARD_DETECT" pad="10"/>
<connect gate="G$1" pin="COMMON_SW" pad="11"/>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="DAT1" pad="8"/>
<connect gate="G$1" pin="DAT2" pad="9"/>
<connect gate="G$1" pin="DATA_IN" pad="2"/>
<connect gate="G$1" pin="DATA_OUT" pad="7"/>
<connect gate="G$1" pin="GND" pad="13"/>
<connect gate="G$1" pin="SCLK" pad="5"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS1" pad="3"/>
<connect gate="G$1" pin="VSS2" pad="6"/>
<connect gate="G$1" pin="WRITE_PROTECT" pad="12"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="18650" package="SDCARD">
<connects>
<connect gate="G$1" pin="CARD_DETECT" pad="CD"/>
<connect gate="G$1" pin="COMMON_SW" pad="COM"/>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="DAT1" pad="8"/>
<connect gate="G$1" pin="DAT2" pad="9"/>
<connect gate="G$1" pin="DATA_IN" pad="2"/>
<connect gate="G$1" pin="DATA_OUT" pad="7"/>
<connect gate="G$1" pin="GND" pad="MT1"/>
<connect gate="G$1" pin="SCLK" pad="5"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS1" pad="3"/>
<connect gate="G$1" pin="VSS2" pad="6"/>
<connect gate="G$1" pin="WRITE_PROTECT" pad="WP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="06132" package="06132">
<connects>
<connect gate="G$1" pin="CARD_DETECT" pad="CD"/>
<connect gate="G$1" pin="COMMON_SW" pad="GP3"/>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="DAT1" pad="8"/>
<connect gate="G$1" pin="DAT2" pad="9"/>
<connect gate="G$1" pin="DATA_IN" pad="2"/>
<connect gate="G$1" pin="DATA_OUT" pad="7"/>
<connect gate="G$1" pin="GND" pad="GP"/>
<connect gate="G$1" pin="SCLK" pad="5"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS1" pad="3"/>
<connect gate="G$1" pin="VSS2" pad="6"/>
<connect gate="G$1" pin="WRITE_PROTECT" pad="WP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DS1307SOIC">
<gates>
<gate name="G$1" symbol="DS1307SOIC" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="SO8">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="SQW" pad="7"/>
<connect gate="G$1" pin="VBAT" pad="3"/>
<connect gate="G$1" pin="VDD" pad="8"/>
<connect gate="G$1" pin="X1" pad="1"/>
<connect gate="G$1" pin="X2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BATTERY_HOLDER" prefix="VBAT" uservalue="yes">
<description>&lt;b&gt;Coin Cell Holder&lt;/b&gt; :-  __BU2032SM-HD-G__&lt;p&gt;
&lt;b&gt;AA  Battery Holder&lt;/b&gt; :- __2462K-ND__&lt;p&gt;
&lt;b&gt;LI-ION Holder &lt;/b&gt;:- __BH-18650-PC__</description>
<gates>
<gate name="G$1" symbol="BATTERY" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="BATTERY_HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AA" package="AA_BATTERY_HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-DIM" package="AA-BATTERY-HOLDER-DIMENSION">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LI-ION" package="LI-BATTERY-HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Passives">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="0.508" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="0.254" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL2">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-LARGE">
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-4.826" y="0" drill="0.9" diameter="1.905"/>
<pad name="2" x="4.572" y="0" drill="0.9" diameter="1.905"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="GRM43D">
<wire x1="2.25" y1="1.6" x2="1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.3" y1="1.8" x2="2.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.8" x2="2.3" y2="-1.8" width="0.127" layer="21"/>
<smd name="A" x="1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<smd name="C" x="-1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<text x="-2" y="2" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-2" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.2" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="2.2" y2="1.6" layer="51"/>
</package>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.5588" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-5MM">
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.5" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="AXIAL-5MM">
<wire x1="-1.14" y1="0.762" x2="1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0.762" x2="1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="-0.762" x2="-1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="-0.762" x2="-1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.394" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.394" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.5" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.5" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.17" size="0.4" layer="25">&gt;Name</text>
<text x="-1.032" y="-0.208" size="0.4" layer="21" ratio="15">&gt;Value</text>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-0.8" y="0.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.7" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="CTZ3">
<description>CTZ3 Series land pattern for variable capacitor - CTZ3E-50C-W1-PF</description>
<wire x1="-1.6" y1="1.4" x2="-1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.25" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.4" x2="-1" y2="2.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.2" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.05" y1="2.25" x2="-1.7" y2="1.45" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.45" x2="-1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.35" x2="-1.05" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.05" y1="2.25" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.7" y1="-2.35" x2="1.05" y2="-2.35" width="0.127" layer="21"/>
<smd name="+" x="0" y="2.05" dx="1.5" dy="1.2" layer="1"/>
<smd name="-" x="0" y="-2.05" dx="1.5" dy="1.2" layer="1"/>
<text x="-2" y="3" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2" y="-3.4" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="CAP-PTH-SMALLEZ">
<description>This is the "EZ" version of the .1" spaced ceramic thru-hole cap.&lt;br&gt;
It has reduced top mask to make it harder to put the component on the wrong side of the board.</description>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651" stop="no"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651" stop="no"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
<circle x="0" y="0" radius="0.898025" width="0" layer="30"/>
<circle x="2.54" y="0" radius="0.915809375" width="0" layer="30"/>
<circle x="0" y="0" radius="0.40160625" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.40160625" width="0" layer="29"/>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762"/>
<pad name="2" x="2.5" y="0" drill="0.762"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="AXIAL-0.3EZ">
<description>This is the "EZ" version of the standard .3" spaced resistor package.&lt;br&gt;
It has a reduced top mask to make it harder to install upside-down.</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="-3.81" y="0" radius="0.508" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.523634375" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="3.81" y="0" radius="1.04726875" width="0" layer="30"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CAP-PTH-SMALL2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CAP-PTH-LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="GRM43D">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CAP-PTH-5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="AXIAL-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ASMD" package="CTZ3">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="CAP-PTH-SMALLEZ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="AXIAL-0.3EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="semicon-smd-ipc">
<description>&lt;b&gt;IPC Standard SMD Semiconductors&lt;/b&gt;&lt;p&gt;
A few devices defined according to the IPC standard.&lt;p&gt;
Based on:&lt;p&gt;
IPC-SM-782&lt;br&gt;
IRevision A, August 1993&lt;br&gt;
Includes Amendment 1, October 1996&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOT23W">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.973" y1="1.983" x2="1.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-1.983" x2="-1.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-1.983" x2="-1.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="1.983" x2="1.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="1.422" y1="0.66" x2="1.422" y2="-0.66" width="0.1524" layer="51"/>
<wire x1="1.422" y1="-0.66" x2="-1.422" y2="-0.66" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.66" x2="-1.422" y2="0.66" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="0.66" x2="1.422" y2="0.66" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.303" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.303" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.303" dx="1" dy="1.4" layer="1"/>
<text x="-2.03" y="2.0701" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9949" y="-3.3701" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<rectangle x1="-0.5001" y1="-0.5001" x2="0.5001" y2="0.5001" layer="35"/>
</package>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SOT89">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="2.235" y1="-1.245" x2="-2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="2.235" y1="1.219" x2="2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="-2.235" y1="-1.245" x2="-2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-2.235" y1="1.219" x2="2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-0.7874" y1="1.5748" x2="-0.3556" y2="2.0066" width="0.1998" layer="51"/>
<wire x1="-0.3556" y1="2.0066" x2="0.3556" y2="2.0066" width="0.1998" layer="51"/>
<wire x1="0.3556" y1="2.0066" x2="0.7874" y2="1.5748" width="0.1998" layer="51"/>
<wire x1="0.7874" y1="1.5748" x2="0.7874" y2="1.2954" width="0.1998" layer="51"/>
<wire x1="0.7874" y1="1.2954" x2="-0.7874" y2="1.2954" width="0.1998" layer="51"/>
<wire x1="-0.7874" y1="1.2954" x2="-0.7874" y2="1.5748" width="0.1998" layer="51"/>
<smd name="1" x="-1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="3" x="1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="2" x="0" y="-1.727" dx="0.8" dy="1.9" layer="1"/>
<smd name="2@1" x="0" y="0.94" dx="2.032" dy="3.65" layer="1" roundness="75"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4051" y="-4.3449" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<polygon width="0.1998" layer="51">
<vertex x="-0.7874" y="1.3208"/>
<vertex x="-0.7874" y="1.5748"/>
<vertex x="-0.3556" y="2.0066"/>
<vertex x="0.3048" y="2.0066"/>
<vertex x="0.3556" y="2.0066"/>
<vertex x="0.7874" y="1.5748"/>
<vertex x="0.7874" y="1.2954"/>
<vertex x="-0.7874" y="1.2954"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.0399" x2="0.3081" y2="-1.4239" width="0.1524" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="NPN-TRANSISTOR_" prefix="Q" uservalue="yes">
<description>&lt;B&gt;NPN TRANSISTOR&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="SOT23W" package="SOT23W">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23" package="SOT23">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT89" package="SOT89">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="zetex">
<description>&lt;b&gt;Zetex Power MOS FETs, Bridges, Diodes&lt;/b&gt;&lt;p&gt;
http://www.zetex.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOT-23">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="-1.4224" y1="0.381" x2="1.4732" y2="0.381" width="0.1524" layer="21"/>
<wire x1="1.4732" y1="0.381" x2="1.4732" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="1.4732" y1="-0.381" x2="-1.4224" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="-0.381" x2="-1.4224" y2="0.381" width="0.1524" layer="21"/>
<smd name="3" x="0.9906" y="1.016" dx="0.7874" dy="0.889" layer="1"/>
<smd name="2" x="-0.9398" y="1.016" dx="0.7874" dy="0.889" layer="1"/>
<smd name="1" x="0.0254" y="-1.016" dx="0.7874" dy="0.889" layer="1"/>
<text x="-1.397" y="1.778" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.397" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="0.7874" y1="0.4318" x2="1.1684" y2="0.9398" layer="51"/>
<rectangle x1="-1.143" y1="0.4318" x2="-0.762" y2="0.9398" layer="51"/>
<rectangle x1="-0.1778" y1="-0.9398" x2="0.2032" y2="-0.4318" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="P_MOSFET">
<wire x1="-1.651" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-0.508" x2="1.397" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-3.048" x2="0" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.159" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="2.159" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="3.048" x2="1.397" y2="3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="3.048" x2="1.397" y2="0.381" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.159" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<rectangle x1="-2.032" y1="1.397" x2="-1.524" y2="2.921" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<rectangle x1="-2.032" y1="-2.921" x2="-1.524" y2="-1.397" layer="94"/>
<rectangle x1="0.762" y1="-0.635" x2="2.032" y2="-0.508" layer="94"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<polygon width="0.1016" layer="94">
<vertex x="-0.127" y="0"/>
<vertex x="-1.143" y="-0.635"/>
<vertex x="-1.143" y="0.635"/>
</polygon>
<polygon width="0.1016" layer="94">
<vertex x="1.397" y="-0.508"/>
<vertex x="0.762" y="0.508"/>
<vertex x="2.032" y="0.508"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="PMOSSOT23" prefix="T" uservalue="yes">
<description>&lt;b&gt;MOS FET&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="P_MOSFET" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="A" pin="D" pad="1"/>
<connect gate="A" pin="G" pad="3"/>
<connect gate="A" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="led">
<packages>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CHIPLED_0603">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="U57X32">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-0.254" x2="-2.9718" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.1082" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-0.254" x2="-5.5118" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-4.6482" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.2129" y1="0.0539" x2="-0.0539" y2="2.2129" width="0.1524" layer="51" curve="-90.010616"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0539" y1="-2.2129" x2="2.2129" y2="-0.0539" width="0.1524" layer="51" curve="90.005308"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="29"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="29"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="0.925" x2="0.35" y2="0.925" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="-0.35" y1="-0.925" x2="0.35" y2="-0.925" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.3" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.6" x2="0.4" y2="1.6" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="LUMILED+">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; with cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="1">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LUMILED">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; without cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LED10MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
10 mm, round</description>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.254" layer="21" curve="-306.869898"/>
<wire x1="4.445" y1="0" x2="0" y2="-4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="3.81" y1="0" x2="0" y2="-3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="3.175" y1="0" x2="0" y2="-3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.445" y1="0" x2="0" y2="4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.81" y1="0" x2="0" y2="3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="0" x2="0" y2="3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="0" y2="2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="21"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="6.35" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KA-3528ASYC">
<description>&lt;b&gt;SURFACE MOUNT LED LAMP&lt;/b&gt; 3.5x2.8mm&lt;p&gt;
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<wire x1="-1.55" y1="1.35" x2="1.55" y2="1.35" width="0.1016" layer="21"/>
<wire x1="1.55" y1="1.35" x2="1.55" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-1.35" x2="-1.55" y2="-1.35" width="0.1016" layer="21"/>
<wire x1="-1.55" y1="-1.35" x2="-1.55" y2="1.35" width="0.1016" layer="51"/>
<wire x1="-0.65" y1="0.95" x2="0.65" y2="0.95" width="0.1016" layer="21" curve="-68.40813"/>
<wire x1="0.65" y1="-0.95" x2="-0.65" y2="-0.95" width="0.1016" layer="21" curve="-68.40813"/>
<circle x="0" y="0" radius="1.15" width="0.1016" layer="51"/>
<smd name="A" x="-1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<smd name="C" x="1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.75" y1="0.6" x2="-1.6" y2="1.1" layer="51"/>
<rectangle x1="-1.75" y1="-1.1" x2="-1.6" y2="-0.6" layer="51"/>
<rectangle x1="1.6" y1="-1.1" x2="1.75" y2="-0.6" layer="51" rot="R180"/>
<rectangle x1="1.6" y1="0.6" x2="1.75" y2="1.1" layer="51" rot="R180"/>
<polygon width="0.1016" layer="51">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-0.625"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="21">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-1.175"/>
<vertex x="1" y="-1.175"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
</package>
<package name="SML0805">
<description>&lt;b&gt;SML0805-2CW-TR (0805 PROFILE)&lt;/b&gt; COOL WHITE&lt;p&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0093.pdf</description>
<wire x1="-0.95" y1="-0.55" x2="0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="-0.55" x2="0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="0.55" x2="-0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="-0.95" y1="0.55" x2="-0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="-0.175" y1="-0.025" x2="0" y2="0.15" width="0.0634" layer="21"/>
<wire x1="0" y1="0.15" x2="0.15" y2="0" width="0.0634" layer="21"/>
<wire x1="0.15" y1="0" x2="-0.025" y2="-0.175" width="0.0634" layer="21"/>
<wire x1="-0.025" y1="-0.175" x2="-0.175" y2="-0.025" width="0.0634" layer="21"/>
<circle x="-0.275" y="0.4" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SML1206">
<description>&lt;b&gt;SML10XXKH-TR (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;SML10R3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10E3KH-TR&lt;/td&gt;&lt;td&gt;SUPER REDSUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10O3KH-TR&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PY3KH-TR&lt;/td&gt;&lt;td&gt;PURE YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10OY3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10AG3KH-TR&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10BG3KH-TR&lt;/td&gt;&lt;td&gt;BLUE GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PB1KH-TR&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10CW1KH-TR&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;

Source: http://www.ledtronics.com/ds/smd-1206/dstr0094.PDF</description>
<wire x1="-1.5" y1="0.5" x2="-1.5" y2="-0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="1.5" y1="-0.5" x2="1.5" y2="0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<circle x="-0.725" y="0.525" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="0.4" x2="-1.15" y2="0.8" layer="51"/>
<rectangle x1="-1.6" y1="-0.8" x2="-1.15" y2="-0.4" layer="51"/>
<rectangle x1="-1.175" y1="-0.6" x2="-1" y2="-0.275" layer="51"/>
<rectangle x1="1.15" y1="-0.8" x2="1.6" y2="-0.4" layer="51" rot="R180"/>
<rectangle x1="1.15" y1="0.4" x2="1.6" y2="0.8" layer="51" rot="R180"/>
<rectangle x1="1" y1="0.275" x2="1.175" y2="0.6" layer="51" rot="R180"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
</package>
<package name="SML0603">
<description>&lt;b&gt;SML0603-XXX (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;AG3K&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;B1K&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R1K&lt;/td&gt;&lt;td&gt;SUPER RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R3K&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3K&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3KH&lt;/td&gt;&lt;td&gt;SOFT ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3KH&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3K&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;2CW&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0092.pdf</description>
<wire x1="-0.75" y1="0.35" x2="0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="0.35" x2="0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="-0.35" x2="-0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="-0.35" x2="-0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.3" x2="-0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="0.45" y1="0.3" x2="0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="0.35" x2="0.2" y2="0.35" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-0.35" x2="0.2" y2="-0.35" width="0.1016" layer="21"/>
<smd name="C" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4" y1="0.175" x2="0" y2="0.4" layer="51"/>
<rectangle x1="-0.25" y1="0.175" x2="0" y2="0.4" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;

- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K
&lt;p&gt;
Source: http://www.osram.convergy.de&lt;p&gt;

&lt;u&gt;LUXEON:&lt;/u&gt;&lt;br&gt;
- &lt;u&gt;LUMILED®&lt;/u&gt;&lt;br&gt;
LXK2-PW12-R00, LXK2-PW12-S00, LXK2-PW14-U00, LXK2-PW14-V00&lt;br&gt;
LXK2-PM12-R00, LXK2-PM12-S00, LXK2-PM14-U00&lt;br&gt;
LXK2-PE12-Q00, LXK2-PE12-R00, LXK2-PE12-S00, LXK2-PE14-T00, LXK2-PE14-U00&lt;br&gt;
LXK2-PB12-K00, LXK2-PB12-L00, LXK2-PB12-M00, LXK2-PB14-N00, LXK2-PB14-P00, LXK2-PB14-Q00&lt;br&gt;
LXK2-PR12-L00, LXK2-PR12-M00, LXK2-PR14-Q00, LXK2-PR14-R00&lt;br&gt;
LXK2-PD12-Q00, LXK2-PD12-R00, LXK2-PD12-S00&lt;br&gt;
LXK2-PH12-R00, LXK2-PH12-S00&lt;br&gt;
LXK2-PL12-P00, LXK2-PL12-Q00, LXK2-PL12-R00
&lt;p&gt;
Source: www.luxeon.com&lt;p&gt;

&lt;u&gt;KINGBRIGHT:&lt;/U&gt;&lt;p&gt;
KA-3528ASYC&lt;br&gt;
Source: www.kingbright.com</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED+" package="LUMILED+">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED" package="LUMILED">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA-3528ASYC" package="KA-3528ASYC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0805" package="SML0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML1206" package="SML1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0603" package="SML0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="crystal">
<description>&lt;b&gt;Crystals and Crystal Resonators&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="HC49/S">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.048" y1="-2.159" x2="3.048" y2="-2.159" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="3.048" y2="2.159" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="-1.651" x2="3.048" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="3.048" y1="1.651" x2="-3.048" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="-0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="-0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.762" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="-3.048" y2="-2.159" width="0.4064" layer="21" curve="180"/>
<wire x1="3.048" y1="-2.159" x2="3.048" y2="2.159" width="0.4064" layer="21" curve="180"/>
<wire x1="-3.048" y1="1.651" x2="-3.048" y2="-1.651" width="0.1524" layer="21" curve="180"/>
<wire x1="3.048" y1="-1.651" x2="3.048" y2="1.651" width="0.1524" layer="21" curve="180"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.937" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.445" y1="-2.54" x2="4.445" y2="2.54" layer="43"/>
<rectangle x1="-5.08" y1="-1.905" x2="-4.445" y2="1.905" layer="43"/>
<rectangle x1="-5.715" y1="-1.27" x2="-5.08" y2="1.27" layer="43"/>
<rectangle x1="4.445" y1="-1.905" x2="5.08" y2="1.905" layer="43"/>
<rectangle x1="5.08" y1="-1.27" x2="5.715" y2="1.27" layer="43"/>
</package>
<package name="HC49GW">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-6.35" x2="5.08" y2="-6.35" width="0.8128" layer="21"/>
<wire x1="4.445" y1="6.731" x2="1.016" y2="6.731" width="0.1524" layer="21"/>
<wire x1="1.016" y1="6.731" x2="-1.016" y2="6.731" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="6.731" x2="-4.445" y2="6.731" width="0.1524" layer="21"/>
<wire x1="4.445" y1="6.731" x2="5.08" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="6.096" x2="-4.445" y2="6.731" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="5.08" x2="-0.3302" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="2.54" x2="0.3048" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="2.54" x2="0.3048" y2="5.08" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.08" x2="-0.3302" y2="5.08" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="5.08" x2="0.9398" y2="3.81" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="5.08" x2="-0.9398" y2="3.81" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="3.81" x2="2.032" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="3.81" x2="0.9398" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="3.81" x2="-2.032" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="3.81" x2="-0.9398" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-6.604" x2="-2.413" y2="-8.255" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-6.477" x2="2.413" y2="-8.382" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-6.35" x2="5.08" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.096" x2="-5.08" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="0" y1="8.382" x2="0" y2="6.985" width="0.6096" layer="51"/>
<smd name="1" x="-2.413" y="-8.001" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="2.413" y="-8.001" dx="1.27" dy="2.54" layer="1"/>
<smd name="3" x="0" y="8.001" dx="1.27" dy="2.794" layer="1"/>
<text x="-5.588" y="-5.08" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-8.255" x2="5.715" y2="8.255" layer="43"/>
</package>
<package name="HC49TL-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.334" y1="-5.715" x2="-5.461" y2="-5.715" width="0.8128" layer="21"/>
<wire x1="4.445" y1="7.366" x2="1.143" y2="7.366" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="7.366" x2="-4.445" y2="7.366" width="0.1524" layer="21"/>
<wire x1="4.445" y1="7.366" x2="5.08" y2="6.731" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="6.731" x2="-4.445" y2="7.366" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="5.715" x2="-0.3302" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="3.175" x2="0.3048" y2="3.175" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="3.175" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="5.715" x2="0.9398" y2="4.445" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="5.715" x2="-0.9398" y2="4.445" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="4.445" x2="2.032" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="4.445" x2="0.9398" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="4.445" x2="-2.032" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="4.445" x2="-0.9398" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-5.842" x2="-2.413" y2="-7.62" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-5.842" x2="2.413" y2="-7.62" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-5.715" x2="5.08" y2="6.731" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.731" x2="-5.08" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="1.143" y1="7.366" x2="-1.143" y2="7.366" width="0.1524" layer="51"/>
<wire x1="0" y1="7.874" x2="0" y2="7.62" width="0.6096" layer="51"/>
<pad name="1" x="-2.413" y="-7.62" drill="0.8128"/>
<pad name="2" x="2.413" y="-7.62" drill="0.8128"/>
<pad name="3" x="0" y="7.874" drill="0.8128"/>
<text x="-5.461" y="-4.445" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-4.699" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.3048" y1="7.366" x2="0.3048" y2="7.5692" layer="51"/>
<rectangle x1="-6.35" y1="-6.985" x2="6.35" y2="-4.445" layer="43"/>
<rectangle x1="-5.715" y1="-4.445" x2="5.715" y2="8.255" layer="43"/>
</package>
<package name="HC49U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="9.906" x2="-4.445" y2="9.906" width="0.1524" layer="21"/>
<wire x1="4.445" y1="9.906" x2="5.08" y2="9.271" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.271" x2="-4.445" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="1.905" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-1.905" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-3.302" x2="-2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-3.302" x2="2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-3.175" x2="5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="9.271" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.413" y="-5.08" drill="0.8128"/>
<text x="-5.461" y="-1.397" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-6.35" y1="-4.445" x2="6.35" y2="-1.905" layer="43"/>
<rectangle x1="-5.715" y1="-1.905" x2="5.715" y2="10.795" layer="43"/>
</package>
<package name="HC49U-LM">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="9.906" x2="-4.445" y2="9.906" width="0.1524" layer="21"/>
<wire x1="4.445" y1="9.906" x2="5.08" y2="9.271" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.271" x2="-4.445" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.572" y1="3.81" x2="4.572" y2="3.81" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-3.302" x2="-2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-3.302" x2="2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.048" x2="5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.572" x2="5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="4.572" y1="3.81" x2="5.08" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="3.048" width="0.1524" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="4.572" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="3.81" x2="-5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="3.175" width="0.1524" layer="51"/>
<pad name="1" x="-2.413" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.413" y="-5.08" drill="0.8128"/>
<pad name="M" x="-5.715" y="3.81" drill="0.8128"/>
<pad name="M1" x="5.715" y="3.81" drill="0.8128"/>
<text x="-5.08" y="10.414" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.572" y="0" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-5.08" x2="5.715" y2="10.795" layer="43"/>
</package>
<package name="HC49U-V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-2.921" y1="-2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="2.286" x2="2.921" y2="2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="-1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="-2.921" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21" curve="-180"/>
<wire x1="2.921" y1="2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21" curve="-180"/>
<wire x1="-2.921" y1="2.286" x2="-2.921" y2="-2.286" width="0.4064" layer="21" curve="180"/>
<wire x1="-2.921" y1="1.778" x2="-2.921" y2="-1.778" width="0.1524" layer="21" curve="180"/>
<wire x1="-0.254" y1="0.889" x2="0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.889" x2="0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.889" x2="-0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.889" x2="-0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.889" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.889" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-2.794" x2="3.81" y2="2.794" layer="43"/>
<rectangle x1="-4.318" y1="-2.54" x2="-3.81" y2="2.54" layer="43"/>
<rectangle x1="-4.826" y1="-2.286" x2="-4.318" y2="2.286" layer="43"/>
<rectangle x1="-5.334" y1="-1.778" x2="-4.826" y2="1.778" layer="43"/>
<rectangle x1="-5.588" y1="-1.27" x2="-5.334" y2="1.016" layer="43"/>
<rectangle x1="3.81" y1="-2.54" x2="4.318" y2="2.54" layer="43"/>
<rectangle x1="4.318" y1="-2.286" x2="4.826" y2="2.286" layer="43"/>
<rectangle x1="4.826" y1="-1.778" x2="5.334" y2="1.778" layer="43"/>
<rectangle x1="5.334" y1="-1.016" x2="5.588" y2="1.016" layer="43"/>
</package>
<package name="HC49U70">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.048" y1="-2.54" x2="3.048" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="2.54" x2="3.048" y2="2.54" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="-2.032" x2="3.048" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-0.3302" y1="1.016" x2="-0.3302" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.3302" y1="-1.016" x2="0.3048" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="-1.016" x2="0.3048" y2="1.016" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.016" x2="-0.3302" y2="1.016" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="1.016" x2="0.6858" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="1.016" x2="-0.6858" y2="0" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="0" x2="1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="0" x2="0.6858" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="0" x2="-1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="0" x2="-0.6858" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="-2.54" width="0.4064" layer="21" curve="180"/>
<wire x1="3.048" y1="2.54" x2="3.048" y2="-2.54" width="0.4064" layer="21" curve="-180"/>
<wire x1="-3.048" y1="-2.032" x2="-3.048" y2="2.032" width="0.1524" layer="21" curve="-180"/>
<wire x1="3.048" y1="2.032" x2="3.048" y2="-2.032" width="0.1524" layer="21" curve="-180"/>
<wire x1="3.048" y1="2.032" x2="-3.048" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.572" y1="-2.794" x2="-4.064" y2="2.794" layer="43"/>
<rectangle x1="-5.08" y1="-2.54" x2="-4.572" y2="2.54" layer="43"/>
<rectangle x1="-5.588" y1="-2.032" x2="-5.08" y2="2.032" layer="43"/>
<rectangle x1="-5.842" y1="-1.27" x2="-5.588" y2="1.27" layer="43"/>
<rectangle x1="-4.064" y1="-3.048" x2="4.064" y2="3.048" layer="43"/>
<rectangle x1="4.064" y1="-2.794" x2="4.572" y2="2.794" layer="43"/>
<rectangle x1="4.572" y1="-2.54" x2="5.08" y2="2.54" layer="43"/>
<rectangle x1="5.08" y1="-2.032" x2="5.588" y2="2.032" layer="43"/>
<rectangle x1="5.588" y1="-1.27" x2="5.842" y2="1.27" layer="43"/>
</package>
<package name="HC49UP">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.016" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.016" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<text x="-5.715" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-3.048" x2="6.604" y2="3.048" layer="43"/>
</package>
<package name="HC13U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-9.906" y1="-3.048" x2="-9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="-9.271" y1="-3.048" x2="9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="9.271" y1="-3.048" x2="9.906" y2="-3.048" width="1.27" layer="21"/>
<wire x1="8.636" y1="33.401" x2="-8.636" y2="33.401" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="32.766" x2="-8.636" y2="33.401" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.636" y1="33.401" x2="9.271" y2="32.766" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.017" y1="15.24" x2="9.017" y2="15.24" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="21.59" x2="-0.3302" y2="19.05" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="19.05" x2="0.3048" y2="19.05" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="19.05" x2="0.3048" y2="21.59" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="21.59" x2="-0.3302" y2="21.59" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="21.59" x2="0.9398" y2="20.32" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="21.59" x2="-0.9398" y2="20.32" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="20.32" x2="1.905" y2="20.32" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="20.32" x2="0.9398" y2="19.05" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="20.32" x2="-1.905" y2="20.32" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="20.32" x2="-0.9398" y2="19.05" width="0.3048" layer="21"/>
<wire x1="9.144" y1="15.24" x2="10.16" y2="15.24" width="0.6096" layer="51"/>
<wire x1="-10.16" y1="15.24" x2="-9.144" y2="15.24" width="0.6096" layer="51"/>
<wire x1="-6.223" y1="-3.175" x2="-6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="6.223" y1="-3.175" x2="6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="9.271" y1="15.748" x2="9.271" y2="32.766" width="0.1524" layer="21"/>
<wire x1="9.271" y1="14.732" x2="9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="9.271" y1="15.748" x2="9.271" y2="14.732" width="0.1524" layer="51"/>
<wire x1="-9.271" y1="14.732" x2="-9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="15.748" x2="-9.271" y2="32.766" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="15.748" x2="-9.271" y2="14.732" width="0.1524" layer="51"/>
<pad name="1" x="-6.223" y="-5.08" drill="1.016"/>
<pad name="2" x="6.223" y="-5.08" drill="1.016"/>
<pad name="M" x="-10.16" y="15.24" drill="0.8128"/>
<pad name="M1" x="10.16" y="15.24" drill="0.8128"/>
<text x="-10.16" y="0" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-5.08" y="-1.27" size="1.778" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-5.08" x2="10.795" y2="34.925" layer="43"/>
</package>
<package name="HC18U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.461" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="5.08" y1="-3.175" x2="-5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="-5.08" y1="-3.175" x2="-5.461" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="10.16" x2="-4.445" y2="10.16" width="0.1524" layer="21"/>
<wire x1="4.445" y1="10.16" x2="5.08" y2="9.525" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.525" x2="-4.445" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.572" y1="3.81" x2="4.572" y2="3.81" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.048" x2="5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.572" x2="5.08" y2="9.525" width="0.1524" layer="21"/>
<wire x1="4.572" y1="3.81" x2="5.08" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="3.048" width="0.1524" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="4.572" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="3.81" x2="-5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="3.175" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.54" y="-5.08" drill="0.8128"/>
<pad name="M" x="-5.715" y="3.81" drill="0.8128"/>
<pad name="M1" x="5.715" y="3.81" drill="0.8128"/>
<text x="-5.08" y="10.668" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.889" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.35" y1="-5.08" x2="6.35" y2="10.795" layer="43"/>
</package>
<package name="HC18U-V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="1.905" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="4.445" y2="2.54" width="0.4064" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="4.445" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.4064" layer="21" curve="90"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.4064" layer="21" curve="-90"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.4064" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.4064" layer="21" curve="90"/>
<wire x1="-4.318" y1="-1.905" x2="4.318" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.905" x2="4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.778" x2="4.445" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.318" y1="1.905" x2="4.445" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.318" y1="1.905" x2="-4.318" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.778" x2="-4.318" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.778" x2="-4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-1.905" x2="-4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-0.3302" y1="1.27" x2="-0.3302" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="-1.27" x2="0.3048" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="-1.27" x2="0.3048" y2="1.27" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="1.27" x2="-0.3302" y2="1.27" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="1.27" x2="0.9398" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="1.27" x2="-0.9398" y2="0" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="0" x2="0.9398" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="0" x2="-0.9398" y2="-1.27" width="0.3048" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128"/>
<pad name="2" x="2.54" y="0" drill="0.8128"/>
<text x="-5.0546" y="3.2766" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.6228" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-3.175" x2="5.715" y2="3.175" layer="43"/>
</package>
<package name="HC33U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-9.906" y1="-3.048" x2="-9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="-9.271" y1="-3.048" x2="9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="9.271" y1="-3.048" x2="9.906" y2="-3.048" width="1.27" layer="21"/>
<wire x1="8.636" y1="16.51" x2="-8.636" y2="16.51" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="15.875" x2="-8.636" y2="16.51" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.636" y1="16.51" x2="9.271" y2="15.875" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.017" y1="7.62" x2="9.017" y2="7.62" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="13.97" x2="-0.3302" y2="11.43" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="11.43" x2="0.3048" y2="11.43" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="11.43" x2="0.3048" y2="13.97" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="13.97" x2="-0.3302" y2="13.97" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="13.97" x2="0.9398" y2="12.7" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="13.97" x2="-0.9398" y2="12.7" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="12.7" x2="1.905" y2="12.7" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="12.7" x2="0.9398" y2="11.43" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="12.7" x2="-1.905" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="12.7" x2="-0.9398" y2="11.43" width="0.3048" layer="21"/>
<wire x1="9.144" y1="7.62" x2="10.16" y2="7.62" width="0.6096" layer="51"/>
<wire x1="-10.16" y1="7.62" x2="-9.144" y2="7.62" width="0.6096" layer="51"/>
<wire x1="-6.223" y1="-3.175" x2="-6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="6.223" y1="-3.175" x2="6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="9.271" y1="8.128" x2="9.271" y2="15.875" width="0.1524" layer="21"/>
<wire x1="9.271" y1="7.112" x2="9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="9.271" y1="8.128" x2="9.271" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-9.271" y1="7.112" x2="-9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="8.128" x2="-9.271" y2="15.875" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="8.128" x2="-9.271" y2="7.112" width="0.1524" layer="51"/>
<pad name="1" x="-6.223" y="-5.08" drill="1.016"/>
<pad name="2" x="6.223" y="-5.08" drill="1.016"/>
<pad name="M" x="-10.16" y="7.62" drill="0.8128"/>
<pad name="M1" x="10.16" y="7.62" drill="0.8128"/>
<text x="-7.62" y="17.272" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.62" y1="-3.175" x2="-6.985" y2="16.51" layer="21"/>
<rectangle x1="6.985" y1="-3.175" x2="7.62" y2="16.51" layer="21"/>
<rectangle x1="-10.795" y1="-5.715" x2="10.795" y2="17.145" layer="43"/>
</package>
<package name="HC33U-V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-0.3302" y1="2.54" x2="-0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="0" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="2.54" x2="-0.3302" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.9652" y1="2.54" x2="0.9652" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-0.9652" y1="2.54" x2="-0.9652" y2="1.27" width="0.3048" layer="21"/>
<wire x1="0.9652" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.9652" y1="1.27" x2="0.9652" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.9652" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.9652" y1="1.27" x2="-0.9652" y2="0" width="0.3048" layer="21"/>
<wire x1="-5.207" y1="4.064" x2="5.207" y2="4.064" width="0.254" layer="21"/>
<wire x1="-5.207" y1="-4.064" x2="5.207" y2="-4.064" width="0.254" layer="21"/>
<wire x1="-5.207" y1="-3.683" x2="5.207" y2="-3.683" width="0.0508" layer="21"/>
<wire x1="-5.207" y1="3.683" x2="5.207" y2="3.683" width="0.0508" layer="21"/>
<wire x1="-5.207" y1="-3.683" x2="-5.207" y2="3.683" width="0.0508" layer="21" curve="-180"/>
<wire x1="5.207" y1="3.683" x2="5.207" y2="-3.683" width="0.0508" layer="21" curve="-180"/>
<wire x1="5.207" y1="4.064" x2="5.207" y2="-4.064" width="0.254" layer="21" curve="-180"/>
<wire x1="-5.207" y1="4.064" x2="-5.207" y2="-4.064" width="0.254" layer="21" curve="180"/>
<pad name="1" x="-6.223" y="0" drill="1.016"/>
<pad name="2" x="6.223" y="0" drill="1.016"/>
<text x="-5.08" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.255" y1="-3.81" x2="-6.985" y2="3.81" layer="43"/>
<rectangle x1="-8.89" y1="-3.175" x2="-8.255" y2="3.175" layer="43"/>
<rectangle x1="-9.525" y1="-2.54" x2="-8.89" y2="2.54" layer="43"/>
<rectangle x1="-6.985" y1="-4.445" x2="6.985" y2="4.445" layer="43"/>
<rectangle x1="6.985" y1="-3.81" x2="8.255" y2="3.81" layer="43"/>
<rectangle x1="8.255" y1="-3.175" x2="8.89" y2="3.175" layer="43"/>
<rectangle x1="8.89" y1="-2.54" x2="9.525" y2="2.54" layer="43"/>
</package>
<package name="SM49">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-5.461" y1="2.413" x2="5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.159" x2="-5.715" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993"/>
<wire x1="-3.9826" y1="1.143" x2="-3.9826" y2="-1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="-5.1091" y1="1.143" x2="-5.1091" y2="-1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485"/>
<wire x1="3.9826" y1="1.143" x2="3.9826" y2="-1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="6.477" y1="0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.1091" y1="1.143" x2="5.1091" y2="-1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-0.381" x2="5.715" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="2.159" x2="5.715" y2="1.143" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-5.461" y1="-2.413" x2="5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.143" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.08" dy="1.778" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.08" dy="1.778" layer="1"/>
<text x="-5.715" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.064" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-2.54" x2="6.604" y2="2.794" layer="43"/>
</package>
<package name="TC26H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-0.889" y1="1.651" x2="0.889" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.762" y1="7.747" x2="1.016" y2="7.493" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="7.493" x2="-0.762" y2="7.747" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="7.747" x2="0.762" y2="7.747" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.651" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.651" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.27" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.508" y1="4.953" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.953" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="5.334" x2="0" y2="5.334" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.191" x2="0" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0" y2="3.683" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0.508" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0" y2="5.842" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0.508" y2="5.334" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.889" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="-1.397" y="2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.667" y="2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="0.762" x2="1.778" y2="8.382" layer="43"/>
</package>
<package name="TC26V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-0.127" y1="-0.508" x2="0.127" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="0.127" y1="0.508" x2="0.127" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="0.127" y1="0.508" x2="-0.127" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.127" y1="-0.508" x2="-0.127" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-0.508" x2="-0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.508" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.381" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0" y1="1.016" x2="0.7184" y2="0.7184" width="0.1524" layer="21" curve="-44.999323"/>
<wire x1="-0.7184" y1="0.7184" x2="0" y2="1.016" width="0.1524" layer="21" curve="-44.999323"/>
<wire x1="-0.7184" y1="-0.7184" x2="0" y2="-1.016" width="0.1524" layer="21" curve="44.999323"/>
<wire x1="0" y1="-1.016" x2="0.7184" y2="-0.7184" width="0.1524" layer="21" curve="44.999323"/>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="-2.032" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TC38H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-1.397" y1="1.651" x2="1.397" y2="1.651" width="0.1524" layer="21"/>
<wire x1="1.27" y1="9.906" x2="1.524" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="9.652" x2="-1.27" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="9.906" x2="1.27" y2="9.906" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.651" x2="1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.032" x2="1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.032" x2="1.524" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.651" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.032" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.032" x2="-1.524" y2="9.652" width="0.1524" layer="21"/>
<wire x1="1.397" y1="2.032" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.5588" y1="0.7112" x2="0.508" y2="0.762" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.016" width="0.4064" layer="21"/>
<wire x1="-0.5588" y1="0.7112" x2="-0.508" y2="0.762" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="5.588" x2="-0.762" y2="5.207" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.207" x2="-0.762" y2="5.207" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.207" x2="0.762" y2="5.588" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="5.588" x2="0.762" y2="5.588" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="5.969" x2="0" y2="5.969" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="4.826" x2="0" y2="4.826" width="0.1524" layer="21"/>
<wire x1="0" y1="4.826" x2="0" y2="4.318" width="0.1524" layer="21"/>
<wire x1="0" y1="4.826" x2="0.762" y2="4.826" width="0.1524" layer="21"/>
<wire x1="0" y1="5.969" x2="0" y2="6.477" width="0.1524" layer="21"/>
<wire x1="0" y1="5.969" x2="0.762" y2="5.969" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="-1.905" y="2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.175" y="2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="1.016" x2="1.778" y2="10.414" layer="43"/>
</package>
<package name="86SMX">
<description>&lt;b&gt;CRYSTAL RESONATOR&lt;/b&gt;</description>
<wire x1="-3.81" y1="1.905" x2="2.413" y2="1.905" width="0.0508" layer="21"/>
<wire x1="-3.81" y1="2.286" x2="2.413" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="2.413" y2="-1.905" width="0.0508" layer="21"/>
<wire x1="-3.81" y1="-2.286" x2="2.413" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.286" x2="-6.604" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.334" y1="1.905" x2="-5.334" y2="1.016" width="0.0508" layer="51"/>
<wire x1="-5.334" y1="-1.905" x2="-3.81" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="-2.286" x2="-5.969" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-2.286" x2="-4.191" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-2.286" x2="-3.81" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-2.54" x2="-4.191" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-2.2098" x2="-6.604" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="-6.604" y1="-2.286" x2="-6.35" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-2.54" x2="-5.969" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.016" x2="-5.334" y2="-1.016" width="0.0508" layer="21"/>
<wire x1="-5.334" y1="-1.016" x2="-5.334" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="-5.334" y1="-1.905" x2="-6.35" y2="-2.2098" width="0.0508" layer="51"/>
<wire x1="-4.191" y1="-2.54" x2="-4.191" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="2.54" x2="-4.191" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.905" x2="-3.81" y2="1.905" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="2.286" x2="-5.969" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="2.286" x2="-4.191" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="2.286" x2="-3.81" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-6.604" y1="2.286" x2="-6.35" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="2.2098" x2="-6.604" y2="2.286" width="0.0508" layer="21"/>
<wire x1="-5.969" y1="2.54" x2="-5.969" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.905" x2="-6.35" y2="2.2098" width="0.0508" layer="51"/>
<wire x1="-4.191" y1="2.54" x2="-4.191" y2="2.286" width="0.1524" layer="51"/>
<wire x1="6.604" y1="2.286" x2="6.604" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-1.905" x2="6.223" y2="1.905" width="0.0508" layer="21"/>
<wire x1="6.223" y1="-1.905" x2="6.604" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="6.604" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-2.54" x2="5.842" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="2.794" y1="-2.286" x2="2.794" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="5.842" y1="-2.54" x2="5.842" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-2.286" x2="6.223" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.905" x2="6.223" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="6.223" y1="1.905" x2="6.604" y2="2.286" width="0.0508" layer="21"/>
<wire x1="6.223" y1="2.286" x2="6.604" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="2.54" x2="5.842" y2="2.54" width="0.1524" layer="51"/>
<wire x1="2.794" y1="2.286" x2="2.794" y2="2.54" width="0.1524" layer="51"/>
<wire x1="5.842" y1="2.54" x2="5.842" y2="2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="1.905" x2="6.223" y2="1.905" width="0.0508" layer="51"/>
<wire x1="2.413" y1="2.286" x2="6.223" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.651" x2="-0.254" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="0.381" x2="0.254" y2="0.381" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.381" x2="0.254" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.254" y1="1.651" x2="-0.254" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.651" x2="0.635" y2="1.016" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0.381" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.016" x2="1.016" y2="1.016" width="0.0508" layer="21"/>
<wire x1="-0.635" y1="1.651" x2="-0.635" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.016" x2="-0.635" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.016" x2="-1.016" y2="1.016" width="0.0508" layer="21"/>
<smd name="2" x="4.318" y="-2.286" dx="3.556" dy="2.1844" layer="1"/>
<smd name="3" x="4.318" y="2.286" dx="3.556" dy="2.1844" layer="1"/>
<smd name="1" x="-5.08" y="-2.286" dx="2.286" dy="2.1844" layer="1"/>
<smd name="4" x="-5.08" y="2.286" dx="2.286" dy="2.1844" layer="1"/>
<text x="-3.683" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.683" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MM20SS">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-2.032" y1="-1.27" x2="2.032" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-1.778" x2="2.032" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.032" y1="1.27" x2="-2.032" y2="1.27" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="1.778" x2="2.032" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.778" x2="-4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="0.381" x2="-2.921" y2="-0.381" width="0.0508" layer="21"/>
<wire x1="-4.064" y1="-1.778" x2="-3.556" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.552" x2="-4.064" y2="-1.778" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="1.27" x2="-2.921" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-2.921" y1="1.27" x2="-2.921" y2="0.381" width="0.0508" layer="51"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-1.905" x2="-3.048" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-0.381" x2="-2.921" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="-2.921" y1="-1.27" x2="-2.032" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="-3.556" y1="-1.778" x2="-2.032" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-1.27" x2="-3.556" y2="-1.552" width="0.0508" layer="51"/>
<wire x1="-4.064" y1="1.778" x2="-3.556" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="1.552" x2="-4.064" y2="1.778" width="0.0508" layer="21"/>
<wire x1="-2.921" y1="1.27" x2="-3.556" y2="1.552" width="0.0508" layer="51"/>
<wire x1="-3.048" y1="1.778" x2="-3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="1.905" x2="-2.54" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="1.778" x2="-2.032" y2="1.778" width="0.1524" layer="51"/>
<wire x1="4.064" y1="-1.778" x2="4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="4.064" y2="-1.778" width="0.0508" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="3.81" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.556" y1="-1.778" x2="4.064" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.556" y2="1.27" width="0.0508" layer="21"/>
<wire x1="3.556" y1="1.27" x2="2.032" y2="1.27" width="0.0508" layer="51"/>
<wire x1="3.048" y1="-1.905" x2="3.048" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-1.778" x2="2.54" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-1.905" x2="3.048" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="2.032" y1="-1.27" x2="3.556" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="2.032" y1="-1.778" x2="3.556" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="3.81" y1="1.27" x2="4.064" y2="1.778" width="0.0508" layer="21"/>
<wire x1="3.556" y1="1.778" x2="4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="1.778" width="0.1524" layer="51"/>
<wire x1="3.048" y1="1.778" x2="3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="2.54" y1="1.905" x2="3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="2.032" y1="1.778" x2="3.556" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="-0.254" x2="-0.508" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-0.254" x2="-0.508" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.254" x2="-1.778" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.254" x2="-1.778" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.635" x2="-1.143" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.635" x2="-0.508" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.635" x2="-1.143" y2="1.016" width="0.0508" layer="21"/>
<wire x1="-1.778" y1="-0.635" x2="-1.143" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-0.508" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="-1.016" width="0.0508" layer="21"/>
<circle x="3.048" y="0" radius="0.254" width="0.1524" layer="21"/>
<smd name="1" x="-2.794" y="-1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="2" x="2.794" y="-1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="3" x="2.794" y="1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="4" x="-2.794" y="1.524" dx="1.27" dy="1.8796" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MM39SL">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.683" y1="-1.651" x2="3.683" y2="-1.651" width="0.0508" layer="21"/>
<wire x1="-3.683" y1="-2.286" x2="3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="1.651" x2="-3.683" y2="1.651" width="0.0508" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="1.651" x2="-4.826" y2="0.762" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="-2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="-1.651" x2="-3.683" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="-2.055" x2="-6.223" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.715" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="0.762" x2="-4.826" y2="-0.762" width="0.0508" layer="21"/>
<wire x1="-4.826" y1="-0.762" x2="-4.826" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="-4.826" y1="-1.651" x2="-5.715" y2="-2.055" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="2.286" x2="-3.683" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-3.683" y1="1.651" x2="-4.826" y2="1.651" width="0.0508" layer="51"/>
<wire x1="-6.223" y1="2.286" x2="-5.715" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.055" x2="-6.223" y2="2.286" width="0.0508" layer="21"/>
<wire x1="-4.826" y1="1.651" x2="-5.715" y2="2.055" width="0.0508" layer="51"/>
<wire x1="6.223" y1="-2.286" x2="6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-1.651" x2="5.842" y2="1.651" width="0.0508" layer="21"/>
<wire x1="5.842" y1="-1.651" x2="6.223" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="5.715" y1="-2.286" x2="6.223" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="5.715" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.651" x2="5.842" y2="-1.651" width="0.0508" layer="21"/>
<wire x1="3.683" y1="-1.651" x2="5.715" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="5.842" y1="1.651" x2="6.223" y2="2.286" width="0.0508" layer="21"/>
<wire x1="5.842" y1="1.651" x2="5.715" y2="1.651" width="0.0508" layer="21"/>
<wire x1="5.715" y1="2.286" x2="6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="2.286" x2="5.715" y2="2.286" width="0.1524" layer="51"/>
<wire x1="5.715" y1="1.651" x2="3.683" y2="1.651" width="0.0508" layer="51"/>
<wire x1="-3.81" y1="-0.254" x2="-2.54" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.254" x2="-2.54" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.254" x2="-3.81" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.254" x2="-3.81" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="1.016" width="0.1016" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-0.635" x2="-3.175" y2="-1.016" width="0.1016" layer="21"/>
<circle x="5.08" y="0" radius="0.254" width="0.1524" layer="21"/>
<smd name="1" x="-4.699" y="-1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="2" x="4.699" y="-1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="3" x="4.699" y="1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="4" x="-4.699" y="1.778" dx="1.778" dy="1.778" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CTS406">
<description>&lt;b&gt;Model 406 6.0x3.5mm Low Cost Surface Mount Crystal&lt;/b&gt;&lt;p&gt;
Source: 008-0260-0_E.pdf</description>
<wire x1="-2.475" y1="1.65" x2="-1.05" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-1.05" y1="1.65" x2="1.05" y2="1.65" width="0.2032" layer="21"/>
<wire x1="1.05" y1="1.65" x2="2.475" y2="1.65" width="0.2032" layer="51"/>
<wire x1="2.9" y1="1.225" x2="2.9" y2="0.3" width="0.2032" layer="51"/>
<wire x1="2.9" y1="0.3" x2="2.9" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="2.9" y1="-0.3" x2="2.9" y2="-1.225" width="0.2032" layer="51"/>
<wire x1="2.475" y1="-1.65" x2="1.05" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="1.05" y1="-1.65" x2="-1.05" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-1.05" y1="-1.65" x2="-2.475" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="-1.225" x2="-2.9" y2="-0.3" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="-0.3" x2="-2.9" y2="0.3" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="0.3" x2="-2.9" y2="1.225" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="1.225" x2="-2.475" y2="1.65" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="2.475" y1="1.65" x2="2.9" y2="1.225" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="2.9" y1="-1.225" x2="2.475" y2="-1.65" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="-2.475" y1="-1.65" x2="-2.9" y2="-1.225" width="0.2032" layer="51" curve="89.516721"/>
<circle x="-2.05" y="-0.2" radius="0.182" width="0" layer="21"/>
<smd name="1" x="-2.2" y="-1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="2" x="2.2" y="-1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="3" x="2.2" y="1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="4" x="-2.2" y="1.2" dx="1.9" dy="1.4" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="Q">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRYSTAL" prefix="Q" uservalue="yes">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="Q" x="0" y="0"/>
</gates>
<devices>
<device name="HC49S" package="HC49/S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1667008" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49GW" package="HC49GW">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49TL-H" package="HC49TL-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49U-H" package="HC49U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1666973" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49U-LM" package="HC49U-LM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1666956" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49U-V" package="HC49U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1666969" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49U70" package="HC49U70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49UP" package="HC49UP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC13U-H" package="HC13U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC18U-H" package="HC18U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC18U-V" package="HC18U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC33U-H" package="HC33U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC33U-V" package="HC33U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="SM49" package="SM49">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="TC26H" package="TC26H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="TC26V" package="TC26V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="TC38H" package="TC38H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="68SMX" package="86SMX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="6344860" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="MM20SS" package="MM20SS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="MM39SL" package="MM39SL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="CTS406" package="CTS406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+07V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+7V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+09V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+9V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+05V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+7V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+07V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+9V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+09V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="+5V" symbol="+05V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" deviceset="DINA3_L" device="" value="MCU SECTION"/>
<part name="FRAME2" library="frames" deviceset="A4L-LOC" device="" value="USBTOUART SECTION"/>
<part name="FRAME3" library="frames" deviceset="A3L-LOC" device="" value="SENSOR SECTION"/>
<part name="FRAME4" library="frames" deviceset="A3L-LOC" device="" value="COMMUNICATION SECTION"/>
<part name="FRAME5" library="frames" deviceset="FRAME_A_L" device="" value="POWER SECTION"/>
<part name="SOLAR" library="adafruit" deviceset="DCBARREL" device="PTH" value="SOLAR"/>
<part name="ADAPTER" library="adafruit" deviceset="DCBARREL" device="PTH" value="DC"/>
<part name="USB" library="SparkFun-Connectors" deviceset="USB-MINIB" device="-5PIN"/>
<part name="BQ24210" library="weatherpilot_lbr" deviceset="BQ24210_DQC_10" device=""/>
<part name="C1" library="SparkFun-Passives" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="RISET" library="SparkFun-Passives" deviceset="RESISTOR" device="0402-RES"/>
<part name="RT1" library="SparkFun-Passives" deviceset="RESISTOR" device="0402-RES"/>
<part name="R_TEMP" library="SparkFun-Passives" deviceset="RESISTOR" device="0402-RES"/>
<part name="C2" library="SparkFun-Passives" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="R_CHG" library="SparkFun-Passives" deviceset="RESISTOR" device="0402-RES" value="220"/>
<part name="R_PG" library="SparkFun-Passives" deviceset="RESISTOR" device="0402-RES" value="220"/>
<part name="LED_CHG" library="adafruit" deviceset="LED" device="CHIP-LED0603" value="CHG"/>
<part name="LED_PG" library="adafruit" deviceset="LED" device="CHIP-LED0603" value="PG"/>
<part name="C3" library="SparkFun-Passives" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="U$5" library="weatherpilot_lbr" deviceset="SI702X" device=""/>
<part name="BLE112" library="weatherpilot_lbr" deviceset="BLE112-A-VISUAL" device=""/>
<part name="RAK410" library="weatherpilot_lbr" deviceset="RAK410" device=""/>
<part name="IC1" library="weatherpilot_lbr" deviceset="SIM900" device=""/>
<part name="U$7" library="weatherpilot_lbr" deviceset="CP2104-GM" device=""/>
<part name="U5" library="weatherpilot_lbr" deviceset="ATMEGA2560" device="AU"/>
<part name="R12" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="10k"/>
<part name="R13" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="100k"/>
<part name="R14" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="10k"/>
<part name="R15" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="10k"/>
<part name="C13" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="C14" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="10uF"/>
<part name="C15" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.47uF"/>
<part name="C16" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.47uF"/>
<part name="R16" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="10k"/>
<part name="X1" library="weatherpilot_lbr" deviceset="PHB_1MM27_2X05_*" device="SNS"/>
<part name="BLE_LED" library="weatherpilot_lbr" deviceset="LED" device="0603" value="BLE_LED"/>
<part name="R17" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="10k"/>
<part name="U$10" library="weatherpilot_lbr" deviceset="SIM-HOLDER-HINGED" device=""/>
<part name="C17" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="R26" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="22"/>
<part name="R27" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="22"/>
<part name="R28" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="22"/>
<part name="R29" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="10k"/>
<part name="C19" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="NET_T" library="semicon-smd-ipc" deviceset="NPN-TRANSISTOR_" device="SOT23" value="GSM_T"/>
<part name="R30" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="220"/>
<part name="R31" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="GSM_NET" library="weatherpilot_lbr" deviceset="LED" device="0603" value="GSM_NET"/>
<part name="R32" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="C24" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="C25" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="R33" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="100"/>
<part name="RF-SMA" library="weatherpilot_lbr" deviceset="RF-SMA" device=""/>
<part name="C26" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="C27" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="C28" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="47uF"/>
<part name="R34" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="4k7"/>
<part name="T1" library="zetex" deviceset="PMOSSOT23" device="" value="GSM_T"/>
<part name="R35" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="C29" library="weatherpilot_lbr" deviceset="CAP_POL" device="PTH4" value="1000uF"/>
<part name="C30" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="C31" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="SDM03MT40_2" library="weatherpilot_lbr" deviceset="SDM03MT40-7-F" device="" value="SDM03Mt40-7-F"/>
<part name="CP_RX_LED" library="weatherpilot_lbr" deviceset="LED" device="0603" value="RX"/>
<part name="CP_TX_LED" library="weatherpilot_lbr" deviceset="LED" device="0603" value="TX"/>
<part name="R44" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="220"/>
<part name="R45" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="220"/>
<part name="C21" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="R46" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES"/>
<part name="RESET" library="weatherpilot_lbr" deviceset="SPST_TACT" device="-EVQQ2"/>
<part name="R1" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="R7" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="220"/>
<part name="GSMPWR" library="weatherpilot_lbr" deviceset="LED" device="0603" value="GSM_PWR"/>
<part name="XC6221A332MR-G" library="weatherpilot_lbr" deviceset="XC6221A332MR-G" device=""/>
<part name="C4" library="SparkFun-Passives" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="C5" library="SparkFun-Passives" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="C6" library="SparkFun-Passives" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="C7" library="SparkFun-Passives" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="J1" library="SparkFun-Connectors" deviceset="AVR_SPI_PRG_6" device="PTH"/>
<part name="LI_BAT_INPUT" library="SparkFun-Connectors" deviceset="M02" device="JST-PTH-VERT"/>
<part name="I2C_JP" library="SparkFun-Connectors" deviceset="M04" device="JST-PTH-VERT"/>
<part name="J2" library="SparkFun-Connectors" deviceset="AVR_SPI_PRG_6" device="PTH"/>
<part name="PL_PORT" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="PG_PORT" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="PF_PORT" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="PC_PORT" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="PB_PORT" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="PJ_PORT" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="PH_PORT" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="PE_PORT" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="PD_PORT" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="PA_PORT" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="ADC_HIGH_PORT" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="POWER_JP" library="SparkFun-Connectors" deviceset="M02" device="JST-PTH-VERT"/>
<part name="U$8" library="weatherpilot_lbr" deviceset="SDMMC" device="06132"/>
<part name="R2" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="R3" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="R4" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="R5" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="R6" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="CD" library="led" deviceset="LED" device="CHIPLED_0603" value="CD"/>
<part name="WP" library="led" deviceset="LED" device="CHIPLED_0603" value="WP"/>
<part name="R8" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES"/>
<part name="R9" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES"/>
<part name="DS1307" library="weatherpilot_lbr" deviceset="DS1307SOIC" device=""/>
<part name="R10" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="R11" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="R18" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="R19" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="Q1" library="crystal" deviceset="CRYSTAL" device="TC26H" value="32.768kH"/>
<part name="VBAT1" library="weatherpilot_lbr" deviceset="BATTERY_HOLDER" device="" value="RTC_COIN"/>
<part name="R20" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="BLE_PORT2" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="BLE_PORT1" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="BLE_PORT0" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="RAK_UART" library="SparkFun-Connectors" deviceset="M04" device="SMD2" value="RAK_JP"/>
<part name="GSM_UART" library="SparkFun-Connectors" deviceset="M04" device="SMD2" value="GSM_JP"/>
<part name="CP_UART" library="SparkFun-Connectors" deviceset="M04" device="SMD2" value="CP_JP"/>
<part name="SIM_PROTECTION_DIODE" library="weatherpilot_lbr" deviceset="SDM03MT40-7-F" device=""/>
<part name="SDM03MT40" library="weatherpilot_lbr" deviceset="SDM03MT40-7-F" device=""/>
<part name="RAK_FRM_UPD_JP" library="SparkFun-Connectors" deviceset="M08" device="BM08B-SRSS-TB"/>
<part name="SUPPLY1" library="supply2" deviceset="+7V" device="" value="+7V"/>
<part name="SUPPLY2" library="supply2" deviceset="+9V" device=""/>
<part name="SUPPLY3" library="supply2" deviceset="+5V" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="R21" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="BLE_USB" library="SparkFun-Connectors" deviceset="M04" device="SMD2"/>
<part name="C8" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="C9" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="C10" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="C11" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="C12" library="weatherpilot_lbr" deviceset="CAP" device="0402-CAP" value="0.1uF"/>
<part name="R22" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="R23" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="R24" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="R25" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="1k"/>
<part name="R36" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="10k"/>
<part name="R37" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="5k6"/>
<part name="R38" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="10k"/>
<part name="R39" library="weatherpilot_lbr" deviceset="RESISTOR" device="0402-RES" value="12k"/>
</parts>
<sheets>
<sheet>
<description>MCU Section</description>
<plain>
<wire x1="1.27" y1="262.89" x2="153.67" y2="262.89" width="1.27" layer="94" style="longdash"/>
<wire x1="153.67" y1="262.89" x2="386.08" y2="262.89" width="1.27" layer="94" style="longdash"/>
<wire x1="386.08" y1="262.89" x2="386.08" y2="165.1" width="1.27" layer="94" style="longdash"/>
<wire x1="386.08" y1="165.1" x2="386.08" y2="36.83" width="1.27" layer="94" style="longdash"/>
<wire x1="386.08" y1="36.83" x2="285.75" y2="36.83" width="1.27" layer="94" style="longdash"/>
<wire x1="285.75" y1="36.83" x2="285.75" y2="1.27" width="1.27" layer="94" style="longdash"/>
<wire x1="285.75" y1="1.27" x2="153.67" y2="1.27" width="1.27" layer="94" style="longdash"/>
<wire x1="153.67" y1="1.27" x2="1.27" y2="1.27" width="1.27" layer="94" style="longdash"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="262.89" width="1.27" layer="94" style="longdash"/>
<wire x1="153.67" y1="262.89" x2="153.67" y2="124.46" width="1.27" layer="94" style="longdash"/>
<wire x1="153.67" y1="124.46" x2="153.67" y2="66.04" width="1.27" layer="94" style="longdash"/>
<wire x1="153.67" y1="66.04" x2="153.67" y2="1.27" width="1.27" layer="94" style="longdash"/>
<wire x1="386.08" y1="165.1" x2="287.02" y2="165.1" width="1.27" layer="94"/>
<wire x1="287.02" y1="165.1" x2="287.02" y2="137.16" width="1.27" layer="94"/>
<wire x1="287.02" y1="137.16" x2="259.08" y2="137.16" width="1.27" layer="94"/>
<wire x1="259.08" y1="137.16" x2="259.08" y2="125.73" width="1.27" layer="94"/>
<wire x1="259.08" y1="125.73" x2="259.08" y2="38.1" width="1.27" layer="94"/>
<wire x1="259.08" y1="38.1" x2="285.75" y2="38.1" width="1.27" layer="94"/>
<wire x1="285.75" y1="38.1" x2="285.75" y2="36.83" width="1.27" layer="94"/>
<wire x1="259.08" y1="125.73" x2="153.67" y2="125.73" width="1.27" layer="94"/>
<wire x1="153.67" y1="125.73" x2="153.67" y2="124.46" width="1.27" layer="94"/>
<wire x1="153.67" y1="38.1" x2="153.67" y2="66.04" width="1.27" layer="94"/>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FRAME1" gate="G$2" x="287.02" y="0"/>
<instance part="U5" gate="1" x="80.01" y="137.16"/>
<instance part="R46" gate="G$1" x="41.91" y="218.44" rot="R90"/>
<instance part="RESET" gate="G$1" x="31.75" y="223.52" rot="R270"/>
<instance part="J1" gate="G$1" x="82.55" y="19.05"/>
<instance part="J2" gate="G$1" x="82.55" y="35.56"/>
<instance part="PL_PORT" gate="G$1" x="180.34" y="227.33"/>
<instance part="PG_PORT" gate="G$1" x="200.66" y="227.33"/>
<instance part="PF_PORT" gate="G$1" x="237.49" y="226.06"/>
<instance part="PC_PORT" gate="G$1" x="271.78" y="226.06"/>
<instance part="PB_PORT" gate="G$1" x="293.37" y="226.06"/>
<instance part="PJ_PORT" gate="G$1" x="180.34" y="198.12"/>
<instance part="PH_PORT" gate="G$1" x="208.28" y="199.39"/>
<instance part="PE_PORT" gate="G$1" x="241.3" y="199.39"/>
<instance part="PD_PORT" gate="G$1" x="276.86" y="198.12"/>
<instance part="PA_PORT" gate="G$1" x="228.6" y="166.37"/>
<instance part="ADC_HIGH_PORT" gate="G$1" x="262.89" y="166.37"/>
<instance part="POWER_JP" gate="G$1" x="179.07" y="171.45"/>
<instance part="U$8" gate="G$1" x="345.44" y="93.98"/>
<instance part="R2" gate="G$1" x="309.88" y="106.68" rot="R90"/>
<instance part="R3" gate="G$1" x="304.8" y="105.41" rot="R90"/>
<instance part="R4" gate="G$1" x="299.72" y="104.14" rot="R90"/>
<instance part="R5" gate="G$1" x="294.64" y="101.6" rot="R90"/>
<instance part="R6" gate="G$1" x="287.02" y="96.52" rot="R270"/>
<instance part="CD" gate="G$1" x="335.28" y="132.08"/>
<instance part="WP" gate="G$1" x="325.12" y="132.08"/>
<instance part="R8" gate="G$1" x="325.12" y="142.24" rot="R90"/>
<instance part="R9" gate="G$1" x="335.28" y="142.24" rot="R90"/>
<instance part="DS1307" gate="G$1" x="213.36" y="88.9"/>
<instance part="R10" gate="G$1" x="194.31" y="91.44" rot="R270"/>
<instance part="R11" gate="G$1" x="189.23" y="91.44" rot="R270"/>
<instance part="Q1" gate="G$1" x="207.01" y="107.95" rot="MR0"/>
<instance part="VBAT1" gate="G$1" x="242.57" y="72.39" rot="R90"/>
<instance part="R20" gate="G$1" x="228.6" y="93.98" rot="R270"/>
<instance part="C8" gate="G$1" x="21.59" y="245.11"/>
<instance part="C9" gate="G$1" x="31.75" y="245.11"/>
<instance part="C10" gate="G$1" x="40.64" y="245.11"/>
<instance part="C11" gate="G$1" x="50.8" y="245.11"/>
<instance part="C12" gate="G$1" x="60.96" y="245.11"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U5" gate="1" pin="AGND"/>
<junction x="44.45" y="193.04"/>
<wire x1="44.45" y1="193.04" x2="41.91" y2="193.04" width="0.1524" layer="91"/>
<label x="41.91" y="193.04" size="2.54" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="1" pin="GND3"/>
<junction x="44.45" y="167.64"/>
<pinref part="U5" gate="1" pin="GND2"/>
<junction x="44.45" y="170.18"/>
<wire x1="44.45" y1="167.64" x2="44.45" y2="170.18" width="0.1524" layer="91"/>
<pinref part="U5" gate="1" pin="GND1"/>
<junction x="44.45" y="172.72"/>
<wire x1="44.45" y1="170.18" x2="44.45" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U5" gate="1" pin="GND"/>
<junction x="44.45" y="175.26"/>
<wire x1="44.45" y1="172.72" x2="44.45" y2="175.26" width="0.1524" layer="91"/>
<wire x1="44.45" y1="175.26" x2="41.91" y2="175.26" width="0.1524" layer="91"/>
<label x="41.91" y="175.26" size="2.54" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RESET" gate="G$1" pin="P"/>
<junction x="26.67" y="223.52"/>
<pinref part="RESET" gate="G$1" pin="P1"/>
<junction x="26.67" y="220.98"/>
<wire x1="26.67" y1="223.52" x2="26.67" y2="220.98" width="0.1524" layer="91"/>
<wire x1="26.67" y1="220.98" x2="24.13" y2="220.98" width="0.1524" layer="91"/>
<label x="24.13" y="220.98" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="6"/>
<junction x="92.71" y="16.51"/>
<wire x1="92.71" y1="16.51" x2="97.79" y2="16.51" width="0.1524" layer="91"/>
<label x="97.79" y="16.51" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="6"/>
<junction x="92.71" y="33.02"/>
<wire x1="92.71" y1="33.02" x2="97.79" y2="33.02" width="0.1524" layer="91"/>
<label x="97.79" y="33.02" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="POWER_JP" gate="G$1" pin="1"/>
<junction x="186.69" y="171.45"/>
<wire x1="186.69" y1="171.45" x2="190.5" y2="171.45" width="0.1524" layer="91"/>
<label x="190.5" y="171.45" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="VSS1"/>
<junction x="332.74" y="83.82"/>
<wire x1="332.74" y1="83.82" x2="326.39" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="VSS2"/>
<junction x="332.74" y="81.28"/>
<wire x1="332.74" y1="81.28" x2="326.39" y2="81.28" width="0.1524" layer="91"/>
<wire x1="332.74" y1="83.82" x2="332.74" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="GND"/>
<junction x="332.74" y="76.2"/>
<wire x1="332.74" y1="76.2" x2="326.39" y2="76.2" width="0.1524" layer="91"/>
<label x="326.39" y="76.2" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="332.74" y1="81.28" x2="332.74" y2="76.2" width="0.1524" layer="91"/>
<label x="326.39" y="81.28" size="1.27" layer="95" rot="R180" xref="yes"/>
<label x="326.39" y="83.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="COMMON_SW"/>
<junction x="332.74" y="111.76"/>
<wire x1="332.74" y1="111.76" x2="326.39" y2="111.76" width="0.1524" layer="91"/>
<label x="326.39" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="VBAT1" gate="G$1" pin="-"/>
<junction x="242.57" y="62.23"/>
<wire x1="242.57" y1="62.23" x2="238.76" y2="62.23" width="0.1524" layer="91"/>
<label x="238.76" y="62.23" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DS1307" gate="G$1" pin="GND"/>
<junction x="213.36" y="78.74"/>
<wire x1="213.36" y1="78.74" x2="213.36" y2="73.66" width="0.1524" layer="91"/>
<label x="213.36" y="73.66" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<junction x="50.8" y="242.57"/>
<pinref part="C10" gate="G$1" pin="2"/>
<junction x="40.64" y="242.57"/>
<wire x1="50.8" y1="242.57" x2="40.64" y2="242.57" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<junction x="31.75" y="242.57"/>
<wire x1="40.64" y1="242.57" x2="31.75" y2="242.57" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<junction x="21.59" y="242.57"/>
<wire x1="31.75" y1="242.57" x2="21.59" y2="242.57" width="0.1524" layer="91"/>
<wire x1="21.59" y1="242.57" x2="15.24" y2="242.57" width="0.1524" layer="91"/>
<label x="15.24" y="242.57" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C12" gate="G$1" pin="2"/>
<junction x="60.96" y="242.57"/>
<wire x1="60.96" y1="242.57" x2="50.8" y2="242.57" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MCU_RESET" class="0">
<segment>
<pinref part="U5" gate="1" pin="RESET"/>
<junction x="44.45" y="213.36"/>
<pinref part="R46" gate="G$1" pin="1"/>
<junction x="41.91" y="213.36"/>
<wire x1="44.45" y1="213.36" x2="41.91" y2="213.36" width="0.1524" layer="91"/>
<wire x1="41.91" y1="213.36" x2="36.83" y2="213.36" width="0.1524" layer="91"/>
<junction x="36.83" y="213.36"/>
<wire x1="36.83" y1="213.36" x2="35.56" y2="213.36" width="0.1524" layer="91"/>
<pinref part="RESET" gate="G$1" pin="S1"/>
<junction x="36.83" y="220.98"/>
<wire x1="36.83" y1="213.36" x2="36.83" y2="220.98" width="0.1524" layer="91"/>
<pinref part="RESET" gate="G$1" pin="S"/>
<junction x="36.83" y="223.52"/>
<wire x1="36.83" y1="220.98" x2="36.83" y2="223.52" width="0.1524" layer="91"/>
<label x="35.56" y="213.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="5"/>
<junction x="74.93" y="16.51"/>
<wire x1="74.93" y1="16.51" x2="69.85" y2="16.51" width="0.1524" layer="91"/>
<label x="69.85" y="16.51" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="5"/>
<junction x="74.93" y="33.02"/>
<wire x1="74.93" y1="33.02" x2="69.85" y2="33.02" width="0.1524" layer="91"/>
<label x="69.85" y="33.02" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PG_PORT" gate="G$1" pin="7"/>
<junction x="205.74" y="234.95"/>
<wire x1="205.74" y1="234.95" x2="207.01" y2="234.95" width="0.1524" layer="91"/>
<label x="207.01" y="234.95" size="1.27" layer="95" xref="yes"/>
<pinref part="PG_PORT" gate="G$1" pin="8"/>
<junction x="205.74" y="237.49"/>
<wire x1="205.74" y1="234.95" x2="205.74" y2="237.49" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PL0_49" class="0">
<segment>
<pinref part="U5" gate="1" pin="PL0(ICP4)"/>
<junction x="44.45" y="132.08"/>
<wire x1="44.45" y1="132.08" x2="41.91" y2="132.08" width="0.1524" layer="91"/>
<label x="41.91" y="132.08" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PL_PORT" gate="G$1" pin="1"/>
<junction x="185.42" y="219.71"/>
<wire x1="185.42" y1="219.71" x2="186.69" y2="219.71" width="0.1524" layer="91"/>
<label x="186.69" y="219.71" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PL1_48" class="0">
<segment>
<pinref part="U5" gate="1" pin="PL1(ICP5)"/>
<junction x="44.45" y="134.62"/>
<wire x1="44.45" y1="134.62" x2="41.91" y2="134.62" width="0.1524" layer="91"/>
<label x="41.91" y="134.62" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PL_PORT" gate="G$1" pin="2"/>
<junction x="185.42" y="222.25"/>
<wire x1="185.42" y1="222.25" x2="186.69" y2="222.25" width="0.1524" layer="91"/>
<label x="186.69" y="222.25" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PL2_47" class="0">
<segment>
<pinref part="U5" gate="1" pin="PL2(T5)"/>
<junction x="44.45" y="137.16"/>
<wire x1="44.45" y1="137.16" x2="41.91" y2="137.16" width="0.1524" layer="91"/>
<label x="41.91" y="137.16" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PL_PORT" gate="G$1" pin="3"/>
<junction x="185.42" y="224.79"/>
<wire x1="185.42" y1="224.79" x2="186.69" y2="224.79" width="0.1524" layer="91"/>
<label x="186.69" y="224.79" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PL3_46" class="0">
<segment>
<pinref part="U5" gate="1" pin="PL3(OC5A)"/>
<junction x="44.45" y="139.7"/>
<wire x1="44.45" y1="139.7" x2="41.91" y2="139.7" width="0.1524" layer="91"/>
<label x="41.91" y="139.7" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PL_PORT" gate="G$1" pin="4"/>
<junction x="185.42" y="227.33"/>
<wire x1="185.42" y1="227.33" x2="186.69" y2="227.33" width="0.1524" layer="91"/>
<label x="186.69" y="227.33" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PL4_45" class="0">
<segment>
<pinref part="U5" gate="1" pin="PL4(OC5B)"/>
<junction x="44.45" y="142.24"/>
<wire x1="44.45" y1="142.24" x2="41.91" y2="142.24" width="0.1524" layer="91"/>
<label x="41.91" y="142.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PL_PORT" gate="G$1" pin="5"/>
<junction x="185.42" y="229.87"/>
<wire x1="185.42" y1="229.87" x2="186.69" y2="229.87" width="0.1524" layer="91"/>
<label x="186.69" y="229.87" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PL5_44" class="0">
<segment>
<pinref part="U5" gate="1" pin="PL5(OC5C)"/>
<junction x="44.45" y="144.78"/>
<wire x1="44.45" y1="144.78" x2="41.91" y2="144.78" width="0.1524" layer="91"/>
<label x="41.91" y="144.78" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PL_PORT" gate="G$1" pin="6"/>
<junction x="185.42" y="232.41"/>
<wire x1="185.42" y1="232.41" x2="186.69" y2="232.41" width="0.1524" layer="91"/>
<label x="186.69" y="232.41" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PL6_43" class="0">
<segment>
<pinref part="U5" gate="1" pin="PL6"/>
<junction x="44.45" y="147.32"/>
<wire x1="44.45" y1="147.32" x2="41.91" y2="147.32" width="0.1524" layer="91"/>
<label x="41.91" y="147.32" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PL_PORT" gate="G$1" pin="7"/>
<junction x="185.42" y="234.95"/>
<wire x1="185.42" y1="234.95" x2="186.69" y2="234.95" width="0.1524" layer="91"/>
<label x="186.69" y="234.95" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PL7_42" class="0">
<segment>
<pinref part="U5" gate="1" pin="PL7"/>
<junction x="44.45" y="149.86"/>
<wire x1="44.45" y1="149.86" x2="41.91" y2="149.86" width="0.1524" layer="91"/>
<label x="41.91" y="149.86" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PL_PORT" gate="G$1" pin="8"/>
<junction x="185.42" y="237.49"/>
<wire x1="185.42" y1="237.49" x2="186.69" y2="237.49" width="0.1524" layer="91"/>
<label x="186.69" y="237.49" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PK7_ADC15" class="0">
<segment>
<pinref part="U5" gate="1" pin="PK7(ADC15/PCINT23)"/>
<junction x="44.45" y="127"/>
<wire x1="44.45" y1="127" x2="41.91" y2="127" width="0.1524" layer="91"/>
<label x="41.91" y="127" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ADC_HIGH_PORT" gate="G$1" pin="8"/>
<junction x="267.97" y="176.53"/>
<wire x1="267.97" y1="176.53" x2="269.24" y2="176.53" width="0.1524" layer="91"/>
<label x="269.24" y="176.53" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PK6_ADC14" class="0">
<segment>
<pinref part="U5" gate="1" pin="PK6(ADC14/PCINT22)"/>
<junction x="44.45" y="124.46"/>
<wire x1="44.45" y1="124.46" x2="41.91" y2="124.46" width="0.1524" layer="91"/>
<label x="41.91" y="124.46" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ADC_HIGH_PORT" gate="G$1" pin="7"/>
<junction x="267.97" y="173.99"/>
<wire x1="267.97" y1="173.99" x2="269.24" y2="173.99" width="0.1524" layer="91"/>
<label x="269.24" y="173.99" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PK5_ADC13" class="0">
<segment>
<pinref part="U5" gate="1" pin="PK5(ADC13/PCINT21)"/>
<junction x="44.45" y="121.92"/>
<wire x1="44.45" y1="121.92" x2="41.91" y2="121.92" width="0.1524" layer="91"/>
<label x="41.91" y="121.92" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ADC_HIGH_PORT" gate="G$1" pin="6"/>
<junction x="267.97" y="171.45"/>
<wire x1="267.97" y1="171.45" x2="269.24" y2="171.45" width="0.1524" layer="91"/>
<label x="269.24" y="171.45" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PK4_ADC12" class="0">
<segment>
<pinref part="U5" gate="1" pin="PK4(ADC12/PCINT20)"/>
<junction x="44.45" y="119.38"/>
<wire x1="44.45" y1="119.38" x2="41.91" y2="119.38" width="0.1524" layer="91"/>
<label x="41.91" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ADC_HIGH_PORT" gate="G$1" pin="5"/>
<junction x="267.97" y="168.91"/>
<wire x1="267.97" y1="168.91" x2="269.24" y2="168.91" width="0.1524" layer="91"/>
<label x="269.24" y="168.91" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PK3_ADC11" class="0">
<segment>
<pinref part="U5" gate="1" pin="PK3(ADC11/PCINT19)"/>
<junction x="44.45" y="116.84"/>
<wire x1="44.45" y1="116.84" x2="41.91" y2="116.84" width="0.1524" layer="91"/>
<label x="41.91" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ADC_HIGH_PORT" gate="G$1" pin="4"/>
<junction x="267.97" y="166.37"/>
<wire x1="267.97" y1="166.37" x2="269.24" y2="166.37" width="0.1524" layer="91"/>
<label x="269.24" y="166.37" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PK2_ADC10" class="0">
<segment>
<pinref part="U5" gate="1" pin="PK2(ADC10/PCINT18)"/>
<junction x="44.45" y="114.3"/>
<wire x1="44.45" y1="114.3" x2="41.91" y2="114.3" width="0.1524" layer="91"/>
<label x="41.91" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ADC_HIGH_PORT" gate="G$1" pin="3"/>
<junction x="267.97" y="163.83"/>
<wire x1="267.97" y1="163.83" x2="269.24" y2="163.83" width="0.1524" layer="91"/>
<label x="269.24" y="163.83" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PK1_ADC9" class="0">
<segment>
<pinref part="U5" gate="1" pin="PK1(ADC9/PCINT17)"/>
<junction x="44.45" y="111.76"/>
<wire x1="44.45" y1="111.76" x2="41.91" y2="111.76" width="0.1524" layer="91"/>
<label x="41.91" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ADC_HIGH_PORT" gate="G$1" pin="2"/>
<junction x="267.97" y="161.29"/>
<wire x1="267.97" y1="161.29" x2="269.24" y2="161.29" width="0.1524" layer="91"/>
<label x="269.24" y="161.29" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PK0_ADC8" class="0">
<segment>
<pinref part="U5" gate="1" pin="PK0(ADC8/PCINT16)"/>
<junction x="44.45" y="109.22"/>
<wire x1="44.45" y1="109.22" x2="41.91" y2="109.22" width="0.1524" layer="91"/>
<label x="41.91" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ADC_HIGH_PORT" gate="G$1" pin="1"/>
<junction x="267.97" y="158.75"/>
<wire x1="267.97" y1="158.75" x2="269.24" y2="158.75" width="0.1524" layer="91"/>
<label x="269.24" y="158.75" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PJ7" class="0">
<segment>
<pinref part="U5" gate="1" pin="PJ7"/>
<junction x="44.45" y="104.14"/>
<wire x1="44.45" y1="104.14" x2="41.91" y2="104.14" width="0.1524" layer="91"/>
<label x="41.91" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PJ_PORT" gate="G$1" pin="8"/>
<junction x="185.42" y="208.28"/>
<wire x1="185.42" y1="208.28" x2="186.69" y2="208.28" width="0.1524" layer="91"/>
<label x="186.69" y="208.28" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PJ6" class="0">
<segment>
<pinref part="U5" gate="1" pin="PJ6(PCINT15)"/>
<junction x="44.45" y="101.6"/>
<wire x1="44.45" y1="101.6" x2="41.91" y2="101.6" width="0.1524" layer="91"/>
<label x="41.91" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PJ_PORT" gate="G$1" pin="7"/>
<junction x="185.42" y="205.74"/>
<wire x1="185.42" y1="205.74" x2="186.69" y2="205.74" width="0.1524" layer="91"/>
<label x="186.69" y="205.74" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PJ5" class="0">
<segment>
<pinref part="U5" gate="1" pin="PJ5(PCINT14)"/>
<junction x="44.45" y="99.06"/>
<wire x1="44.45" y1="99.06" x2="41.91" y2="99.06" width="0.1524" layer="91"/>
<label x="41.91" y="99.06" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PJ_PORT" gate="G$1" pin="6"/>
<junction x="185.42" y="203.2"/>
<wire x1="185.42" y1="203.2" x2="186.69" y2="203.2" width="0.1524" layer="91"/>
<label x="186.69" y="203.2" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PJ4" class="0">
<segment>
<pinref part="U5" gate="1" pin="PJ4(PCINT13)"/>
<junction x="44.45" y="96.52"/>
<wire x1="44.45" y1="96.52" x2="41.91" y2="96.52" width="0.1524" layer="91"/>
<label x="41.91" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PJ_PORT" gate="G$1" pin="5"/>
<junction x="185.42" y="200.66"/>
<wire x1="185.42" y1="200.66" x2="186.69" y2="200.66" width="0.1524" layer="91"/>
<label x="186.69" y="200.66" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PJ3" class="0">
<segment>
<pinref part="U5" gate="1" pin="PJ3(PCINT12)"/>
<junction x="44.45" y="93.98"/>
<wire x1="44.45" y1="93.98" x2="41.91" y2="93.98" width="0.1524" layer="91"/>
<label x="41.91" y="93.98" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PJ_PORT" gate="G$1" pin="4"/>
<junction x="185.42" y="198.12"/>
<wire x1="185.42" y1="198.12" x2="186.69" y2="198.12" width="0.1524" layer="91"/>
<label x="186.69" y="198.12" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PJ2" class="0">
<segment>
<pinref part="U5" gate="1" pin="PJ2(XCK3/PCINT11)"/>
<junction x="44.45" y="91.44"/>
<wire x1="44.45" y1="91.44" x2="41.91" y2="91.44" width="0.1524" layer="91"/>
<label x="41.91" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PJ_PORT" gate="G$1" pin="3"/>
<junction x="185.42" y="195.58"/>
<wire x1="185.42" y1="195.58" x2="186.69" y2="195.58" width="0.1524" layer="91"/>
<label x="186.69" y="195.58" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GSM_RX" class="0">
<segment>
<pinref part="U5" gate="1" pin="PJ1(TXD3/PCINT10)"/>
<junction x="44.45" y="88.9"/>
<wire x1="44.45" y1="88.9" x2="41.91" y2="88.9" width="0.1524" layer="91"/>
<label x="41.91" y="88.9" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PJ_PORT" gate="G$1" pin="2"/>
<junction x="185.42" y="193.04"/>
<wire x1="185.42" y1="193.04" x2="186.69" y2="193.04" width="0.1524" layer="91"/>
<label x="186.69" y="193.04" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GSM_TX" class="0">
<segment>
<pinref part="U5" gate="1" pin="PJ0(RXD3/PCINT9)"/>
<junction x="44.45" y="86.36"/>
<wire x1="44.45" y1="86.36" x2="41.91" y2="86.36" width="0.1524" layer="91"/>
<label x="41.91" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PJ_PORT" gate="G$1" pin="1"/>
<junction x="185.42" y="190.5"/>
<wire x1="185.42" y1="190.5" x2="186.69" y2="190.5" width="0.1524" layer="91"/>
<label x="186.69" y="190.5" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_UART_TX" class="0">
<segment>
<pinref part="U5" gate="1" pin="PH0(RXD2)"/>
<junction x="44.45" y="63.5"/>
<wire x1="44.45" y1="63.5" x2="41.91" y2="63.5" width="0.1524" layer="91"/>
<label x="41.91" y="63.5" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PH_PORT" gate="G$1" pin="1"/>
<junction x="213.36" y="191.77"/>
<wire x1="213.36" y1="191.77" x2="214.63" y2="191.77" width="0.1524" layer="91"/>
<label x="214.63" y="191.77" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_UART_RX" class="0">
<segment>
<pinref part="U5" gate="1" pin="PH1(TXD2)"/>
<junction x="44.45" y="66.04"/>
<wire x1="44.45" y1="66.04" x2="41.91" y2="66.04" width="0.1524" layer="91"/>
<label x="41.91" y="66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PH_PORT" gate="G$1" pin="2"/>
<junction x="213.36" y="194.31"/>
<wire x1="213.36" y1="194.31" x2="214.63" y2="194.31" width="0.1524" layer="91"/>
<label x="214.63" y="194.31" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PH2" class="0">
<segment>
<pinref part="U5" gate="1" pin="PH2(XCK2)"/>
<junction x="44.45" y="68.58"/>
<wire x1="44.45" y1="68.58" x2="41.91" y2="68.58" width="0.1524" layer="91"/>
<label x="41.91" y="68.58" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PH_PORT" gate="G$1" pin="3"/>
<junction x="213.36" y="196.85"/>
<wire x1="213.36" y1="196.85" x2="214.63" y2="196.85" width="0.1524" layer="91"/>
<label x="214.63" y="196.85" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_BOOT" class="0">
<segment>
<pinref part="U5" gate="1" pin="PH3(OC4A)"/>
<junction x="44.45" y="71.12"/>
<wire x1="44.45" y1="71.12" x2="41.91" y2="71.12" width="0.1524" layer="91"/>
<label x="41.91" y="71.12" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PH_PORT" gate="G$1" pin="4"/>
<junction x="213.36" y="199.39"/>
<wire x1="213.36" y1="199.39" x2="214.63" y2="199.39" width="0.1524" layer="91"/>
<label x="214.63" y="199.39" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GSM_ON" class="0">
<segment>
<pinref part="U5" gate="1" pin="PH4(OC4B)"/>
<junction x="44.45" y="73.66"/>
<wire x1="44.45" y1="73.66" x2="41.91" y2="73.66" width="0.1524" layer="91"/>
<label x="41.91" y="73.66" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PH_PORT" gate="G$1" pin="5"/>
<junction x="213.36" y="201.93"/>
<wire x1="213.36" y1="201.93" x2="214.63" y2="201.93" width="0.1524" layer="91"/>
<label x="214.63" y="201.93" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GSM_RESET" class="0">
<segment>
<pinref part="U5" gate="1" pin="PH5(OC4C)"/>
<junction x="44.45" y="76.2"/>
<wire x1="44.45" y1="76.2" x2="41.91" y2="76.2" width="0.1524" layer="91"/>
<label x="41.91" y="76.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PH_PORT" gate="G$1" pin="6"/>
<junction x="213.36" y="204.47"/>
<wire x1="213.36" y1="204.47" x2="214.63" y2="204.47" width="0.1524" layer="91"/>
<label x="214.63" y="204.47" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PH7" class="0">
<segment>
<pinref part="U5" gate="1" pin="PH7(T4)"/>
<junction x="44.45" y="81.28"/>
<wire x1="44.45" y1="81.28" x2="41.91" y2="81.28" width="0.1524" layer="91"/>
<label x="41.91" y="81.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PH_PORT" gate="G$1" pin="8"/>
<junction x="213.36" y="209.55"/>
<wire x1="213.36" y1="209.55" x2="214.63" y2="209.55" width="0.1524" layer="91"/>
<label x="214.63" y="209.55" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PA0_22" class="0">
<segment>
<pinref part="U5" gate="1" pin="(AD0)PA0"/>
<junction x="115.57" y="195.58"/>
<wire x1="115.57" y1="195.58" x2="118.11" y2="195.58" width="0.1524" layer="91"/>
<label x="118.11" y="195.58" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PA_PORT" gate="G$1" pin="1"/>
<junction x="233.68" y="158.75"/>
<wire x1="233.68" y1="158.75" x2="234.95" y2="158.75" width="0.1524" layer="91"/>
<label x="234.95" y="158.75" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PA1_23" class="0">
<segment>
<pinref part="U5" gate="1" pin="(AD1)PA1"/>
<junction x="115.57" y="198.12"/>
<wire x1="115.57" y1="198.12" x2="118.11" y2="198.12" width="0.1524" layer="91"/>
<label x="118.11" y="198.12" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PA_PORT" gate="G$1" pin="2"/>
<junction x="233.68" y="161.29"/>
<wire x1="233.68" y1="161.29" x2="234.95" y2="161.29" width="0.1524" layer="91"/>
<label x="234.95" y="161.29" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PA2_24" class="0">
<segment>
<pinref part="U5" gate="1" pin="(AD2)PA2"/>
<junction x="115.57" y="200.66"/>
<wire x1="115.57" y1="200.66" x2="118.11" y2="200.66" width="0.1524" layer="91"/>
<label x="118.11" y="200.66" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PA_PORT" gate="G$1" pin="3"/>
<junction x="233.68" y="163.83"/>
<wire x1="233.68" y1="163.83" x2="234.95" y2="163.83" width="0.1524" layer="91"/>
<label x="234.95" y="163.83" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PA3_25" class="0">
<segment>
<pinref part="U5" gate="1" pin="(AD3)PA3"/>
<junction x="115.57" y="203.2"/>
<wire x1="115.57" y1="203.2" x2="118.11" y2="203.2" width="0.1524" layer="91"/>
<label x="118.11" y="203.2" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PA_PORT" gate="G$1" pin="4"/>
<junction x="233.68" y="166.37"/>
<wire x1="233.68" y1="166.37" x2="234.95" y2="166.37" width="0.1524" layer="91"/>
<label x="234.95" y="166.37" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PA4_26" class="0">
<segment>
<pinref part="U5" gate="1" pin="(AD4)PA4"/>
<junction x="115.57" y="205.74"/>
<wire x1="115.57" y1="205.74" x2="118.11" y2="205.74" width="0.1524" layer="91"/>
<label x="118.11" y="205.74" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PA_PORT" gate="G$1" pin="5"/>
<junction x="233.68" y="168.91"/>
<wire x1="233.68" y1="168.91" x2="234.95" y2="168.91" width="0.1524" layer="91"/>
<label x="234.95" y="168.91" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PA5_27" class="0">
<segment>
<pinref part="U5" gate="1" pin="(AD5)PA5"/>
<junction x="115.57" y="208.28"/>
<wire x1="115.57" y1="208.28" x2="118.11" y2="208.28" width="0.1524" layer="91"/>
<label x="118.11" y="208.28" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PA_PORT" gate="G$1" pin="6"/>
<junction x="233.68" y="171.45"/>
<wire x1="233.68" y1="171.45" x2="234.95" y2="171.45" width="0.1524" layer="91"/>
<label x="234.95" y="171.45" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PA6_28" class="0">
<segment>
<pinref part="U5" gate="1" pin="(AD6)PA6"/>
<junction x="115.57" y="210.82"/>
<wire x1="115.57" y1="210.82" x2="118.11" y2="210.82" width="0.1524" layer="91"/>
<label x="118.11" y="210.82" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PA_PORT" gate="G$1" pin="7"/>
<junction x="233.68" y="173.99"/>
<wire x1="233.68" y1="173.99" x2="234.95" y2="173.99" width="0.1524" layer="91"/>
<label x="234.95" y="173.99" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PA7_29" class="0">
<segment>
<pinref part="U5" gate="1" pin="(AD7)PA7"/>
<junction x="115.57" y="213.36"/>
<wire x1="115.57" y1="213.36" x2="118.11" y2="213.36" width="0.1524" layer="91"/>
<label x="118.11" y="213.36" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PA_PORT" gate="G$1" pin="8"/>
<junction x="233.68" y="176.53"/>
<wire x1="233.68" y1="176.53" x2="234.95" y2="176.53" width="0.1524" layer="91"/>
<label x="234.95" y="176.53" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SS" class="0">
<segment>
<pinref part="U5" gate="1" pin="(SS/PCINT0)PB0"/>
<junction x="115.57" y="172.72"/>
<wire x1="115.57" y1="172.72" x2="118.11" y2="172.72" width="0.1524" layer="91"/>
<label x="118.11" y="172.72" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PB_PORT" gate="G$1" pin="1"/>
<junction x="298.45" y="218.44"/>
<wire x1="298.45" y1="218.44" x2="299.72" y2="218.44" width="0.1524" layer="91"/>
<label x="299.72" y="218.44" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<junction x="299.72" y="99.06"/>
<wire x1="299.72" y1="99.06" x2="299.72" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="CS"/>
<junction x="332.74" y="96.52"/>
<wire x1="299.72" y1="96.52" x2="332.74" y2="96.52" width="0.1524" layer="91"/>
<label x="312.42" y="97.79" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="U5" gate="1" pin="(SCK/PCINT1)PB1"/>
<junction x="115.57" y="175.26"/>
<wire x1="115.57" y1="175.26" x2="118.11" y2="175.26" width="0.1524" layer="91"/>
<label x="118.11" y="175.26" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<junction x="74.93" y="19.05"/>
<wire x1="74.93" y1="19.05" x2="69.85" y2="19.05" width="0.1524" layer="91"/>
<label x="69.85" y="19.05" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="3"/>
<junction x="74.93" y="35.56"/>
<wire x1="74.93" y1="35.56" x2="69.85" y2="35.56" width="0.1524" layer="91"/>
<label x="69.85" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PB_PORT" gate="G$1" pin="2"/>
<junction x="298.45" y="220.98"/>
<wire x1="298.45" y1="220.98" x2="299.72" y2="220.98" width="0.1524" layer="91"/>
<label x="299.72" y="220.98" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="SCLK"/>
<junction x="332.74" y="99.06"/>
<wire x1="332.74" y1="99.06" x2="326.39" y2="99.06" width="0.1524" layer="91"/>
<label x="326.39" y="99.06" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="U5" gate="1" pin="(MOSI/PCINT2)PB2"/>
<junction x="115.57" y="177.8"/>
<wire x1="115.57" y1="177.8" x2="118.11" y2="177.8" width="0.1524" layer="91"/>
<label x="118.11" y="177.8" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="4"/>
<junction x="92.71" y="19.05"/>
<wire x1="92.71" y1="19.05" x2="97.79" y2="19.05" width="0.1524" layer="91"/>
<label x="97.79" y="19.05" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="4"/>
<junction x="92.71" y="35.56"/>
<wire x1="92.71" y1="35.56" x2="97.79" y2="35.56" width="0.1524" layer="91"/>
<label x="97.79" y="35.56" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PB_PORT" gate="G$1" pin="3"/>
<junction x="298.45" y="223.52"/>
<wire x1="298.45" y1="223.52" x2="299.72" y2="223.52" width="0.1524" layer="91"/>
<label x="300.99" y="223.52" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<junction x="304.8" y="100.33"/>
<wire x1="304.8" y1="100.33" x2="317.5" y2="100.33" width="0.1524" layer="91"/>
<wire x1="317.5" y1="100.33" x2="318.77" y2="101.6" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="DATA_IN"/>
<junction x="332.74" y="101.6"/>
<wire x1="318.77" y1="101.6" x2="332.74" y2="101.6" width="0.1524" layer="91"/>
<label x="325.12" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="U5" gate="1" pin="(MISO/PCINT3)PB3"/>
<junction x="115.57" y="180.34"/>
<wire x1="115.57" y1="180.34" x2="118.11" y2="180.34" width="0.1524" layer="91"/>
<label x="118.11" y="180.34" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<junction x="74.93" y="21.59"/>
<wire x1="74.93" y1="21.59" x2="69.85" y2="21.59" width="0.1524" layer="91"/>
<label x="69.85" y="21.59" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<junction x="74.93" y="38.1"/>
<wire x1="74.93" y1="38.1" x2="69.85" y2="38.1" width="0.1524" layer="91"/>
<label x="69.85" y="38.1" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PB_PORT" gate="G$1" pin="4"/>
<junction x="298.45" y="226.06"/>
<wire x1="298.45" y1="226.06" x2="299.72" y2="226.06" width="0.1524" layer="91"/>
<label x="299.72" y="226.06" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<junction x="309.88" y="101.6"/>
<wire x1="309.88" y1="101.6" x2="316.23" y2="101.6" width="0.1524" layer="91"/>
<wire x1="316.23" y1="101.6" x2="318.77" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="DATA_OUT"/>
<junction x="332.74" y="104.14"/>
<wire x1="318.77" y1="104.14" x2="332.74" y2="104.14" width="0.1524" layer="91"/>
<label x="327.66" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="EN" class="0">
<segment>
<pinref part="U5" gate="1" pin="(OC2A/PCINT4)PB4"/>
<junction x="115.57" y="182.88"/>
<wire x1="115.57" y1="182.88" x2="118.11" y2="182.88" width="0.1524" layer="91"/>
<label x="118.11" y="182.88" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PB_PORT" gate="G$1" pin="5"/>
<junction x="298.45" y="228.6"/>
<wire x1="298.45" y1="228.6" x2="299.72" y2="228.6" width="0.1524" layer="91"/>
<label x="300.99" y="228.6" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PB5_11" class="0">
<segment>
<pinref part="U5" gate="1" pin="(OC1A/PCINT5)PB5"/>
<junction x="115.57" y="185.42"/>
<wire x1="115.57" y1="185.42" x2="118.11" y2="185.42" width="0.1524" layer="91"/>
<label x="118.11" y="185.42" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PB_PORT" gate="G$1" pin="6"/>
<junction x="298.45" y="231.14"/>
<wire x1="298.45" y1="231.14" x2="299.72" y2="231.14" width="0.1524" layer="91"/>
<label x="300.99" y="231.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PB6_12" class="0">
<segment>
<pinref part="U5" gate="1" pin="(OC1B/PCINT6)PB6"/>
<junction x="115.57" y="187.96"/>
<wire x1="115.57" y1="187.96" x2="118.11" y2="187.96" width="0.1524" layer="91"/>
<label x="118.11" y="187.96" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PB_PORT" gate="G$1" pin="7"/>
<junction x="298.45" y="233.68"/>
<wire x1="298.45" y1="233.68" x2="299.72" y2="233.68" width="0.1524" layer="91"/>
<label x="300.99" y="233.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PB7_13" class="0">
<segment>
<pinref part="U5" gate="1" pin="(OC0A/OC1C/PCINT7)PB7"/>
<junction x="115.57" y="190.5"/>
<wire x1="115.57" y1="190.5" x2="118.11" y2="190.5" width="0.1524" layer="91"/>
<label x="118.11" y="190.5" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PB_PORT" gate="G$1" pin="8"/>
<junction x="298.45" y="236.22"/>
<wire x1="298.45" y1="236.22" x2="299.72" y2="236.22" width="0.1524" layer="91"/>
<label x="300.99" y="236.22" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PC0_37" class="0">
<segment>
<pinref part="U5" gate="1" pin="(A8)PC0"/>
<junction x="115.57" y="149.86"/>
<wire x1="115.57" y1="149.86" x2="118.11" y2="149.86" width="0.1524" layer="91"/>
<label x="118.11" y="149.86" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PC_PORT" gate="G$1" pin="1"/>
<junction x="276.86" y="218.44"/>
<wire x1="276.86" y1="218.44" x2="278.13" y2="218.44" width="0.1524" layer="91"/>
<label x="278.13" y="218.44" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PC1_36" class="0">
<segment>
<pinref part="U5" gate="1" pin="(A9)PC1"/>
<junction x="115.57" y="152.4"/>
<wire x1="115.57" y1="152.4" x2="118.11" y2="152.4" width="0.1524" layer="91"/>
<label x="118.11" y="152.4" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PC_PORT" gate="G$1" pin="2"/>
<junction x="276.86" y="220.98"/>
<wire x1="276.86" y1="220.98" x2="278.13" y2="220.98" width="0.1524" layer="91"/>
<label x="278.13" y="220.98" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PC2_35" class="0">
<segment>
<pinref part="U5" gate="1" pin="(A10)PC2"/>
<junction x="115.57" y="154.94"/>
<wire x1="115.57" y1="154.94" x2="118.11" y2="154.94" width="0.1524" layer="91"/>
<label x="118.11" y="154.94" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PC_PORT" gate="G$1" pin="3"/>
<junction x="276.86" y="223.52"/>
<wire x1="276.86" y1="223.52" x2="278.13" y2="223.52" width="0.1524" layer="91"/>
<label x="278.13" y="223.52" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PC3_34" class="0">
<segment>
<pinref part="U5" gate="1" pin="(A11)PC3"/>
<junction x="115.57" y="157.48"/>
<wire x1="115.57" y1="157.48" x2="118.11" y2="157.48" width="0.1524" layer="91"/>
<label x="118.11" y="157.48" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PC_PORT" gate="G$1" pin="4"/>
<junction x="276.86" y="226.06"/>
<wire x1="276.86" y1="226.06" x2="278.13" y2="226.06" width="0.1524" layer="91"/>
<label x="278.13" y="226.06" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PC4_33" class="0">
<segment>
<pinref part="U5" gate="1" pin="(A12)PC4"/>
<junction x="115.57" y="160.02"/>
<wire x1="115.57" y1="160.02" x2="118.11" y2="160.02" width="0.1524" layer="91"/>
<label x="118.11" y="160.02" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PC_PORT" gate="G$1" pin="5"/>
<junction x="276.86" y="228.6"/>
<wire x1="276.86" y1="228.6" x2="278.13" y2="228.6" width="0.1524" layer="91"/>
<label x="278.13" y="228.6" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PC5_32" class="0">
<segment>
<pinref part="U5" gate="1" pin="(A13)PC5"/>
<junction x="115.57" y="162.56"/>
<wire x1="115.57" y1="162.56" x2="118.11" y2="162.56" width="0.1524" layer="91"/>
<label x="118.11" y="162.56" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PC_PORT" gate="G$1" pin="6"/>
<junction x="276.86" y="231.14"/>
<wire x1="276.86" y1="231.14" x2="278.13" y2="231.14" width="0.1524" layer="91"/>
<label x="278.13" y="231.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PC6_31" class="0">
<segment>
<pinref part="U5" gate="1" pin="(A14)PC6"/>
<junction x="115.57" y="165.1"/>
<wire x1="115.57" y1="165.1" x2="118.11" y2="165.1" width="0.1524" layer="91"/>
<label x="118.11" y="165.1" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PC_PORT" gate="G$1" pin="7"/>
<junction x="276.86" y="233.68"/>
<wire x1="276.86" y1="233.68" x2="278.13" y2="233.68" width="0.1524" layer="91"/>
<label x="278.13" y="233.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PC7_30" class="0">
<segment>
<pinref part="U5" gate="1" pin="(A15)PC7"/>
<junction x="115.57" y="167.64"/>
<wire x1="115.57" y1="167.64" x2="118.11" y2="167.64" width="0.1524" layer="91"/>
<label x="118.11" y="167.64" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PC_PORT" gate="G$1" pin="8"/>
<junction x="276.86" y="236.22"/>
<wire x1="276.86" y1="236.22" x2="278.13" y2="236.22" width="0.1524" layer="91"/>
<label x="278.13" y="236.22" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_MODE" class="0">
<segment>
<pinref part="U5" gate="1" pin="(OC3C/INT5)PE5"/>
<junction x="115.57" y="116.84"/>
<wire x1="115.57" y1="116.84" x2="118.11" y2="116.84" width="0.1524" layer="91"/>
<label x="118.11" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PE_PORT" gate="G$1" pin="6"/>
<junction x="246.38" y="204.47"/>
<wire x1="246.38" y1="204.47" x2="247.65" y2="204.47" width="0.1524" layer="91"/>
<label x="247.65" y="204.47" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PE6" class="0">
<segment>
<pinref part="U5" gate="1" pin="(T3/INT6)PE6"/>
<junction x="115.57" y="119.38"/>
<wire x1="115.57" y1="119.38" x2="118.11" y2="119.38" width="0.1524" layer="91"/>
<label x="118.11" y="119.38" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PE_PORT" gate="G$1" pin="7"/>
<junction x="246.38" y="207.01"/>
<wire x1="246.38" y1="207.01" x2="247.65" y2="207.01" width="0.1524" layer="91"/>
<label x="247.65" y="207.01" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PE7" class="0">
<segment>
<pinref part="U5" gate="1" pin="(CLKO/ICP3/INT7)PE7"/>
<junction x="115.57" y="121.92"/>
<wire x1="115.57" y1="121.92" x2="118.11" y2="121.92" width="0.1524" layer="91"/>
<label x="118.11" y="121.92" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PE_PORT" gate="G$1" pin="8"/>
<junction x="246.38" y="209.55"/>
<wire x1="246.38" y1="209.55" x2="247.65" y2="209.55" width="0.1524" layer="91"/>
<label x="247.65" y="209.55" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U5" gate="1" pin="(SCL/INT0)PD0"/>
<junction x="115.57" y="127"/>
<wire x1="115.57" y1="127" x2="118.11" y2="127" width="0.1524" layer="91"/>
<label x="118.11" y="127" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PD_PORT" gate="G$1" pin="1"/>
<junction x="281.94" y="190.5"/>
<wire x1="281.94" y1="190.5" x2="283.21" y2="190.5" width="0.1524" layer="91"/>
<label x="283.21" y="190.5" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DS1307" gate="G$1" pin="SCL"/>
<junction x="200.66" y="86.36"/>
<pinref part="R10" gate="G$1" pin="2"/>
<junction x="194.31" y="86.36"/>
<wire x1="200.66" y1="86.36" x2="194.31" y2="86.36" width="0.1524" layer="91"/>
<label x="199.39" y="87.63" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U5" gate="1" pin="(SDA/INT1)PD1"/>
<junction x="115.57" y="129.54"/>
<wire x1="115.57" y1="129.54" x2="118.11" y2="129.54" width="0.1524" layer="91"/>
<label x="118.11" y="129.54" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PD_PORT" gate="G$1" pin="2"/>
<junction x="281.94" y="193.04"/>
<wire x1="281.94" y1="193.04" x2="283.21" y2="193.04" width="0.1524" layer="91"/>
<label x="283.21" y="193.04" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DS1307" gate="G$1" pin="SDA"/>
<junction x="200.66" y="83.82"/>
<pinref part="R11" gate="G$1" pin="2"/>
<junction x="189.23" y="86.36"/>
<wire x1="200.66" y1="83.82" x2="191.77" y2="83.82" width="0.1524" layer="91"/>
<wire x1="191.77" y1="83.82" x2="189.23" y2="86.36" width="0.1524" layer="91"/>
<wire x1="189.23" y1="86.36" x2="186.69" y2="86.36" width="0.1524" layer="91"/>
<label x="186.69" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_UART1_TX" class="0">
<segment>
<pinref part="U5" gate="1" pin="(RXD1/INT2)PD2"/>
<junction x="115.57" y="132.08"/>
<wire x1="115.57" y1="132.08" x2="118.11" y2="132.08" width="0.1524" layer="91"/>
<label x="118.11" y="132.08" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PD_PORT" gate="G$1" pin="3"/>
<junction x="281.94" y="195.58"/>
<wire x1="281.94" y1="195.58" x2="283.21" y2="195.58" width="0.1524" layer="91"/>
<label x="283.21" y="195.58" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_UART1_RX" class="0">
<segment>
<pinref part="U5" gate="1" pin="(TXD1/INT3)PD3"/>
<junction x="115.57" y="134.62"/>
<wire x1="115.57" y1="134.62" x2="118.11" y2="134.62" width="0.1524" layer="91"/>
<label x="118.11" y="134.62" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PD_PORT" gate="G$1" pin="4"/>
<junction x="281.94" y="198.12"/>
<wire x1="281.94" y1="198.12" x2="283.21" y2="198.12" width="0.1524" layer="91"/>
<label x="283.21" y="198.12" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PD4" class="0">
<segment>
<pinref part="U5" gate="1" pin="(ICP1)PD4"/>
<junction x="115.57" y="137.16"/>
<wire x1="115.57" y1="137.16" x2="118.11" y2="137.16" width="0.1524" layer="91"/>
<label x="118.11" y="137.16" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PD_PORT" gate="G$1" pin="5"/>
<junction x="281.94" y="200.66"/>
<wire x1="281.94" y1="200.66" x2="283.21" y2="200.66" width="0.1524" layer="91"/>
<label x="283.21" y="200.66" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PD5" class="0">
<segment>
<pinref part="U5" gate="1" pin="(XCK1)PD5"/>
<junction x="115.57" y="139.7"/>
<wire x1="115.57" y1="139.7" x2="118.11" y2="139.7" width="0.1524" layer="91"/>
<label x="118.11" y="139.7" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PD_PORT" gate="G$1" pin="6"/>
<junction x="281.94" y="203.2"/>
<wire x1="281.94" y1="203.2" x2="283.21" y2="203.2" width="0.1524" layer="91"/>
<label x="283.21" y="203.2" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PD6" class="0">
<segment>
<pinref part="U5" gate="1" pin="(T1)PD6"/>
<junction x="115.57" y="142.24"/>
<wire x1="115.57" y1="142.24" x2="118.11" y2="142.24" width="0.1524" layer="91"/>
<label x="118.11" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PD_PORT" gate="G$1" pin="7"/>
<junction x="281.94" y="205.74"/>
<wire x1="281.94" y1="205.74" x2="283.21" y2="205.74" width="0.1524" layer="91"/>
<label x="283.21" y="205.74" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PD7_38" class="0">
<segment>
<pinref part="U5" gate="1" pin="(T0)PD7"/>
<junction x="115.57" y="144.78"/>
<wire x1="115.57" y1="144.78" x2="118.11" y2="144.78" width="0.1524" layer="91"/>
<label x="118.11" y="144.78" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PD_PORT" gate="G$1" pin="8"/>
<junction x="281.94" y="208.28"/>
<wire x1="281.94" y1="208.28" x2="283.21" y2="208.28" width="0.1524" layer="91"/>
<label x="283.21" y="208.28" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_RESET" class="0">
<segment>
<pinref part="U5" gate="1" pin="(OC3B/INT4)PE4"/>
<junction x="115.57" y="114.3"/>
<wire x1="115.57" y1="114.3" x2="118.11" y2="114.3" width="0.1524" layer="91"/>
<label x="118.11" y="114.3" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PE_PORT" gate="G$1" pin="5"/>
<junction x="246.38" y="201.93"/>
<wire x1="246.38" y1="201.93" x2="247.65" y2="201.93" width="0.1524" layer="91"/>
<label x="247.65" y="201.93" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_WPS" class="0">
<segment>
<pinref part="U5" gate="1" pin="(OC3A/AIN1)PE3"/>
<junction x="115.57" y="111.76"/>
<wire x1="115.57" y1="111.76" x2="118.11" y2="111.76" width="0.1524" layer="91"/>
<label x="118.11" y="111.76" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PE_PORT" gate="G$1" pin="4"/>
<junction x="246.38" y="199.39"/>
<wire x1="246.38" y1="199.39" x2="247.65" y2="199.39" width="0.1524" layer="91"/>
<label x="247.65" y="199.39" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PE2" class="0">
<segment>
<pinref part="U5" gate="1" pin="(XCK0/AIN0)PE2"/>
<junction x="115.57" y="109.22"/>
<wire x1="115.57" y1="109.22" x2="118.11" y2="109.22" width="0.1524" layer="91"/>
<label x="118.11" y="109.22" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PE_PORT" gate="G$1" pin="3"/>
<junction x="246.38" y="196.85"/>
<wire x1="246.38" y1="196.85" x2="247.65" y2="196.85" width="0.1524" layer="91"/>
<label x="247.65" y="196.85" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PF7_ADC7" class="0">
<segment>
<pinref part="U5" gate="1" pin="(ADC7/TDI)PF7"/>
<junction x="115.57" y="99.06"/>
<wire x1="115.57" y1="99.06" x2="118.11" y2="99.06" width="0.1524" layer="91"/>
<label x="118.11" y="99.06" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PF_PORT" gate="G$1" pin="8"/>
<junction x="242.57" y="236.22"/>
<wire x1="242.57" y1="236.22" x2="243.84" y2="236.22" width="0.1524" layer="91"/>
<label x="243.84" y="236.22" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PF6_ADC6" class="0">
<segment>
<pinref part="U5" gate="1" pin="(ADC6/TDO)PF6"/>
<junction x="115.57" y="96.52"/>
<wire x1="115.57" y1="96.52" x2="118.11" y2="96.52" width="0.1524" layer="91"/>
<label x="118.11" y="96.52" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PF_PORT" gate="G$1" pin="7"/>
<junction x="242.57" y="233.68"/>
<wire x1="242.57" y1="233.68" x2="243.84" y2="233.68" width="0.1524" layer="91"/>
<label x="243.84" y="233.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PF5_ADC5" class="0">
<segment>
<pinref part="U5" gate="1" pin="(ADC5/TMS)PF5"/>
<junction x="115.57" y="93.98"/>
<wire x1="115.57" y1="93.98" x2="118.11" y2="93.98" width="0.1524" layer="91"/>
<label x="118.11" y="93.98" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PF_PORT" gate="G$1" pin="6"/>
<junction x="242.57" y="231.14"/>
<wire x1="242.57" y1="231.14" x2="243.84" y2="231.14" width="0.1524" layer="91"/>
<label x="243.84" y="231.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PF4_ADC4" class="0">
<segment>
<pinref part="U5" gate="1" pin="(ADC4/TCK)PF4"/>
<junction x="115.57" y="91.44"/>
<wire x1="115.57" y1="91.44" x2="118.11" y2="91.44" width="0.1524" layer="91"/>
<label x="118.11" y="91.44" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PF_PORT" gate="G$1" pin="5"/>
<junction x="242.57" y="228.6"/>
<wire x1="242.57" y1="228.6" x2="243.84" y2="228.6" width="0.1524" layer="91"/>
<label x="243.84" y="228.6" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PF3_ADC3" class="0">
<segment>
<pinref part="U5" gate="1" pin="(ADC3)PF3"/>
<junction x="115.57" y="88.9"/>
<wire x1="115.57" y1="88.9" x2="118.11" y2="88.9" width="0.1524" layer="91"/>
<label x="118.11" y="88.9" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PF_PORT" gate="G$1" pin="4"/>
<junction x="242.57" y="226.06"/>
<wire x1="242.57" y1="226.06" x2="243.84" y2="226.06" width="0.1524" layer="91"/>
<label x="243.84" y="226.06" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PF2_ADC2" class="0">
<segment>
<pinref part="U5" gate="1" pin="(ADC2)PF2"/>
<junction x="115.57" y="86.36"/>
<wire x1="115.57" y1="86.36" x2="118.11" y2="86.36" width="0.1524" layer="91"/>
<label x="118.11" y="86.36" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PF_PORT" gate="G$1" pin="3"/>
<junction x="242.57" y="223.52"/>
<wire x1="242.57" y1="223.52" x2="243.84" y2="223.52" width="0.1524" layer="91"/>
<label x="243.84" y="223.52" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="DC_PWR_LEVEL" class="0">
<segment>
<pinref part="U5" gate="1" pin="(ADC1)PF1"/>
<junction x="115.57" y="83.82"/>
<wire x1="115.57" y1="83.82" x2="118.11" y2="83.82" width="0.1524" layer="91"/>
<label x="118.11" y="83.82" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PF_PORT" gate="G$1" pin="2"/>
<junction x="242.57" y="220.98"/>
<wire x1="242.57" y1="220.98" x2="243.84" y2="220.98" width="0.1524" layer="91"/>
<label x="243.84" y="220.98" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SOLAR_LEVEL" class="0">
<segment>
<pinref part="U5" gate="1" pin="(ADC0)PF0"/>
<junction x="115.57" y="81.28"/>
<wire x1="115.57" y1="81.28" x2="118.11" y2="81.28" width="0.1524" layer="91"/>
<label x="118.11" y="81.28" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PF_PORT" gate="G$1" pin="1"/>
<junction x="242.57" y="218.44"/>
<wire x1="242.57" y1="218.44" x2="243.84" y2="218.44" width="0.1524" layer="91"/>
<label x="243.84" y="218.44" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_ERROR" class="0">
<segment>
<pinref part="U5" gate="1" pin="(OC0B)PG5"/>
<junction x="115.57" y="76.2"/>
<wire x1="115.57" y1="76.2" x2="118.11" y2="76.2" width="0.1524" layer="91"/>
<label x="118.11" y="76.2" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PG_PORT" gate="G$1" pin="6"/>
<junction x="205.74" y="232.41"/>
<wire x1="205.74" y1="232.41" x2="207.01" y2="232.41" width="0.1524" layer="91"/>
<label x="207.01" y="232.41" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PG4" class="0">
<segment>
<pinref part="U5" gate="1" pin="(TOSC1)PG4"/>
<junction x="115.57" y="73.66"/>
<wire x1="115.57" y1="73.66" x2="118.11" y2="73.66" width="0.1524" layer="91"/>
<label x="118.11" y="73.66" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PG_PORT" gate="G$1" pin="5"/>
<junction x="205.74" y="229.87"/>
<wire x1="205.74" y1="229.87" x2="207.01" y2="229.87" width="0.1524" layer="91"/>
<label x="207.01" y="229.87" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PG3" class="0">
<segment>
<pinref part="U5" gate="1" pin="(TOSC2)PG3"/>
<junction x="115.57" y="71.12"/>
<wire x1="115.57" y1="71.12" x2="118.11" y2="71.12" width="0.1524" layer="91"/>
<label x="118.11" y="71.12" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PG_PORT" gate="G$1" pin="4"/>
<junction x="205.74" y="227.33"/>
<wire x1="205.74" y1="227.33" x2="207.01" y2="227.33" width="0.1524" layer="91"/>
<label x="207.01" y="227.33" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PG2_39" class="0">
<segment>
<pinref part="U5" gate="1" pin="(ALE)PG2"/>
<junction x="115.57" y="68.58"/>
<wire x1="115.57" y1="68.58" x2="118.11" y2="68.58" width="0.1524" layer="91"/>
<label x="118.11" y="68.58" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PG_PORT" gate="G$1" pin="3"/>
<junction x="205.74" y="224.79"/>
<wire x1="205.74" y1="224.79" x2="207.01" y2="224.79" width="0.1524" layer="91"/>
<label x="207.01" y="224.79" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PG1_41" class="0">
<segment>
<pinref part="U5" gate="1" pin="(RD)PG1"/>
<junction x="115.57" y="66.04"/>
<wire x1="115.57" y1="66.04" x2="118.11" y2="66.04" width="0.1524" layer="91"/>
<label x="118.11" y="66.04" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PG_PORT" gate="G$1" pin="2"/>
<junction x="205.74" y="222.25"/>
<wire x1="205.74" y1="222.25" x2="207.01" y2="222.25" width="0.1524" layer="91"/>
<label x="207.01" y="222.25" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PG0_40" class="0">
<segment>
<pinref part="U5" gate="1" pin="(WR)PG0"/>
<junction x="115.57" y="63.5"/>
<wire x1="115.57" y1="63.5" x2="118.11" y2="63.5" width="0.1524" layer="91"/>
<label x="118.11" y="63.5" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PG_PORT" gate="G$1" pin="1"/>
<junction x="205.74" y="219.71"/>
<wire x1="205.74" y1="219.71" x2="207.01" y2="219.71" width="0.1524" layer="91"/>
<label x="207.01" y="219.71" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R46" gate="G$1" pin="2"/>
<junction x="41.91" y="223.52"/>
<wire x1="41.91" y1="223.52" x2="44.45" y2="223.52" width="0.1524" layer="91"/>
<label x="44.45" y="223.52" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="1" pin="AVCC"/>
<junction x="44.45" y="195.58"/>
<pinref part="U5" gate="1" pin="AREF"/>
<junction x="44.45" y="198.12"/>
<wire x1="44.45" y1="195.58" x2="44.45" y2="198.12" width="0.1524" layer="91"/>
<wire x1="44.45" y1="198.12" x2="41.91" y2="198.12" width="0.1524" layer="91"/>
<label x="41.91" y="198.12" size="2.54" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="1" pin="VCC3"/>
<junction x="44.45" y="177.8"/>
<pinref part="U5" gate="1" pin="VCC2"/>
<junction x="44.45" y="180.34"/>
<wire x1="44.45" y1="177.8" x2="44.45" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U5" gate="1" pin="VCC1"/>
<junction x="44.45" y="182.88"/>
<wire x1="44.45" y1="180.34" x2="44.45" y2="182.88" width="0.1524" layer="91"/>
<pinref part="U5" gate="1" pin="VCC"/>
<junction x="44.45" y="185.42"/>
<wire x1="44.45" y1="182.88" x2="44.45" y2="185.42" width="0.1524" layer="91"/>
<wire x1="44.45" y1="185.42" x2="41.91" y2="185.42" width="0.1524" layer="91"/>
<label x="41.91" y="185.42" size="2.54" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<junction x="92.71" y="21.59"/>
<wire x1="92.71" y1="21.59" x2="96.52" y2="21.59" width="0.1524" layer="91"/>
<wire x1="96.52" y1="21.59" x2="97.79" y2="22.86" width="0.1524" layer="91"/>
<label x="97.79" y="22.86" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<junction x="92.71" y="38.1"/>
<wire x1="92.71" y1="38.1" x2="96.52" y2="38.1" width="0.1524" layer="91"/>
<wire x1="96.52" y1="38.1" x2="97.79" y2="39.37" width="0.1524" layer="91"/>
<label x="97.79" y="39.37" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="POWER_JP" gate="G$1" pin="2"/>
<junction x="186.69" y="173.99"/>
<wire x1="186.69" y1="173.99" x2="190.5" y2="173.99" width="0.1524" layer="91"/>
<label x="190.5" y="173.99" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="VDD"/>
<junction x="332.74" y="86.36"/>
<wire x1="332.74" y1="86.36" x2="326.39" y2="86.36" width="0.1524" layer="91"/>
<label x="326.39" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<junction x="309.88" y="111.76"/>
<pinref part="R3" gate="G$1" pin="2"/>
<junction x="304.8" y="110.49"/>
<wire x1="309.88" y1="111.76" x2="306.07" y2="111.76" width="0.1524" layer="91"/>
<wire x1="306.07" y1="111.76" x2="304.8" y2="110.49" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<junction x="299.72" y="109.22"/>
<wire x1="304.8" y1="110.49" x2="300.99" y2="110.49" width="0.1524" layer="91"/>
<wire x1="300.99" y1="110.49" x2="299.72" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<junction x="294.64" y="106.68"/>
<wire x1="299.72" y1="109.22" x2="297.18" y2="109.22" width="0.1524" layer="91"/>
<wire x1="297.18" y1="109.22" x2="294.64" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<junction x="287.02" y="101.6"/>
<wire x1="294.64" y1="106.68" x2="292.1" y2="106.68" width="0.1524" layer="91"/>
<wire x1="292.1" y1="106.68" x2="287.02" y2="101.6" width="0.1524" layer="91"/>
<wire x1="287.02" y1="101.6" x2="284.48" y2="101.6" width="0.1524" layer="91"/>
<wire x1="284.48" y1="101.6" x2="283.21" y2="102.87" width="0.1524" layer="91"/>
<label x="283.21" y="102.87" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<junction x="335.28" y="147.32"/>
<pinref part="R8" gate="G$1" pin="2"/>
<junction x="325.12" y="147.32"/>
<wire x1="335.28" y1="147.32" x2="325.12" y2="147.32" width="0.1524" layer="91"/>
<wire x1="325.12" y1="147.32" x2="322.58" y2="147.32" width="0.1524" layer="91"/>
<label x="323.85" y="147.32" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<junction x="194.31" y="96.52"/>
<pinref part="R11" gate="G$1" pin="1"/>
<junction x="189.23" y="96.52"/>
<wire x1="194.31" y1="96.52" x2="189.23" y2="96.52" width="0.1524" layer="91"/>
<wire x1="189.23" y1="96.52" x2="186.69" y2="96.52" width="0.1524" layer="91"/>
<label x="186.69" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DS1307" gate="G$1" pin="VDD"/>
<junction x="215.9" y="101.6"/>
<wire x1="215.9" y1="101.6" x2="223.52" y2="101.6" width="0.1524" layer="91"/>
<wire x1="223.52" y1="101.6" x2="224.79" y2="102.87" width="0.1524" layer="91"/>
<wire x1="224.79" y1="102.87" x2="224.79" y2="106.68" width="0.1524" layer="91"/>
<junction x="224.79" y="102.87"/>
<label x="224.79" y="106.68" size="1.27" layer="95" xref="yes"/>
<pinref part="R20" gate="G$1" pin="1"/>
<junction x="228.6" y="99.06"/>
<wire x1="224.79" y1="102.87" x2="228.6" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="1"/>
<junction x="50.8" y="250.19"/>
<pinref part="C10" gate="G$1" pin="1"/>
<junction x="40.64" y="250.19"/>
<wire x1="50.8" y1="250.19" x2="40.64" y2="250.19" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<junction x="31.75" y="250.19"/>
<wire x1="40.64" y1="250.19" x2="31.75" y2="250.19" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<junction x="21.59" y="250.19"/>
<wire x1="31.75" y1="250.19" x2="21.59" y2="250.19" width="0.1524" layer="91"/>
<wire x1="21.59" y1="250.19" x2="15.24" y2="250.19" width="0.1524" layer="91"/>
<label x="15.24" y="250.19" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C12" gate="G$1" pin="1"/>
<junction x="60.96" y="250.19"/>
<wire x1="60.96" y1="250.19" x2="50.8" y2="250.19" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CP_RX" class="0">
<segment>
<pinref part="U5" gate="1" pin="(TXD0)PE1"/>
<junction x="115.57" y="106.68"/>
<wire x1="115.57" y1="106.68" x2="118.11" y2="106.68" width="0.1524" layer="91"/>
<label x="118.11" y="106.68" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PE_PORT" gate="G$1" pin="2"/>
<junction x="246.38" y="194.31"/>
<wire x1="246.38" y1="194.31" x2="247.65" y2="194.31" width="0.1524" layer="91"/>
<label x="247.65" y="194.31" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CP_TX" class="0">
<segment>
<pinref part="U5" gate="1" pin="(RXD0/PCIN8)PE0"/>
<junction x="115.57" y="104.14"/>
<wire x1="115.57" y1="104.14" x2="118.11" y2="104.14" width="0.1524" layer="91"/>
<label x="118.11" y="104.14" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PE_PORT" gate="G$1" pin="1"/>
<junction x="246.38" y="191.77"/>
<wire x1="246.38" y1="191.77" x2="247.65" y2="191.77" width="0.1524" layer="91"/>
<label x="247.65" y="191.77" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="DAT1"/>
<junction x="332.74" y="93.98"/>
<pinref part="R5" gate="G$1" pin="1"/>
<junction x="294.64" y="96.52"/>
<wire x1="332.74" y1="93.98" x2="294.64" y2="93.98" width="0.1524" layer="91"/>
<wire x1="294.64" y1="93.98" x2="294.64" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SD_WP" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="WRITE_PROTECT"/>
<junction x="332.74" y="109.22"/>
<wire x1="332.74" y1="109.22" x2="314.96" y2="109.22" width="0.1524" layer="91"/>
<wire x1="314.96" y1="109.22" x2="313.69" y2="110.49" width="0.1524" layer="91"/>
<wire x1="313.69" y1="110.49" x2="313.69" y2="116.84" width="0.1524" layer="91"/>
<wire x1="313.69" y1="116.84" x2="314.96" y2="118.11" width="0.1524" layer="91"/>
<wire x1="314.96" y1="118.11" x2="323.85" y2="118.11" width="0.1524" layer="91"/>
<wire x1="323.85" y1="118.11" x2="325.12" y2="119.38" width="0.1524" layer="91"/>
<pinref part="WP" gate="G$1" pin="C"/>
<junction x="325.12" y="127"/>
<wire x1="325.12" y1="119.38" x2="325.12" y2="120.65" width="0.1524" layer="91"/>
<junction x="325.12" y="120.65"/>
<wire x1="325.12" y1="120.65" x2="325.12" y2="127" width="0.1524" layer="91"/>
<wire x1="325.12" y1="120.65" x2="322.58" y2="120.65" width="0.1524" layer="91"/>
<label x="322.58" y="120.65" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SD_CD" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="CARD_DETECT"/>
<junction x="332.74" y="114.3"/>
<wire x1="332.74" y1="114.3" x2="326.39" y2="114.3" width="0.1524" layer="91"/>
<label x="326.39" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="332.74" y1="114.3" x2="332.74" y2="118.11" width="0.1524" layer="91"/>
<wire x1="332.74" y1="118.11" x2="335.28" y2="120.65" width="0.1524" layer="91"/>
<pinref part="CD" gate="G$1" pin="C"/>
<junction x="335.28" y="127"/>
<wire x1="335.28" y1="120.65" x2="335.28" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<junction x="287.02" y="91.44"/>
<pinref part="U$8" gate="G$1" pin="DAT2"/>
<junction x="332.74" y="91.44"/>
<wire x1="287.02" y1="91.44" x2="332.74" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<junction x="325.12" y="137.16"/>
<pinref part="WP" gate="G$1" pin="A"/>
<junction x="325.12" y="134.62"/>
<wire x1="325.12" y1="137.16" x2="325.12" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<junction x="335.28" y="137.16"/>
<pinref part="CD" gate="G$1" pin="A"/>
<junction x="335.28" y="134.62"/>
<wire x1="335.28" y1="137.16" x2="335.28" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="DS1307" gate="G$1" pin="VBAT"/>
<junction x="226.06" y="83.82"/>
<wire x1="226.06" y1="83.82" x2="241.3" y2="83.82" width="0.1524" layer="91"/>
<wire x1="241.3" y1="83.82" x2="242.57" y2="82.55" width="0.1524" layer="91"/>
<pinref part="VBAT1" gate="G$1" pin="+"/>
<junction x="242.57" y="80.01"/>
<wire x1="242.57" y1="82.55" x2="242.57" y2="80.01" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DS_SQW" class="0">
<segment>
<pinref part="DS1307" gate="G$1" pin="SQW"/>
<wire x1="226.06" y1="86.36" x2="228.6" y2="86.36" width="0.1524" layer="91"/>
<label x="238.76" y="86.36" size="1.27" layer="95" xref="yes"/>
<junction x="228.6" y="86.36"/>
<wire x1="228.6" y1="86.36" x2="237.49" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<junction x="228.6" y="88.9"/>
<wire x1="228.6" y1="86.36" x2="228.6" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GSM_PWR" class="0">
<segment>
<pinref part="U5" gate="1" pin="PH6(OC2B)"/>
<junction x="44.45" y="78.74"/>
<wire x1="44.45" y1="78.74" x2="41.91" y2="78.74" width="0.1524" layer="91"/>
<label x="41.91" y="78.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PH_PORT" gate="G$1" pin="7"/>
<junction x="213.36" y="207.01"/>
<wire x1="213.36" y1="207.01" x2="214.63" y2="207.01" width="0.1524" layer="91"/>
<label x="214.63" y="207.01" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="DS1307" gate="G$1" pin="X1"/>
<junction x="205.74" y="101.6"/>
<pinref part="Q1" gate="G$1" pin="2"/>
<junction x="204.47" y="107.95"/>
<wire x1="205.74" y1="101.6" x2="204.47" y2="101.6" width="0.1524" layer="91"/>
<wire x1="204.47" y1="101.6" x2="204.47" y2="107.95" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="DS1307" gate="G$1" pin="X2"/>
<junction x="208.28" y="101.6"/>
<pinref part="Q1" gate="G$1" pin="1"/>
<junction x="209.55" y="107.95"/>
<wire x1="208.28" y1="101.6" x2="209.55" y2="101.6" width="0.1524" layer="91"/>
<wire x1="209.55" y1="101.6" x2="209.55" y2="107.95" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>usbtouart</description>
<plain>
<wire x1="7.62" y1="168.91" x2="125.73" y2="168.91" width="1.27" layer="94" style="longdash"/>
<wire x1="194.31" y1="168.91" x2="250.19" y2="168.91" width="1.27" layer="94" style="longdash"/>
<wire x1="250.19" y1="168.91" x2="250.19" y2="27.94" width="1.27" layer="94" style="longdash"/>
<wire x1="250.19" y1="27.94" x2="194.31" y2="27.94" width="1.27" layer="94" style="longdash"/>
<wire x1="125.73" y1="26.67" x2="7.62" y2="27.94" width="1.27" layer="94" style="longdash"/>
<wire x1="7.62" y1="27.94" x2="7.62" y2="76.2" width="1.27" layer="94" style="longdash"/>
<wire x1="7.62" y1="76.2" x2="7.62" y2="115.57" width="1.27" layer="94" style="longdash"/>
<wire x1="7.62" y1="115.57" x2="7.62" y2="168.91" width="1.27" layer="94" style="longdash"/>
<wire x1="125.73" y1="26.67" x2="194.31" y2="27.94" width="1.27" layer="94" style="longdash"/>
<wire x1="194.31" y1="27.94" x2="194.31" y2="168.91" width="1.27" layer="94" style="longdash"/>
<wire x1="194.31" y1="168.91" x2="125.73" y2="168.91" width="1.27" layer="94" style="longdash"/>
<wire x1="125.73" y1="168.91" x2="125.73" y2="116.84" width="1.27" layer="94" style="longdash"/>
<wire x1="125.73" y1="116.84" x2="125.73" y2="87.63" width="1.27" layer="94" style="longdash"/>
<wire x1="125.73" y1="87.63" x2="125.73" y2="60.96" width="1.27" layer="94" style="longdash"/>
<wire x1="125.73" y1="60.96" x2="125.73" y2="26.67" width="1.27" layer="94" style="longdash"/>
<wire x1="7.62" y1="115.57" x2="52.07" y2="115.57" width="1.27" layer="94" style="shortdash"/>
<wire x1="52.07" y1="115.57" x2="52.07" y2="113.03" width="1.27" layer="94" style="shortdash"/>
<wire x1="52.07" y1="113.03" x2="74.93" y2="113.03" width="1.27" layer="94" style="shortdash"/>
<wire x1="74.93" y1="113.03" x2="74.93" y2="115.57" width="1.27" layer="94" style="shortdash"/>
<wire x1="74.93" y1="115.57" x2="125.73" y2="115.57" width="1.27" layer="94" style="shortdash"/>
<wire x1="125.73" y1="115.57" x2="125.73" y2="116.84" width="1.27" layer="94" style="shortdash"/>
<wire x1="7.62" y1="76.2" x2="49.53" y2="76.2" width="1.27" layer="94" style="shortdash"/>
<wire x1="49.53" y1="76.2" x2="49.53" y2="78.74" width="1.27" layer="94" style="shortdash"/>
<wire x1="49.53" y1="78.74" x2="59.69" y2="78.74" width="1.27" layer="94" style="shortdash"/>
<wire x1="59.69" y1="78.74" x2="59.69" y2="85.09" width="1.27" layer="94" style="shortdash"/>
<wire x1="59.69" y1="85.09" x2="69.85" y2="85.09" width="1.27" layer="94" style="shortdash"/>
<wire x1="69.85" y1="85.09" x2="69.85" y2="88.9" width="1.27" layer="94" style="shortdash"/>
<wire x1="69.85" y1="88.9" x2="125.73" y2="88.9" width="1.27" layer="94" style="shortdash"/>
<wire x1="125.73" y1="88.9" x2="125.73" y2="87.63" width="1.27" layer="94" style="shortdash"/>
<wire x1="49.53" y1="76.2" x2="49.53" y2="44.45" width="1.27" layer="94" style="shortdash"/>
<wire x1="49.53" y1="44.45" x2="55.88" y2="44.45" width="1.27" layer="94" style="shortdash"/>
<wire x1="55.88" y1="44.45" x2="87.63" y2="44.45" width="1.27" layer="94" style="shortdash"/>
<wire x1="87.63" y1="44.45" x2="87.63" y2="58.42" width="1.27" layer="94" style="shortdash"/>
<wire x1="87.63" y1="58.42" x2="125.73" y2="58.42" width="1.27" layer="94" style="shortdash"/>
<wire x1="125.73" y1="58.42" x2="125.73" y2="60.96" width="1.27" layer="94" style="shortdash"/>
<wire x1="55.88" y1="44.45" x2="55.88" y2="27.94" width="1.27" layer="94" style="shortdash"/>
<text x="10.16" y="162.56" size="5.08" layer="94">USB to UART</text>
<text x="81.28" y="109.22" size="5.08" layer="94">Protctn Diode</text>
<text x="81.28" y="80.01" size="5.08" layer="94">RX TX LEDs</text>
<text x="104.14" y="54.61" size="2.54" layer="94">RESET MCU </text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="U$7" gate="A" x="104.14" y="123.19" rot="R180"/>
<instance part="C26" gate="G$1" x="35.56" y="83.82" rot="MR0"/>
<instance part="C27" gate="G$1" x="45.72" y="83.82"/>
<instance part="C28" gate="G$1" x="12.7" y="41.91"/>
<instance part="R34" gate="G$1" x="41.91" y="45.72" rot="R90"/>
<instance part="SDM03MT40_2" gate="G$1" x="40.64" y="100.33"/>
<instance part="CP_RX_LED" gate="G$1" x="85.09" y="74.93" rot="R90"/>
<instance part="CP_TX_LED" gate="G$1" x="85.09" y="64.77" rot="R90"/>
<instance part="R44" gate="G$1" x="73.66" y="74.93" rot="R180"/>
<instance part="R45" gate="G$1" x="73.66" y="64.77" rot="R180"/>
<instance part="C21" gate="G$1" x="95.25" y="40.64" rot="MR0"/>
<instance part="CP_UART" gate="G$1" x="30.48" y="52.07" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="CP_DM" class="0">
<segment>
<pinref part="U$7" gate="A" pin="D_MINUS"/>
<junction x="104.14" y="130.81"/>
<wire x1="104.14" y1="130.81" x2="106.68" y2="130.81" width="0.1524" layer="91"/>
<label x="106.68" y="130.81" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SDM03MT40_2" gate="G$1" pin="4"/>
<junction x="50.8" y="97.79"/>
<wire x1="50.8" y1="97.79" x2="60.96" y2="97.79" width="0.1524" layer="91"/>
<wire x1="60.96" y1="97.79" x2="67.31" y2="91.44" width="0.1524" layer="91"/>
<wire x1="67.31" y1="91.44" x2="69.85" y2="91.44" width="0.1524" layer="91"/>
<label x="71.12" y="91.44" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="CP_DP" class="0">
<segment>
<pinref part="U$7" gate="A" pin="D_PLUS"/>
<junction x="104.14" y="128.27"/>
<wire x1="104.14" y1="128.27" x2="106.68" y2="128.27" width="0.1524" layer="91"/>
<label x="106.68" y="128.27" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SDM03MT40_2" gate="G$1" pin="5"/>
<junction x="50.8" y="100.33"/>
<wire x1="50.8" y1="100.33" x2="60.96" y2="100.33" width="0.1524" layer="91"/>
<wire x1="60.96" y1="100.33" x2="66.04" y2="95.25" width="0.1524" layer="91"/>
<wire x1="66.04" y1="95.25" x2="69.85" y2="95.25" width="0.1524" layer="91"/>
<label x="69.85" y="95.25" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$7" gate="A" pin="GND"/>
<junction x="104.14" y="125.73"/>
<wire x1="104.14" y1="125.73" x2="106.68" y2="125.73" width="0.1524" layer="91"/>
<label x="106.68" y="125.73" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<junction x="45.72" y="81.28"/>
<pinref part="C26" gate="G$1" pin="2"/>
<junction x="35.56" y="81.28"/>
<wire x1="45.72" y1="81.28" x2="35.56" y2="81.28" width="0.1524" layer="91"/>
<wire x1="35.56" y1="81.28" x2="26.67" y2="81.28" width="0.1524" layer="91"/>
<wire x1="26.67" y1="81.28" x2="25.4" y2="80.01" width="0.1524" layer="91"/>
<label x="25.4" y="80.01" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SDM03MT40_2" gate="G$1" pin="3"/>
<junction x="30.48" y="97.79"/>
<pinref part="SDM03MT40_2" gate="G$1" pin="2"/>
<junction x="30.48" y="100.33"/>
<wire x1="30.48" y1="97.79" x2="30.48" y2="100.33" width="0.1524" layer="91"/>
<pinref part="SDM03MT40_2" gate="G$1" pin="1"/>
<junction x="30.48" y="102.87"/>
<wire x1="30.48" y1="100.33" x2="30.48" y2="102.87" width="0.1524" layer="91"/>
<wire x1="30.48" y1="97.79" x2="29.21" y2="97.79" width="0.1524" layer="91"/>
<wire x1="29.21" y1="97.79" x2="27.94" y2="96.52" width="0.1524" layer="91"/>
<label x="27.94" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<junction x="12.7" y="39.37"/>
<wire x1="12.7" y1="39.37" x2="12.7" y2="34.29" width="0.1524" layer="91"/>
<label x="12.7" y="34.29" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CP_UART" gate="G$1" pin="4"/>
<junction x="25.4" y="57.15"/>
<wire x1="25.4" y1="57.15" x2="25.4" y2="60.96" width="0.1524" layer="91"/>
<label x="25.4" y="60.96" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CP_RST*" class="0">
<segment>
<pinref part="U$7" gate="A" pin="*RST"/>
<junction x="104.14" y="143.51"/>
<wire x1="104.14" y1="143.51" x2="106.68" y2="143.51" width="0.1524" layer="91"/>
<label x="106.68" y="143.51" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R34" gate="G$1" pin="1"/>
<junction x="41.91" y="40.64"/>
<wire x1="41.91" y1="40.64" x2="41.91" y2="33.02" width="0.1524" layer="91"/>
<label x="41.91" y="33.02" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="CP_DTR" class="0">
<segment>
<pinref part="U$7" gate="A" pin="DTR"/>
<junction x="27.94" y="128.27"/>
<wire x1="27.94" y1="128.27" x2="24.13" y2="128.27" width="0.1524" layer="91"/>
<label x="24.13" y="128.27" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="74.93" y="38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="C21" gate="G$1" pin="2"/>
<junction x="95.25" y="38.1"/>
<wire x1="95.25" y1="38.1" x2="74.93" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CP_TX" class="0">
<segment>
<pinref part="U$7" gate="A" pin="TXD"/>
<junction x="27.94" y="133.35"/>
<wire x1="27.94" y1="133.35" x2="24.13" y2="133.35" width="0.1524" layer="91"/>
<label x="24.13" y="133.35" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CP_UART" gate="G$1" pin="3"/>
<junction x="27.94" y="57.15"/>
<wire x1="27.94" y1="57.15" x2="27.94" y2="60.96" width="0.1524" layer="91"/>
<label x="27.94" y="60.96" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CP_RX" class="0">
<segment>
<pinref part="U$7" gate="A" pin="RXD"/>
<junction x="27.94" y="135.89"/>
<wire x1="27.94" y1="135.89" x2="24.13" y2="135.89" width="0.1524" layer="91"/>
<label x="24.13" y="135.89" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CP_UART" gate="G$1" pin="2"/>
<junction x="30.48" y="57.15"/>
<wire x1="30.48" y1="57.15" x2="30.48" y2="60.96" width="0.1524" layer="91"/>
<label x="30.48" y="60.96" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CP_VPP" class="0">
<segment>
<pinref part="U$7" gate="A" pin="VPP"/>
<junction x="27.94" y="146.05"/>
<wire x1="27.94" y1="146.05" x2="25.4" y2="146.05" width="0.1524" layer="91"/>
<label x="25.4" y="146.05" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="1"/>
<junction x="12.7" y="46.99"/>
<wire x1="12.7" y1="46.99" x2="12.7" y2="49.53" width="0.1524" layer="91"/>
<label x="12.7" y="49.53" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="SDM03MT40_2" gate="G$1" pin="6"/>
<wire x1="50.8" y1="107.95" x2="50.8" y2="102.87" width="0.1524" layer="91"/>
<label x="50.8" y="107.95" size="1.778" layer="95" xref="yes"/>
<junction x="50.8" y="102.87"/>
</segment>
<segment>
<pinref part="CP_UART" gate="G$1" pin="1"/>
<junction x="33.02" y="57.15"/>
<wire x1="33.02" y1="57.15" x2="33.02" y2="60.96" width="0.1524" layer="91"/>
<label x="33.02" y="60.96" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U$7" gate="A" pin="VDD"/>
<junction x="104.14" y="135.89"/>
<wire x1="104.14" y1="135.89" x2="106.68" y2="135.89" width="0.1524" layer="91"/>
<label x="106.68" y="135.89" size="1.778" layer="95" xref="yes"/>
<pinref part="U$7" gate="A" pin="VIO"/>
<junction x="104.14" y="133.35"/>
<wire x1="104.14" y1="133.35" x2="106.68" y2="133.35" width="0.1524" layer="91"/>
<label x="106.68" y="133.35" size="1.778" layer="95" xref="yes"/>
<wire x1="104.14" y1="135.89" x2="104.14" y2="133.35" width="0.1524" layer="91"/>
<pinref part="U$7" gate="A" pin="REGIN"/>
<junction x="104.14" y="138.43"/>
<wire x1="104.14" y1="138.43" x2="106.68" y2="138.43" width="0.1524" layer="91"/>
<label x="106.68" y="138.43" size="1.778" layer="95" xref="yes"/>
<wire x1="104.14" y1="135.89" x2="104.14" y2="138.43" width="0.1524" layer="91"/>
<pinref part="U$7" gate="A" pin="VBUS"/>
<junction x="104.14" y="140.97"/>
<wire x1="104.14" y1="140.97" x2="106.68" y2="140.97" width="0.1524" layer="91"/>
<label x="106.68" y="140.97" size="1.778" layer="95" xref="yes"/>
<wire x1="104.14" y1="138.43" x2="104.14" y2="140.97" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="1"/>
<junction x="35.56" y="88.9"/>
<pinref part="C27" gate="G$1" pin="1"/>
<junction x="45.72" y="88.9"/>
<wire x1="35.56" y1="88.9" x2="45.72" y2="88.9" width="0.1524" layer="91"/>
<wire x1="45.72" y1="88.9" x2="52.07" y2="88.9" width="0.1524" layer="91"/>
<label x="52.07" y="88.9" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<junction x="41.91" y="50.8"/>
<wire x1="41.91" y1="50.8" x2="41.91" y2="52.07" width="0.1524" layer="91"/>
<label x="41.91" y="52.07" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R45" gate="G$1" pin="2"/>
<junction x="68.58" y="64.77"/>
<pinref part="R44" gate="G$1" pin="2"/>
<junction x="68.58" y="74.93"/>
<wire x1="68.58" y1="64.77" x2="68.58" y2="74.93" width="0.1524" layer="91"/>
<wire x1="68.58" y1="74.93" x2="66.04" y2="74.93" width="0.1524" layer="91"/>
<label x="66.04" y="74.93" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R45" gate="G$1" pin="1"/>
<junction x="78.74" y="64.77"/>
<pinref part="CP_TX_LED" gate="G$1" pin="A"/>
<junction x="82.55" y="64.77"/>
<wire x1="78.74" y1="64.77" x2="82.55" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R44" gate="G$1" pin="1"/>
<junction x="78.74" y="74.93"/>
<pinref part="CP_RX_LED" gate="G$1" pin="A"/>
<junction x="82.55" y="74.93"/>
<wire x1="78.74" y1="74.93" x2="82.55" y2="74.93" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MCU_RESET" class="0">
<segment>
<pinref part="C21" gate="G$1" pin="1"/>
<junction x="95.25" y="45.72"/>
<wire x1="95.25" y1="45.72" x2="95.25" y2="48.26" width="0.1524" layer="91"/>
<label x="95.25" y="48.26" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="CP_TX_LED" class="0">
<segment>
<pinref part="U$7" gate="A" pin="GPIO.0"/>
<junction x="27.94" y="151.13"/>
<wire x1="27.94" y1="151.13" x2="25.4" y2="151.13" width="0.1524" layer="91"/>
<label x="25.4" y="151.13" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CP_TX_LED" gate="G$1" pin="C"/>
<junction x="90.17" y="64.77"/>
<wire x1="90.17" y1="64.77" x2="92.71" y2="64.77" width="0.1524" layer="91"/>
<label x="92.71" y="64.77" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="CP_RX_LED" class="0">
<segment>
<pinref part="U$7" gate="A" pin="GPIO.1"/>
<junction x="27.94" y="153.67"/>
<wire x1="27.94" y1="153.67" x2="25.4" y2="153.67" width="0.1524" layer="91"/>
<label x="25.4" y="153.67" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CP_RX_LED" gate="G$1" pin="C"/>
<junction x="90.17" y="74.93"/>
<wire x1="90.17" y1="74.93" x2="92.71" y2="74.93" width="0.1524" layer="91"/>
<label x="92.71" y="74.93" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>sensor sheet</description>
<plain>
<wire x1="6.35" y1="254" x2="124.46" y2="254" width="1.27" layer="94" style="longdash"/>
<wire x1="124.46" y1="254" x2="381" y2="254" width="1.27" layer="94" style="longdash"/>
<wire x1="381" y1="254" x2="381" y2="25.4" width="1.27" layer="94" style="longdash"/>
<wire x1="381" y1="25.4" x2="287.02" y2="25.4" width="1.27" layer="94" style="longdash"/>
<wire x1="287.02" y1="25.4" x2="287.02" y2="6.35" width="1.27" layer="94" style="longdash"/>
<wire x1="287.02" y1="6.35" x2="6.35" y2="6.35" width="1.27" layer="94" style="longdash"/>
<wire x1="6.35" y1="6.35" x2="6.35" y2="204.47" width="1.27" layer="94" style="longdash"/>
<wire x1="6.35" y1="204.47" x2="6.35" y2="254" width="1.27" layer="94" style="longdash"/>
<wire x1="6.35" y1="204.47" x2="124.46" y2="204.47" width="1.27" layer="94" style="longdash"/>
<wire x1="124.46" y1="204.47" x2="124.46" y2="254" width="1.27" layer="94" style="longdash"/>
<text x="8.89" y="207.01" size="5.08" layer="94">I2C Humidity and Temperature</text>
</plain>
<instances>
<instance part="FRAME3" gate="G$1" x="0" y="0"/>
<instance part="U$5" gate="G$1" x="53.34" y="233.68"/>
<instance part="R18" gate="G$1" x="80.01" y="248.92" rot="R180"/>
<instance part="R19" gate="G$1" x="97.79" y="248.92" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="+3V3" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="VDD"/>
<junction x="35.56" y="241.3"/>
<wire x1="35.56" y1="241.3" x2="25.4" y2="241.3" width="0.1524" layer="91"/>
<label x="25.4" y="241.3" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="DNC"/>
<junction x="71.12" y="223.52"/>
<wire x1="71.12" y1="223.52" x2="90.17" y2="223.52" width="0.1524" layer="91"/>
<label x="90.17" y="223.52" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="DNC_2"/>
<junction x="71.12" y="220.98"/>
<wire x1="71.12" y1="220.98" x2="90.17" y2="220.98" width="0.1524" layer="91"/>
<label x="90.17" y="220.98" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<junction x="85.09" y="248.92"/>
<wire x1="85.09" y1="248.92" x2="85.09" y2="251.46" width="0.1524" layer="91"/>
<wire x1="85.09" y1="251.46" x2="86.36" y2="251.46" width="0.1524" layer="91"/>
<label x="86.36" y="251.46" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<junction x="102.87" y="248.92"/>
<wire x1="102.87" y1="248.92" x2="110.49" y2="248.92" width="0.1524" layer="91"/>
<label x="110.49" y="248.92" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="GND"/>
<junction x="35.56" y="236.22"/>
<wire x1="35.56" y1="236.22" x2="25.4" y2="236.22" width="0.1524" layer="91"/>
<label x="25.4" y="236.22" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="T_GND"/>
<junction x="35.56" y="226.06"/>
<wire x1="35.56" y1="226.06" x2="25.4" y2="226.06" width="0.1524" layer="91"/>
<label x="24.13" y="226.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="SDA"/>
<junction x="71.12" y="241.3"/>
<wire x1="71.12" y1="241.3" x2="74.93" y2="241.3" width="0.1524" layer="91"/>
<label x="90.17" y="241.3" size="1.778" layer="95" xref="yes"/>
<junction x="74.93" y="241.3"/>
<wire x1="74.93" y1="241.3" x2="90.17" y2="241.3" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<junction x="74.93" y="248.92"/>
<wire x1="74.93" y1="241.3" x2="74.93" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="SCL"/>
<junction x="71.12" y="236.22"/>
<wire x1="71.12" y1="236.22" x2="80.01" y2="236.22" width="0.1524" layer="91"/>
<label x="90.17" y="236.22" size="1.778" layer="95" xref="yes"/>
<junction x="80.01" y="236.22"/>
<wire x1="80.01" y1="236.22" x2="90.17" y2="236.22" width="0.1524" layer="91"/>
<wire x1="80.01" y1="236.22" x2="80.01" y2="243.84" width="0.1524" layer="91"/>
<wire x1="80.01" y1="243.84" x2="91.44" y2="243.84" width="0.1524" layer="91"/>
<wire x1="91.44" y1="243.84" x2="92.71" y2="245.11" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
<junction x="92.71" y="248.92"/>
<wire x1="92.71" y1="245.11" x2="92.71" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>RF Communication</description>
<plain>
<wire x1="5.08" y1="255.27" x2="199.39" y2="255.27" width="1.27" layer="94" style="longdash"/>
<wire x1="199.39" y1="255.27" x2="382.27" y2="255.27" width="1.27" layer="94" style="longdash"/>
<wire x1="382.27" y1="255.27" x2="382.27" y2="25.4" width="1.27" layer="94" style="longdash"/>
<wire x1="382.27" y1="25.4" x2="288.29" y2="25.4" width="1.27" layer="94" style="longdash"/>
<wire x1="288.29" y1="25.4" x2="287.02" y2="25.4" width="1.27" layer="94" style="longdash"/>
<wire x1="287.02" y1="25.4" x2="287.02" y2="5.08" width="1.27" layer="94" style="longdash"/>
<wire x1="287.02" y1="5.08" x2="199.39" y2="5.08" width="1.27" layer="94" style="longdash"/>
<wire x1="199.39" y1="5.08" x2="101.6" y2="5.08" width="1.27" layer="94" style="longdash"/>
<wire x1="101.6" y1="5.08" x2="5.08" y2="5.08" width="1.27" layer="94" style="longdash"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="45.72" width="1.27" layer="94" style="longdash"/>
<wire x1="5.08" y1="45.72" x2="5.08" y2="152.4" width="1.27" layer="94" style="longdash"/>
<wire x1="5.08" y1="152.4" x2="5.08" y2="180.34" width="1.27" layer="94" style="longdash"/>
<wire x1="5.08" y1="180.34" x2="5.08" y2="205.74" width="1.27" layer="94" style="longdash"/>
<wire x1="5.08" y1="205.74" x2="5.08" y2="255.27" width="1.27" layer="94" style="longdash"/>
<wire x1="5.08" y1="152.4" x2="199.39" y2="152.4" width="1.27" layer="94" style="longdash"/>
<wire x1="199.39" y1="152.4" x2="199.39" y2="255.27" width="1.27" layer="94" style="longdash"/>
<wire x1="101.6" y1="38.1" x2="101.6" y2="5.08" width="1.27" layer="94" style="longdash"/>
<wire x1="199.39" y1="151.13" x2="199.39" y2="53.34" width="1.27" layer="94" style="longdash"/>
<wire x1="199.39" y1="53.34" x2="199.39" y2="39.37" width="1.27" layer="94" style="longdash"/>
<wire x1="199.39" y1="39.37" x2="199.39" y2="5.08" width="1.27" layer="94" style="longdash"/>
<wire x1="199.39" y1="55.88" x2="199.39" y2="53.34" width="1.27" layer="94" style="shortdash"/>
<wire x1="5.08" y1="45.72" x2="40.64" y2="45.72" width="1.27" layer="94" style="shortdash"/>
<wire x1="40.64" y1="45.72" x2="40.64" y2="36.83" width="1.27" layer="94" style="shortdash"/>
<wire x1="40.64" y1="36.83" x2="101.6" y2="36.83" width="1.27" layer="94" style="shortdash"/>
<wire x1="101.6" y1="36.83" x2="101.6" y2="38.1" width="1.27" layer="94" style="shortdash"/>
<wire x1="5.08" y1="205.74" x2="57.15" y2="205.74" width="1.27" layer="94" style="shortdash"/>
<wire x1="57.15" y1="205.74" x2="57.15" y2="194.31" width="1.27" layer="94" style="shortdash"/>
<wire x1="57.15" y1="194.31" x2="64.77" y2="194.31" width="1.27" layer="94" style="shortdash"/>
<wire x1="64.77" y1="194.31" x2="64.77" y2="180.34" width="1.27" layer="94" style="shortdash"/>
<wire x1="64.77" y1="180.34" x2="5.08" y2="180.34" width="1.27" layer="94" style="shortdash"/>
<wire x1="288.29" y1="25.4" x2="287.02" y2="25.4" width="1.27" layer="94" style="shortdash"/>
<wire x1="199.39" y1="40.64" x2="199.39" y2="39.37" width="1.27" layer="94" style="shortdash"/>
<text x="7.62" y="248.92" size="5.08" layer="94">RAK Wifi</text>
<text x="19.05" y="127" size="5.08" layer="94">BLE112</text>
<text x="57.15" y="29.21" size="5.08" layer="94">BLE PRGMR</text>
<text x="364.49" y="35.56" size="5.08" layer="94">GSM</text>
<text x="355.6" y="243.84" size="2.54" layer="94">Sim Card</text>
<text x="226.06" y="54.61" size="2.54" layer="94">Net Light</text>
<text x="233.68" y="191.77" size="2.54" layer="94">GSM PWR</text>
<text x="214.63" y="121.92" size="2.54" layer="94">RF_ANT</text>
</plain>
<instances>
<instance part="FRAME4" gate="G$1" x="0" y="0"/>
<instance part="BLE112" gate="G$1" x="119.38" y="88.9"/>
<instance part="RAK410" gate="G$1" x="104.14" y="213.36"/>
<instance part="IC1" gate="G$1" x="335.28" y="102.87"/>
<instance part="R12" gate="G$1" x="25.4" y="223.52" rot="R90"/>
<instance part="R13" gate="G$1" x="110.49" y="195.58" rot="R180"/>
<instance part="R14" gate="G$1" x="29.21" y="193.04" rot="R270"/>
<instance part="R15" gate="G$1" x="36.83" y="193.04" rot="R270"/>
<instance part="C13" gate="G$1" x="156.21" y="238.76"/>
<instance part="C14" gate="G$1" x="166.37" y="238.76"/>
<instance part="C15" gate="G$1" x="99.06" y="119.38"/>
<instance part="C16" gate="G$1" x="88.9" y="119.38"/>
<instance part="R16" gate="G$1" x="85.09" y="139.7" rot="R270"/>
<instance part="X1" gate="G$1" x="39.37" y="16.51"/>
<instance part="BLE_LED" gate="G$1" x="26.67" y="31.75" rot="R90"/>
<instance part="R17" gate="G$1" x="16.51" y="31.75"/>
<instance part="U$10" gate="G$1" x="355.6" y="229.87"/>
<instance part="C17" gate="G$1" x="320.04" y="237.49"/>
<instance part="R26" gate="G$1" x="334.01" y="224.79" rot="R180"/>
<instance part="R27" gate="G$1" x="335.28" y="220.98" rot="R180"/>
<instance part="R28" gate="G$1" x="336.55" y="217.17" rot="R180"/>
<instance part="R29" gate="G$1" x="238.76" y="204.47" rot="R270"/>
<instance part="C19" gate="G$1" x="360.68" y="160.02"/>
<instance part="NET_T" gate="G$1" x="234.95" y="44.45" rot="MR0"/>
<instance part="R30" gate="G$1" x="213.36" y="45.72"/>
<instance part="R31" gate="G$1" x="246.38" y="39.37" rot="R180"/>
<instance part="GSM_NET" gate="G$1" x="223.52" y="45.72" rot="R90"/>
<instance part="R32" gate="G$1" x="246.38" y="44.45"/>
<instance part="C24" gate="G$1" x="232.41" y="119.38" rot="R90"/>
<instance part="C25" gate="G$1" x="232.41" y="109.22" rot="R90"/>
<instance part="R33" gate="G$1" x="238.76" y="114.3" rot="R90"/>
<instance part="RF-SMA" gate="G$1" x="223.52" y="88.9"/>
<instance part="T1" gate="A" x="227.33" y="200.66"/>
<instance part="R35" gate="G$1" x="224.79" y="180.34"/>
<instance part="IC1" gate="GND@1" x="347.98" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@2" x="344.17" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@3" x="340.36" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@4" x="336.55" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@5" x="332.74" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@6" x="328.93" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@7" x="325.12" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@8" x="321.31" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@9" x="317.5" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@10" x="313.69" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@11" x="309.88" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@12" x="306.07" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@13" x="302.26" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@14" x="298.45" y="187.96" rot="R180"/>
<instance part="IC1" gate="GND@15" x="294.64" y="187.96" rot="R180"/>
<instance part="IC1" gate="VBAT@1" x="307.34" y="199.39" rot="R270"/>
<instance part="IC1" gate="VBAT@2" x="300.99" y="199.39" rot="R270"/>
<instance part="IC1" gate="VBAT@3" x="294.64" y="199.39" rot="R270"/>
<instance part="C29" gate="G$1" x="313.69" y="198.12"/>
<instance part="C30" gate="G$1" x="318.77" y="195.58"/>
<instance part="C31" gate="G$1" x="323.85" y="195.58"/>
<instance part="R7" gate="G$1" x="227.33" y="68.58"/>
<instance part="GSMPWR" gate="G$1" x="238.76" y="68.58" rot="R90"/>
<instance part="BLE_PORT2" gate="G$1" x="30.48" y="104.14"/>
<instance part="BLE_PORT1" gate="G$1" x="30.48" y="73.66"/>
<instance part="BLE_PORT0" gate="G$1" x="67.31" y="52.07"/>
<instance part="RAK_UART" gate="G$1" x="180.34" y="173.99" rot="R180"/>
<instance part="GSM_UART" gate="G$1" x="222.25" y="149.86"/>
<instance part="SIM_PROTECTION_DIODE" gate="G$1" x="251.46" y="240.03"/>
<instance part="RAK_FRM_UPD_JP" gate="G$1" x="109.22" y="172.72"/>
<instance part="R21" gate="G$1" x="238.76" y="101.6" rot="R90"/>
<instance part="BLE_USB" gate="G$1" x="140.97" y="26.67" rot="R90"/>
<instance part="R22" gate="G$1" x="10.16" y="97.79" rot="R90"/>
<instance part="R23" gate="G$1" x="17.78" y="97.79" rot="R90"/>
<instance part="R24" gate="G$1" x="11.43" y="74.93" rot="R90"/>
<instance part="R25" gate="G$1" x="19.05" y="74.93" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="RAK_MCU_RESET" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="MCU_RESET"/>
<junction x="58.42" y="213.36"/>
<wire x1="58.42" y1="213.36" x2="54.61" y2="213.36" width="0.1524" layer="91"/>
<label x="54.61" y="213.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RAK_FRM_UPD_JP" gate="G$1" pin="3"/>
<junction x="114.3" y="170.18"/>
<wire x1="114.3" y1="170.18" x2="116.84" y2="170.18" width="0.1524" layer="91"/>
<label x="116.84" y="170.18" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_ERROR" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="MCU_ERROR"/>
<junction x="58.42" y="218.44"/>
<wire x1="58.42" y1="218.44" x2="54.61" y2="218.44" width="0.1524" layer="91"/>
<label x="54.61" y="218.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_MODE" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="MCU_MODE"/>
<junction x="58.42" y="223.52"/>
<wire x1="58.42" y1="223.52" x2="54.61" y2="223.52" width="0.1524" layer="91"/>
<label x="54.61" y="223.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<junction x="25.4" y="218.44"/>
<wire x1="25.4" y1="218.44" x2="22.86" y2="218.44" width="0.1524" layer="91"/>
<label x="22.86" y="218.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RAK410" gate="G$1" pin="GND"/>
<junction x="149.86" y="231.14"/>
<wire x1="149.86" y1="231.14" x2="154.94" y2="231.14" width="0.1524" layer="91"/>
<label x="154.94" y="231.14" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<junction x="115.57" y="195.58"/>
<wire x1="115.57" y1="195.58" x2="116.84" y2="195.58" width="0.1524" layer="91"/>
<wire x1="116.84" y1="195.58" x2="118.11" y2="194.31" width="0.1524" layer="91"/>
<wire x1="118.11" y1="194.31" x2="118.11" y2="193.04" width="0.1524" layer="91"/>
<label x="119.38" y="193.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<junction x="166.37" y="243.84"/>
<pinref part="C13" gate="G$1" pin="1"/>
<junction x="156.21" y="243.84"/>
<wire x1="166.37" y1="243.84" x2="156.21" y2="243.84" width="0.1524" layer="91"/>
<pinref part="RAK410" gate="G$1" pin="GND_6"/>
<junction x="119.38" y="243.84"/>
<pinref part="RAK410" gate="G$1" pin="GND_5"/>
<junction x="121.92" y="243.84"/>
<wire x1="119.38" y1="243.84" x2="121.92" y2="243.84" width="0.1524" layer="91"/>
<pinref part="RAK410" gate="G$1" pin="GND_4"/>
<junction x="124.46" y="243.84"/>
<wire x1="121.92" y1="243.84" x2="124.46" y2="243.84" width="0.1524" layer="91"/>
<pinref part="RAK410" gate="G$1" pin="GND_3"/>
<junction x="127" y="243.84"/>
<wire x1="124.46" y1="243.84" x2="127" y2="243.84" width="0.1524" layer="91"/>
<pinref part="RAK410" gate="G$1" pin="GND_2"/>
<junction x="129.54" y="243.84"/>
<wire x1="127" y1="243.84" x2="129.54" y2="243.84" width="0.1524" layer="91"/>
<pinref part="RAK410" gate="G$1" pin="GND_1"/>
<junction x="132.08" y="243.84"/>
<wire x1="129.54" y1="243.84" x2="132.08" y2="243.84" width="0.1524" layer="91"/>
<wire x1="132.08" y1="243.84" x2="156.21" y2="243.84" width="0.1524" layer="91"/>
<wire x1="166.37" y1="243.84" x2="181.61" y2="243.84" width="0.1524" layer="91"/>
<label x="181.61" y="243.84" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE112" gate="G$1" pin="GND"/>
<junction x="99.06" y="109.22"/>
<wire x1="99.06" y1="109.22" x2="95.25" y2="109.22" width="0.1524" layer="91"/>
<label x="95.25" y="109.22" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="C16" gate="G$1" pin="2"/>
<junction x="88.9" y="116.84"/>
<pinref part="C15" gate="G$1" pin="2"/>
<junction x="99.06" y="116.84"/>
<wire x1="88.9" y1="116.84" x2="99.06" y2="116.84" width="0.1524" layer="91"/>
<wire x1="99.06" y1="116.84" x2="99.06" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="BLE112" gate="G$1" pin="GND@4"/>
<junction x="139.7" y="99.06"/>
<wire x1="139.7" y1="99.06" x2="143.51" y2="99.06" width="0.1524" layer="91"/>
<label x="143.51" y="99.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE112" gate="G$1" pin="GND@3"/>
<junction x="129.54" y="60.96"/>
<wire x1="129.54" y1="60.96" x2="134.62" y2="60.96" width="0.1524" layer="91"/>
<label x="134.62" y="60.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE112" gate="G$1" pin="GND@2"/>
<junction x="99.06" y="86.36"/>
<wire x1="99.06" y1="86.36" x2="95.25" y2="86.36" width="0.1524" layer="91"/>
<label x="95.25" y="86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="1"/>
<junction x="46.99" y="11.43"/>
<wire x1="46.99" y1="11.43" x2="54.61" y2="11.43" width="0.1524" layer="91"/>
<label x="55.88" y="11.43" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="360.68" y1="157.48" x2="360.68" y2="154.94" width="0.1524" layer="91"/>
<label x="360.68" y="154.94" size="1.27" layer="95" xref="yes"/>
<junction x="360.68" y="157.48"/>
</segment>
<segment>
<pinref part="U$10" gate="G$1" pin="C5"/>
<junction x="342.9" y="234.95"/>
<pinref part="C17" gate="G$1" pin="2"/>
<junction x="320.04" y="234.95"/>
<wire x1="342.9" y1="234.95" x2="320.04" y2="234.95" width="0.1524" layer="91"/>
<wire x1="320.04" y1="234.95" x2="304.8" y2="234.95" width="0.1524" layer="91"/>
<label x="304.8" y="234.95" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RF-SMA" gate="G$1" pin="GND"/>
<junction x="226.06" y="86.36"/>
<pinref part="RF-SMA" gate="G$1" pin="GND@1"/>
<junction x="226.06" y="83.82"/>
<wire x1="226.06" y1="86.36" x2="226.06" y2="83.82" width="0.1524" layer="91"/>
<pinref part="RF-SMA" gate="G$1" pin="GND@2"/>
<junction x="226.06" y="81.28"/>
<wire x1="226.06" y1="83.82" x2="226.06" y2="81.28" width="0.1524" layer="91"/>
<pinref part="RF-SMA" gate="G$1" pin="GND@3"/>
<junction x="226.06" y="78.74"/>
<wire x1="226.06" y1="81.28" x2="226.06" y2="78.74" width="0.1524" layer="91"/>
<wire x1="226.06" y1="78.74" x2="227.33" y2="80.01" width="0.1524" layer="91"/>
<wire x1="227.33" y1="80.01" x2="227.33" y2="82.55" width="0.1524" layer="91"/>
<wire x1="227.33" y1="82.55" x2="228.6" y2="82.55" width="0.1524" layer="91"/>
<label x="228.6" y="82.55" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="1"/>
<junction x="227.33" y="109.22"/>
<pinref part="C24" gate="G$1" pin="1"/>
<junction x="227.33" y="119.38"/>
<wire x1="227.33" y1="109.22" x2="227.33" y2="119.38" width="0.1524" layer="91"/>
<wire x1="227.33" y1="119.38" x2="224.79" y2="119.38" width="0.1524" layer="91"/>
<label x="224.79" y="119.38" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="NET_T" gate="G$1" pin="E"/>
<junction x="232.41" y="39.37"/>
<wire x1="232.41" y1="39.37" x2="226.06" y2="39.37" width="0.1524" layer="91"/>
<label x="226.06" y="39.37" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<junction x="251.46" y="39.37"/>
<wire x1="251.46" y1="39.37" x2="254" y2="39.37" width="0.1524" layer="91"/>
<label x="254" y="39.37" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="GND@15" pin="GND"/>
<junction x="294.64" y="187.96"/>
<pinref part="IC1" gate="GND@14" pin="GND"/>
<junction x="298.45" y="187.96"/>
<wire x1="294.64" y1="187.96" x2="298.45" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@13" pin="GND"/>
<junction x="302.26" y="187.96"/>
<wire x1="298.45" y1="187.96" x2="302.26" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@12" pin="GND"/>
<junction x="306.07" y="187.96"/>
<wire x1="302.26" y1="187.96" x2="306.07" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@11" pin="GND"/>
<junction x="309.88" y="187.96"/>
<wire x1="306.07" y1="187.96" x2="309.88" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@10" pin="GND"/>
<junction x="313.69" y="187.96"/>
<wire x1="309.88" y1="187.96" x2="313.69" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@9" pin="GND"/>
<junction x="317.5" y="187.96"/>
<wire x1="313.69" y1="187.96" x2="317.5" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@8" pin="GND"/>
<junction x="321.31" y="187.96"/>
<wire x1="317.5" y1="187.96" x2="321.31" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@7" pin="GND"/>
<junction x="325.12" y="187.96"/>
<wire x1="321.31" y1="187.96" x2="325.12" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@6" pin="GND"/>
<junction x="328.93" y="187.96"/>
<wire x1="325.12" y1="187.96" x2="328.93" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@5" pin="GND"/>
<junction x="332.74" y="187.96"/>
<wire x1="328.93" y1="187.96" x2="332.74" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@4" pin="GND"/>
<junction x="336.55" y="187.96"/>
<wire x1="332.74" y1="187.96" x2="336.55" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@3" pin="GND"/>
<junction x="340.36" y="187.96"/>
<wire x1="336.55" y1="187.96" x2="340.36" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@2" pin="GND"/>
<junction x="344.17" y="187.96"/>
<wire x1="340.36" y1="187.96" x2="344.17" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="GND@1" pin="GND"/>
<junction x="347.98" y="187.96"/>
<wire x1="344.17" y1="187.96" x2="347.98" y2="187.96" width="0.1524" layer="91"/>
<wire x1="347.98" y1="187.96" x2="349.25" y2="187.96" width="0.1524" layer="91"/>
<wire x1="349.25" y1="187.96" x2="350.52" y2="186.69" width="0.1524" layer="91"/>
<wire x1="350.52" y1="186.69" x2="350.52" y2="184.15" width="0.1524" layer="91"/>
<wire x1="350.52" y1="184.15" x2="351.79" y2="182.88" width="0.1524" layer="91"/>
<wire x1="351.79" y1="182.88" x2="353.06" y2="182.88" width="0.1524" layer="91"/>
<label x="353.06" y="182.88" size="1.778" layer="95" xref="yes"/>
<wire x1="317.5" y1="187.96" x2="318.77" y2="189.23" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="2"/>
<junction x="323.85" y="193.04"/>
<pinref part="C30" gate="G$1" pin="2"/>
<junction x="318.77" y="193.04"/>
<wire x1="323.85" y1="193.04" x2="318.77" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="-"/>
<junction x="313.69" y="193.04"/>
<wire x1="318.77" y1="193.04" x2="313.69" y2="193.04" width="0.1524" layer="91"/>
<wire x1="318.77" y1="189.23" x2="318.77" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="238.76" y1="199.39" x2="238.76" y2="196.85" width="0.1524" layer="91"/>
<label x="238.76" y="196.85" size="1.778" layer="95" xref="yes"/>
<junction x="238.76" y="199.39"/>
</segment>
<segment>
<pinref part="GSMPWR" gate="G$1" pin="C"/>
<junction x="243.84" y="68.58"/>
<wire x1="243.84" y1="68.58" x2="246.38" y2="68.58" width="0.1524" layer="91"/>
<wire x1="246.38" y1="68.58" x2="246.38" y2="67.31" width="0.1524" layer="91"/>
<label x="246.38" y="67.31" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT2" gate="G$1" pin="8"/>
<junction x="35.56" y="114.3"/>
<wire x1="35.56" y1="114.3" x2="38.1" y2="114.3" width="0.1524" layer="91"/>
<label x="38.1" y="114.3" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT0" gate="G$1" pin="7"/>
<junction x="72.39" y="59.69"/>
<wire x1="72.39" y1="59.69" x2="74.93" y2="59.69" width="0.1524" layer="91"/>
<label x="74.93" y="59.69" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RAK_UART" gate="G$1" pin="4"/>
<junction x="175.26" y="168.91"/>
<wire x1="175.26" y1="168.91" x2="171.45" y2="168.91" width="0.1524" layer="91"/>
<label x="171.45" y="168.91" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="GSM_UART" gate="G$1" pin="4"/>
<junction x="227.33" y="154.94"/>
<wire x1="227.33" y1="154.94" x2="231.14" y2="154.94" width="0.1524" layer="91"/>
<label x="231.14" y="154.94" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SIM_PROTECTION_DIODE" gate="G$1" pin="1"/>
<junction x="241.3" y="242.57"/>
<pinref part="SIM_PROTECTION_DIODE" gate="G$1" pin="2"/>
<junction x="241.3" y="240.03"/>
<wire x1="241.3" y1="242.57" x2="241.3" y2="240.03" width="0.1524" layer="91"/>
<pinref part="SIM_PROTECTION_DIODE" gate="G$1" pin="3"/>
<junction x="241.3" y="237.49"/>
<wire x1="241.3" y1="240.03" x2="241.3" y2="237.49" width="0.1524" layer="91"/>
<wire x1="241.3" y1="237.49" x2="236.22" y2="237.49" width="0.1524" layer="91"/>
<label x="236.22" y="237.49" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RAK_FRM_UPD_JP" gate="G$1" pin="2"/>
<junction x="114.3" y="167.64"/>
<wire x1="114.3" y1="167.64" x2="116.84" y2="167.64" width="0.1524" layer="91"/>
<label x="116.84" y="167.64" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_USB" gate="G$1" pin="1"/>
<junction x="143.51" y="31.75"/>
<wire x1="143.51" y1="31.75" x2="143.51" y2="35.56" width="0.1524" layer="91"/>
<label x="143.51" y="35.56" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<junction x="17.78" y="92.71"/>
<pinref part="R22" gate="G$1" pin="1"/>
<junction x="10.16" y="92.71"/>
<wire x1="17.78" y1="92.71" x2="10.16" y2="92.71" width="0.1524" layer="91"/>
<wire x1="10.16" y1="92.71" x2="10.16" y2="88.9" width="0.1524" layer="91"/>
<label x="10.16" y="88.9" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<junction x="19.05" y="69.85"/>
<pinref part="R24" gate="G$1" pin="1"/>
<junction x="11.43" y="69.85"/>
<wire x1="19.05" y1="69.85" x2="11.43" y2="69.85" width="0.1524" layer="91"/>
<wire x1="11.43" y1="69.85" x2="11.43" y2="62.23" width="0.1524" layer="91"/>
<label x="12.7" y="62.23" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="BIAS"/>
<junction x="58.42" y="228.6"/>
<pinref part="R12" gate="G$1" pin="2"/>
<junction x="25.4" y="228.6"/>
<wire x1="58.42" y1="228.6" x2="25.4" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RAK_MCU_UART1_TX" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="MCU_UART1_TX"/>
<junction x="83.82" y="198.12"/>
<wire x1="83.82" y1="198.12" x2="83.82" y2="191.77" width="0.1524" layer="91"/>
<label x="83.82" y="191.77" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="RAK_UART" gate="G$1" pin="2"/>
<junction x="175.26" y="173.99"/>
<wire x1="175.26" y1="173.99" x2="171.45" y2="173.99" width="0.1524" layer="91"/>
<label x="171.45" y="173.99" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_UART1_RX" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="MCU_UART1_RX"/>
<junction x="86.36" y="198.12"/>
<wire x1="86.36" y1="198.12" x2="86.36" y2="191.77" width="0.1524" layer="91"/>
<label x="86.36" y="191.77" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="RAK_UART" gate="G$1" pin="3"/>
<junction x="175.26" y="171.45"/>
<wire x1="175.26" y1="171.45" x2="171.45" y2="171.45" width="0.1524" layer="91"/>
<label x="171.45" y="171.45" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_WPS" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="MCU_WPS"/>
<junction x="93.98" y="198.12"/>
<wire x1="93.98" y1="198.12" x2="93.98" y2="191.77" width="0.1524" layer="91"/>
<label x="93.98" y="191.77" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<junction x="29.21" y="187.96"/>
<wire x1="29.21" y1="187.96" x2="29.21" y2="185.42" width="0.1524" layer="91"/>
<label x="29.21" y="185.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_SPI_INT" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="MCU_SPI_INT"/>
<junction x="96.52" y="198.12"/>
<wire x1="96.52" y1="198.12" x2="96.52" y2="191.77" width="0.1524" layer="91"/>
<label x="96.52" y="191.77" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="RAK_FRM_UPD_JP" gate="G$1" pin="8"/>
<junction x="114.3" y="182.88"/>
<wire x1="114.3" y1="182.88" x2="116.84" y2="182.88" width="0.1524" layer="91"/>
<label x="116.84" y="182.88" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_BOOT" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="MCU_BOOT"/>
<junction x="99.06" y="198.12"/>
<wire x1="99.06" y1="198.12" x2="99.06" y2="191.77" width="0.1524" layer="91"/>
<label x="99.06" y="191.77" size="1.778" layer="95" rot="R270" xref="yes"/>
<wire x1="99.06" y1="198.12" x2="104.14" y2="198.12" width="0.1524" layer="91"/>
<wire x1="104.14" y1="198.12" x2="105.41" y2="196.85" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<junction x="105.41" y="195.58"/>
<wire x1="105.41" y1="196.85" x2="105.41" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RAK_MCU_US0_TX" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="MCU_USO_TX"/>
<junction x="149.86" y="208.28"/>
<wire x1="149.86" y1="208.28" x2="163.83" y2="208.28" width="0.1524" layer="91"/>
<label x="163.83" y="208.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<junction x="36.83" y="187.96"/>
<wire x1="36.83" y1="187.96" x2="36.83" y2="185.42" width="0.1524" layer="91"/>
<label x="36.83" y="185.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RAK_FRM_UPD_JP" gate="G$1" pin="4"/>
<junction x="114.3" y="172.72"/>
<wire x1="114.3" y1="172.72" x2="116.84" y2="172.72" width="0.1524" layer="91"/>
<label x="116.84" y="172.72" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_US0_RX" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="MCU_USO_RX"/>
<junction x="149.86" y="210.82"/>
<wire x1="149.86" y1="210.82" x2="163.83" y2="210.82" width="0.1524" layer="91"/>
<label x="163.83" y="210.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RAK_FRM_UPD_JP" gate="G$1" pin="5"/>
<junction x="114.3" y="175.26"/>
<wire x1="114.3" y1="175.26" x2="116.84" y2="175.26" width="0.1524" layer="91"/>
<label x="116.84" y="175.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_US0_CLK" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="MCU_USO_CLK"/>
<junction x="149.86" y="213.36"/>
<wire x1="149.86" y1="213.36" x2="163.83" y2="213.36" width="0.1524" layer="91"/>
<label x="163.83" y="213.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RAK_FRM_UPD_JP" gate="G$1" pin="6"/>
<junction x="114.3" y="177.8"/>
<wire x1="114.3" y1="177.8" x2="116.84" y2="177.8" width="0.1524" layer="91"/>
<label x="116.84" y="177.8" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAK_MCU_US0_CS" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="MCU_USO_CS"/>
<junction x="149.86" y="215.9"/>
<wire x1="149.86" y1="215.9" x2="163.83" y2="215.9" width="0.1524" layer="91"/>
<label x="163.83" y="215.9" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RAK_FRM_UPD_JP" gate="G$1" pin="7"/>
<junction x="114.3" y="180.34"/>
<wire x1="114.3" y1="180.34" x2="116.84" y2="180.34" width="0.1524" layer="91"/>
<label x="116.84" y="180.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="RAK410" gate="G$1" pin="VDD"/>
<junction x="149.86" y="228.6"/>
<wire x1="149.86" y1="228.6" x2="154.94" y2="228.6" width="0.1524" layer="91"/>
<label x="154.94" y="228.6" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<junction x="29.21" y="198.12"/>
<wire x1="29.21" y1="198.12" x2="29.21" y2="200.66" width="0.1524" layer="91"/>
<label x="29.21" y="200.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<junction x="36.83" y="198.12"/>
<wire x1="36.83" y1="198.12" x2="36.83" y2="200.66" width="0.1524" layer="91"/>
<label x="36.83" y="200.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<junction x="156.21" y="236.22"/>
<pinref part="C14" gate="G$1" pin="2"/>
<junction x="166.37" y="236.22"/>
<wire x1="156.21" y1="236.22" x2="166.37" y2="236.22" width="0.1524" layer="91"/>
<wire x1="166.37" y1="236.22" x2="175.26" y2="236.22" width="0.1524" layer="91"/>
<wire x1="175.26" y1="236.22" x2="177.8" y2="238.76" width="0.1524" layer="91"/>
<wire x1="177.8" y1="238.76" x2="181.61" y2="238.76" width="0.1524" layer="91"/>
<label x="181.61" y="238.76" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE112" gate="G$1" pin="AVDD"/>
<junction x="99.06" y="106.68"/>
<wire x1="99.06" y1="106.68" x2="95.25" y2="106.68" width="0.1524" layer="91"/>
<label x="95.25" y="106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BLE112" gate="G$1" pin="AVDD@2"/>
<junction x="99.06" y="104.14"/>
<wire x1="99.06" y1="104.14" x2="95.25" y2="104.14" width="0.1524" layer="91"/>
<label x="95.25" y="104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<junction x="99.06" y="124.46"/>
<pinref part="C16" gate="G$1" pin="1"/>
<junction x="88.9" y="124.46"/>
<wire x1="99.06" y1="124.46" x2="88.9" y2="124.46" width="0.1524" layer="91"/>
<wire x1="88.9" y1="124.46" x2="88.9" y2="125.73" width="0.1524" layer="91"/>
<wire x1="88.9" y1="125.73" x2="87.63" y2="127" width="0.1524" layer="91"/>
<label x="87.63" y="127" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<junction x="85.09" y="144.78"/>
<wire x1="85.09" y1="144.78" x2="85.09" y2="146.05" width="0.1524" layer="91"/>
<label x="85.09" y="146.05" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BLE112" gate="G$1" pin="DVDD"/>
<junction x="127" y="60.96"/>
<wire x1="127" y1="60.96" x2="127" y2="55.88" width="0.1524" layer="91"/>
<label x="127" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="2"/>
<junction x="31.75" y="11.43"/>
<wire x1="31.75" y1="11.43" x2="24.13" y2="11.43" width="0.1524" layer="91"/>
<wire x1="24.13" y1="11.43" x2="22.86" y2="12.7" width="0.1524" layer="91"/>
<label x="22.86" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<junction x="11.43" y="31.75"/>
<wire x1="11.43" y1="31.75" x2="11.43" y2="40.64" width="0.1524" layer="91"/>
<wire x1="11.43" y1="40.64" x2="12.7" y2="41.91" width="0.1524" layer="91"/>
<label x="13.97" y="41.91" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT2" gate="G$1" pin="7"/>
<junction x="35.56" y="111.76"/>
<wire x1="35.56" y1="111.76" x2="38.1" y2="111.76" width="0.1524" layer="91"/>
<label x="38.1" y="111.76" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT0" gate="G$1" pin="8"/>
<junction x="72.39" y="62.23"/>
<wire x1="72.39" y1="62.23" x2="74.93" y2="62.23" width="0.1524" layer="91"/>
<label x="74.93" y="62.23" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RAK_UART" gate="G$1" pin="1"/>
<junction x="175.26" y="176.53"/>
<wire x1="175.26" y1="176.53" x2="171.45" y2="176.53" width="0.1524" layer="91"/>
<label x="171.45" y="176.53" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="GSM_UART" gate="G$1" pin="1"/>
<junction x="227.33" y="147.32"/>
<wire x1="227.33" y1="147.32" x2="231.14" y2="147.32" width="0.1524" layer="91"/>
<label x="231.14" y="147.32" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RAK_FRM_UPD_JP" gate="G$1" pin="1"/>
<junction x="114.3" y="165.1"/>
<wire x1="114.3" y1="165.1" x2="116.84" y2="165.1" width="0.1524" layer="91"/>
<label x="116.84" y="165.1" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE112" gate="G$1" pin="VDD_USB"/>
<junction x="99.06" y="88.9"/>
<wire x1="99.06" y1="88.9" x2="95.25" y2="88.9" width="0.1524" layer="91"/>
<label x="95.25" y="88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_USB" gate="G$1" pin="4"/>
<junction x="135.89" y="31.75"/>
<wire x1="135.89" y1="31.75" x2="135.89" y2="35.56" width="0.1524" layer="91"/>
<label x="135.89" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BLE_USB_PLUS" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="USB+"/>
<junction x="99.06" y="83.82"/>
<wire x1="99.06" y1="83.82" x2="95.25" y2="83.82" width="0.1524" layer="91"/>
<label x="95.25" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_USB" gate="G$1" pin="2"/>
<junction x="140.97" y="31.75"/>
<wire x1="140.97" y1="31.75" x2="140.97" y2="35.56" width="0.1524" layer="91"/>
<label x="140.97" y="35.56" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="BLE_USB_MINUS" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="USB-"/>
<junction x="99.06" y="81.28"/>
<wire x1="99.06" y1="81.28" x2="95.25" y2="81.28" width="0.1524" layer="91"/>
<label x="95.25" y="81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_USB" gate="G$1" pin="3"/>
<junction x="138.43" y="31.75"/>
<wire x1="138.43" y1="31.75" x2="138.43" y2="35.56" width="0.1524" layer="91"/>
<label x="138.43" y="35.56" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="BLE_RESET_N" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="RESET"/>
<junction x="139.7" y="96.52"/>
<wire x1="139.7" y1="96.52" x2="143.51" y2="96.52" width="0.1524" layer="91"/>
<label x="143.51" y="96.52" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="85.09" y1="134.62" x2="85.09" y2="132.08" width="0.1524" layer="91"/>
<wire x1="85.09" y1="132.08" x2="87.63" y2="132.08" width="0.1524" layer="91"/>
<label x="87.63" y="132.08" size="1.778" layer="95" xref="yes"/>
<junction x="85.09" y="134.62"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="7"/>
<junction x="46.99" y="19.05"/>
<wire x1="46.99" y1="19.05" x2="54.61" y2="19.05" width="0.1524" layer="91"/>
<label x="55.88" y="19.05" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT0" gate="G$1" pin="6"/>
<junction x="72.39" y="57.15"/>
<wire x1="72.39" y1="57.15" x2="74.93" y2="57.15" width="0.1524" layer="91"/>
<label x="74.93" y="57.15" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_P2_2" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P2_2"/>
<junction x="99.06" y="101.6"/>
<wire x1="99.06" y1="101.6" x2="95.25" y2="101.6" width="0.1524" layer="91"/>
<label x="95.25" y="101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="3"/>
<junction x="46.99" y="13.97"/>
<wire x1="46.99" y1="13.97" x2="54.61" y2="13.97" width="0.1524" layer="91"/>
<label x="55.88" y="13.97" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT2" gate="G$1" pin="6"/>
<junction x="35.56" y="109.22"/>
<wire x1="35.56" y1="109.22" x2="38.1" y2="109.22" width="0.1524" layer="91"/>
<label x="38.1" y="109.22" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_P2_1" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P2_1"/>
<junction x="99.06" y="99.06"/>
<wire x1="99.06" y1="99.06" x2="95.25" y2="99.06" width="0.1524" layer="91"/>
<label x="95.25" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="4"/>
<junction x="31.75" y="13.97"/>
<wire x1="31.75" y1="13.97" x2="24.13" y2="13.97" width="0.1524" layer="91"/>
<wire x1="24.13" y1="13.97" x2="22.86" y2="15.24" width="0.1524" layer="91"/>
<label x="22.86" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT2" gate="G$1" pin="5"/>
<junction x="35.56" y="106.68"/>
<wire x1="35.56" y1="106.68" x2="38.1" y2="106.68" width="0.1524" layer="91"/>
<label x="38.1" y="106.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_P2_0" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P2_0"/>
<junction x="99.06" y="96.52"/>
<wire x1="99.06" y1="96.52" x2="95.25" y2="96.52" width="0.1524" layer="91"/>
<label x="95.25" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT2" gate="G$1" pin="4"/>
<junction x="35.56" y="104.14"/>
<wire x1="35.56" y1="104.14" x2="38.1" y2="104.14" width="0.1524" layer="91"/>
<label x="38.1" y="104.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_UART_RX" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P1_7"/>
<junction x="99.06" y="93.98"/>
<wire x1="99.06" y1="93.98" x2="95.25" y2="93.98" width="0.1524" layer="91"/>
<label x="95.25" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT2" gate="G$1" pin="3"/>
<junction x="35.56" y="101.6"/>
<wire x1="35.56" y1="101.6" x2="38.1" y2="101.6" width="0.1524" layer="91"/>
<label x="38.1" y="101.6" size="1.27" layer="95" xref="yes"/>
<wire x1="35.56" y1="101.6" x2="22.86" y2="101.6" width="0.1524" layer="91"/>
<wire x1="22.86" y1="101.6" x2="21.59" y2="102.87" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="2"/>
<junction x="17.78" y="102.87"/>
<wire x1="21.59" y1="102.87" x2="17.78" y2="102.87" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BLE_UART_RTS" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P1_5"/>
<junction x="99.06" y="78.74"/>
<wire x1="99.06" y1="78.74" x2="95.25" y2="78.74" width="0.1524" layer="91"/>
<label x="95.25" y="78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT2" gate="G$1" pin="1"/>
<junction x="35.56" y="96.52"/>
<wire x1="35.56" y1="96.52" x2="38.1" y2="96.52" width="0.1524" layer="91"/>
<label x="38.1" y="96.52" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_UART_TX" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P1_6"/>
<junction x="99.06" y="91.44"/>
<wire x1="99.06" y1="91.44" x2="95.25" y2="91.44" width="0.1524" layer="91"/>
<label x="95.25" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT2" gate="G$1" pin="2"/>
<junction x="35.56" y="99.06"/>
<wire x1="35.56" y1="99.06" x2="38.1" y2="99.06" width="0.1524" layer="91"/>
<label x="38.1" y="99.06" size="1.27" layer="95" xref="yes"/>
<wire x1="35.56" y1="99.06" x2="16.51" y2="99.06" width="0.1524" layer="91"/>
<wire x1="16.51" y1="99.06" x2="12.7" y2="102.87" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
<junction x="10.16" y="102.87"/>
<wire x1="12.7" y1="102.87" x2="10.16" y2="102.87" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BLE_UART_CTS" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P1_4"/>
<junction x="111.76" y="60.96"/>
<wire x1="111.76" y1="60.96" x2="111.76" y2="57.15" width="0.1524" layer="91"/>
<label x="111.76" y="57.15" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT1" gate="G$1" pin="8"/>
<junction x="35.56" y="83.82"/>
<wire x1="35.56" y1="83.82" x2="38.1" y2="83.82" width="0.1524" layer="91"/>
<label x="38.1" y="83.82" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_P1_3" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P1_3"/>
<junction x="114.3" y="60.96"/>
<wire x1="114.3" y1="60.96" x2="114.3" y2="57.15" width="0.1524" layer="91"/>
<label x="114.3" y="57.15" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT1" gate="G$1" pin="7"/>
<junction x="35.56" y="81.28"/>
<wire x1="35.56" y1="81.28" x2="38.1" y2="81.28" width="0.1524" layer="91"/>
<label x="38.1" y="81.28" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_P1_2" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P1_2"/>
<junction x="116.84" y="60.96"/>
<wire x1="116.84" y1="60.96" x2="116.84" y2="57.15" width="0.1524" layer="91"/>
<label x="116.84" y="57.15" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT1" gate="G$1" pin="6"/>
<junction x="35.56" y="78.74"/>
<wire x1="35.56" y1="78.74" x2="38.1" y2="78.74" width="0.1524" layer="91"/>
<label x="38.1" y="78.74" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_P1_1" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P1_1"/>
<junction x="119.38" y="60.96"/>
<wire x1="119.38" y1="60.96" x2="119.38" y2="57.15" width="0.1524" layer="91"/>
<label x="119.38" y="57.15" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT1" gate="G$1" pin="5"/>
<junction x="35.56" y="76.2"/>
<wire x1="35.56" y1="76.2" x2="38.1" y2="76.2" width="0.1524" layer="91"/>
<label x="38.1" y="76.2" size="1.27" layer="95" xref="yes"/>
<wire x1="35.56" y1="76.2" x2="25.4" y2="76.2" width="0.1524" layer="91"/>
<wire x1="25.4" y1="76.2" x2="21.59" y2="80.01" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="2"/>
<junction x="19.05" y="80.01"/>
<wire x1="21.59" y1="80.01" x2="19.05" y2="80.01" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BLE_P1_0" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P1_0"/>
<junction x="121.92" y="60.96"/>
<wire x1="121.92" y1="60.96" x2="121.92" y2="57.15" width="0.1524" layer="91"/>
<label x="121.92" y="57.15" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_LED" gate="G$1" pin="C"/>
<junction x="31.75" y="31.75"/>
<wire x1="31.75" y1="31.75" x2="36.83" y2="31.75" width="0.1524" layer="91"/>
<label x="38.1" y="31.75" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT1" gate="G$1" pin="4"/>
<junction x="35.56" y="73.66"/>
<wire x1="35.56" y1="73.66" x2="38.1" y2="73.66" width="0.1524" layer="91"/>
<label x="38.1" y="73.66" size="1.27" layer="95" xref="yes"/>
<wire x1="35.56" y1="73.66" x2="20.32" y2="73.66" width="0.1524" layer="91"/>
<wire x1="20.32" y1="73.66" x2="13.97" y2="80.01" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="2"/>
<junction x="11.43" y="80.01"/>
<wire x1="13.97" y1="80.01" x2="11.43" y2="80.01" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BLE_P0_7" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P0_7"/>
<junction x="124.46" y="60.96"/>
<wire x1="124.46" y1="60.96" x2="124.46" y2="57.15" width="0.1524" layer="91"/>
<label x="124.46" y="57.15" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT1" gate="G$1" pin="3"/>
<junction x="35.56" y="71.12"/>
<wire x1="35.56" y1="71.12" x2="38.1" y2="71.12" width="0.1524" layer="91"/>
<label x="38.1" y="71.12" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_P0_6" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P0_6"/>
<junction x="139.7" y="78.74"/>
<wire x1="139.7" y1="78.74" x2="142.24" y2="78.74" width="0.1524" layer="91"/>
<label x="142.24" y="78.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT1" gate="G$1" pin="2"/>
<junction x="35.56" y="68.58"/>
<wire x1="35.56" y1="68.58" x2="38.1" y2="68.58" width="0.1524" layer="91"/>
<label x="38.1" y="68.58" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P0_5"/>
<junction x="139.7" y="81.28"/>
<wire x1="139.7" y1="81.28" x2="142.24" y2="81.28" width="0.1524" layer="91"/>
<label x="142.24" y="81.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT1" gate="G$1" pin="1"/>
<junction x="35.56" y="66.04"/>
<wire x1="35.56" y1="66.04" x2="38.1" y2="66.04" width="0.1524" layer="91"/>
<label x="38.1" y="66.04" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SS" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P0_4"/>
<junction x="139.7" y="83.82"/>
<wire x1="139.7" y1="83.82" x2="142.24" y2="83.82" width="0.1524" layer="91"/>
<label x="142.24" y="83.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT0" gate="G$1" pin="1"/>
<junction x="72.39" y="44.45"/>
<wire x1="72.39" y1="44.45" x2="74.93" y2="44.45" width="0.1524" layer="91"/>
<label x="74.93" y="44.45" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P0_3"/>
<junction x="139.7" y="86.36"/>
<wire x1="139.7" y1="86.36" x2="142.24" y2="86.36" width="0.1524" layer="91"/>
<label x="142.24" y="86.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT0" gate="G$1" pin="2"/>
<junction x="72.39" y="46.99"/>
<wire x1="72.39" y1="46.99" x2="74.93" y2="46.99" width="0.1524" layer="91"/>
<label x="74.93" y="46.99" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P0_2"/>
<junction x="139.7" y="88.9"/>
<wire x1="139.7" y1="88.9" x2="142.24" y2="88.9" width="0.1524" layer="91"/>
<label x="142.24" y="88.9" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT0" gate="G$1" pin="3"/>
<junction x="72.39" y="49.53"/>
<wire x1="72.39" y1="49.53" x2="74.93" y2="49.53" width="0.1524" layer="91"/>
<label x="74.93" y="49.53" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_P0_1" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P0_1"/>
<junction x="139.7" y="91.44"/>
<wire x1="139.7" y1="91.44" x2="142.24" y2="91.44" width="0.1524" layer="91"/>
<label x="142.24" y="91.44" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT0" gate="G$1" pin="4"/>
<junction x="72.39" y="52.07"/>
<wire x1="72.39" y1="52.07" x2="74.93" y2="52.07" width="0.1524" layer="91"/>
<label x="74.93" y="52.07" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BLE_P0_0" class="0">
<segment>
<pinref part="BLE112" gate="G$1" pin="P0_0"/>
<junction x="139.7" y="93.98"/>
<wire x1="139.7" y1="93.98" x2="142.24" y2="93.98" width="0.1524" layer="91"/>
<label x="142.24" y="93.98" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BLE_PORT0" gate="G$1" pin="5"/>
<junction x="72.39" y="54.61"/>
<wire x1="72.39" y1="54.61" x2="74.93" y2="54.61" width="0.1524" layer="91"/>
<label x="74.93" y="54.61" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<junction x="21.59" y="31.75"/>
<pinref part="BLE_LED" gate="G$1" pin="A"/>
<junction x="24.13" y="31.75"/>
<wire x1="21.59" y1="31.75" x2="24.13" y2="31.75" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U$10" gate="G$1" pin="C2"/>
<junction x="342.9" y="232.41"/>
<wire x1="342.9" y1="232.41" x2="339.09" y2="232.41" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="1"/>
<junction x="339.09" y="224.79"/>
<wire x1="339.09" y1="232.41" x2="339.09" y2="224.79" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U$10" gate="G$1" pin="C3"/>
<junction x="342.9" y="227.33"/>
<wire x1="342.9" y1="227.33" x2="340.36" y2="227.33" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="1"/>
<junction x="340.36" y="220.98"/>
<wire x1="340.36" y1="227.33" x2="340.36" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U$10" gate="G$1" pin="C7"/>
<junction x="342.9" y="224.79"/>
<wire x1="342.9" y1="224.79" x2="342.9" y2="217.17" width="0.1524" layer="91"/>
<wire x1="342.9" y1="217.17" x2="341.63" y2="217.17" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="1"/>
<junction x="341.63" y="217.17"/>
</segment>
</net>
<net name="GSM_ON" class="0">
<segment>
<pinref part="T1" gate="A" pin="G"/>
<wire x1="222.25" y1="198.12" x2="222.25" y2="199.39" width="0.1524" layer="91"/>
<label x="222.25" y="199.39" size="1.778" layer="95" rot="R90" xref="yes"/>
<junction x="222.25" y="198.12"/>
<wire x1="222.25" y1="198.12" x2="220.98" y2="196.85" width="0.1524" layer="91"/>
<wire x1="220.98" y1="196.85" x2="220.98" y2="182.88" width="0.1524" layer="91"/>
<wire x1="220.98" y1="182.88" x2="219.71" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="1"/>
<junction x="219.71" y="180.34"/>
<wire x1="219.71" y1="182.88" x2="219.71" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V_GSM" class="0">
<segment>
<pinref part="C19" gate="G$1" pin="1"/>
<junction x="360.68" y="165.1"/>
<wire x1="360.68" y1="168.91" x2="363.22" y2="168.91" width="0.1524" layer="91"/>
<wire x1="360.68" y1="165.1" x2="360.68" y2="168.91" width="0.1524" layer="91"/>
<label x="363.22" y="168.91" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<junction x="208.28" y="45.72"/>
<wire x1="208.28" y1="45.72" x2="208.28" y2="35.56" width="0.1524" layer="91"/>
<label x="208.28" y="35.56" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="1"/>
<junction x="323.85" y="200.66"/>
<pinref part="C30" gate="G$1" pin="1"/>
<junction x="318.77" y="200.66"/>
<wire x1="323.85" y1="200.66" x2="318.77" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="+"/>
<junction x="313.69" y="200.66"/>
<wire x1="318.77" y1="200.66" x2="313.69" y2="200.66" width="0.1524" layer="91"/>
<pinref part="IC1" gate="VBAT@1" pin="VBAT"/>
<junction x="307.34" y="199.39"/>
<wire x1="313.69" y1="200.66" x2="308.61" y2="200.66" width="0.1524" layer="91"/>
<wire x1="308.61" y1="200.66" x2="307.34" y2="199.39" width="0.1524" layer="91"/>
<pinref part="IC1" gate="VBAT@2" pin="VBAT"/>
<junction x="300.99" y="199.39"/>
<wire x1="307.34" y1="199.39" x2="300.99" y2="199.39" width="0.1524" layer="91"/>
<pinref part="IC1" gate="VBAT@3" pin="VBAT"/>
<junction x="294.64" y="199.39"/>
<wire x1="300.99" y1="199.39" x2="294.64" y2="199.39" width="0.1524" layer="91"/>
<wire x1="294.64" y1="199.39" x2="292.1" y2="199.39" width="0.1524" layer="91"/>
<wire x1="292.1" y1="199.39" x2="290.83" y2="200.66" width="0.1524" layer="91"/>
<label x="290.83" y="200.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="T1" gate="A" pin="D"/>
<junction x="227.33" y="205.74"/>
<pinref part="R29" gate="G$1" pin="1"/>
<junction x="238.76" y="209.55"/>
<wire x1="238.76" y1="209.55" x2="227.33" y2="209.55" width="0.1524" layer="91"/>
<wire x1="227.33" y1="209.55" x2="227.33" y2="205.74" width="0.1524" layer="91"/>
<wire x1="238.76" y1="209.55" x2="243.84" y2="209.55" width="0.1524" layer="91"/>
<label x="243.84" y="209.55" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<junction x="222.25" y="68.58"/>
<wire x1="217.17" y1="68.58" x2="222.25" y2="68.58" width="0.1524" layer="91"/>
<label x="217.17" y="68.58" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SIM_RST" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SIM_RST"/>
<junction x="353.06" y="62.23"/>
<wire x1="353.06" y1="62.23" x2="353.06" y2="59.69" width="0.1524" layer="91"/>
<label x="353.06" y="59.69" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<junction x="328.93" y="224.79"/>
<wire x1="328.93" y1="224.79" x2="325.12" y2="224.79" width="0.1524" layer="91"/>
<label x="325.12" y="224.79" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SIM_PROTECTION_DIODE" gate="G$1" pin="6"/>
<junction x="261.62" y="242.57"/>
<wire x1="261.62" y1="242.57" x2="266.7" y2="242.57" width="0.1524" layer="91"/>
<label x="266.7" y="242.57" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SIM_CLK" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SIM_CLK"/>
<junction x="350.52" y="62.23"/>
<wire x1="350.52" y1="62.23" x2="350.52" y2="59.69" width="0.1524" layer="91"/>
<label x="350.52" y="59.69" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<junction x="330.2" y="220.98"/>
<wire x1="330.2" y1="220.98" x2="328.93" y2="220.98" width="0.1524" layer="91"/>
<label x="328.93" y="220.98" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SIM_PROTECTION_DIODE" gate="G$1" pin="4"/>
<junction x="261.62" y="237.49"/>
<wire x1="261.62" y1="237.49" x2="266.7" y2="237.49" width="0.1524" layer="91"/>
<label x="266.7" y="237.49" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SIM_DATA" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SIM_DATA"/>
<junction x="347.98" y="62.23"/>
<wire x1="347.98" y1="62.23" x2="347.98" y2="59.69" width="0.1524" layer="91"/>
<label x="347.98" y="59.69" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<junction x="331.47" y="217.17"/>
<wire x1="331.47" y1="217.17" x2="328.93" y2="217.17" width="0.1524" layer="91"/>
<label x="328.93" y="217.17" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SIM_PROTECTION_DIODE" gate="G$1" pin="5"/>
<junction x="261.62" y="240.03"/>
<wire x1="261.62" y1="240.03" x2="266.7" y2="240.03" width="0.1524" layer="91"/>
<label x="266.7" y="240.03" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SIM_VDD" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SIM_VDD"/>
<junction x="345.44" y="62.23"/>
<wire x1="345.44" y1="62.23" x2="345.44" y2="59.69" width="0.1524" layer="91"/>
<label x="345.44" y="59.69" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U$10" gate="G$1" pin="C1"/>
<junction x="342.9" y="237.49"/>
<wire x1="342.9" y1="237.49" x2="340.36" y2="237.49" width="0.1524" layer="91"/>
<wire x1="340.36" y1="237.49" x2="335.28" y2="242.57" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<junction x="320.04" y="242.57"/>
<wire x1="335.28" y1="242.57" x2="320.04" y2="242.57" width="0.1524" layer="91"/>
<wire x1="320.04" y1="242.57" x2="304.8" y2="242.57" width="0.1524" layer="91"/>
<label x="304.8" y="242.57" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GSM_RF_ANT" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RF_ANT"/>
<junction x="335.28" y="143.51"/>
<wire x1="335.28" y1="143.51" x2="335.28" y2="146.05" width="0.1524" layer="91"/>
<label x="335.28" y="146.05" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<junction x="234.95" y="119.38"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="234.95" y1="119.38" x2="238.76" y2="119.38" width="0.1524" layer="91"/>
<junction x="238.76" y="119.38"/>
<wire x1="238.76" y1="119.38" x2="238.76" y2="123.19" width="0.1524" layer="91"/>
<wire x1="238.76" y1="123.19" x2="241.3" y2="123.19" width="0.1524" layer="91"/>
<label x="241.3" y="123.19" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GSM_NETLIGHT" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="NETLIGHT\"/>
<junction x="355.6" y="143.51"/>
<wire x1="355.6" y1="143.51" x2="358.14" y2="143.51" width="0.1524" layer="91"/>
<label x="358.14" y="143.51" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<junction x="251.46" y="44.45"/>
<wire x1="251.46" y1="44.45" x2="254" y2="44.45" width="0.1524" layer="91"/>
<label x="254" y="44.45" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="RF-SMA" gate="G$1" pin="1"/>
<junction x="226.06" y="88.9"/>
<wire x1="238.76" y1="88.9" x2="226.06" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="1"/>
<junction x="238.76" y="96.52"/>
<wire x1="238.76" y1="88.9" x2="238.76" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GSM_TX" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="TXD"/>
<junction x="294.64" y="102.87"/>
<wire x1="294.64" y1="102.87" x2="292.1" y2="102.87" width="0.1524" layer="91"/>
<label x="292.1" y="102.87" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="GSM_UART" gate="G$1" pin="2"/>
<junction x="227.33" y="149.86"/>
<wire x1="227.33" y1="149.86" x2="231.14" y2="149.86" width="0.1524" layer="91"/>
<label x="231.14" y="149.86" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GSM_RX" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RXD"/>
<junction x="294.64" y="100.33"/>
<wire x1="294.64" y1="100.33" x2="292.1" y2="100.33" width="0.1524" layer="91"/>
<label x="292.1" y="100.33" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="GSM_UART" gate="G$1" pin="3"/>
<junction x="227.33" y="152.4"/>
<wire x1="227.33" y1="152.4" x2="231.14" y2="152.4" width="0.1524" layer="91"/>
<label x="231.14" y="152.4" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<junction x="218.44" y="45.72"/>
<pinref part="GSM_NET" gate="G$1" pin="A"/>
<junction x="220.98" y="45.72"/>
<wire x1="218.44" y1="45.72" x2="220.98" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="GSM_NET" gate="G$1" pin="C"/>
<junction x="228.6" y="45.72"/>
<wire x1="228.6" y1="45.72" x2="228.6" y2="49.53" width="0.1524" layer="91"/>
<pinref part="NET_T" gate="G$1" pin="C"/>
<junction x="232.41" y="49.53"/>
<wire x1="228.6" y1="49.53" x2="232.41" y2="49.53" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="NET_T" gate="G$1" pin="B"/>
<junction x="237.49" y="44.45"/>
<pinref part="R32" gate="G$1" pin="1"/>
<junction x="241.3" y="44.45"/>
<wire x1="237.49" y1="44.45" x2="241.3" y2="44.45" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="2"/>
<junction x="241.3" y="39.37"/>
<wire x1="241.3" y1="44.45" x2="241.3" y2="39.37" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GSM_PWR" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PWRKEY"/>
<junction x="294.64" y="123.19"/>
<wire x1="294.64" y1="123.19" x2="292.1" y2="123.19" width="0.1524" layer="91"/>
<label x="292.1" y="123.19" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GSM_RESET" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="NRESET"/>
<junction x="294.64" y="85.09"/>
<wire x1="294.64" y1="85.09" x2="292.1" y2="85.09" width="0.1524" layer="91"/>
<label x="292.1" y="85.09" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BAT" class="0">
<segment>
<pinref part="R35" gate="G$1" pin="2"/>
<junction x="229.87" y="180.34"/>
<pinref part="T1" gate="A" pin="S"/>
<junction x="227.33" y="195.58"/>
<wire x1="227.33" y1="195.58" x2="231.14" y2="195.58" width="0.1524" layer="91"/>
<wire x1="231.14" y1="195.58" x2="231.14" y2="180.34" width="0.1524" layer="91"/>
<wire x1="231.14" y1="180.34" x2="231.14" y2="176.53" width="0.1524" layer="91"/>
<junction x="231.14" y="180.34"/>
<wire x1="229.87" y1="180.34" x2="231.14" y2="180.34" width="0.1524" layer="91"/>
<label x="231.14" y="176.53" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<junction x="232.41" y="68.58"/>
<pinref part="GSMPWR" gate="G$1" pin="A"/>
<junction x="236.22" y="68.58"/>
<wire x1="232.41" y1="68.58" x2="236.22" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<junction x="238.76" y="106.68"/>
<pinref part="R33" gate="G$1" pin="1"/>
<pinref part="C25" gate="G$1" pin="2"/>
<junction x="234.95" y="109.22"/>
<wire x1="234.95" y1="109.22" x2="238.76" y2="109.22" width="0.1524" layer="91"/>
<junction x="238.76" y="109.22"/>
<wire x1="238.76" y1="106.68" x2="238.76" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Power Section</description>
<plain>
<wire x1="5.08" y1="209.55" x2="118.11" y2="209.55" width="1.016" layer="94" style="longdash"/>
<wire x1="118.11" y1="209.55" x2="274.32" y2="209.55" width="1.016" layer="94" style="longdash"/>
<wire x1="274.32" y1="209.55" x2="274.32" y2="153.67" width="1.016" layer="94" style="longdash"/>
<wire x1="274.32" y1="153.67" x2="172.72" y2="153.67" width="1.016" layer="94" style="longdash"/>
<wire x1="172.72" y1="153.67" x2="172.72" y2="116.84" width="1.016" layer="94" style="longdash"/>
<wire x1="172.72" y1="116.84" x2="118.11" y2="116.84" width="1.016" layer="94" style="longdash"/>
<wire x1="118.11" y1="116.84" x2="118.11" y2="143.51" width="1.016" layer="94" style="longdash"/>
<wire x1="118.11" y1="143.51" x2="118.11" y2="209.55" width="1.016" layer="94" style="longdash"/>
<wire x1="118.11" y1="143.51" x2="5.08" y2="143.51" width="1.016" layer="94" style="longdash"/>
<wire x1="5.08" y1="143.51" x2="5.08" y2="208.28" width="1.016" layer="94" style="longdash"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="69.85" width="1.016" layer="94" style="longdash"/>
<wire x1="5.08" y1="69.85" x2="5.08" y2="143.51" width="1.016" layer="94" style="longdash"/>
<wire x1="5.08" y1="1.27" x2="118.11" y2="1.27" width="1.016" layer="94" style="longdash"/>
<wire x1="118.11" y1="1.27" x2="118.11" y2="69.85" width="1.016" layer="94" style="longdash"/>
<wire x1="118.11" y1="69.85" x2="118.11" y2="116.84" width="1.016" layer="94" style="longdash"/>
<wire x1="118.11" y1="1.27" x2="170.18" y2="1.27" width="1.016" layer="94" style="longdash"/>
<wire x1="170.18" y1="1.27" x2="170.18" y2="36.83" width="1.016" layer="94" style="longdash"/>
<wire x1="170.18" y1="36.83" x2="274.32" y2="36.83" width="1.016" layer="94" style="longdash"/>
<wire x1="274.32" y1="36.83" x2="274.32" y2="153.67" width="1.016" layer="94" style="longdash"/>
<text x="200.66" y="156.21" size="5.08" layer="94">Solar charge controller</text>
<text x="57.15" y="33.02" size="5.08" layer="94" rot="R180">+3v3  regulator</text>
<text x="63.5" y="204.47" size="2.54" layer="94">Input ORing, Solar, DC and USB</text>
<wire x1="5.08" y1="69.85" x2="118.11" y2="69.85" width="1.016" layer="94" style="shortdash"/>
</plain>
<instances>
<instance part="FRAME5" gate="G$1" x="0" y="0"/>
<instance part="FRAME5" gate="G$2" x="172.72" y="0"/>
<instance part="SOLAR" gate="G$1" x="24.13" y="199.39"/>
<instance part="ADAPTER" gate="G$1" x="24.13" y="186.69"/>
<instance part="USB" gate="G$1" x="26.67" y="171.45" rot="R180"/>
<instance part="BQ24210" gate="A" x="147.32" y="193.04"/>
<instance part="C1" gate="G$1" x="135.89" y="177.8" rot="R180"/>
<instance part="RISET" gate="G$1" x="140.97" y="201.93" rot="R180"/>
<instance part="RT1" gate="G$1" x="144.78" y="147.32" rot="R270"/>
<instance part="R_TEMP" gate="G$1" x="144.78" y="134.62" rot="R270"/>
<instance part="C2" gate="G$1" x="138.43" y="134.62" rot="R180"/>
<instance part="R_CHG" gate="G$1" x="184.15" y="170.18" rot="R180"/>
<instance part="R_PG" gate="G$1" x="184.15" y="161.29" rot="R180"/>
<instance part="LED_CHG" gate="G$1" x="171.45" y="170.18" rot="R270"/>
<instance part="LED_PG" gate="G$1" x="171.45" y="161.29" rot="R270"/>
<instance part="C3" gate="G$1" x="252.73" y="181.61" rot="R180"/>
<instance part="R1" gate="G$1" x="90.17" y="161.29" rot="R90"/>
<instance part="XC6221A332MR-G" gate="G$1" x="64.77" y="15.24" rot="R270"/>
<instance part="C4" gate="G$1" x="30.48" y="19.05" rot="R180"/>
<instance part="C5" gate="G$1" x="82.55" y="16.51" rot="R180"/>
<instance part="C6" gate="G$1" x="21.59" y="19.05" rot="R180"/>
<instance part="C7" gate="G$1" x="95.25" y="16.51" rot="R180"/>
<instance part="LI_BAT_INPUT" gate="G$1" x="20.32" y="60.96" rot="R270"/>
<instance part="I2C_JP" gate="G$1" x="45.72" y="58.42" rot="R270"/>
<instance part="SDM03MT40" gate="G$1" x="71.12" y="184.15"/>
<instance part="SUPPLY1" gate="G$1" x="12.7" y="203.2"/>
<instance part="SUPPLY2" gate="G$1" x="12.7" y="191.77"/>
<instance part="SUPPLY3" gate="+5V" x="29.21" y="153.67" rot="R180"/>
<instance part="GND1" gate="1" x="46.99" y="149.86"/>
<instance part="+3V1" gate="G$1" x="95.25" y="30.48"/>
<instance part="R36" gate="G$1" x="34.29" y="111.76" rot="MR90"/>
<instance part="R37" gate="G$1" x="34.29" y="97.79" rot="MR90"/>
<instance part="R38" gate="G$1" x="48.26" y="111.76" rot="MR90"/>
<instance part="R39" gate="G$1" x="48.26" y="97.79" rot="MR90"/>
</instances>
<busses>
</busses>
<nets>
<net name="CP_DM" class="0">
<segment>
<pinref part="USB" gate="G$1" pin="D-"/>
<junction x="29.21" y="163.83"/>
<wire x1="29.21" y1="163.83" x2="35.56" y2="163.83" width="0.1524" layer="91"/>
<label x="35.56" y="163.83" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="CP_DP" class="0">
<segment>
<pinref part="USB" gate="G$1" pin="D+"/>
<junction x="29.21" y="166.37"/>
<wire x1="29.21" y1="166.37" x2="35.56" y2="166.37" width="0.1524" layer="91"/>
<label x="35.56" y="166.37" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="90.17" y1="156.21" x2="90.17" y2="151.13" width="0.1524" layer="91"/>
<wire x1="90.17" y1="151.13" x2="91.44" y2="149.86" width="0.1524" layer="91"/>
<wire x1="91.44" y1="149.86" x2="93.98" y2="149.86" width="0.1524" layer="91"/>
<label x="93.98" y="149.86" size="1.778" layer="95" xref="yes"/>
<pinref part="R1" gate="G$1" pin="1"/>
<junction x="90.17" y="156.21"/>
</segment>
<segment>
<pinref part="SOLAR" gate="G$1" pin="GNDBREAK"/>
<junction x="26.67" y="199.39"/>
<pinref part="SOLAR" gate="G$1" pin="GND"/>
<junction x="26.67" y="196.85"/>
<wire x1="26.67" y1="199.39" x2="26.67" y2="196.85" width="0.1524" layer="91"/>
<wire x1="26.67" y1="196.85" x2="31.75" y2="196.85" width="0.1524" layer="91"/>
<wire x1="31.75" y1="196.85" x2="33.02" y2="195.58" width="0.1524" layer="91"/>
<label x="33.02" y="195.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="ADAPTER" gate="G$1" pin="GNDBREAK"/>
<junction x="26.67" y="186.69"/>
<pinref part="ADAPTER" gate="G$1" pin="GND"/>
<junction x="26.67" y="184.15"/>
<wire x1="26.67" y1="186.69" x2="26.67" y2="184.15" width="0.1524" layer="91"/>
<wire x1="26.67" y1="184.15" x2="33.02" y2="184.15" width="0.1524" layer="91"/>
<wire x1="33.02" y1="184.15" x2="34.29" y2="185.42" width="0.1524" layer="91"/>
<wire x1="34.29" y1="185.42" x2="35.56" y2="185.42" width="0.1524" layer="91"/>
<label x="35.56" y="185.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="USB" gate="G$1" pin="GND"/>
<junction x="29.21" y="171.45"/>
<wire x1="29.21" y1="171.45" x2="34.29" y2="171.45" width="0.1524" layer="91"/>
<label x="34.29" y="171.45" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="USB" gate="G$1" pin="MTN3"/>
<junction x="24.13" y="176.53"/>
<pinref part="USB" gate="G$1" pin="MTN4"/>
<junction x="21.59" y="176.53"/>
<wire x1="24.13" y1="176.53" x2="21.59" y2="176.53" width="0.1524" layer="91"/>
<wire x1="21.59" y1="176.53" x2="19.05" y2="176.53" width="0.1524" layer="91"/>
<wire x1="19.05" y1="176.53" x2="17.78" y2="177.8" width="0.1524" layer="91"/>
<wire x1="17.78" y1="177.8" x2="16.51" y2="177.8" width="0.1524" layer="91"/>
<label x="16.51" y="177.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="USB" gate="G$1" pin="MTN1"/>
<junction x="24.13" y="156.21"/>
<pinref part="USB" gate="G$1" pin="MTN2"/>
<junction x="21.59" y="156.21"/>
<wire x1="24.13" y1="156.21" x2="21.59" y2="156.21" width="0.1524" layer="91"/>
<wire x1="21.59" y1="156.21" x2="19.05" y2="156.21" width="0.1524" layer="91"/>
<wire x1="19.05" y1="156.21" x2="17.78" y2="154.94" width="0.1524" layer="91"/>
<wire x1="17.78" y1="154.94" x2="16.51" y2="154.94" width="0.1524" layer="91"/>
<label x="16.51" y="154.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<junction x="135.89" y="172.72"/>
<wire x1="135.89" y1="172.72" x2="135.89" y2="166.37" width="0.1524" layer="91"/>
<label x="135.89" y="166.37" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BQ24210" gate="A" pin="VSS"/>
<junction x="147.32" y="187.96"/>
<wire x1="147.32" y1="187.96" x2="146.05" y2="187.96" width="0.1524" layer="91"/>
<label x="146.05" y="187.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RISET" gate="G$1" pin="1"/>
<junction x="146.05" y="201.93"/>
<wire x1="146.05" y1="201.93" x2="149.86" y2="201.93" width="0.1524" layer="91"/>
<wire x1="149.86" y1="201.93" x2="151.13" y2="200.66" width="0.1524" layer="91"/>
<wire x1="151.13" y1="200.66" x2="153.67" y2="200.66" width="0.1524" layer="91"/>
<label x="153.67" y="200.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="BQ24210" gate="A" pin="EPAD"/>
<junction x="208.28" y="193.04"/>
<wire x1="208.28" y1="193.04" x2="209.55" y2="193.04" width="0.1524" layer="91"/>
<wire x1="209.55" y1="193.04" x2="210.82" y2="194.31" width="0.1524" layer="91"/>
<wire x1="210.82" y1="194.31" x2="212.09" y2="194.31" width="0.1524" layer="91"/>
<label x="212.09" y="194.31" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R_TEMP" gate="G$1" pin="2"/>
<junction x="144.78" y="129.54"/>
<wire x1="144.78" y1="129.54" x2="144.78" y2="125.73" width="0.1524" layer="91"/>
<wire x1="144.78" y1="125.73" x2="146.05" y2="124.46" width="0.1524" layer="91"/>
<wire x1="146.05" y1="124.46" x2="147.32" y2="124.46" width="0.1524" layer="91"/>
<label x="148.59" y="124.46" size="1.778" layer="95" xref="yes"/>
<pinref part="C2" gate="G$1" pin="1"/>
<junction x="138.43" y="129.54"/>
<wire x1="138.43" y1="129.54" x2="144.78" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<junction x="252.73" y="176.53"/>
<wire x1="252.73" y1="176.53" x2="256.54" y2="176.53" width="0.1524" layer="91"/>
<label x="256.54" y="176.53" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="XC6221A332MR-G" gate="G$1" pin="VSS"/>
<junction x="54.61" y="15.24"/>
<wire x1="54.61" y1="15.24" x2="31.75" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<junction x="30.48" y="13.97"/>
<wire x1="31.75" y1="15.24" x2="30.48" y2="13.97" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<junction x="21.59" y="13.97"/>
<wire x1="30.48" y1="13.97" x2="21.59" y2="13.97" width="0.1524" layer="91"/>
<label x="39.37" y="16.51" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<junction x="82.55" y="11.43"/>
<pinref part="C7" gate="G$1" pin="1"/>
<junction x="95.25" y="11.43"/>
<wire x1="82.55" y1="11.43" x2="95.25" y2="11.43" width="0.1524" layer="91"/>
<wire x1="95.25" y1="11.43" x2="97.79" y2="11.43" width="0.1524" layer="91"/>
<label x="97.79" y="11.43" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="I2C_JP" gate="G$1" pin="1"/>
<junction x="43.18" y="53.34"/>
<wire x1="43.18" y1="53.34" x2="43.18" y2="46.99" width="0.1524" layer="91"/>
<label x="43.18" y="46.99" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="LI_BAT_INPUT" gate="G$1" pin="1"/>
<junction x="20.32" y="53.34"/>
<wire x1="20.32" y1="53.34" x2="20.32" y2="46.99" width="0.1524" layer="91"/>
<label x="20.32" y="46.99" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<junction x="46.99" y="152.4"/>
<wire x1="46.99" y1="152.4" x2="54.61" y2="152.4" width="0.1524" layer="91"/>
<label x="54.61" y="152.4" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R37" gate="G$1" pin="1"/>
<junction x="34.29" y="92.71"/>
<pinref part="R39" gate="G$1" pin="1"/>
<junction x="48.26" y="92.71"/>
<wire x1="34.29" y1="92.71" x2="48.26" y2="92.71" width="0.1524" layer="91"/>
<wire x1="48.26" y1="92.71" x2="48.26" y2="85.09" width="0.1524" layer="91"/>
<label x="48.26" y="85.09" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ISET" class="0">
<segment>
<pinref part="BQ24210" gate="A" pin="ISET"/>
<junction x="147.32" y="190.5"/>
<wire x1="147.32" y1="190.5" x2="146.05" y2="190.5" width="0.1524" layer="91"/>
<label x="146.05" y="190.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RISET" gate="G$1" pin="2"/>
<junction x="135.89" y="201.93"/>
<wire x1="135.89" y1="201.93" x2="132.08" y2="201.93" width="0.1524" layer="91"/>
<label x="132.08" y="201.93" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VTSB" class="0">
<segment>
<pinref part="BQ24210" gate="A" pin="VTSB"/>
<junction x="147.32" y="185.42"/>
<wire x1="147.32" y1="185.42" x2="146.05" y2="185.42" width="0.1524" layer="91"/>
<label x="146.05" y="185.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="135.89" y1="160.02" x2="144.78" y2="160.02" width="0.1524" layer="91"/>
<junction x="144.78" y="160.02"/>
<pinref part="RT1" gate="G$1" pin="1"/>
<junction x="144.78" y="152.4"/>
<wire x1="144.78" y1="160.02" x2="144.78" y2="152.4" width="0.1524" layer="91"/>
<label x="135.89" y="160.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TS" class="0">
<segment>
<pinref part="BQ24210" gate="A" pin="TS"/>
<junction x="147.32" y="182.88"/>
<wire x1="147.32" y1="182.88" x2="146.05" y2="182.88" width="0.1524" layer="91"/>
<label x="146.05" y="182.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RT1" gate="G$1" pin="2"/>
<junction x="144.78" y="142.24"/>
<pinref part="R_TEMP" gate="G$1" pin="1"/>
<junction x="144.78" y="139.7"/>
<wire x1="144.78" y1="142.24" x2="144.78" y2="139.7" width="0.1524" layer="91"/>
<wire x1="144.78" y1="142.24" x2="138.43" y2="142.24" width="0.1524" layer="91"/>
<label x="135.89" y="142.24" size="1.778" layer="95" rot="R180" xref="yes"/>
<junction x="138.43" y="142.24"/>
<wire x1="138.43" y1="142.24" x2="135.89" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<junction x="138.43" y="137.16"/>
<wire x1="138.43" y1="137.16" x2="138.43" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EN" class="0">
<segment>
<pinref part="BQ24210" gate="A" pin="*EN"/>
<wire x1="208.28" y1="182.88" x2="214.63" y2="182.88" width="0.1524" layer="91"/>
<wire x1="214.63" y1="182.88" x2="215.9" y2="181.61" width="0.1524" layer="91"/>
<wire x1="215.9" y1="181.61" x2="217.17" y2="181.61" width="0.1524" layer="91"/>
<label x="217.17" y="181.61" size="1.778" layer="95" xref="yes"/>
<junction x="208.28" y="182.88"/>
</segment>
</net>
<net name="CHG" class="0">
<segment>
<pinref part="BQ24210" gate="A" pin="*CHG"/>
<junction x="208.28" y="185.42"/>
<wire x1="208.28" y1="185.42" x2="214.63" y2="185.42" width="0.1524" layer="91"/>
<wire x1="214.63" y1="185.42" x2="215.9" y2="186.69" width="0.1524" layer="91"/>
<wire x1="215.9" y1="186.69" x2="217.17" y2="186.69" width="0.1524" layer="91"/>
<label x="217.17" y="186.69" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LED_CHG" gate="G$1" pin="C"/>
<junction x="166.37" y="170.18"/>
<wire x1="166.37" y1="170.18" x2="161.29" y2="170.18" width="0.1524" layer="91"/>
<label x="161.29" y="170.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BAT" class="0">
<segment>
<pinref part="BQ24210" gate="A" pin="BAT"/>
<junction x="208.28" y="190.5"/>
<wire x1="208.28" y1="190.5" x2="228.6" y2="190.5" width="0.1524" layer="91"/>
<label x="228.6" y="190.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R_CHG" gate="G$1" pin="1"/>
<junction x="189.23" y="170.18"/>
<wire x1="189.23" y1="170.18" x2="194.31" y2="170.18" width="0.1524" layer="91"/>
<label x="194.31" y="170.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R_PG" gate="G$1" pin="1"/>
<junction x="189.23" y="161.29"/>
<wire x1="189.23" y1="161.29" x2="189.23" y2="163.83" width="0.1524" layer="91"/>
<label x="189.23" y="163.83" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<junction x="252.73" y="184.15"/>
<wire x1="252.73" y1="184.15" x2="256.54" y2="184.15" width="0.1524" layer="91"/>
<label x="256.54" y="184.15" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<junction x="21.59" y="21.59"/>
<pinref part="C4" gate="G$1" pin="2"/>
<junction x="30.48" y="21.59"/>
<wire x1="21.59" y1="21.59" x2="30.48" y2="21.59" width="0.1524" layer="91"/>
<pinref part="XC6221A332MR-G" gate="G$1" pin="VIN"/>
<junction x="54.61" y="20.32"/>
<wire x1="54.61" y1="20.32" x2="40.64" y2="20.32" width="0.1524" layer="91"/>
<wire x1="30.48" y1="21.59" x2="39.37" y2="21.59" width="0.1524" layer="91"/>
<wire x1="39.37" y1="21.59" x2="40.64" y2="20.32" width="0.1524" layer="91"/>
<wire x1="21.59" y1="21.59" x2="13.97" y2="21.59" width="0.1524" layer="91"/>
<wire x1="13.97" y1="21.59" x2="12.7" y2="20.32" width="0.1524" layer="91"/>
<wire x1="12.7" y1="20.32" x2="12.7" y2="12.7" width="0.1524" layer="91"/>
<wire x1="12.7" y1="12.7" x2="15.24" y2="10.16" width="0.1524" layer="91"/>
<pinref part="XC6221A332MR-G" gate="G$1" pin="CE"/>
<junction x="54.61" y="10.16"/>
<wire x1="15.24" y1="10.16" x2="54.61" y2="10.16" width="0.1524" layer="91"/>
<label x="40.64" y="22.86" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LI_BAT_INPUT" gate="G$1" pin="2"/>
<junction x="22.86" y="53.34"/>
<wire x1="22.86" y1="53.34" x2="22.86" y2="46.99" width="0.1524" layer="91"/>
<label x="22.86" y="46.99" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R_CHG" gate="G$1" pin="2"/>
<junction x="179.07" y="170.18"/>
<pinref part="LED_CHG" gate="G$1" pin="A"/>
<junction x="173.99" y="170.18"/>
<wire x1="179.07" y1="170.18" x2="173.99" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R_PG" gate="G$1" pin="2"/>
<junction x="179.07" y="161.29"/>
<pinref part="LED_PG" gate="G$1" pin="A"/>
<junction x="173.99" y="161.29"/>
<wire x1="179.07" y1="161.29" x2="173.99" y2="161.29" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V_PWR" class="0">
<segment>
<pinref part="BQ24210" gate="A" pin="VBUS"/>
<junction x="147.32" y="193.04"/>
<wire x1="147.32" y1="193.04" x2="135.89" y2="193.04" width="0.1524" layer="91"/>
<junction x="135.89" y="193.04"/>
<wire x1="135.89" y1="193.04" x2="132.08" y2="193.04" width="0.1524" layer="91"/>
<label x="132.08" y="193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="C1" gate="G$1" pin="2"/>
<junction x="135.89" y="180.34"/>
<wire x1="135.89" y1="180.34" x2="135.89" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="85.09" y1="193.04" x2="85.09" y2="186.69" width="0.1524" layer="91"/>
<wire x1="85.09" y1="186.69" x2="85.09" y2="177.8" width="0.1524" layer="91"/>
<wire x1="85.09" y1="177.8" x2="88.9" y2="177.8" width="0.1524" layer="91"/>
<wire x1="88.9" y1="177.8" x2="90.17" y2="176.53" width="0.1524" layer="91"/>
<wire x1="90.17" y1="176.53" x2="90.17" y2="166.37" width="0.1524" layer="91"/>
<wire x1="85.09" y1="193.04" x2="93.98" y2="193.04" width="0.1524" layer="91"/>
<label x="93.98" y="193.04" size="1.778" layer="95" xref="yes"/>
<pinref part="R1" gate="G$1" pin="2"/>
<junction x="90.17" y="166.37"/>
<junction x="85.09" y="186.69"/>
<pinref part="SDM03MT40" gate="G$1" pin="4"/>
<junction x="81.28" y="181.61"/>
<pinref part="SDM03MT40" gate="G$1" pin="5"/>
<junction x="81.28" y="184.15"/>
<wire x1="81.28" y1="181.61" x2="81.28" y2="184.15" width="0.1524" layer="91"/>
<pinref part="SDM03MT40" gate="G$1" pin="6"/>
<junction x="81.28" y="186.69"/>
<wire x1="81.28" y1="184.15" x2="81.28" y2="186.69" width="0.1524" layer="91"/>
<wire x1="81.28" y1="186.69" x2="85.09" y2="186.69" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="I2C_JP" gate="G$1" pin="4"/>
<junction x="50.8" y="53.34"/>
<wire x1="50.8" y1="53.34" x2="50.8" y2="46.99" width="0.1524" layer="91"/>
<label x="50.8" y="46.99" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<junction x="95.25" y="27.94"/>
<pinref part="C7" gate="G$1" pin="2"/>
<junction x="95.25" y="19.05"/>
<pinref part="C5" gate="G$1" pin="2"/>
<junction x="82.55" y="19.05"/>
<pinref part="XC6221A332MR-G" gate="G$1" pin="VOUT"/>
<junction x="74.93" y="20.32"/>
<wire x1="74.93" y1="20.32" x2="82.55" y2="20.32" width="0.1524" layer="91"/>
<junction x="82.55" y="20.32"/>
<wire x1="82.55" y1="19.05" x2="82.55" y2="20.32" width="0.1524" layer="91"/>
<wire x1="82.55" y1="20.32" x2="95.25" y2="20.32" width="0.1524" layer="91"/>
<junction x="95.25" y="20.32"/>
<wire x1="95.25" y1="20.32" x2="97.79" y2="20.32" width="0.1524" layer="91"/>
<wire x1="95.25" y1="19.05" x2="95.25" y2="20.32" width="0.1524" layer="91"/>
<label x="97.79" y="20.32" size="1.27" layer="95" xref="yes"/>
<wire x1="95.25" y1="20.32" x2="95.25" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="I2C_JP" gate="G$1" pin="2"/>
<junction x="45.72" y="53.34"/>
<wire x1="45.72" y1="53.34" x2="45.72" y2="46.99" width="0.1524" layer="91"/>
<label x="45.72" y="46.99" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="I2C_JP" gate="G$1" pin="3"/>
<junction x="48.26" y="53.34"/>
<wire x1="48.26" y1="53.34" x2="48.26" y2="46.99" width="0.1524" layer="91"/>
<label x="48.26" y="46.99" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="+7V" class="0">
<segment>
<pinref part="SUPPLY1" gate="G$1" pin="+7V"/>
<junction x="12.7" y="200.66"/>
<wire x1="12.7" y1="200.66" x2="15.24" y2="200.66" width="0.1524" layer="91"/>
<wire x1="15.24" y1="200.66" x2="15.24" y2="205.74" width="0.1524" layer="91"/>
<pinref part="SOLAR" gate="G$1" pin="PWR"/>
<junction x="26.67" y="201.93"/>
<wire x1="26.67" y1="201.93" x2="50.8" y2="201.93" width="0.1524" layer="91"/>
<wire x1="50.8" y1="201.93" x2="50.8" y2="193.04" width="0.1524" layer="91"/>
<wire x1="50.8" y1="193.04" x2="60.96" y2="193.04" width="0.1524" layer="91"/>
<label x="50.8" y="201.93" size="1.778" layer="95" xref="yes"/>
<pinref part="SDM03MT40" gate="G$1" pin="1"/>
<junction x="60.96" y="186.69"/>
<wire x1="60.96" y1="193.04" x2="60.96" y2="186.69" width="0.1524" layer="91"/>
<wire x1="15.24" y1="205.74" x2="26.67" y2="205.74" width="0.1524" layer="91"/>
<wire x1="26.67" y1="205.74" x2="26.67" y2="201.93" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R38" gate="G$1" pin="2"/>
<junction x="48.26" y="116.84"/>
<wire x1="48.26" y1="116.84" x2="48.26" y2="123.19" width="0.1524" layer="91"/>
<wire x1="48.26" y1="123.19" x2="50.8" y2="125.73" width="0.1524" layer="91"/>
<label x="50.8" y="125.73" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="+9V" class="0">
<segment>
<pinref part="SUPPLY2" gate="G$1" pin="+9V"/>
<junction x="12.7" y="189.23"/>
<wire x1="12.7" y1="189.23" x2="15.24" y2="189.23" width="0.1524" layer="91"/>
<wire x1="15.24" y1="189.23" x2="15.24" y2="191.77" width="0.1524" layer="91"/>
<pinref part="ADAPTER" gate="G$1" pin="PWR"/>
<junction x="26.67" y="189.23"/>
<wire x1="26.67" y1="189.23" x2="45.72" y2="189.23" width="0.1524" layer="91"/>
<wire x1="45.72" y1="189.23" x2="46.99" y2="187.96" width="0.1524" layer="91"/>
<wire x1="46.99" y1="187.96" x2="57.15" y2="187.96" width="0.1524" layer="91"/>
<label x="45.72" y="190.5" size="1.778" layer="95" xref="yes"/>
<pinref part="SDM03MT40" gate="G$1" pin="2"/>
<junction x="60.96" y="184.15"/>
<wire x1="60.96" y1="184.15" x2="57.15" y2="184.15" width="0.1524" layer="91"/>
<wire x1="57.15" y1="184.15" x2="57.15" y2="187.96" width="0.1524" layer="91"/>
<wire x1="15.24" y1="191.77" x2="26.67" y2="191.77" width="0.1524" layer="91"/>
<wire x1="26.67" y1="191.77" x2="26.67" y2="189.23" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<junction x="34.29" y="116.84"/>
<wire x1="34.29" y1="116.84" x2="34.29" y2="123.19" width="0.1524" layer="91"/>
<wire x1="34.29" y1="123.19" x2="31.75" y2="125.73" width="0.1524" layer="91"/>
<label x="31.75" y="125.73" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="SUPPLY3" gate="+5V" pin="+5V"/>
<junction x="29.21" y="156.21"/>
<pinref part="USB" gate="G$1" pin="VBUS"/>
<junction x="29.21" y="161.29"/>
<wire x1="29.21" y1="161.29" x2="46.99" y2="161.29" width="0.1524" layer="91"/>
<wire x1="46.99" y1="161.29" x2="48.26" y2="162.56" width="0.1524" layer="91"/>
<wire x1="48.26" y1="162.56" x2="48.26" y2="175.26" width="0.1524" layer="91"/>
<wire x1="48.26" y1="175.26" x2="50.8" y2="177.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="177.8" x2="57.15" y2="177.8" width="0.1524" layer="91"/>
<label x="46.99" y="161.29" size="1.778" layer="95" xref="yes"/>
<pinref part="SDM03MT40" gate="G$1" pin="3"/>
<junction x="60.96" y="181.61"/>
<wire x1="60.96" y1="181.61" x2="57.15" y2="181.61" width="0.1524" layer="91"/>
<wire x1="57.15" y1="181.61" x2="57.15" y2="177.8" width="0.1524" layer="91"/>
<wire x1="29.21" y1="156.21" x2="29.21" y2="161.29" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PG" class="0">
<segment>
<pinref part="BQ24210" gate="A" pin="*PG"/>
<junction x="208.28" y="180.34"/>
<wire x1="208.28" y1="180.34" x2="210.82" y2="180.34" width="0.1524" layer="91"/>
<wire x1="210.82" y1="180.34" x2="213.36" y2="177.8" width="0.1524" layer="91"/>
<label x="218.44" y="177.8" size="1.778" layer="95" xref="yes"/>
<wire x1="213.36" y1="177.8" x2="218.44" y2="177.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED_PG" gate="G$1" pin="C"/>
<junction x="166.37" y="161.29"/>
<wire x1="166.37" y1="161.29" x2="161.29" y2="161.29" width="0.1524" layer="91"/>
<label x="161.29" y="161.29" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DC_PWR_LEVEL" class="0">
<segment>
<pinref part="R37" gate="G$1" pin="2"/>
<junction x="34.29" y="102.87"/>
<pinref part="R36" gate="G$1" pin="1"/>
<junction x="34.29" y="106.68"/>
<wire x1="34.29" y1="102.87" x2="34.29" y2="106.68" width="0.1524" layer="91"/>
<wire x1="34.29" y1="102.87" x2="30.48" y2="102.87" width="0.1524" layer="91"/>
<label x="30.48" y="102.87" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SOLAR_LEVEL" class="0">
<segment>
<pinref part="R39" gate="G$1" pin="2"/>
<junction x="48.26" y="102.87"/>
<pinref part="R38" gate="G$1" pin="1"/>
<junction x="48.26" y="106.68"/>
<wire x1="48.26" y1="102.87" x2="48.26" y2="106.68" width="0.1524" layer="91"/>
<wire x1="48.26" y1="102.87" x2="55.88" y2="102.87" width="0.1524" layer="91"/>
<label x="55.88" y="102.87" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="102,1,332.74,83.82,VSS1,GND,,,,"/>
<approved hash="102,1,332.74,86.36,VDD,+3V3,,,,"/>
<approved hash="102,1,332.74,81.28,VSS2,GND,,,,"/>
<approved hash="104,5,26.67,201.93,CN1,PWR,+7V,,,"/>
<approved hash="104,5,26.67,199.39,CN1,GNDBREAK,GND,,,"/>
<approved hash="104,5,26.67,189.23,CN2,PWR,+9V,,,"/>
<approved hash="104,5,26.67,186.69,CN2,GNDBREAK,GND,,,"/>
<approved hash="104,5,147.32,187.96,U1,VSS,GND,,,"/>
<approved hash="202,5,208.28,187.96,U1,VDPM,,,,"/>
<approved hash="104,5,208.28,193.04,U1,EPAD,GND,,,"/>
<approved hash="104,4,99.06,106.68,U3,AVDD,+3V3,,,"/>
<approved hash="104,4,99.06,104.14,U3,AVDD,+3V3,,,"/>
<approved hash="104,4,127,60.96,U3,DVDD,+3V3,,,"/>
<approved hash="104,4,99.06,88.9,U3,VDD_USB,+3V3,,,"/>
<approved hash="104,4,307.34,199.39,IC1VBAT@1,VBAT,V_GSM,,,"/>
<approved hash="104,4,300.99,199.39,IC1VBAT@2,VBAT,V_GSM,,,"/>
<approved hash="104,4,294.64,199.39,IC1VBAT@3,VBAT,V_GSM,,,"/>
<approved hash="202,2,104.14,123.19,U$7,RI,,,,"/>
<approved hash="104,2,104.14,133.35,U$7,VIO,+3V3,,,"/>
<approved hash="104,2,104.14,135.89,U$7,VDD,+3V3,,,"/>
<approved hash="202,2,27.94,125.73,U$7,DCD,,,,"/>
<approved hash="202,2,27.94,130.81,U$7,DSR,,,,"/>
<approved hash="202,2,27.94,140.97,U$7,CTS,,,,"/>
<approved hash="104,1,44.45,193.04,U5,AGND,GND,,,"/>
<approved hash="104,1,44.45,195.58,U5,AVCC,+3V3,,,"/>
<approved hash="104,1,44.45,172.72,U5,GND1,GND,,,"/>
<approved hash="104,1,44.45,170.18,U5,GND2,GND,,,"/>
<approved hash="104,1,44.45,167.64,U5,GND3,GND,,,"/>
<approved hash="104,1,44.45,185.42,U5,VCC,+3V3,,,"/>
<approved hash="104,1,44.45,182.88,U5,VCC1,+3V3,,,"/>
<approved hash="104,1,44.45,180.34,U5,VCC2,+3V3,,,"/>
<approved hash="104,1,44.45,177.8,U5,VCC3,+3V3,,,"/>
<approved hash="202,1,44.45,203.2,U5,XTAL1,,,,"/>
<approved hash="113,5,45.9147,127.677,I2C_JP,,,,,"/>
<approved hash="113,4,138.235,28.5327,BLE_USB,,,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
