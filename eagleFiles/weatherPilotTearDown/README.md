weatherPilot tearDown
=====================

- DHT22 provision using berg pin footprint.
- Digital temperature Sensor TMP75 I2C interface,
- Humidity SHT21
- WiFi module RAK410
- Battery management IC - BQ24210
- Power source are :
  - DC power Jack.
  - Battery.
  - Solar Panel if any.
- USB to UART Bridge - FTDI chip.


Pin occupy
===========
- DHt22 : 3 Pins
- TMP75 takes I2C- one Alert Output to one GPIO.
- SHT21/25 goes one I2C lines with different address bits.
-WiFi Rak 410/ RAK413 : On UART + 6 gpios


###Total gpio count
- 3 + 2(i2c) + 1(alert) + 2(rak uart) + 2(usb2uart) + 6 + 1(led)= 17 pins.

MCU Select
===========
- atmega1284
