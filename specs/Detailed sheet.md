Detailed sheet
==============
---------------

###Sensor
---------------
####Temperature sensor options
- TMP75C (NEW) from TI
    - Desired voltage to work on: 1.8v.
    - Interface: SMBus and two-wire interface(I2C.).
    - Alarm features, present, not required in this application.
    - ADC resolution - 12 bit, resolution of 0.0625 C.
    - Package : SOIC(8).
    - Not available in digi key.
####Humidity sensor options
- Silicon labs SI7021 humidity and temperature sensor.
    - Digikey part number: 336-2528-1-ND
    - Manufacturer part number : SI7013-A10-GM1R, silicon Labs
    - Sensor type: Humidity and Temperature.
    - Supply: 1.9v to3.6v, 150uA active current. 60nA standby current.
    - Ordering part number: Si7021-A10-GM1R, digital temperature/ humidity sensor with protective cover.

---------------
####Air quality sensors options
- left blank for now. 

---------------

####Micro controller unit options
- atmega2560 - ordering part number ATmega2560V-8AU 8 MHz, 1.8v - 5.5v package 100A


####GSM options
- Sim900 with UART interface.

####Bluetooth options
- HC-05.supply 1.8v-3.6v, 50mA, UART interface. 2.0v Bluetooth
- Bluegiga BLE112 3.3v. 30 mA, UART interface. 4.0v bluetooth

####Wifi options
- RAK410 with UART interface: operating voltage 3.3v with five power modes.

####Solar panel option
- 6v 200mA or more Power: 3W or more. but not less than 6 V.

####Solar charge controller for single cell
- BQ24210DQCT from TI, 296-28738-1-ND, VBUS 18v, IBUS 0.8 A recommended. used in auxiliary solar chargers. Upto 800mA charge current woth 10% charge currect accuray. Status indication, charging / power Present. This is of linear charging type charger.

####Booster for 5V.
- TPS61028DRCR from TI, 296-18078-2-ND, input voltage range 0.9v to 6.5v, adjustable output upto 5.5v

--------------

####USB to Serial UART Converter
- CP2104, 336-2008-5-ND, manf part number for ordering CP2104-F03-GM, from Silicon labs. USB bus powered 4.0v to 5.25v. Package case is 24WFQFN Exposed Pad.

--------------
####On board 
- MCU.
- Sensors.
- Solar charge controller unit.
- Li ion battery.
- USB port.
- DC jack.
- Wifi module.
- sim900.
- BLE.

--------------
###Power lines on board
- 1.8v.
- 3.3v.
- 3.7v.
- 5.0v.
- USB power supply.
- 6V from solar panel.
- 9v to 18v DC jack input.
- GSM SIM will be connected directly to li ion battery, of 3.7v as source voltage to GSM SIM.
